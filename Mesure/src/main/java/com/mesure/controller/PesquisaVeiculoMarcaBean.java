package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.VeiculoMarca;
import com.mesure.model.VeiculoModelo;
import com.mesure.repository.VeiculoMarcaRepository;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaVeiculoMarcaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	VeiculoMarcaRepository veiculoMarcaRepository;
	
	private List<VeiculoMarca> marcas;
	private List<VeiculoModelo> modelos;
	
	private VeiculoMarca marcaSelecionada;
	
	public void excluir() {
		veiculoMarcaRepository.remover(marcaSelecionada);
		marcas.remove(marcaSelecionada);
		
		FacesUtil.addInfoMessage("Marca '" + marcaSelecionada.getDescricao() + "' excluída com sucesso.");
	}
	
	public void pesquisarMarcas() {
		marcas = veiculoMarcaRepository.marcas();
	}
	
	public void pesquisarModelos() {
		modelos = veiculoMarcaRepository.modelos();
	}	
		
	public List<VeiculoMarca> getMarcas() {
		return marcas;
	}

	public List<VeiculoModelo> getModelos() {
		return modelos;
	}

	public VeiculoMarca getMarcaSelecionada() {
		return marcaSelecionada;
	}

	public void setMarcaSelecionada(VeiculoMarca marcaSelecionada) {
		this.marcaSelecionada = marcaSelecionada;
	}
	
}

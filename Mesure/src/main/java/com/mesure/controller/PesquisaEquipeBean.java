package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Colaborador;
import com.mesure.model.Equipe;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.EquipeRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.service.EquipeService;
import com.mesure.service.NegocioException;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaEquipeBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	EquipeService equipeService;
	
	@Inject
	EquipeRepository equipeRepository;
	
	@Inject
	ColaboradorRepository colaboradorRepository;
	
	private List<Equipe> equipes;
	private Equipe equipeDuplicar;
	
	private List<Colaborador> colaboradores;
		
	public void pesquisarEquipes() {
		equipes = equipeRepository.equipes();
		equipeDuplicar = new Equipe();
		
		// Carrega todos os colaboradores cadastrados e ativos
		ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
		colaboradorFilter.setSomenteAtivos(ESimNao.SIM);		
		colaboradores = colaboradorRepository.colaboradores(colaboradorFilter);
	}
	
	public void validarSelecionados() {
		if (equipes == null || equipes.size() == 0) {
			throw new NegocioException("Nenhum registro para processar");
		}			
		
		boolean selecionado = false;
		
		for (Equipe e : equipes) {
			if (e.isFlag()) {
				selecionado = true;
				break;
			}
		}
		
		if (!selecionado) {
			throw new NegocioException("Nenhuma equipe selecionada.");
		}
		
		equipeDuplicar = new Equipe();
	}
	
	public void duplicarEquipes() {
		for (Equipe e : equipes) {
			if (e.isFlag()) {
				Equipe novaEquipe = new Equipe();
				novaEquipe.setDescricao(equipeDuplicar.getDescricao());
				novaEquipe.setColaborador(equipeDuplicar.getColaborador());
				novaEquipe.setColaboradores(equipeService.colaboradoresDaEquipe(e, ESimNao.NAO));				
				equipeService.salvar(novaEquipe);
			}
		}
		FacesUtil.addInfoMessageGrowl("Registro(s) duplicado(s) com sucesso.");
	}
	
	public void excluirEquipes() {
		for (Equipe e : equipes) {
			if (e.isFlag()) {
				equipeRepository.remover(e);
			}
		}
		FacesUtil.addInfoMessageGrowl("Registro(s) excluído(s) com sucesso.");
	}
	
	public List<Equipe> getEquipes() {
		return equipes;
	}	
	
	public Equipe getEquipeDuplicar() {
		return equipeDuplicar;
	}

	public void setEquipeDuplicar(Equipe equipeDuplicar) {
		this.equipeDuplicar = equipeDuplicar;
	}

	public List<Colaborador> getColaboradores() {
		return colaboradores;
	}
	
}

package com.mesure.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.ECategoriaVeiculo;
import com.mesure.model.VeiculoMarca;
import com.mesure.model.VeiculoModelo;
import com.mesure.service.VeiculoMarcaService;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroVeiculoMarcaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private VeiculoMarcaService veiculoMarcaService;

	private VeiculoMarca marca;
	private VeiculoModelo modelo;

	public CadastroVeiculoMarcaBean() {
		limpar();
	}

	public void inicializar() {
		if (this.marca == null) {
			limpar();
		}
	}

	private void limpar() {
		this.marca = new VeiculoMarca();
		limparModelo();
	}

	public void limparModelo() {
		this.modelo = new VeiculoModelo();
		inicializarModeloCategoria();
	}
	
	public void inicializarModeloCategoria() {
		modelo.setCategoria(ECategoriaVeiculo.CARRO);
	}	

	public void salvar() {
		marca = veiculoMarcaService.salvar(marca);
		limpar();

		FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
	}

	public void incluirModelo() {
		if (!marca.getModelos().contains(modelo)) {
			modelo.setMarca(marca);
			marca.getModelos().add(modelo);
			limparModelo();
		}
	}

	public void excluirModelo() {
		if (modelo != null) {
			// TODO - testar quando o modelo estiver sendo referenciado
			marca.getModelos().remove(modelo);
		}
	}

	// Para carregar as opções do Enum no formulário
	public ECategoriaVeiculo[] getCategorias() {
		return ECategoriaVeiculo.values();
	}
	
	public VeiculoMarca getMarca() {
		return marca;
	}

	public void setMarca(VeiculoMarca marca) {
		this.marca = marca;
	}

	public VeiculoModelo getModelo() {
		return modelo;
	}

	public void setModelo(VeiculoModelo modelo) {
		this.modelo = modelo;
	}
	
}

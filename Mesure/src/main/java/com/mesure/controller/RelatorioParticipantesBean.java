package com.mesure.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import com.mesure.enumerator.EAnaliticoSintetico;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ETipoColaborador;
import com.mesure.enumerator.ETipoRelatorioColaborador;
import com.mesure.model.Colaborador;
import com.mesure.model.Filial;
import com.mesure.repository.ColaboradorPontoRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.EmpresaRepository;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.ColaboradorPontoService;
import com.mesure.service.EquipeService;
import com.mesure.util.jsf.FacesUtil;
import com.mesure.util.report.ExecutorRelatorio;

@Named
@ViewScoped
public class RelatorioParticipantesBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	ColaboradorRepository colaboradorRepository;
	
	@Inject
	ColaboradorPontoRepository colaboradorPontoRepository;
	
	@Inject
	ColaboradorPontoService colaboradorPontoService; 
	
	@Inject
	Seguranca seguranca;	
	
	@Inject
	@UsuarioLogado
	private UsuarioSistema usuarioLogado;
	
	@Inject
	EmpresaRepository empresaRepository;
	
	@Inject
	EquipeService equipeService;
	
	@Inject
	FacesContext facesContext;
	
	@Inject
	HttpServletResponse respose;
	
	@Inject
	EntityManager manager;
	
	private List<Colaborador> colaboradores;
	private List<Filial> filiais;
	
	private Colaborador filtroColaborador;
	private ETipoColaborador filtroTipo;
	private Filial filtroFilial;
	private Date filtroCompetencia;
	private Date filtroDataInicial;
	private Date filtroDataFinal;
	private EAnaliticoSintetico tipoRelatorio;
	private ETipoRelatorioColaborador tipoRelatorioColab;

	public void imprimirControlePonto() {		
		if (tipoRelatorioColab != null) {
			if (tipoRelatorioColab.equals(ETipoRelatorioColaborador.PONTO)) {
				imprimirPonto();
			} else {
				imprimirPontoProjeto();
			}
		}
	}
	
	public void imprimirPonto() {
		Map<String, Object> parametros = new HashMap<>();	
		
		if (this.filtroFilial != null && this.filtroFilial.getId() != null) {
			parametros.put("filial", this.filtroFilial.getId());	
		}			
		
		if (this.filtroColaborador != null && this.filtroColaborador.getId() != null) {
			parametros.put("colaborador", this.filtroColaborador.getId());	
		}
		
		parametros.put("data_inicio", this.filtroDataInicial);
		parametros.put("data_fim", this.filtroDataFinal);

		ExecutorRelatorio executor = new ExecutorRelatorio("/relatorios/participantes/participantes001.jasper",
				this.respose, parametros, "Controle de Ponto.pdf");
		
		Session session = manager.unwrap(Session.class);
		session.doWork(executor);
		
		if (executor.isRelatorioGerado()) {
			facesContext.responseComplete();
		} else {
			FacesUtil.addErrorMessage("A execução do relatório não retornou dados.");
		}		
	}
	
	public void imprimirPontoProjeto() {
		Map<String, Object> parametros = new HashMap<>();
		
		if (this.filtroFilial != null && this.filtroFilial.getId() != null) {
			parametros.put("filial", this.filtroFilial.getId());	
		}			
		
		if (this.filtroColaborador != null && this.filtroColaborador.getId() != null) {
			parametros.put("colaborador", this.filtroColaborador.getId());	
		}
		
		parametros.put("data_inicio", this.filtroDataInicial);
		parametros.put("data_fim", this.filtroDataFinal);	
		parametros.put("local_subreport", "/relatorios/colaboradores/colaboradores002_sub.jasper");

		ExecutorRelatorio executor = new ExecutorRelatorio("/relatorios/participantes/colaboradores002.jasper",
				this.respose, parametros, "Colaborador x Projeto.pdf");
		
		Session session = manager.unwrap(Session.class);
		session.doWork(executor);
		
		if (executor.isRelatorioGerado()) {
			facesContext.responseComplete();
		} else {
			FacesUtil.addErrorMessage("A execução do relatório não retornou dados.");
		}		
	}	
	
	public void imprimirSaidaVeiculos() {		
		Map<String, Object> parametros = new HashMap<>();
		
		if (this.filtroTipo != null && this.filtroTipo.toString() != null) {
			parametros.put("tipo", filtroTipo.toString());		
		}				
		
		if (this.filtroColaborador != null && this.filtroColaborador.getId() != null) {
			parametros.put("colaborador", this.filtroColaborador.getId());	
		}
			
		parametros.put("data_inicio", this.filtroDataInicial);
		parametros.put("data_fim", this.filtroDataFinal);

		ExecutorRelatorio executor = new ExecutorRelatorio("/relatorios/veiculos/veiculos001.jasper",
				this.respose, parametros, "Saída de Veículos.pdf");
		
		Session session = manager.unwrap(Session.class);
		session.doWork(executor);
		
		if (executor.isRelatorioGerado()) {
			facesContext.responseComplete();
		} else {
			FacesUtil.addErrorMessage("A execução do relatório não retornou dados.");
		}
	}	
	
	public void imprimirApontamentoHoras() {
		if (tipoRelatorio != null) {
			if (tipoRelatorio.equals(EAnaliticoSintetico.ANALITICO)) {
				imprimirApontamentoHorasAnalitico();
			} else {
				imprimirApontamentoHorasSintetico();
			}
		}
	}
	
	public void imprimirApontamentoHorasSintetico() {		
		Map<String, Object> parametros = new HashMap<>();
		
		carregarPeriodoFiltro();
		
		if (this.filtroFilial != null && this.filtroFilial.getId() != null) {
			parametros.put("filial", this.filtroFilial.getId());	
		}			
		
		if (this.filtroColaborador != null && this.filtroColaborador.getId() != null) {
			parametros.put("colaborador", this.filtroColaborador.getId());	
		}		

		parametros.put("data_inicio", this.filtroDataInicial);
		parametros.put("data_fim", this.filtroDataFinal);

		ExecutorRelatorio executor = new ExecutorRelatorio("/relatorios/participantes/participantes002.jasper",
				this.respose, parametros, "Apontamento de Horas Sintetico.pdf");
		
		Session session = manager.unwrap(Session.class);
		session.doWork(executor);
		
		if (executor.isRelatorioGerado()) {
			facesContext.responseComplete();
		} else {
			FacesUtil.addErrorMessage("A execução do relatório não retornou dados.");
		}
	}	
	
	public void imprimirApontamentoHorasAnalitico() {		
		Map<String, Object> parametros = new HashMap<>();
		
		carregarPeriodoFiltro();
		
		if (this.filtroFilial != null && this.filtroFilial.getId() != null) {
			parametros.put("filial", this.filtroFilial.getId());	
		}		

		if (this.filtroColaborador != null && this.filtroColaborador.getId() != null) {
			parametros.put("colaborador", this.filtroColaborador.getId());	
		}
		
		parametros.put("data_inicio", this.filtroDataInicial);
		parametros.put("data_fim", this.filtroDataFinal);

		ExecutorRelatorio executor = new ExecutorRelatorio("/relatorios/participantes/participantes003.jasper",
				this.respose, parametros, "Apontamento de Horas Analitico.pdf");
		
		Session session = manager.unwrap(Session.class);
		session.doWork(executor);
		
		if (executor.isRelatorioGerado()) {
			facesContext.responseComplete();
		} else {
			FacesUtil.addErrorMessage("A execução do relatório não retornou dados.");
		}
	}		
	
	public void inicializar() {
		filtroColaborador = usuarioLogado.getUsuario().getColaborador();
		filtroCompetencia = new Date();
		
		// Carrega os registros para o combo de filiais
		filiais = empresaRepository.filiais();	
		
		// Carrega os colaboradores no qual o usuário é responsável além do próprio colaborador ativo
		colaboradores = equipeService.colaboradoresDoResponsavel(usuarioLogado.getUsuario().getColaborador(), ESimNao.SIM);
	}	

	public void onChangeFiltro() {
	
	}	
	
	public void carregarPeriodoFiltro() {
		Calendar c = new GregorianCalendar();		
		c.setTime(filtroCompetencia);
		
		// Primeiro dia do mês
		c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMinimum(Calendar.DAY_OF_MONTH));
		setFiltroDataInicial(c.getTime());
		
		// Último dia do mês		
		c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMaximum(Calendar.DAY_OF_MONTH));
		setFiltroDataFinal(c.getTime());
	}		
	
	public boolean isTipoAnalitico() {
		boolean retorno = false;
		
		if (tipoRelatorio != null) {
			if (tipoRelatorio.equals(EAnaliticoSintetico.ANALITICO)) {			
				
				// Carrega os colaboradores no qual o usuário é responsável além do próprio colaborador ativo
				colaboradores = equipeService.colaboradoresDoResponsavel(usuarioLogado.getUsuario().getColaborador(), ESimNao.SIM);
				
				retorno = true;
			} else {
				colaboradores = new ArrayList<>();
				retorno = false;
			}
		} else {
			colaboradores = new ArrayList<>();
			retorno = false;
		}
		
		return retorno;
	}	
	
	// Para carregar as opções do Enum no formulário
	public ETipoColaborador [] getTipos() {
		return ETipoColaborador.values();
	}		
	
	public EAnaliticoSintetico[] getAnaliticoSintetico() {
		return EAnaliticoSintetico.values();
	}
	
	public ETipoRelatorioColaborador[] getTipoRelatorioColaborador() {
		return ETipoRelatorioColaborador.values();
	}	
	
	public Colaborador getFiltroColaborador() {
		return filtroColaborador;
	}

	public void setFiltroColaborador(Colaborador filtroColaborador) {
		this.filtroColaborador = filtroColaborador;
	}

	public Date getFiltroCompetencia() {
		return filtroCompetencia;
	}

	public void setFiltroCompetencia(Date filtroCompetencia) {
		this.filtroCompetencia = filtroCompetencia;
	}

	public Date getFiltroDataInicial() {
		return filtroDataInicial;
	}

	public void setFiltroDataInicial(Date filtroDataInicial) {
		this.filtroDataInicial = filtroDataInicial;
	}

	public Date getFiltroDataFinal() {
		return filtroDataFinal;
	}

	public void setFiltroDataFinal(Date filtroDataFinal) {
		this.filtroDataFinal = filtroDataFinal;
	}
	
	public List<Colaborador> getColaboradores() {
		return colaboradores;
	}

	public EAnaliticoSintetico getTipoRelatorio() {
		return tipoRelatorio;
	}

	public void setTipoRelatorio(EAnaliticoSintetico tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}

	public ETipoColaborador getFiltroTipo() {
		return filtroTipo;
	}

	public void setFiltroTipo(ETipoColaborador filtroTipo) {
		this.filtroTipo = filtroTipo;
	}
	
	public Filial getFiltroFilial() {
		return filtroFilial;
	}

	public void setFiltroFilial(Filial filtroFilial) {
		this.filtroFilial = filtroFilial;
	}

	public List<Filial> getFiliais() {
		return filiais;
	}

	public void setFiliais(List<Filial> filiais) {
		this.filiais = filiais;
	}

	public ETipoRelatorioColaborador getTipoRelatorioColab() {
		return tipoRelatorioColab;
	}

	public void setTipoRelatorioColab(ETipoRelatorioColaborador tipoRelatorioColab) {
		this.tipoRelatorioColab = tipoRelatorioColab;
	}	
	
}

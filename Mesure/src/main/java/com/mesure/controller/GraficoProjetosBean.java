package com.mesure.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;

import com.mesure.model.Projeto;
import com.mesure.repository.ProjetoRepository;
import com.mesure.service.NegocioException;
import com.mesure.util.Util;

@Named
@ViewScoped
public class GraficoProjetosBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	ProjetoRepository projetoRepository;
	
    private LineChartModel lineModelProjetos;
    private Date dataInicio;
    private Date dataTermino;
    private boolean carregar;
     
    @PostConstruct
    public void init() throws ParseException {
    	sugerirPeriodoConsulta();
    	ajustarPeriodoConsulta();
    	setCarregar(false);    	
    	createChartProjetosIniciados();
    }
    
    public void consultar() throws ParseException {
    	consistirFiltro();
    	ajustarPeriodoConsulta();
    	setCarregar(true);
    	createChartProjetosIniciados();
    }
    
    public void createChartProjetosIniciados() {
        lineModelProjetos = initLineModelProjetos();
        lineModelProjetos.setTitle("Projetos iniciados x finalizados");
        lineModelProjetos.setLegendPosition("e");
        lineModelProjetos.setShowPointLabels(true);
        lineModelProjetos.getAxes().put(AxisType.X, new CategoryAxis("Mês"));
        lineModelProjetos.getAxis(AxisType.X).setTickAngle(-50);
        lineModelProjetos.setAnimate(true);
        Axis yAxis = lineModelProjetos.getAxis(AxisType.Y);
        yAxis.setLabel("Quantidade projetos");
        yAxis.setMin(0);
    }
    
    private LineChartModel initLineModelProjetos() {
    	LineChartModel model = new LineChartModel();
    	
        ChartSeries iniciados = new ChartSeries();
        iniciados.setLabel("Iniciados");

        ChartSeries finalizados = new ChartSeries();
        finalizados.setLabel("Finalizados");

    	Map<String, Integer> mapa = new HashMap<>();
    	List<Projeto> projetos = new ArrayList<>();
    	Calendar dataRef = new GregorianCalendar();
    	
    	int qtde = 0;
    	int qtdeMeses = getMesesConsulta(); 
    	String dataStr = "";
    	
    	mapa.clear();
    	
    	if (isCarregar()) {
    		// Projetos iniciados nos últimos x meses
    		projetos = projetoRepository.projetosIniciadosPorPeriodo(getDataInicio(), getDataTermino());
    	
        	for (Projeto p : projetos) {
        		dataStr = Util.formatarData(p.getDataInicio(), "MM/yyyy");
        		qtde = 1;
        		if (mapa.containsKey(dataStr)) {
        			qtde = mapa.get(dataStr).intValue();
        			qtde++;
        		}
        		mapa.put(dataStr, qtde);
        	}    		
    	}
    	
        // Carrega no gráfico os projetos iniciados
    	dataRef.setTime(getDataInicio());
    	
    	for (int i = 1; i <= qtdeMeses; i++) {
    		dataStr = Util.formatarData(dataRef.getTime(), "MM/yyyy");
    		qtde = 0;
    		if (mapa.containsKey(dataStr)) {
    			qtde = mapa.get(dataStr).intValue();
    		}    		
    		iniciados.set(dataStr, qtde);
    		dataRef.add(Calendar.MONTH, 1);
    	}
    	
    	mapa.clear();
    	
    	if (isCarregar()) {
        	// Projetos finalizados nos últimos x meses
        	projetos = projetoRepository.projetosFinalizadosPorPeriodo(getDataInicio(), getDataTermino());
        	
        	for (Projeto p : projetos) {
        		dataStr = Util.formatarData(p.getDataEncerramento(), "MM/yyyy");
        		qtde = 1;
        		if (mapa.containsKey(dataStr)) {
        			qtde = mapa.get(dataStr).intValue();
        			qtde++;
        		}
        		mapa.put(dataStr, qtde);
        	}    		
    	}
    	
        // Carrega no gráfico os projetos finalizados
    	dataRef.setTime(getDataInicio());
    	
    	for (int i = 1; i <= qtdeMeses; i++) {
    		dataStr = Util.formatarData(dataRef.getTime(), "MM/yyyy");
    		qtde = 0;
    		if (mapa.containsKey(dataStr)) {
    			qtde = mapa.get(dataStr).intValue();
    		}    		
    		finalizados.set(dataStr, qtde);
    		dataRef.add(Calendar.MONTH, 1);
    	}
        
        model.addSeries(iniciados);
        model.addSeries(finalizados);
    	
        return model;
    }
    
    private void consistirFiltro() {
    	if (getDataInicio() == null) {
    		throw new NegocioException("Data inicial deve ser informada");
    	}
    	
    	if (getDataTermino() == null) {
    		throw new NegocioException("Data final deve ser informada");
    	}
    	
    	if (getDataInicio().getTime() > getDataTermino().getTime()) {
    		throw new NegocioException("Data final deve ser maior ou igual que a data inicial");
    	}
    	
    	if (getMesesConsulta() > 24) {
    		throw new NegocioException("O período máximo da consulta é de 24 meses");
    	}
    }
    
    private void sugerirPeriodoConsulta() {
    	// Sugere o período da consulta como sendo os últimos 12 meses    	
    	Calendar c = new GregorianCalendar();
    	c.setTime(new Date());
    	
    	// Data de início dos dados
    	c.add(Calendar.MONTH, -12);    	
    	setDataInicio(c.getTime());
    	
    	// Data de término dos dados
    	c.add(Calendar.MONTH, 11);    	
    	setDataTermino(c.getTime());    	
    }
    
    private void ajustarPeriodoConsulta() {
    	Calendar c = new GregorianCalendar();
    	
    	// Data de início dos dados
    	c.setTime(getDataInicio());    	    	
    	c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMinimum(Calendar.DAY_OF_MONTH));
    	setDataInicio(c.getTime());
    	
    	// Data de término dos dados
    	c.setTime(getDataTermino());    	    	
    	c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMaximum(Calendar.DAY_OF_MONTH));
    	setDataTermino(c.getTime());
    }
    
    private int getMesesConsulta() {
    	return Util.DiferencaEmMeses(getDataInicio(), getDataTermino()) + 1;
    }
    
	public LineChartModel getLineModelProjetos() {
		return lineModelProjetos;
	}
	
	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}

	public boolean isCarregar() {
		return carregar;
	}

	public void setCarregar(boolean carregar) {
		this.carregar = carregar;
	}

}

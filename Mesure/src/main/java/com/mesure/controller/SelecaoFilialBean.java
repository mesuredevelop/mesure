package com.mesure.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.FacesException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.Colaborador;
import com.mesure.model.Filial;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.EmpresaRepository;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.ColaboradorService;
import com.mesure.service.NegocioException;

@Named
@ViewScoped
public class SelecaoFilialBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EmpresaRepository empresaRepository;
	
	@Inject
	private ColaboradorRepository colaboradorRepository;
	
	@Inject
	private ColaboradorService colaboradorService;
	
	@Inject
	@UsuarioLogado
	private UsuarioSistema usuarioLogado;
	
	private List<Filial> filiais = new ArrayList<>();
	
	private Filial filialSelecionada;
	
	public void carregarFiliais() {
		filiais = empresaRepository.filiais();
	}
	
	public String setarFilialAtiva() {
		String retorno = "";
		
		if (filialSelecionada == null) {
			throw new NegocioException("A filial não foi selecionada");
		}
		
		Colaborador colaborador = colaboradorRepository.porId(usuarioLogado.getColaborador().getId());

		if (colaborador == null) {
			throw new NegocioException("Erro ao buscar informações do colaborador");
		}
		
		// Seta a filial selecionada para o colaborador e atualiza na base
		colaborador.setFilial(filialSelecionada);
		colaboradorService.salvar(colaborador);
		
		// Atualiza o objeto que armazena o usuário logado
		usuarioLogado.atualizarColaborador(colaborador);

		// Redireciona para a página inicial
		redirect("/Home.xhtml");
		
		return retorno;
	}
	
	public void redirect(String page) {
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			String contextPath = externalContext.getRequestContextPath();
			externalContext.redirect(contextPath + page);
			facesContext.responseComplete();
		} catch (IOException e) {
			throw new FacesException(e);
		}
	}
	
	public String getDescricaoFilialAtiva() {
		if (usuarioLogado.getColaborador() != null) {
			return usuarioLogado.getColaborador().getFilial().getNome();	
		}		
		return "";
	}

	public List<Filial> getFiliais() {
		return filiais;
	}

	public Filial getFilialSelecionada() {
		return filialSelecionada;
	}

	public void setFilialSelecionada(Filial filialSelecionada) {
		this.filialSelecionada = filialSelecionada;
	}
	
	
}

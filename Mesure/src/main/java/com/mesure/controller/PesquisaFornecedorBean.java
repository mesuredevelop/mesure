package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.Fornecedor;
import com.mesure.repository.FornecedorRepository;
import com.mesure.repository.filter.FornecedorFilter;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaFornecedorBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	FornecedorRepository fornecedorRepository;
	
	private FornecedorFilter filtro;
	private List<Fornecedor> fornecedoresFiltrados;
	
	private Fornecedor fornecedorSelecionado;
	
	public PesquisaFornecedorBean() {
		filtro = new FornecedorFilter();
	}	
	
	public void excluir() {
		fornecedorRepository.remover(fornecedorSelecionado);
		fornecedoresFiltrados.remove(fornecedorSelecionado);
		
		FacesUtil.addInfoMessage("Fornecedor '" + fornecedorSelecionado.getNome() + "' excluído com sucesso.");
	}
	
	public void mensagem() {
		FacesUtil.addInfoMessage("Mensagem teste!!!");
	}
	
	public void pesquisar() {
		fornecedoresFiltrados = fornecedorRepository.filtrados(filtro);
	}
		
	public FornecedorFilter getFiltro() {
		return filtro;
	}

	public List<Fornecedor> getFornecedoresFiltrados() {
		return fornecedoresFiltrados;
	}

	public Fornecedor getFornecedorSelecionado() {
		return fornecedorSelecionado;		
	}

	public void setFornecedorSelecionado(Fornecedor fornecedorSelecionado) {
		this.fornecedorSelecionado = fornecedorSelecionado;
	}
	
}

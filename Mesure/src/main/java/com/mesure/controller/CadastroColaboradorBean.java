package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoColaborador;
import com.mesure.enumerator.ETipoAnotacaoColaborador;
import com.mesure.enumerator.ETipoColaborador;
import com.mesure.model.Colaborador;
import com.mesure.model.ColaboradorTreinamento;
import com.mesure.model.ColaboradorAdvertencia;
import com.mesure.model.ColaboradorAtestado;
import com.mesure.model.ColaboradorEquipamento;
import com.mesure.model.ColaboradorEstabilidade;
import com.mesure.model.Empresa;
import com.mesure.model.EquipamentoProtecao;
import com.mesure.model.Filial;
import com.mesure.model.Treinamento;
import com.mesure.model.Usuario;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.EmpresaRepository;
import com.mesure.repository.EquipamentoProtecaoRepository;
import com.mesure.repository.TreinamentoRepository;
import com.mesure.repository.UsuarioRepository;
import com.mesure.repository.filter.EmpresaFilter;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.ColaboradorService;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroColaboradorBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ColaboradorRepository colaboradorRepository;
	
	@Inject
	private ColaboradorService colaboradorService;
	
	@Inject
	private UsuarioRepository usuarioRepository;
	
	@Inject
	EmpresaRepository empresaRepository;
	
	@Inject
	TreinamentoRepository treinamentoRepository;
	
	@Inject
	EquipamentoProtecaoRepository equipamentoProtecaoRepository;
	
	@Inject
	@UsuarioLogado
	private UsuarioSistema usuarioLogado;	
	
	private Colaborador colaborador;
	private ColaboradorTreinamento treinamento;
	private ColaboradorAdvertencia advertencia;
	private ColaboradorAtestado atestado;
	private ColaboradorEquipamento equipamento;
	private ColaboradorEstabilidade estabilidade;
	
	private Empresa empresa;
	
	private List<Empresa> empresas;
	private List<Filial> filiais;	
	
	private List<Usuario> usuarios;
	private List<Treinamento> treinamentos;
	private List<EquipamentoProtecao> equipamentos;
		
    private List<String> cargosAutoComplete;

	public CadastroColaboradorBean() {		
		limpar();
	}
	
	public void inicializar() {
		if (this.colaborador == null) {
			limpar();
		}
		
		inicializarSituacaoColaborador();
		
		// Carrega todos os usuários ativos cadastrados
		usuarios = usuarioRepository.ativos();
		
		// carrega todas as empresas cadastradas
		empresas = empresaRepository.empresas(new EmpresaFilter());
		
		// Carrega todos os treinamentos/qualificações cadastrados
		treinamentos = treinamentoRepository.treinamentos();
		
		// Carrega todos os equipamentos de proteção individual (EPI) cadastrados
		equipamentos = equipamentoProtecaoRepository.equipamentos();
		
		// Carrega os cargos para autocomplete
		carregarCargos();
		
		// Se é um novo colaborador, ou seja, modo de inclusão
		if (colaborador != null && !colaborador.isEditando())  {
			empresa = usuarioLogado.getFilialAtiva().getEmpresa();
			colaborador.setFilial(usuarioLogado.getFilialAtiva());
		}
		
		// Carrega as filiais da empresa para apresentar no formulário
		if (empresa != null) {
			carregarFiliais();
		}	
	}
	
	public void inicializarSituacaoColaborador() {
		if (colaborador != null)  {
			if (!colaborador.isEditando()) {
				colaborador.setSituacao(ESituacaoColaborador.TRABALHANDO);
			}
		}		
	}
	
    public void carregarCargos() {
    	cargosAutoComplete = colaboradorRepository.cargosAutoComplete();
    }
    
	public void carregarFiliais() {
		filiais = null;
		
		if (empresa != null) {
			filiais = empresaRepository.filialDe(empresa);
		}		
	}    
	
	private void limpar() {
		this.colaborador = new Colaborador();
		limparTreinamento();
		limparAdvertencia();
		limparAtestado();
		limparEquipamento();
		limparEstabilidade();
		
		empresa = null;
		filiais = null;		
	}
	
	public void limparTreinamento() {
		this.treinamento = new ColaboradorTreinamento();
	}
	
	public void limparAdvertencia() {
		this.advertencia = new ColaboradorAdvertencia();
	}
	
	public void limparAtestado() {
		this.atestado = new ColaboradorAtestado();
	}
	
	public void limparEquipamento() {
		this.equipamento = new ColaboradorEquipamento();
	}
	
	public void limparEstabilidade() {
		this.estabilidade = new ColaboradorEstabilidade();
	}	
	
	public void salvar() {		
		tratarDataDesligamento();
		
		colaborador = colaboradorService.salvar(colaborador);
		
		// Se alterou o cadastro do colaborador ativo, atualiza a informação no 
		// objeto que armazena o usuário logado por conta da filial ativa
		if (this.colaborador.equals(usuarioLogado.getUsuario().getColaborador())) {
			usuarioLogado.atualizarColaborador(this.colaborador);	
		}		
		
		limpar();
		inicializarSituacaoColaborador();

		FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
	}
	
	public void incluirTreinamento() {
		if (!colaborador.getTreinamentos().contains(treinamento)) {
			treinamento.setColaborador(colaborador);
			colaborador.getTreinamentos().add(treinamento);
		}
		limparTreinamento();
	}	
		
	public void excluirTreinamento() {
		if (treinamento != null) {
			colaborador.getTreinamentos().remove(treinamento);	
		}		
	}	
	
	public void incluirAdvertencia() {
		if (!colaborador.getAdvertencias().contains(advertencia)) {
			advertencia.setColaborador(colaborador);
			colaborador.getAdvertencias().add(advertencia);
		}
		limparAdvertencia();
	}	
		
	public void excluirAdvertencia() {
		if (advertencia != null) {
			colaborador.getAdvertencias().remove(advertencia);	
		}		
	}
	
	public void incluirAtestado() {
		if (!colaborador.getAtestados().contains(atestado)) {
			atestado.setColaborador(colaborador);
			colaborador.getAtestados().add(atestado);
		}
		limparAtestado();
	}	
		
	public void excluirAtestado() {
		if (atestado != null) {
			colaborador.getAtestados().remove(atestado);	
		}		
	}
	
	public void incluirEquipamento() {
		if (!colaborador.getEquipamentos().contains(equipamento)) {
			equipamento.setColaborador(colaborador);
			colaborador.getEquipamentos().add(equipamento);
		}
		limparEquipamento();
	}	
		
	public void excluirEquipamento() {
		if (equipamento != null) {
			colaborador.getEquipamentos().remove(equipamento);	
		}		
	}
	
	public void incluirEstabilidade() {
		if (!colaborador.getEstabilidades().contains(estabilidade)) {
			estabilidade.setColaborador(colaborador);
			colaborador.getEstabilidades().add(estabilidade);
		}
		limparEstabilidade();
	}	
		
	public void excluirEstabilidade() {
		if (estabilidade != null) {
			colaborador.getEstabilidades().remove(estabilidade);	
		}		
	}
	
	public void tratarDataDesligamento() {
		if (colaborador.getSituacao() != ESituacaoColaborador.DEMITIDO) {
			colaborador.setDataDesligamento(null);
		}
	}
	
	public void onChangeSituacao() {
		tratarDataDesligamento();
	}
	
	public void onChangeEmpresa() {
		carregarFiliais();
	}	
	
	// Para carregar as opções do Enum no formulário
	public ESituacaoColaborador [] getSituacoes() {
		return ESituacaoColaborador.values();
	}
	
	// Para carregar as opções do Enum no formulário
	public ETipoColaborador [] getTipos() {
		return ETipoColaborador.values();
	}
	
	// Para carregar as opções do Enum no formulário
	public ETipoAnotacaoColaborador [] getTiposAnotacao() {
		return ETipoAnotacaoColaborador.values();
	}	
	
	// Para carregar as opções do Enum no formulário
	public ESimNao[] getOpcoesSimNao() {
		return ESimNao.values();
	}	
	
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
		
		if (this.colaborador != null) {
			this.empresa = this.colaborador.getFilial().getEmpresa();
		}		
	}
	
	public ColaboradorTreinamento getTreinamento() {
		return treinamento;
	}

	public void setTreinamento(ColaboradorTreinamento treinamento) {
		this.treinamento = treinamento;
	}
	
	public ColaboradorAdvertencia getAdvertencia() {
		return advertencia;
	}

	public void setAdvertencia(ColaboradorAdvertencia advertencia) {
		this.advertencia = advertencia;
	}	

	public ColaboradorAtestado getAtestado() {
		return atestado;
	}

	public void setAtestado(ColaboradorAtestado atestado) {
		this.atestado = atestado;
	}
	
	public ColaboradorEquipamento getEquipamento() {
		return equipamento;
	}

	public void setEquipamento(ColaboradorEquipamento equipamento) {
		this.equipamento = equipamento;
	}
	
	public ColaboradorEstabilidade getEstabilidade() {
		return estabilidade;
	}

	public void setEstabilidade(ColaboradorEstabilidade estabilidade) {
		this.estabilidade = estabilidade;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public List<Filial> getFiliais() {
		return filiais;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	
	public List<Treinamento> getTreinamentos() {
		return treinamentos;
	}
	
	public List<EquipamentoProtecao> getEquipamentos() {
		return equipamentos;
	}

	public List<String> getCargosAutoComplete() {
		return cargosAutoComplete;
	}

}


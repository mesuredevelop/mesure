package com.mesure.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.mesure.enumerator.EAcaoLoteApuracaoHoras;
import com.mesure.enumerator.EStatusLoteApuracaoHoras;
import com.mesure.model.ApuracaoHorasLote;
import com.mesure.repository.ApuracaoHorasLoteRepository;
import com.mesure.service.ApuracaoHorasLoteService;
import com.mesure.service.NegocioException;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class DetalheLoteApuracaoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private HttpServletRequest request;
	
	@Inject
	ApuracaoHorasLoteService apuracaoHorasLoteService;
	
	@Inject
	ApuracaoHorasLoteRepository apuracaoHorasLoteRepository;
	
	private ApuracaoHorasLote lote;
	
	private boolean carregouRegistros;
	
	public void inicializar() {
		setCarregouRegistros(false);
		
		String param_lote = request.getParameter("lote");
		
		if (StringUtils.isBlank(param_lote)) {
			lote = null;
		} 
				
		if (lote == null) {
			throw new NegocioException("Não foi possível carregar as informações.");
		}
		
		if (lote.getStatus().equals(EStatusLoteApuracaoHoras.PENDENTE)) {
			throw new NegocioException("Este lote ainda não foi processado");
		}
		
		setCarregouRegistros(true);
	}
	
	public void fecharLote() throws ParseException {
		lote.setFlag(true);
		List<ApuracaoHorasLote> lotes = new ArrayList<>();		
		lotes.add(lote);
		apuracaoHorasLoteService.setLotes(lotes);
		apuracaoHorasLoteService.executarAcaoLote(EAcaoLoteApuracaoHoras.FECHAR);
		lote = apuracaoHorasLoteRepository.porId(lote.getId());

		FacesUtil.addInfoMessageGrowl("Operação realizada com sucesso.");		
	}
	
	public void liberarLote() throws ParseException {
		lote.setFlag(true);
		List<ApuracaoHorasLote> lotes = new ArrayList<>();		
		lotes.add(lote);
		apuracaoHorasLoteService.setLotes(lotes);
		apuracaoHorasLoteService.executarAcaoLote(EAcaoLoteApuracaoHoras.LIBERAR);
		lote = apuracaoHorasLoteRepository.porId(lote.getId());

		FacesUtil.addInfoMessageGrowl("Operação realizada com sucesso.");	
	}
	
	public boolean isPodeFecharLote() {
		return lote.getStatus().equals(EStatusLoteApuracaoHoras.PROCESSADO);
	}
	
	public boolean isPodeLiberarLote() {
		return lote.getStatus().equals(EStatusLoteApuracaoHoras.FECHADO);
	}

	public ApuracaoHorasLote getLote() {
		return lote;
	}

	public void setLote(ApuracaoHorasLote lote) {
		this.lote = lote;
	}

	public boolean isCarregouRegistros() {
		return carregouRegistros;
	}

	public void setCarregouRegistros(boolean carregouRegistros) {
		this.carregouRegistros = carregouRegistros;
	}
	
}

package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.Usuario;
import com.mesure.repository.UsuarioRepository;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaUsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	UsuarioRepository usuarioRepository;
	
	private List<Usuario> usuariosFiltrados;
	
	private Usuario usuarioSelecionado;
	
	public void excluir() {
		usuarioRepository.remover(usuarioSelecionado);
		usuariosFiltrados.remove(usuarioSelecionado);
		
		FacesUtil.addInfoMessage("Usuario '" + usuarioSelecionado.getNome() + "' excluído com sucesso.");
	}
	
	public void pesquisar() {
		usuariosFiltrados = usuarioRepository.filtrados();
	}
		
	public List<Usuario> getUsuariosFiltrados() {
		return usuariosFiltrados;
	}

	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;		
	}

	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}	

}


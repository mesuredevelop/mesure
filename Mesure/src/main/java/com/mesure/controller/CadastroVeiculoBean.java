package com.mesure.controller;

import com.mesure.enumerator.EGravidadeMulta;
import com.mesure.enumerator.EPeriodoDia;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoDetran;
import com.mesure.enumerator.ESituacaoManutencao;
import com.mesure.enumerator.ESituacaoMulta;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.enumerator.ETipoEndereco;
import com.mesure.model.Colaborador;
import com.mesure.model.EnderecoCliente;
import com.mesure.model.Fornecedor;
import com.mesure.model.Projeto;
import com.mesure.model.Veiculo;
import com.mesure.model.VeiculoManutencao;
import com.mesure.model.VeiculoMarca;
import com.mesure.model.VeiculoModelo;
import com.mesure.model.VeiculoMovimento;
import com.mesure.model.VeiculoMulta;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.FornecedorRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.VeiculoMarcaRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.NegocioException;
import com.mesure.service.ProjetoService;
import com.mesure.service.VeiculoService;
import com.mesure.util.Util;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class CadastroVeiculoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private VeiculoService veiculoService;

    @Inject
    VeiculoMarcaRepository veiculoMarcaRepository;

    @Inject
    ColaboradorRepository colaboradorRepository;

    @Inject
    FornecedorRepository fornecedorRepository;

    @Inject
    ProjetoRepository projetoRepository;

    @Inject
    ProjetoService projetoService;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    private Veiculo veiculo;

    private VeiculoMarca marcaPai;

    private List<VeiculoMarca> marcas;
    private List<VeiculoModelo> modelos;

    private List<Projeto> projetos;
    private List<Colaborador> colaboradores;
    private List<Fornecedor> fornecedores;

    private VeiculoMovimento movimento;
    private VeiculoManutencao manutencao;
    private VeiculoMulta multa;

    private Long idProjeto;

    public CadastroVeiculoBean() {
        limpar();
    }

    public void inicializar() {
        if (this.veiculo == null) {
            limpar();
        }

        // Carrega todas as marcas cadastradas
        marcas = veiculoMarcaRepository.marcas();

        // Carrega os projetos
        ProjetoFilter projetoFilter = new ProjetoFilter();
        // Somente projetos em aberto
        EStatusProjeto[] statuses = {EStatusProjeto.COMECAR, EStatusProjeto.ANDAMENTO, EStatusProjeto.ATV_EXTERNA};
        projetoFilter.setStatuses(statuses);
        if (usuarioLogado.getFilialAtiva().getFiltrarProjetos().equals(ESimNao.SIM)) {
            projetoFilter.setFilial(usuarioLogado.getFilialAtiva());
        }
        projetoFilter.setPropriedadeOrdenacao("titulo");
        projetos = projetoRepository.filtrados(projetoFilter);

        // Carrega todos os colaboradores cadastrados
        ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
        colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
        colaboradores = colaboradorRepository.colaboradores(colaboradorFilter);

        // Carrega todos os fornecedores cadastrados
        fornecedores = fornecedorRepository.fornecedores();

        if (marcaPai != null) {
            carregarModelos();
        }
    }

    public void carregarModelos() {
        modelos = null;

        if (marcaPai != null) {
            modelos = veiculoMarcaRepository.modelosDe(marcaPai);
        }
    }

    public void onChangeMarca() {
        carregarModelos();
    }

    public void onChangeProjeto() {
        consultarProjeto();
    }

    public void salvar() {
        veiculo = veiculoService.salvar(veiculo);
        limpar();

        FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
    }

    private void limpar() {
        this.veiculo = new Veiculo();
        marcaPai = null;
        modelos = null;
        limparMovimento();
        limparManutencao();
        limparMulta();
    }

    // Este método deve ser chamado ao abrir o cadastro de movimentações em modo de EDIÇÃO
    public void inicializarMovimento() {
        idProjeto = new Long(0);

        if (movimento != null) {
            if (movimento.getProjeto() != null && movimento.getProjeto().getId() != null) {
                idProjeto = movimento.getProjeto().getId();
            }
        }
    }

    public void incluirMovimento() {

        if (movimento.getDataRetorno() != null && movimento.getDataRetorno().getTime() < movimento.getDataMovimento().getTime()) {
            throw new NegocioException("Data Retorno deve ser maior ou igual que Data Saída");
        }

        if (movimento.getDataRetorno() != null) {
            if (movimento.getDataRetorno().getTime() == movimento.getDataMovimento().getTime()) {
                if (movimento.getHoraRetorno() != null && movimento.getHoraRetorno().getTime() <= movimento.getHoraSaida().getTime()) {
                    throw new NegocioException("Hora Retorno deve ser maior que Hora Saída");
                }
            }
        }

        if (movimento.getDataRetorno() == null && movimento.getHoraRetorno() != null) {
            throw new NegocioException("Ao informar a hora de retorno deve-se informar também a data de retorno");
        }

        if (movimento.getDataRetorno() != null && movimento.getHoraRetorno() == null) {
            throw new NegocioException("Ao informar a data de retorno deve-se informar também a hora de retorno");
        }

        movimento.setDiaSemana(Util.diaDaSemana(movimento.getDataMovimento()));

        if (!veiculo.getMovimentacoes().contains(movimento)) {
            movimento.setVeiculo(veiculo);
            veiculo.getMovimentacoes().add(movimento);
            limparMovimento();
        }
    }

    public void limparMovimento() {
        movimento = new VeiculoMovimento();
        movimento.setDataMovimento(new Date());
        movimento.setHoraSaida(new Date());
        idProjeto = new Long(0);
    }

    public void excluirMovimento() {
        if (movimento != null) {

            // Verifica se o movimento foi gerado através de uma programação
            if (movimento.getProgramacaoExecucao() != null) {
                throw new NegocioException("Essa movimentação não pode ser excluída pois foi gerada através de uma programação");
            }

            veiculo.getMovimentacoes().remove(movimento);
        }
    }

    public void incluirManutencao() {
        if (manutencao.getProximaManutencao() != null && manutencao.getProximaManutencao().getTime() <= manutencao.getData().getTime()) {
            throw new NegocioException("A data da próxima manutenção deve ser maior que a data da manutenção realizada");
        }

        if (!veiculo.getManutencoes().contains(manutencao)) {
            manutencao.setVeiculo(veiculo);
            veiculo.getManutencoes().add(manutencao);
            limparManutencao();
        }
    }

    public void limparManutencao() {
        manutencao = new VeiculoManutencao();
        manutencao.setData(new Date());
    }

    public void excluirManutencao() {
        if (manutencao != null) {
            veiculo.getManutencoes().remove(manutencao);
        }
    }

    public void incluirMulta() {
        if (multa.getDataPrazo() != null && multa.getDataPrazo().getTime() <= multa.getDataNotificacao().getTime()) {
            throw new NegocioException("Prazo deve ser maior que Data notificação");
        }

        if (!veiculo.getMultas().contains(multa)) {
            multa.setVeiculo(veiculo);
            veiculo.getMultas().add(multa);
            limparMulta();
        }
    }

    public void limparMulta() {
        multa = new VeiculoMulta();
    }

    public void excluirMulta() {
        if (multa != null) {
            veiculo.getMultas().remove(multa);
        }
    }

    public void consultarProjeto() {
        if (idProjeto != null && idProjeto != 0) {
            movimento.setProjeto(projetoRepository.porId(idProjeto));

            if (movimento.getProjeto() != null) {

                // Sugere o endereço da obra como sendo o destino 
                EnderecoCliente end = movimento.getProjeto().getEnderecoObra();

                if (end != null) {
                    movimento.setDestino(projetoService.montarDescricaoEnderecoObra(end));
                } else {
                    // Verifica se o cliente vinculado ao projeto possui algum endereço do tipo OBRA e sugere como sendo o destino
                    for (EnderecoCliente endC : movimento.getProjeto().getCliente().getEnderecos()) {
                        if (endC.getTipoEndereco() == ETipoEndereco.OBRA) {
                            movimento.setDestino(projetoService.montarDescricaoEnderecoObra(endC));
                            break;
                        }
                    }
                }
            } else {
                idProjeto = new Long(0);
            }
        } else {
            idProjeto = new Long(0);
            movimento.setProjeto(null);
        }
    }

    // Para carregar as opções do Enum no formulário
    public ESituacaoDetran[] getSituacoesDetran() {
        return ESituacaoDetran.values();
    }

    // Para carregar as opções do Enum no formulário
    public EPeriodoDia[] getPeriodos() {
        return EPeriodoDia.values();
    }

    // Para carregar as opções do Enum no formulário
    public ESituacaoManutencao[] getSituacoesManutencao() {
        return ESituacaoManutencao.values();
    }

    // Para carregar as opções do Enum no formulário
    public EGravidadeMulta[] getGravidadesMulta() {
        return EGravidadeMulta.values();
    }

    // Para carregar as opções do Enum no formulário
    public ESituacaoMulta[] getSituacoesMulta() {
        return ESituacaoMulta.values();
    }

    // Para carregar as opções do Enum no formulário
    public ESimNao[] getOpcoesSimNao() {
        return ESimNao.values();
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;

        if (this.veiculo != null) {
            this.marcaPai = this.veiculo.getModelo().getMarca();
        }
    }

    public VeiculoMarca getMarcaPai() {
        return marcaPai;
    }

    public void setMarcaPai(VeiculoMarca marcaPai) {
        this.marcaPai = marcaPai;
    }

    public List<VeiculoMarca> getMarcas() {
        return marcas;
    }

    public List<VeiculoModelo> getModelos() {
        return modelos;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }

    public List<Fornecedor> getFornecedores() {
        return fornecedores;
    }

    public VeiculoMovimento getMovimento() {
        return movimento;
    }

    public void setMovimento(VeiculoMovimento movimento) {
        this.movimento = movimento;
    }

    public VeiculoManutencao getManutencao() {
        return manutencao;
    }

    public void setManutencao(VeiculoManutencao manutencao) {
        this.manutencao = manutencao;
    }

    public VeiculoMulta getMulta() {
        return multa;
    }

    public void setMulta(VeiculoMulta multa) {
        this.multa = multa;
    }

    public Long getIdProjeto() {
        return idProjeto;
    }

    public void setIdProjeto(Long idProjeto) {
        this.idProjeto = idProjeto;
    }

}

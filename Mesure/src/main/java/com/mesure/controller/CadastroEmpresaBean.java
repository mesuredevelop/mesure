package com.mesure.controller;

import com.mesure.enumerator.EEstado;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ETipoHoraExtra100;
import com.mesure.model.Empresa;
import com.mesure.model.Filial;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.EmpresaService;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class CadastroEmpresaBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EmpresaService empresaService;
    
	@Inject
	@UsuarioLogado
	private UsuarioSistema usuarioLogado;    

    private Empresa empresa;

    private Filial filial;

    public CadastroEmpresaBean() {
        limpar();
    }

    public void inicializar() {
        if (this.empresa == null) {
            limpar();
        }
    }

    private void limpar() {
        this.empresa = new Empresa();
        limparFilial();
    }

    public void limparFilial() {
        this.filial = new Filial();
    }

    public void salvar() {
        empresa = empresaService.salvar(empresa);
        
        // Atualiza a filial ativa para garantir que fique com a informação correta
        for (Filial f : empresa.getFiliais()) {
        	if (f.equals(usuarioLogado.getFilialAtiva())) {
        		usuarioLogado.atualizarFilial(f);
        	}
        }
        
        limpar();
        FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
    }

    public void incluirFilial() {
        if (!empresa.getFiliais().contains(filial)) {
            filial.setEmpresa(empresa);
            empresa.getFiliais().add(filial);
            limparFilial();
        }
    }

    public void excluirFilial() {
        if (filial != null) {
            // TODO - testar quando a filial estiver sendo referenciada
            empresa.getFiliais().remove(filial);
        }
    }

    // Para carregar as opções do Enum no formulário
    public EEstado[] getEstados() {
        return EEstado.values();
    }

    // Para carregar as opções do Enum no formulário
    public ESimNao[] getOpcoesSimNao() {
        return ESimNao.values();
    }

    // Para carregar as opções do Enum no formulário
    public ETipoHoraExtra100[] getTiposHora100() {
        return ETipoHoraExtra100.values();
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }

}

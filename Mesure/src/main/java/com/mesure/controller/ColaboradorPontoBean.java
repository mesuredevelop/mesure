package com.mesure.controller;

import com.mesure.consult.repository.CNHorasFaltantesRepository;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ETipoRegistroPonto;
import com.mesure.model.Colaborador;
import com.mesure.model.ColaboradorPonto;
import com.mesure.model.ColaboradorPontoExtra;
import com.mesure.repository.ColaboradorPontoRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.filter.ColaboradorPontoFilter;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.ColaboradorPontoService;
import com.mesure.service.EquipeService;
import com.mesure.service.NegocioException;
import com.mesure.util.Util;
import com.mesure.util.jpa.Transactional;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

@Named
@ViewScoped
public class ColaboradorPontoBean implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Inject
    ColaboradorRepository colaboradorRepository;
    
    @Inject
    ColaboradorPontoRepository colaboradorPontoRepository;
    
    @Inject
    ColaboradorPontoService colaboradorPontoService;
    
    @Inject
    CNHorasFaltantesRepository cnhorasFaltantesRepository;    
    
    @Inject
    EquipeService equipeService;
    
    @Inject
    Seguranca seguranca;
    
    @Inject
    FacesContext facesContext;
    
    @Inject
    HttpServletResponse respose;
    
    @Inject
    EntityManager manager;
    
    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;
    
    private List<ColaboradorPonto> marcacoesPonto;
    private List<Colaborador> colaboradores;
    
    private ColaboradorPonto colaboradorPonto;

    // filtro de pesquisa
    private Colaborador filtroColaborador;
    private Date filtroCompetencia;
    private Date filtroDataInicial;
    private Date filtroDataFinal;
    
    private boolean carregouRegistros;
    
    public ColaboradorPontoBean() {
        limparPonto();
    }
    
    public void inicializar() {
        limparPesquisa();
        
        filtroColaborador = usuarioLogado.getUsuario().getColaborador();
        filtroCompetencia = new Date();

        // Carrega os colaboradores no qual o usuário é responsável além do próprio colaborador ativo
        colaboradores = equipeService.colaboradoresDoResponsavel(usuarioLogado.getUsuario().getColaborador(), ESimNao.SIM);
    }
    
    public void limparPesquisa() {
        marcacoesPonto = new ArrayList<>();
        setCarregouRegistros(false);
    }
    
    public void pesquisar() {
        carregarPeriodoFiltro();
        
        ColaboradorPontoFilter colaboradorPontoFilter = new ColaboradorPontoFilter();
        
        colaboradorPontoFilter.setColaborador(filtroColaborador);
        colaboradorPontoFilter.setDataPontoDe(getFiltroDataInicial());
        colaboradorPontoFilter.setDataPontoAte(getFiltroDataFinal());

        // Faz a consulta na base de dados
        marcacoesPonto = colaboradorPontoRepository.marcacoes(colaboradorPontoFilter);
        setCarregouRegistros(true);
    }
    
    @Transactional
    public void calcularHoras() throws ParseException {
        if (marcacoesPonto != null && marcacoesPonto.size() > 0) {
            // Faz o cálculo para cada dia registrado
            for (ColaboradorPonto cp : marcacoesPonto) {

                // Total de horas executadas em projetos
                cp = colaboradorPontoService.calcularHorasExecucoesProjetos(cp);
                // Total de horas extras lançadas
                cp = colaboradorPontoService.calcularHorasAtividadesExtras(cp);

                // Calcula os campos de totais 
                cp.calcularTotais();

                // Grava o registro no banco de dados
                cp = colaboradorPontoService.salvar(cp);
            }
            FacesUtil.addInfoMessageGrowl("Cálculo executado com sucesso.");
        }
    }

    // Este método deve ser chamado ao abrir o cadastro de Marcação de Ponto em modo de INCLUSÃO
    public void inicializarPonto() {
        colaboradorPonto.setColaborador(filtroColaborador);
    }
    
    @Transactional
    public void incluirPonto() throws ParseException {
        try {
            // Se é um registro novo
            if (!marcacoesPonto.contains(colaboradorPonto)) {
                // Verifica se a data já está cadastrada no período
                for (ColaboradorPonto cp : marcacoesPonto) {
                    if (!cp.equals(colaboradorPonto)) {
                        if (cp.getData().getTime() == colaboradorPonto.getData().getTime()) {
                            throw new NegocioException("Data já cadastrada");
                        }
                    }
                }

                // Verifica se a data informada está dentro do mês em questão
                if (colaboradorPonto.getData().getTime() < getFiltroDataInicial().getTime() || colaboradorPonto.getData().getTime() > getFiltroDataFinal().getTime()) {
                    throw new NegocioException("Data fora do mês de referência");
                }
                
                // Verifica se permite apontamentos atrasados
                cnhorasFaltantesRepository.verificarApontamentosAtrasados(colaboradorPonto.getColaborador(), colaboradorPonto.getData());
                
            }

            // Quando for ponto Normal, Feriado ou Final de semana, deve considerar as horas que o usuário informar
            if (colaboradorPonto.getTipoRegistro().equals(ETipoRegistroPonto.NORMAL)
                    || colaboradorPonto.getTipoRegistro().equals(ETipoRegistroPonto.FERIADO)
                    || colaboradorPonto.getTipoRegistro().equals(ETipoRegistroPonto.FDSEMANA)) {

                // Quando for NORMAL deve informar a hora de entrada
                if (colaboradorPonto.getTipoRegistro().equals(ETipoRegistroPonto.NORMAL)
                        && colaboradorPonto.getHoraEntrada() == null) {
                    throw new NegocioException("Hora entrada deve ser informada");
                }

                // Consiste hora de entrada x hora de saída do ponto
                if (colaboradorPonto.getHoraSaida() != null && colaboradorPonto.getHoraEntrada().getTime() > colaboradorPonto.getHoraSaida().getTime()) {
                    throw new NegocioException("Hora saída deve ser maior que Hora entrada");
                }

                // Consiste hora de saída x hora de retorno do almoço
                if (colaboradorPonto.getHoraSaidaAlmoco() != null && colaboradorPonto.getHoraRetornoAlmoco() != null && colaboradorPonto.getHoraSaidaAlmoco().getTime() > colaboradorPonto.getHoraRetornoAlmoco().getTime()) {
                    throw new NegocioException("Hora de retorno do almoço deve ser maior que a hora de saída do almoço");
                }

                // Consiste hora de entrada extra x hora de saída extra
                if (colaboradorPonto.getHoraEntradaExtra() != null || colaboradorPonto.getHoraSaidaExtra() != null) {
                    if (colaboradorPonto.getHoraEntradaExtra() == null) {
                        throw new NegocioException("Hora de entrada extra deve ser informada");
                    }
                    
                    if (colaboradorPonto.getHoraSaidaExtra() == null) {
                        throw new NegocioException("Hora de saída extra deve ser informada");
                    }
                    
                    if (colaboradorPonto.getHoraEntradaExtra().getTime() > colaboradorPonto.getHoraSaidaExtra().getTime()) {
                        throw new NegocioException("Hora saída extra deve ser maior que Hora entrada extra");
                    }
                }

                // Total de horas executadas em projetos
                colaboradorPonto = colaboradorPontoService.calcularHorasExecucoesProjetos(colaboradorPonto);
                // Total de horas extras lançadas
                colaboradorPonto = colaboradorPontoService.calcularHorasAtividadesExtras(colaboradorPonto);

                // Calcula os campos de totais
                colaboradorPonto.calcularTotais();
            } else {
                zerarHorasPonto();
            }
            
            colaboradorPonto.setDiaSemana(Util.diaDaSemanaEnum(colaboradorPonto.getData()));

            // Grava o registro no banco de dados
            colaboradorPonto = colaboradorPontoService.salvar(colaboradorPonto);
            
            if (!marcacoesPonto.contains(colaboradorPonto)) {
                marcacoesPonto.add(colaboradorPonto);
                limparPonto();
            }
        } catch (Exception e) {
            throw new NegocioException(StringUtils.join("Não foi possível registrar ponto. Motivo: ", e.getMessage()));
        }
    }
    
    public void antesAbrirModalPonto() {
        if (!isCarregouRegistros()) {
            throw new NegocioException("Clique primeiramente em Mostrar para carregar os registros.");
        }
        limparPonto();
    }
    
    public void limparPonto() {
        colaboradorPonto = new ColaboradorPonto();
    }
    
    public void zerarHorasPonto() {
        colaboradorPonto.setHoraEntrada(null);
        colaboradorPonto.setHoraSaida(null);
        colaboradorPonto.setHoraSaidaAlmoco(null);
        colaboradorPonto.setHoraRetornoAlmoco(null);
        colaboradorPonto.setHoraAlmoco(null);
        colaboradorPonto.setHoraEntradaExtra(null);
        colaboradorPonto.setHoraSaidaExtra(null);
        colaboradorPonto.setSaldoHoras(BigDecimal.ZERO);
        colaboradorPonto.setTotalHoras(BigDecimal.ZERO);
        colaboradorPonto.setTotalHorasAlmoco(BigDecimal.ZERO);
        colaboradorPonto.setTotalHorasProjeto(BigDecimal.ZERO);
        colaboradorPonto.setTotalHorasAtividadeExtra(BigDecimal.ZERO);
        colaboradorPonto.setTotalHorasExtra(BigDecimal.ZERO);
        colaboradorPonto.setTotalHorasFalta(BigDecimal.ZERO);
    }
    
    public void excluirPonto() {
        if (colaboradorPonto != null) {
            colaboradorPontoRepository.remover(colaboradorPonto);

            // Exclui também todas as atividades extras conforme o colaborador e a data
            List<ColaboradorPontoExtra> atvExtras = colaboradorPontoService.atividadesExtras(colaboradorPonto);
            if (atvExtras != null) {
                for (ColaboradorPontoExtra e : atvExtras) {
                    colaboradorPontoRepository.removerAtividadeExtra(e);
                }
            }
            
            marcacoesPonto.remove(colaboradorPonto);
            FacesUtil.addInfoMessageGrowl("Registro excluído com sucesso.");
        }
    }

    // Método responsável por pegar o primeiro e o último dia do mês informado e 
    // armazenar nos atributos filtroDataInicial e filtroDataFinal
    public void carregarPeriodoFiltro() {
        Calendar c = new GregorianCalendar();
        c.setTime(filtroCompetencia);

        // Primeiro dia do mês
        c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMinimum(Calendar.DAY_OF_MONTH));
        setFiltroDataInicial(c.getTime());

        // Último dia do mês		
        c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMaximum(Calendar.DAY_OF_MONTH));
        setFiltroDataFinal(c.getTime());
    }
    
    public void onChangeFiltro() {
        limparPesquisa();
    }
    
    public void onChangeData() {
        // Sugere o tipo de registro
        colaboradorPonto.setTipoRegistro(colaboradorPontoService.sugerirTipoRegistroPonto(colaboradorPonto));
    }

    // Para carregar as opções do Enum no formulário
    public ETipoRegistroPonto[] getTiposRegistro() {
        return ETipoRegistroPonto.values();
    }
    
    public List<ColaboradorPonto> getMarcacoesPonto() {
        return marcacoesPonto;
    }
    
    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }
    
    public ColaboradorPonto getColaboradorPonto() {
        return colaboradorPonto;
    }
    
    public void setColaboradorPonto(ColaboradorPonto colaboradorPonto) {
        this.colaboradorPonto = colaboradorPonto;
    }
    
    public Colaborador getFiltroColaborador() {
        return filtroColaborador;
    }
    
    public void setFiltroColaborador(Colaborador filtroColaborador) {
        this.filtroColaborador = filtroColaborador;
    }
    
    public Date getFiltroCompetencia() {
        return filtroCompetencia;
    }
    
    public void setFiltroCompetencia(Date filtroCompetencia) {
        this.filtroCompetencia = filtroCompetencia;
    }
    
    public Date getFiltroDataInicial() {
        return filtroDataInicial;
    }
    
    public void setFiltroDataInicial(Date filtroDataInicial) {
        this.filtroDataInicial = filtroDataInicial;
    }
    
    public Date getFiltroDataFinal() {
        return filtroDataFinal;
    }
    
    public void setFiltroDataFinal(Date filtroDataFinal) {
        this.filtroDataFinal = filtroDataFinal;
    }
    
    public boolean isCarregouRegistros() {
        return carregouRegistros;
    }
    
    public void setCarregouRegistros(boolean carregouRegistros) {
        this.carregouRegistros = carregouRegistros;
    }
    
}

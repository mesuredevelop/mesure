package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.Atividade;
import com.mesure.repository.AtividadeRepository;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaAtividadeBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	AtividadeRepository atividadeRepository;
	
	private List<Atividade> atividades;
	private Atividade atividadeSelecionada;
	
	public void excluir() {
		atividadeRepository.remover(atividadeSelecionada);
		atividades.remove(atividadeSelecionada);
		
		FacesUtil.addInfoMessage("Atividade '" + atividadeSelecionada.getDescricao() + "' excluída com sucesso.");
	}
	
	public void pesquisarAtividades() {
		atividades = atividadeRepository.atividades();
	}
	
	public List<Atividade> getAtividades() {
		return atividades;
	}	
	
	public Atividade getAtividadeSelecionada() {
		return atividadeSelecionada;
	}

	public void setAtividadeSelecionada(Atividade atividadeSelecionada) {
		this.atividadeSelecionada = atividadeSelecionada;
	}
	
	public void selecionarAtividade() {
//		System.out.println("OnSelect: selecionarAtividade()");
	}
	
	public void selecionarAtividade(Atividade atv) {
//		if (atv != null) {
//			System.out.println("selecionarAtividade: " + atv.getDescricao());
//		} else {
//			System.out.println("selecionarAtividade: Atividade está nula");
//		}
	}
	
	public void selecionarAtividade(Atividade atv, String typeOfSelection, String indexes) {
//		System.out.println("OnSelect:" + atv.getDescricao() + " typeOfSelection: " + typeOfSelection + " indexes: " + indexes);
//		System.out.println("OnSelect: selecionarAtividade(Atividade atv, String typeOfSelection, String indexes)");
	}
	
	public void DeSelecionarAtividade(Atividade atv, String typeOfSelection, String indexes) {
//		System.out.println("OnDeselect:" + atv.getDescricao() + " typeOfSelection: " + typeOfSelection + " indexes: " + indexes);
//		System.out.println("OnDeselect: Atividade atv, String typeOfSelection, String indexes");
	}
	
	public void DeSelecionarAtividade(Atividade atv) {
//		if (atv != null) {
//			System.out.println("DeSelecionarAtividade: " + atv.getDescricao());
//		} else {
//			System.out.println("DeSelecionarAtividade: Atividade está nula");
//		}	
	}

	
}

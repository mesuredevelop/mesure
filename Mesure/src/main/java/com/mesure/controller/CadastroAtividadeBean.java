package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.Atividade;
import com.mesure.model.DispositivoSeguranca;
import com.mesure.model.EquipamentoProtecao;
import com.mesure.model.Etapa;
import com.mesure.model.Treinamento;
import com.mesure.repository.DispositivoSegurancaRepository;
import com.mesure.repository.EquipamentoProtecaoRepository;
import com.mesure.repository.TreinamentoRepository;
import com.mesure.service.AtividadeService;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroAtividadeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject 
	AtividadeService atividadeService;
	
	@Inject
	EquipamentoProtecaoRepository equipamentosRepository;
	
	@Inject
	DispositivoSegurancaRepository dispositivosRepository;
	
	@Inject
	TreinamentoRepository treinamentoRepository;
	
	private List<EquipamentoProtecao> equipamentos;
	private List<DispositivoSeguranca> dispositivos;
	private List<Treinamento> treinamentos;

	private Atividade atividade;
	private Etapa etapa;
	private EquipamentoProtecao equipamentoAtividade;
	private DispositivoSeguranca dispositivoAtividade;	
	private EquipamentoProtecao equipamentoEtapa;
	private DispositivoSeguranca dispositivoEtapa;	
	private Treinamento treinamentoAtividade;

	public CadastroAtividadeBean() {
		limpar();
	}

	public void inicializar() {
		if (this.atividade == null) {
			limpar();
		}
		
		equipamentos = equipamentosRepository.equipamentos();
		dispositivos = dispositivosRepository.dispositivos();
		treinamentos = treinamentoRepository.treinamentos();
	}

	private void limpar() {
		this.atividade = new Atividade();
		limparEtapa();
		limparEquipamentoAtividade();
		limparDispositivoAtividade();	
		limparEquipamentoEtapa();
		limparDispositivoEtapa();
		limparTreinamentoAtividade();
	}

	public void limparEtapa() {
		this.etapa = new Etapa();
	}
	
	public void salvar() {
		atividade = atividadeService.salvar(atividade);
		limpar();

		FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
	}

	public void incluirEquipamentoAtividade() {
		if (equipamentoAtividade == null) {
			FacesUtil.addWarnMessage("Selecione um equipamento para adicionar");
		} else if (atividade.getEquipamentos().contains(equipamentoAtividade)) {
			FacesUtil.addWarnMessage("Equipamento já vinculado nesta atividade");			
		} else {
			atividade.getEquipamentos().add(equipamentoAtividade);
			limparEquipamentoAtividade();
		}
	}
	
	public void incluirDispositivoAtividade() {
		if (dispositivoAtividade == null) {
			FacesUtil.addWarnMessage("Selecione um dispositivo para adicionar");
		} else if (atividade.getDispositivos().contains(dispositivoAtividade)) {
			FacesUtil.addWarnMessage("Dispositivo já vinculado nesta atividade");			
		} else {
			atividade.getDispositivos().add(dispositivoAtividade);
			limparDispositivoAtividade();
		}
	}	
	
	public void incluirEquipamentoEtapa() {
		if (equipamentoEtapa == null) {
			FacesUtil.addWarnMessage("Selecione um equipamento para adicionar");
		} else if (etapa.getEquipamentos().contains(equipamentoEtapa)) {
			FacesUtil.addWarnMessage("Equipamento já vinculado nesta etapa");			
		} else {
			etapa.getEquipamentos().add(equipamentoEtapa);
			limparEquipamentoEtapa();
		}
	}	
	
	public void incluirDispositivoEtapa() {
		if (dispositivoEtapa == null) {
			FacesUtil.addWarnMessage("Selecione um dispositivo para adicionar");
		} else if (etapa.getDispositivos().contains(dispositivoEtapa)) {
			FacesUtil.addWarnMessage("Dispositivo já vinculado nesta etapa");			
		} else {
			etapa.getDispositivos().add(dispositivoEtapa);
			limparDispositivoEtapa();
		}
	}
	
	public void incluirTreinamentoAtividade() {
		if (treinamentoAtividade == null) {
			FacesUtil.addWarnMessage("Selecione um treinamento para adicionar");
		} else if (atividade.getTreinamentos().contains(treinamentoAtividade)) {
			FacesUtil.addWarnMessage("Treinamento já vinculado nesta atividade");			
		} else {
			atividade.getTreinamentos().add(treinamentoAtividade);
			limparTreinamentoAtividade();
		}
	}
	
	public void limparEquipamentoAtividade() {
		this.equipamentoAtividade = new EquipamentoProtecao();
	}	
	
	public void limparDispositivoAtividade() {
		this.dispositivoAtividade = new DispositivoSeguranca();
	}
	
	public void limparEquipamentoEtapa() {
		this.equipamentoEtapa = new EquipamentoProtecao();
	}		
	
	public void limparDispositivoEtapa() {
		this.dispositivoEtapa = new DispositivoSeguranca();
	}	
	
	public void limparTreinamentoAtividade() {
		this.treinamentoAtividade = new Treinamento();
	}
	
	public void excluirEquipamentoAtividade() {
		if (equipamentoAtividade != null) {
			atividade.getEquipamentos().remove(equipamentoAtividade);
		}		
	}	
	
	public void excluirDispositivoAtividade() {
		if (dispositivoAtividade != null) {
			atividade.getDispositivos().remove(dispositivoAtividade);
		}		
	}	
	
	public void excluirEquipamentoEtapa() {
		if (equipamentoEtapa != null) {
			etapa.getEquipamentos().remove(equipamentoEtapa);
		}		
	}		
	
	public void excluirDispositivoEtapa() {
		if (dispositivoEtapa != null) {
			etapa.getDispositivos().remove(dispositivoEtapa);
		}		
	}
	
	public void excluirTreinamentoAtividade() {
		if (treinamentoAtividade != null) {
			atividade.getTreinamentos().remove(treinamentoAtividade);
		}
	}
	
	public void incluirEtapa() {
		if (!atividade.getEtapas().contains(etapa)) {
			etapa.setAtividade(atividade);
			atividade.getEtapas().add(etapa);
			limparEtapa();
		}
	}

	public void excluirEtapa() {
		if (etapa != null) {
			atividade.getEtapas().remove(etapa);
		}
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

	public List<EquipamentoProtecao> getEquipamentos() {
		return equipamentos;
	}

	public void setEquipamentos(List<EquipamentoProtecao> equipamentos) {
		this.equipamentos = equipamentos;
	}

	public List<DispositivoSeguranca> getDispositivos() {
		return dispositivos;
	}

	public void setDispositivos(List<DispositivoSeguranca> dispositivos) {
		this.dispositivos = dispositivos;
	}
	
	public List<Treinamento> getTreinamentos() {
		return treinamentos;
	}

	public void setTreinamentos(List<Treinamento> treinamentos) {
		this.treinamentos = treinamentos;
	}

	public EquipamentoProtecao getEquipamentoAtividade() {
		return equipamentoAtividade;
	}

	public void setEquipamentoAtividade(EquipamentoProtecao equipamentoAtividade) {
		this.equipamentoAtividade = equipamentoAtividade;
	}	

	public DispositivoSeguranca getDispositivoAtividade() {
		return dispositivoAtividade;
	}

	public void setDispositivoAtividade(DispositivoSeguranca dispositivoAtividade) {
		this.dispositivoAtividade = dispositivoAtividade;
	}
	
	public EquipamentoProtecao getEquipamentoEtapa() {
		return equipamentoEtapa;
	}

	public void setEquipamentoEtapa(EquipamentoProtecao equipamentoEtapa) {
		this.equipamentoEtapa = equipamentoEtapa;
	}	

	public DispositivoSeguranca getDispositivoEtapa() {
		return dispositivoEtapa;
	}
	
	public void setDispositivoEtapa(DispositivoSeguranca dispositivoEtapa) {
		this.dispositivoEtapa = dispositivoEtapa;
	}
	
	public Treinamento getTreinamentoAtividade() {
		return treinamentoAtividade;
	}

	public void setTreinamentoAtividade(Treinamento treinamentoAtividade) {
		this.treinamentoAtividade = treinamentoAtividade;
	}
	
}


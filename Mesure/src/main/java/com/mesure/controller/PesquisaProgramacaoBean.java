package com.mesure.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Colaborador;
import com.mesure.model.ProgramacaoExecucao;
import com.mesure.model.ProjetoAtividadePrevista;
import com.mesure.model.ProjetoExecucao;
import com.mesure.model.Veiculo;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.ExecucaoRepository;
import com.mesure.repository.ProgramacaoExecucaoRepository;
import com.mesure.repository.VeiculoRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.repository.filter.ProgramacaoExecucaoFilter;
import com.mesure.repository.filter.VeiculoFilter;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.NegocioException;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaProgramacaoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	ColaboradorRepository colaboradorRepository;
	
	@Inject
	VeiculoRepository veiculoRepository;
	
	@Inject
	ExecucaoRepository execucaoRepository; 
	
	@Inject
	ProgramacaoExecucaoRepository programacaoExecucaoRepository;
	
	@Inject
	@UsuarioLogado
	private UsuarioSistema usuarioLogado;
	
	private ProgramacaoExecucao programacaoSelecionado;
	
	private List<ProgramacaoExecucao> programacoes;
	private List<Colaborador> colaboradores;
	private List<Veiculo> veiculos;
	
	// filtro de pesquisa
	private Colaborador filtroColaborador;
	private Veiculo filtroVeiculo;
	private Date filtroCompetencia;
	private Date filtroDataInicial;
	private Date filtroDataFinal;
	
	public PesquisaProgramacaoBean() {
		limparProgramacao();
	}
	
	public void inicializar() {
		limparPesquisa();
		
		filtroColaborador = usuarioLogado.getColaborador();
		filtroCompetencia = new Date();
		
		// Carrega os registros para o filtro de colaborador
		ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
		colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
		colaboradores = colaboradorRepository.colaboradores(colaboradorFilter);
		
		// Carrega os registros para o filtro de veículo
		veiculos = veiculoRepository.filtrados(new VeiculoFilter());
	}
	
	public void limparPesquisa() {
		programacoes = new ArrayList<>();
	}	
	
	public void pesquisar() throws ParseException {
		carregarPeriodoFiltro();
		
		ProgramacaoExecucaoFilter programacaoExecucaoFilter = new ProgramacaoExecucaoFilter();
		
		programacaoExecucaoFilter.setColaborador(filtroColaborador);
		programacaoExecucaoFilter.setVeiculo(filtroVeiculo);
		programacaoExecucaoFilter.setDataDe(getFiltroDataInicial());
		programacaoExecucaoFilter.setDataAte(getFiltroDataFinal());
		programacaoExecucaoFilter.setFilial(usuarioLogado.getFilialAtiva());
		
		// Faz a consulta na base de dados
		programacoes = programacaoExecucaoRepository.programacoes(programacaoExecucaoFilter);
	}	
			
	private void limparProgramacao() {
		limparPesquisa();
	}

	// Método responsável por pegar o primeiro e o último dia do mês informado e 
	// armazenar nos atributos filtroDataInicial e filtroDataFinal
	public void carregarPeriodoFiltro() {
		Calendar c = new GregorianCalendar();		
		c.setTime(filtroCompetencia);
		
		// Primeiro dia do mês
		c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMinimum(Calendar.DAY_OF_MONTH));
		setFiltroDataInicial(c.getTime());
		
		// Último dia do mês		
		c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMaximum(Calendar.DAY_OF_MONTH));
		setFiltroDataFinal(c.getTime());
	}
	
	public void excluirProgramacao() {
		if (programacaoSelecionado != null) {
			// Quando já existe execução não poderá mais excluir a programação
			ProjetoAtividadePrevista atividade = programacaoExecucaoRepository.atividadePrevistaDaProgramacaoExecucao(programacaoSelecionado);
			if (atividade != null) {
				List<ProjetoExecucao> execucoes = execucaoRepository.execucoesDaAtividadePrevista(atividade);
				if (execucoes != null && execucoes.size() > 0) {
					throw new NegocioException("Essa programação não pode ser excluída pois já possui execução lançada");
				}
			}
			
			programacaoExecucaoRepository.remover(programacaoSelecionado);
				
			programacoes.remove(programacaoSelecionado);
			
			FacesUtil.addInfoMessage("Registro excluído com sucesso.");
		}
	}	
	
	public void onChangeFiltro() {
		limparPesquisa();
	}
	
	// Para carregar as opções do Enum no formulário
	public ESimNao[] getOpcoesSimNao() {
		return ESimNao.values();
	}
	
	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	public List<ProgramacaoExecucao> getProgramacoes() {
		return programacoes;
	}

	public void setProgramacoes(List<ProgramacaoExecucao> programacoes) {
		this.programacoes = programacoes;
	}
	
	public List<Colaborador> getColaboradores() {
		return colaboradores;
	}

	public Colaborador getFiltroColaborador() {
		return filtroColaborador;
	}

	public void setFiltroColaborador(Colaborador filtroColaborador) {
		this.filtroColaborador = filtroColaborador;
	}

	public Veiculo getFiltroVeiculo() {
		return filtroVeiculo;
	}

	public void setFiltroVeiculo(Veiculo filtroVeiculo) {
		this.filtroVeiculo = filtroVeiculo;
	}

	public Date getFiltroCompetencia() {
		return filtroCompetencia;
	}

	public void setFiltroCompetencia(Date filtroCompetencia) {
		this.filtroCompetencia = filtroCompetencia;
	}

	public Date getFiltroDataInicial() {
		return filtroDataInicial;
	}

	public void setFiltroDataInicial(Date filtroDataInicial) {
		this.filtroDataInicial = filtroDataInicial;
	}

	public Date getFiltroDataFinal() {
		return filtroDataFinal;
	}

	public void setFiltroDataFinal(Date filtroDataFinal) {
		this.filtroDataFinal = filtroDataFinal;
	}

	public ProgramacaoExecucao getProgramacaoSelecionado() {
		return programacaoSelecionado;
	}

	public void setProgramacaoSelecionado(ProgramacaoExecucao programacaoSelecionado) {
		this.programacaoSelecionado = programacaoSelecionado;
	}

}

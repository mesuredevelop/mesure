package com.mesure.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Colaborador;
import com.mesure.repository.ColaboradorPontoRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.EquipeService;
import com.mesure.util.jsf.FacesUtil;
import com.mesure.util.report.ExecutorRelatorio;

@Named
@ViewScoped
public class RelatorioColaboradorBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	ColaboradorRepository colaboradorRepository;
	
	@Inject
	ColaboradorPontoRepository colaboradorPontoRepository;
	
	@Inject
	EquipeService equipeService;
	
	@Inject
	Seguranca seguranca;
	
	@Inject
	@UsuarioLogado
	private UsuarioSistema usuarioLogado;
	
	@Inject
	FacesContext facesContext;
	
	@Inject
	HttpServletResponse respose;
	
	@Inject
	EntityManager manager;
	
	private List<Colaborador> colaboradores;
	
	private Colaborador filtroColaborador;
	private Date filtroDataInicial;
	private Date filtroDataFinal;
	
	
	public void imprimirTreinamentos() {		
		Map<String, Object> parametros = new HashMap<>();
				
		if (this.filtroColaborador != null && this.filtroColaborador.getId() != null) {
			parametros.put("colaborador", this.filtroColaborador.getId());	
		}		
		
		parametros.put("data_inicio", this.filtroDataInicial);
		parametros.put("data_fim", this.filtroDataFinal);		
		
		ExecutorRelatorio executor = new ExecutorRelatorio("/relatorios/colaboradores/colaboradores001.jasper",
				this.respose, parametros, "Treinamentos.pdf");
		
		Session session = manager.unwrap(Session.class);
		session.doWork(executor);
		
		if (executor.isRelatorioGerado()) {
			facesContext.responseComplete();
		} else {
			FacesUtil.addErrorMessage("A execução do relatório não retornou dados.");
		}
	}	
	
	public void inicializar() {
		filtroColaborador = usuarioLogado.getUsuario().getColaborador();		
		
		// Carrega os colaboradores no qual o usuário é responsável além do próprio colaborador ativo
		colaboradores = equipeService.colaboradoresDoResponsavel(usuarioLogado.getUsuario().getColaborador(), ESimNao.SIM);
	}		
		
	public Colaborador getFiltroColaborador() {
		return filtroColaborador;
	}

	public void setFiltroColaborador(Colaborador filtroColaborador) {
		this.filtroColaborador = filtroColaborador;
	}

	public List<Colaborador> getColaboradores() {
		return colaboradores;
	}

	public void setColaboradores(List<Colaborador> colaboradores) {
		this.colaboradores = colaboradores;
	}
	
	public Date getFiltroDataInicial() {
		return filtroDataInicial;
	}

	public void setFiltroDataInicial(Date filtroDataInicial) {
		this.filtroDataInicial = filtroDataInicial;
	}

	public Date getFiltroDataFinal() {
		return filtroDataFinal;
	}

	public void setFiltroDataFinal(Date filtroDataFinal) {
		this.filtroDataFinal = filtroDataFinal;
	}	
	
}

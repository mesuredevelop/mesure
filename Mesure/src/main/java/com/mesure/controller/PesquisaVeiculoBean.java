package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.Veiculo;
import com.mesure.repository.VeiculoRepository;
import com.mesure.repository.filter.VeiculoFilter;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaVeiculoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	VeiculoRepository veiculoRepository;
	
	private VeiculoFilter filtro;
	private List<Veiculo> veiculosFiltrados;
	
	private Veiculo veiculoSelecionado;
	
	public PesquisaVeiculoBean() {
		filtro = new VeiculoFilter();
	}	
	
	public void excluir() {
		veiculoRepository.remover(veiculoSelecionado);
		veiculosFiltrados.remove(veiculoSelecionado);
		
		FacesUtil.addInfoMessage("Veículo '" + veiculoSelecionado.getPlaca() + "' excluído com sucesso.");
	}
	
	public void pesquisar() {
		veiculosFiltrados = veiculoRepository.filtrados(filtro);
	}
		
	public VeiculoFilter getFiltro() {
		return filtro;
	}

	public List<Veiculo> getVeiculosFiltrados() {
		return veiculosFiltrados;
	}

	public Veiculo getVeiculoSelecionado() {
		return veiculoSelecionado;		
	}

	public void setVeiculoSelecionado(Veiculo veiculoSelecionado) {
		this.veiculoSelecionado = veiculoSelecionado;
	}	

}


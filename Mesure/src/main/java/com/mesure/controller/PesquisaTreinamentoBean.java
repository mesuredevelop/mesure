package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.ELocalTreinamento;
import com.mesure.model.Fornecedor;
import com.mesure.model.Treinamento;
import com.mesure.repository.FornecedorRepository;
import com.mesure.repository.TreinamentoRepository;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaTreinamentoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	TreinamentoRepository treinamentoRepository;
	
	@Inject
	FornecedorRepository fornecedorRepository;	
	
	private List<Treinamento> treinamentos;	
	private List<Fornecedor> fornecedores;
	
	private Treinamento treinamentoSelecionado;
	
	public PesquisaTreinamentoBean() {
		limpar();
	}
	
	public void inicializar() {
		// Carrega todos os fornecedores cadastrados
		fornecedores = fornecedorRepository.fornecedores();		
	}
	
	public void salvar() {
		treinamentoSelecionado = treinamentoRepository.guardar(treinamentoSelecionado);
		limpar();
	}	
	
	public void excluir() {
		treinamentoRepository.remover(treinamentoSelecionado);
		treinamentos.remove(treinamentoSelecionado);
		
		FacesUtil.addInfoMessage("Treinamento '" + treinamentoSelecionado.getDescricao() + "' excluído com sucesso.");
	}
	
	public void pesquisar() {
		treinamentos = treinamentoRepository.treinamentos();
	}
	
	public void limpar() {
		treinamentoSelecionado = new Treinamento();
	}
	
	// Para carregar as opções do Enum no formulário
	public ELocalTreinamento [] getLocais() {
		return ELocalTreinamento.values();
	}	


	public Treinamento getTreinamentoSelecionado() {
		return treinamentoSelecionado;
	}

	public void setTreinamentoSelecionado(Treinamento treinamentoSelecionado) {
		this.treinamentoSelecionado = treinamentoSelecionado;
	}

	public List<Treinamento> getTreinamentos() {
		return treinamentos;
	}

	public List<Fornecedor> getFornecedores() {
		return fornecedores;
	}

}

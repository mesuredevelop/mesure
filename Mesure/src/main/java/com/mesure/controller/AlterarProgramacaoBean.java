package com.mesure.controller;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Colaborador;
import com.mesure.model.Filial;
import com.mesure.model.ProgramacaoExecucao;
import com.mesure.model.ProjetoAtividadePrevista;
import com.mesure.model.TipoAtividadePrevista;
import com.mesure.model.Veiculo;
import com.mesure.model.VeiculoMovimento;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.EmpresaRepository;
import com.mesure.repository.ExecucaoRepository;
import com.mesure.repository.ProgramacaoExecucaoRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.TipoAtividadePrevistaRepository;
import com.mesure.repository.VeiculoRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.repository.filter.VeiculoFilter;
import com.mesure.service.NegocioException;
import com.mesure.service.ProjetoService;
import com.mesure.util.Util;
import com.mesure.util.jpa.Transactional;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class AlterarProgramacaoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    ProgramacaoExecucaoRepository programacaoExecucaoRepository;

    @Inject
    ExecucaoRepository execucaoRepository;

    @Inject
    ColaboradorRepository colaboradorRepository;

    @Inject
    TipoAtividadePrevistaRepository tipoAtividadePrevistaRepository;

    @Inject
    EmpresaRepository empresaRepository;

    @Inject
    ProjetoRepository projetoRepository;

    @Inject
    ProjetoService projetoService;

    @Inject
    VeiculoRepository veiculoRepository;

    private List<ProgramacaoExecucao> programacoes;
    private List<Colaborador> colaboradores;
    private List<TipoAtividadePrevista> atividades;
    private List<Filial> filiais;
    private List<Veiculo> veiculos;

    private Date data;
    private Colaborador colaborador;
    private TipoAtividadePrevista atividadePrevista;
    private Filial filial;
    private Veiculo veiculo;
    private Date saidaEmpresa;
    private Date retornoEmpresa;
    private Date horaInicio;
    private Date horaTermino;
    private BigDecimal horasViagem;
    private ESimNao motorista;
    private ESimNao ativo;

    private boolean carregouRegistros;

    public void inicializar() {
        programacoes = new ArrayList<>();

        // Carrega os registros para o combo de colaboradores
        ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
        colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
        colaboradores = colaboradorRepository.colaboradores(colaboradorFilter);

        // Carrega os registros para o combo de atividades previstas
        atividades = tipoAtividadePrevistaRepository.tipos();

        // Carrega os registros para o combo de filiais
        filiais = empresaRepository.filiais();

        // Carrega os registros para o combo de veículos
        veiculos = veiculoRepository.filtrados(new VeiculoFilter());

        setCarregouRegistros(false);
    }

    public void mostrar() {
        // Busca todas as programações cuja atividade prevista está com situação 'PENDENTE' e ainda não possui execução lançada
        programacoes = programacaoExecucaoRepository.programacoesSemExecucao();

        if (programacoes == null || programacoes.isEmpty()) {
            throw new NegocioException("Nenhum registro encontrado");
        }
        setCarregouRegistros(true);
    }

    @Transactional
    public void alterar() throws ParseException {
        if (programacoes == null || programacoes.isEmpty()) {
            throw new NegocioException("Nenhum registro para processar");
        }

        int programacoesSelecionadas = 0;
        int programacoesAlteradas = 0;

        // Percorre todas as programações e altera as que o usuário selecionou
        for (ProgramacaoExecucao p : programacoes) {
            // Se selecionou o registro para alteração
            if (p.isFlag()) {
                programacoesSelecionadas++;

                // Busca a programação na base de dados
                ProgramacaoExecucao programacao = programacaoExecucaoRepository.porId(p.getId());

                if (programacao != null && programacao.isEditando()) {

                    boolean alterarProgramacao = false;
                    boolean alterarAtividade = false;
                    boolean alterarMovimento = false;
                    boolean inserirMovimento = false;
                    boolean excluirMovimento = false;

                    // Busca a atividade prevista gerada pela programação
                    ProjetoAtividadePrevista atividade = programacaoExecucaoRepository.atividadePrevistaDaProgramacaoExecucao(programacao);

                    // Busca a movimentação de veículo gerada pela programação
                    VeiculoMovimento movimento = programacaoExecucaoRepository.movimentacaoVeiculoDaProgramacaoExecucao(programacao);

                    // Data
                    if (getData() != null) {
                        programacao.setData(getData());
                        alterarProgramacao = true;
                        if (atividade != null) {
                            atividade.setData(getData());
                            alterarAtividade = true;
                        }
                        if (movimento != null) {
                            movimento.setDataMovimento(getData());
                            movimento.setDataRetorno(getData());
                            movimento.setDiaSemana(Util.diaDaSemana(getData()));
                            alterarMovimento = true;
                        }
                    }
                    // Colaborador
                    if (getColaborador() != null) {
                        programacao.setColaborador(getColaborador());
                        alterarProgramacao = true;
                        if (atividade != null) {
                            atividade.setColaborador(getColaborador());
                            alterarAtividade = true;
                        }
                        if (movimento != null) {
                            movimento.setColaborador(getColaborador());
                            alterarMovimento = true;
                        }
                    }
                    // Atividade Prevista
                    if (getAtividadePrevista() != null) {
                        programacao.setTipoAtividadePrevista(getAtividadePrevista());
                        alterarProgramacao = true;
                        if (atividade != null) {
                            atividade.setTipo(getAtividadePrevista());
                            alterarAtividade = true;
                        }
                    }
                    // Filial
                    if (getFilial() != null) {
                        programacao.setFilial(getFilial());
                        alterarProgramacao = true;
                    }
                    // Saída Empresa
                    if (getSaidaEmpresa() != null) {
                        programacao.setSaidaEmpresa(getSaidaEmpresa());
                        alterarProgramacao = true;
                        if (movimento != null) {
                            movimento.setHoraSaida(getSaidaEmpresa());
                            movimento.setPeriodo(Util.periodoDia(getSaidaEmpresa()));
                            alterarMovimento = true;
                        }
                    }
                    // Retorno Empresa
                    if (getRetornoEmpresa() != null) {
                        programacao.setRetornoEmpresa(getRetornoEmpresa());
                        alterarProgramacao = true;
                        if (movimento != null) {
                            movimento.setHoraRetorno(getRetornoEmpresa());
                            alterarMovimento = true;
                        }
                    }
                    // Hora Início
                    if (getHoraInicio() != null) {
                        programacao.setHoraInicio(getHoraInicio());
                        alterarProgramacao = true;
                    }
                    // Hora Término
                    if (getHoraTermino() != null) {
                        programacao.setHoraTermino(getHoraTermino());
                        alterarProgramacao = true;
                    }
                    // Horas Viagem
                    if (getHorasViagem() != null) {
                        programacao.setHorasViagem(getHorasViagem());
                        alterarProgramacao = true;
                    }
                    // Ativo
                    if (getAtivo() != null) {
                        programacao.setAtivo(getAtivo());
                        alterarProgramacao = true;
                    }
                    // Motorista
                    if (getMotorista() != null && programacao.getVeiculoProprio().equals(ESimNao.NAO)) {
                        // Se mudou de NÃO para SIM - insere o movimento
                        if (programacao.getMotorista().equals(ESimNao.NAO) && getMotorista().equals(ESimNao.SIM)) {
                            inserirMovimento = true;
                            // Se mudou de SIM para NÃO - exclui o movimento
                        } else if (programacao.getMotorista().equals(ESimNao.SIM) && getMotorista().equals(ESimNao.NAO)) {
                            excluirMovimento = true;
                        }
                        programacao.setMotorista(getMotorista());
                        alterarProgramacao = true;
                    }
                    // Veículo
                    if (getVeiculo() != null && programacao.getVeiculoProprio().equals(ESimNao.NAO)) {
                        if (movimento != null) {
                            movimento.setVeiculo(getVeiculo());
                            alterarMovimento = true;
                        } else if (programacao.getMotorista().equals(ESimNao.SIM)) {
                            // Se informou veículo, é motorista mas ainda não possui movimento, então cria o movimento
                            inserirMovimento = true;
                        }
                        programacao.setVeiculo(getVeiculo());
                        alterarProgramacao = true;
                    }

                    // Se deve inserir um movimento
                    if (inserirMovimento && programacao.getMotorista().equals(ESimNao.SIM)) {
                        // Validação necessária para não tentar inserir um movimento sem veículo, evitando assim erro de banco
                        if (programacao.getVeiculo() == null) {
                            throw new NegocioException("Ao informar Motorista = 'Sim' é necessário que a programação tenha um veículo para gerar a movimentação");
                        }
                        // Validação necessária para não tentar inserir um movimento sem hora de saída, evitando assim erro de banco
                        if (programacao.getSaidaEmpresa() == null) {
                            throw new NegocioException("Ao informar Motorista = 'Sim' é necessário que a programação tenha um horário de saída da empresa");
                        }
                    }

                    // Validação quando a programação utiliza veículo próprio e o usuário preencheu Veículo e/ou Motorista
                    if (programacao.getVeiculoProprio().equals(ESimNao.SIM)) {
                        if (getVeiculo() != null || (getMotorista() != null && getMotorista().equals(ESimNao.SIM))) {
                            throw new NegocioException("O motorista/veículo não pode ser alterado, pois a programação utiliza o veículo do colaborador");
                        }
                    }

                    if (alterarProgramacao) {
                        programacoesAlteradas++;

                        // Atualiza a programação no banco de dados
                        programacao = programacaoExecucaoRepository.guardar(programacao);

                        // Atualiza a atividade prevista no banco de dados
                        if (alterarAtividade) {
                            projetoRepository.guardarAtividadePrevista(atividade);
                        }

                        // Verifica se deve incluir, alterar ou excluir a movimentação do veículo no banco de dados
                        if (excluirMovimento) {
                            if (movimento != null) {
                                veiculoRepository.removerMovimento(movimento);
                            }
                        } else if (inserirMovimento) {
                            // Se não possui movimento então cria um novo, caso contrário apenas atualiza
                            if (movimento == null) {
                                movimento = new VeiculoMovimento();
                                movimento.setDataMovimento(programacao.getData());
                                movimento.setDataRetorno(programacao.getData());
                                movimento.setDestino(projetoService.montarDescricaoEnderecoObra(programacao.getProjeto().getEnderecoObra()));
                                movimento.setDiaSemana(Util.diaDaSemana(movimento.getDataMovimento()));
                                movimento.setHoraSaida(programacao.getSaidaEmpresa());
                                movimento.setHoraRetorno(programacao.getRetornoEmpresa());
                                movimento.setPeriodo(Util.periodoDia(movimento.getHoraSaida()));
                                movimento.setColaborador(programacao.getColaborador());
                                movimento.setProjeto(programacao.getProjeto());
                                movimento.setVeiculo(programacao.getVeiculo());
                                movimento.setProgramacaoExecucao(programacao);
                            }
                            veiculoRepository.guardarMovimento(movimento);
                        } else if (alterarMovimento) {
                            // Atualiza a movimentação no banco de dados
                            veiculoRepository.guardarMovimento(movimento);
                        }
                    }
                }
            }
        }

        if (programacoesSelecionadas == 0) {
            throw new NegocioException("Nenhum registro foi selecionado");
        }

        if (programacoesAlteradas == 0) {
            throw new NegocioException("Nenhum campo foi preenchido para alteração ou não houveram alterações a serem feitas");
        }

        // Limpa os dados da tela
        programacoes = new ArrayList<>();
        setCarregouRegistros(false);
        limparCampos();

        FacesUtil.addInfoMessageGrowl("Alteração realizada com sucesso");
    }

    @Transactional
    public void excluir() {
        if (programacoes == null || programacoes.isEmpty()) {
            throw new NegocioException("Nenhum registro para processar");
        }

        int programacoesSelecionadas = 0;

        try {
            // Percorre todas as programações e exclui as que o usuário selecionou
            for (ProgramacaoExecucao p : programacoes) {
                // Se selecionou o registro para alteração
                if (p.isFlag()) {
                    programacoesSelecionadas++;

                    // Busca a programação na base de dados
                    ProgramacaoExecucao programacao = programacaoExecucaoRepository.porId(p.getId());

                    if (programacao != null && programacao.isEditando()) {
                        programacaoExecucaoRepository.remover(programacao);
                    }
                }
            }
        } catch (Exception e) {
            throw new NegocioException("Ocorreu um erro ao excluir a programação");
        }

        if (programacoesSelecionadas == 0) {
            throw new NegocioException("Nenhum registro foi selecionado");
        }

        // Limpa os dados da tela
        programacoes = new ArrayList<>();
        setCarregouRegistros(false);
        limparCampos();

        FacesUtil.addInfoMessageGrowl("Exclusão realizada com sucesso");
    }

    public void marcarTodos() {
        if (programacoes != null && programacoes.size() > 0) {
            // Percorre todas as programações
            for (ProgramacaoExecucao p : programacoes) {
                p.setFlag(true);
            }
        }
    }

    public void desmarcarTodos() {
        if (programacoes != null && programacoes.size() > 0) {
            // Percorre todas as programações
            for (ProgramacaoExecucao p : programacoes) {
                p.setFlag(false);
            }
        }
    }

    public void limparCampos() {
        setData(null);
        setColaborador(null);
        setAtividadePrevista(null);
        setFilial(null);
        setVeiculo(null);
        setSaidaEmpresa(null);
        setRetornoEmpresa(null);
        setHoraInicio(null);
        setHoraTermino(null);
        setHorasViagem(BigDecimal.ZERO);
        setMotorista(null);
        setAtivo(null);
    }

    // Para carregar as opções do Enum no formulário
    public ESimNao[] getOpcoesSimNao() {
        return ESimNao.values();
    }

    public List<ProgramacaoExecucao> getProgramacoes() {
        return programacoes;
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }

    public List<TipoAtividadePrevista> getAtividades() {
        return atividades;
    }

    public List<Filial> getFiliais() {
        return filiais;
    }

    public List<Veiculo> getVeiculos() {
        return veiculos;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public TipoAtividadePrevista getAtividadePrevista() {
        return atividadePrevista;
    }

    public void setAtividadePrevista(TipoAtividadePrevista atividadePrevista) {
        this.atividadePrevista = atividadePrevista;
    }

    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public Date getSaidaEmpresa() {
        return saidaEmpresa;
    }

    public void setSaidaEmpresa(Date saidaEmpresa) {
        this.saidaEmpresa = saidaEmpresa;
    }

    public Date getRetornoEmpresa() {
        return retornoEmpresa;
    }

    public void setRetornoEmpresa(Date retornoEmpresa) {
        this.retornoEmpresa = retornoEmpresa;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Date getHoraTermino() {
        return horaTermino;
    }

    public void setHoraTermino(Date horaTermino) {
        this.horaTermino = horaTermino;
    }

    public BigDecimal getHorasViagem() {
        return horasViagem;
    }

    public void setHorasViagem(BigDecimal horasViagem) {
        this.horasViagem = horasViagem;
    }

    public ESimNao getMotorista() {
        return motorista;
    }

    public void setMotorista(ESimNao motorista) {
        this.motorista = motorista;
    }

    public ESimNao getAtivo() {
        return ativo;
    }

    public void setAtivo(ESimNao ativo) {
        this.ativo = ativo;
    }

    public boolean isCarregouRegistros() {
        return carregouRegistros;
    }

    public void setCarregouRegistros(boolean carregouRegistros) {
        this.carregouRegistros = carregouRegistros;
    }

}

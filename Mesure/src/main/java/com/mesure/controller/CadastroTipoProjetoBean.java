package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.TipoProjeto;
import com.mesure.repository.TipoProjetoRepository;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroTipoProjetoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	TipoProjetoRepository tipoRepository;
	
	private List<TipoProjeto> tiposFiltrados;	
	private TipoProjeto tipoSelecionado;
	
	public CadastroTipoProjetoBean() {
		limpar();
	}
	
	public void salvar() {
		tipoSelecionado = tipoRepository.guardar(tipoSelecionado);
		limpar();
	}	
	
	public void excluir() {
		tipoRepository.remover(tipoSelecionado);
		tiposFiltrados.remove(tipoSelecionado);
		
		FacesUtil.addInfoMessage("Tipo de Projeto '" + tipoSelecionado.getDescricao() + "' excluído com sucesso.");
	}
	
	public void pesquisar() {
		tiposFiltrados = tipoRepository.tipos();
	}

	public List<TipoProjeto> getTiposFiltrados() {
		return tiposFiltrados;
	}	
	
	public TipoProjeto getTipoSelecionado() {
		return tipoSelecionado;
	}
	
	public void setTipoSelecionado(TipoProjeto tipoSelecionado) {
		this.tipoSelecionado = tipoSelecionado;
	}	
	
	public void limpar() {
		this.tipoSelecionado = new TipoProjeto();
	}

}


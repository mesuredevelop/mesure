package com.mesure.controller;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.enumerator.EStatusTitulo;
import com.mesure.model.Cliente;
import com.mesure.model.ContaBancaria;
import com.mesure.model.Empresa;
import com.mesure.model.Filial;
import com.mesure.model.Projeto;
import com.mesure.model.TituloReceber;
import com.mesure.model.TituloReceberBaixa;
import com.mesure.repository.ClienteRepository;
import com.mesure.repository.ContaBancariaRepository;
import com.mesure.repository.EmpresaRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.filter.EmpresaFilter;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.NegocioException;
import com.mesure.service.TituloReceberService;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;

@Named
@ViewScoped
public class CadastroTituloReceberBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private TituloReceberService tituloReceberService;

    @Inject
    EmpresaRepository empresaRepository;

    @Inject
    ClienteRepository clienteRepository;

    @Inject
    ContaBancariaRepository contaBancariaRepository;

    @Inject
    private ProjetoRepository projetoRepository;

    @Inject
    private HttpServletRequest request;

    @Inject
    Seguranca seguranca;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    private TituloReceber tituloReceber;

    private Empresa empresa;
    private Cliente cliente;

    private List<Empresa> empresas;
    private List<Filial> filiais;
    private List<Projeto> projetos;
    private List<Cliente> clientes;
    private List<ContaBancaria> contasBancaria;

    private TituloReceberBaixa tituloReceberBaixa;

    private Long idProjeto;
    private Date dataMovimentoEstornar;

    public CadastroTituloReceberBean() {
        limpar();
    }

    public void inicializar() {
        String param_tituloReceber = request.getParameter("tituloReceber");

        if (StringUtils.isBlank(param_tituloReceber)) {
            tituloReceber = null;
        }

        // Verifica se a filial do projeto é a filial ativa
        if (tituloReceber != null && tituloReceber.isEditando()) {
            if (!tituloReceber.getFilial().equals(usuarioLogado.getFilialAtiva())) {
                tituloReceber = null;
            }
        }

        if (tituloReceber == null) {
            limpar();
        }

        // Carrega os projetos em aberto
        carregarProjetos();

        // carrega todas as empresas cadastradas
        empresas = empresaRepository.empresas(new EmpresaFilter());

        // Carrega todos os clientes cadastrados
        clientes = clienteRepository.clientes();

        // Se é um novo projeto, ou seja, modo de inclusão
        if (tituloReceber != null && !tituloReceber.isEditando()) {
            tituloReceber.setStatus(EStatusTitulo.ABERTO);
            empresa = usuarioLogado.getFilialAtiva().getEmpresa();
            tituloReceber.setFilial(usuarioLogado.getFilialAtiva());
        }

        // Carrega as filiais da empresa para apresentar no formulário
        if (empresa != null) {
            carregarFiliais();
        }

        // Carrega o cliente vinculado ao projeto, para que já fique selecionado na tela
        cliente = null;
        if (tituloReceber.isEditando()) {
            cliente = tituloReceber.getProjeto().getCliente();
        }

        idProjeto = new Long(0);
    }

    private void limpar() {
        tituloReceber = new TituloReceber();

        idProjeto = new Long(0);
        empresa = null;
        filiais = null;

        limparBaixa();
    }

    public void excluirBaixa() {
        if (tituloReceberBaixa != null) {
            tituloReceber.getBaixas().remove(tituloReceberBaixa);
        }
    }

    public void salvar() {
        tituloReceber = tituloReceberService.salvar(tituloReceber);

        FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
    }

    public boolean isHabilitaBotaoGravar() {
        return tituloReceber.getStatus() == EStatusTitulo.ABERTO;
    }

    public void carregarFiliais() {
        filiais = null;

        if (empresa != null) {
            filiais = empresaRepository.filialDe(empresa);
        }
    }

    public void onChangeEmpresa() {
        carregarFiliais();
    }

    public void onChangeCBCliente() {
        tituloReceber.setProjeto(null);
        carregarProjetos();

        if (projetos.isEmpty()) {
            FacesUtil.addWarnMessage("O cliente informado não possui projetos cadastrados.");
        }
    }

    public void limparBaixa() {
        tituloReceberBaixa = new TituloReceberBaixa();
    }

    // Para carregar as opções do Enum no formulário
    public EStatusTitulo[] getStatus() {
        return EStatusTitulo.values();
    }

    public TituloReceber getTituloReceber() {
        return tituloReceber;
    }

    public void setTituloReceber(TituloReceber tituloReceber) {
        this.tituloReceber = tituloReceber;

        if (this.tituloReceber != null) {
            this.empresa = this.tituloReceber.getFilial().getEmpresa();
        }
    }

    public void incluirBaixa() throws ParseException {
        tituloReceberService.consistirBaixaTituloReceber(tituloReceberBaixa);

        if (!tituloReceber.getBaixas().contains(tituloReceberBaixa)) {
            tituloReceberBaixa.setTitulo(tituloReceber);
            tituloReceberBaixa.setValorLiquido(tituloReceberService.getValorLiquidoTituloReceberBaixa(tituloReceberBaixa));
            tituloReceber.getBaixas().add(tituloReceberBaixa);
            limparBaixa();
        }
    }

    public void carregarProjetos() {
        ProjetoFilter projetoFilter = new ProjetoFilter();
        // Somente projetos em aberto
        EStatusProjeto[] statuses = {EStatusProjeto.COMECAR, EStatusProjeto.ANDAMENTO, EStatusProjeto.ATV_EXTERNA};
        projetoFilter.setStatuses(statuses);
        if (usuarioLogado.getFilialAtiva().getFiltrarProjetos().equals(ESimNao.SIM)) {
            projetoFilter.setFilial(usuarioLogado.getFilialAtiva());
        }
        projetoFilter.setCliente(cliente);
        projetoFilter.setPropriedadeOrdenacao("titulo");
        projetos = projetoRepository.filtrados(projetoFilter);

        if (tituloReceber != null) {
            if (tituloReceber.getProjeto() != null && tituloReceber.getProjeto().getId() != null) {
                idProjeto = tituloReceber.getProjeto().getId();
            } else {
                idProjeto = new Long(0);
            }
        }
    }

    public void onChangeCBProjeto() {
        if (tituloReceber.getProjeto() != null) {
            idProjeto = tituloReceber.getProjeto().getId();
        } else {
            idProjeto = new Long(0);
        }
    }

    public void consultarProjetoId() {
        if (idProjeto != null && idProjeto != 0) {
            tituloReceber.setProjeto(projetoRepository.porId(idProjeto));

            if (tituloReceber.getProjeto() == null || (!projetos.contains(tituloReceber.getProjeto()))) {
                idProjeto = new Long(0);
                tituloReceber.setProjeto(null);
            }
        } else {
            idProjeto = new Long(0);
            tituloReceber.setProjeto(null);
        }
    }

    public void onChangeIdProjeto() {
        consultarProjetoId();
    }

    public void onChangeValoresBaixa() {
        tituloReceberBaixa.setValorLiquido(tituloReceberService.getValorLiquidoTituloReceberBaixa(tituloReceberBaixa));
    }

    public void inicializarBaixa() {
        tituloReceberBaixa.setTitulo(tituloReceber);
        tituloReceberBaixa.setDataBaixa(new Date());
        tituloReceberBaixa.setDataMovimento(new Date());
        tituloReceberBaixa.setValorBaixa(tituloReceber.getSaldo());
        tituloReceberBaixa.setValorAcrescimo(BigDecimal.ZERO);
        tituloReceberBaixa.setValorDesconto(BigDecimal.ZERO);
        tituloReceberBaixa.setValorJuros(BigDecimal.ZERO);
        tituloReceberBaixa.setValorMulta(BigDecimal.ZERO);
        tituloReceberBaixa.setValorLiquido(BigDecimal.ZERO);

        contasBancaria = contaBancariaRepository.obterContasBancarias(tituloReceber.getFilial());
    }

    public void estornarBaixa() throws ParseException {
        if (dataMovimentoEstornar != null) {
            List<TituloReceberBaixa> baixasList = new ArrayList<>();

            for (TituloReceberBaixa baixas : tituloReceber.getBaixas()) {
                if (baixas.isFlag() && baixas.getValorBaixa().doubleValue() > 0) {
                    if (dataMovimentoEstornar.before(baixas.getDataMovimento())) {
                        throw new NegocioException("Data do movimento não pode ser anterior a data do movimento da baixa de origem.");
                    }

                    TituloReceberBaixa baixaEstorno = new TituloReceberBaixa();

                    baixaEstorno.setContaBancaria(baixas.getContaBancaria());
                    baixaEstorno.setDataBaixa(baixas.getDataBaixa());
                    baixaEstorno.setDataMovimento(dataMovimentoEstornar);
                    baixaEstorno.setFlag(false);
                    baixaEstorno.setValorAcrescimo(new BigDecimal(baixas.getValorAcrescimo().doubleValue() * -1));
                    baixaEstorno.setValorBaixa(new BigDecimal(baixas.getValorBaixa().doubleValue() * -1));
                    baixaEstorno.setValorDesconto(new BigDecimal(baixas.getValorDesconto().doubleValue() * -1));
                    baixaEstorno.setValorJuros(new BigDecimal(baixas.getValorJuros().doubleValue() * -1));
                    baixaEstorno.setValorLiquido(new BigDecimal(baixas.getValorLiquido().doubleValue() * -1));
                    baixaEstorno.setValorMulta(new BigDecimal(baixas.getValorMulta().doubleValue() * -1));

                    tituloReceberService.consistirBaixaTituloReceber(baixaEstorno);

                    if (!tituloReceber.getBaixas().contains(baixaEstorno)) {
                        tituloReceber.setStatus(EStatusTitulo.ABERTO);
                        baixaEstorno.setTitulo(baixas.getTitulo());
                        baixasList.add(baixaEstorno);
                    }
                }
            }
            tituloReceber.getBaixas().addAll(baixasList);
        } else {
            throw new NegocioException("Data do movimento deve ser informado.");
        }
    }

    public void validarSelecionados() {
        if (tituloReceber.getBaixas() == null || tituloReceber.getBaixas().isEmpty()) {
            throw new NegocioException("Nenhum registro para processar");
        }

        boolean selecionado = false;

        for (TituloReceberBaixa b : tituloReceber.getBaixas()) {
            if (b.isFlag()) {
                selecionado = true;
                break;
            }
        }

        if (!selecionado) {
            throw new NegocioException("Nenhum feriado selecionado.");
        }
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public List<Filial> getFiliais() {
        return filiais;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public List<ContaBancaria> getContasBancaria() {
        return contasBancaria;
    }

    public TituloReceberBaixa getTituloReceberBaixa() {
        return tituloReceberBaixa;
    }

    public void setTituloReceberBaixa(TituloReceberBaixa tituloReceberBaixa) {
        this.tituloReceberBaixa = tituloReceberBaixa;
    }

    public Long getIdProjeto() {
        return idProjeto;
    }

    public void setIdProjeto(Long idProjeto) {
        this.idProjeto = idProjeto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public void setProjetos(List<Projeto> projetos) {
        this.projetos = projetos;
    }

    public Date getDataMovimentoEstornar() {
        return dataMovimentoEstornar;
    }

    public void setDataMovimentoEstornar(Date dataMovimentoEstornar) {
        this.dataMovimentoEstornar = dataMovimentoEstornar;
    }

}

package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.Empresa;
import com.mesure.repository.EmpresaRepository;
import com.mesure.repository.filter.EmpresaFilter;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaEmpresaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	EmpresaRepository empresaRepository;
	
	private EmpresaFilter filtro;
	private List<Empresa> empresasFiltradas;
	
	private Empresa empresaSelecionada;
	
	public PesquisaEmpresaBean() {
		filtro = new EmpresaFilter();
	}	
	
	public void excluir() {
		empresaRepository.remover(empresaSelecionada);
		empresasFiltradas.remove(empresaSelecionada);
		
		FacesUtil.addInfoMessage("Empresa '" + empresaSelecionada.getRazaoSocial() + "' excluída com sucesso.");
	}
	
	public void pesquisar() {
		empresasFiltradas = empresaRepository.empresas(filtro);
	}
		
	public EmpresaFilter getFiltro() {
		return filtro;
	}

	public List<Empresa> getEmpresasFiltradas() {
		return empresasFiltradas;
	}

	public Empresa getEmpresaSelecionada() {
		return empresaSelecionada;		
	}

	public void setEmpresaSelecionada(Empresa empresaSelecionada) {
		this.empresaSelecionada = empresaSelecionada;
	}	

}


package com.mesure.controller;

import com.mesure.enumerator.ETipoContaBancaria;
import com.mesure.model.ContaBancaria;
import com.mesure.model.Filial;
import com.mesure.repository.ContaBancariaRepository;
import com.mesure.repository.EmpresaRepository;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class ContaBancariaBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    ContaBancariaRepository contaBancariaRepository;

    @Inject
    private EmpresaRepository empresaRepository;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    private List<ContaBancaria> contasBancaria;
    private List<Filial> filiais;
    private ContaBancaria conta;

    public ContaBancariaBean() {
        limpar();
    }

    public void limpar() {
        this.conta = new ContaBancaria();
    }

    public void salvar() {
        conta.setFilial(usuarioLogado.getFilialAtiva());
        conta = contaBancariaRepository.guardar(conta);

        if (!contasBancaria.contains(conta)) {
            contasBancaria.add(conta);
        }

        limpar();
        FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
    }

    public void excluir() {
        if (conta != null) {
            contaBancariaRepository.remover(conta);
            contasBancaria.remove(conta);

            FacesUtil.addInfoMessageGrowl("Registro excluído com sucesso.");
        }
    }

    public void pesquisar() {
        contasBancaria = contaBancariaRepository.obterContasBancarias(usuarioLogado.getFilialAtiva());
        filiais = empresaRepository.outrasFiliais(usuarioLogado.getFilialAtiva());
    }

    // Para carregar as opções do Enum no formulário
    public ETipoContaBancaria[] getTiposContaBancaria() {
        return ETipoContaBancaria.values();
    }

    public String getFilialAtiva() {
        String filialAtiva;

        if (conta.getFilial() == null) {
            filialAtiva = usuarioLogado.getFilialAtiva().getNome();
        } else {
            filialAtiva = conta.getFilial().getNome();
        }
        return filialAtiva;
    }

    public List<ContaBancaria> getContasBancaria() {
        return contasBancaria;
    }

    public void setContasBancaria(List<ContaBancaria> contasBancaria) {
        this.contasBancaria = contasBancaria;
    }

    public List<Filial> getFiliais() {
        return filiais;
    }

    public void setFiliais(List<Filial> filiais) {
        this.filiais = filiais;
    }

    public ContaBancaria getConta() {
        return conta;
    }

    public void setConta(ContaBancaria conta) {
        this.conta = conta;
    }

}

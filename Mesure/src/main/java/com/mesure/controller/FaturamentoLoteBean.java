package com.mesure.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.EAcaoLoteFaturamento;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.EStatusLoteFaturamento;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.model.Cliente;
import com.mesure.model.FaturamentoLote;
import com.mesure.model.Projeto;
import com.mesure.repository.ClienteRepository;
import com.mesure.repository.FaturamentoLoteRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.filter.FaturamentoLoteFilter;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.FaturamentoLoteService;
import com.mesure.service.NegocioException;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class FaturamentoLoteBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	@UsuarioLogado
	private UsuarioSistema usuarioLogado;

	@Inject
	ProjetoRepository projetoRepository;
	
    @Inject
    ClienteRepository clienteRepository;	

	@Inject
	FaturamentoLoteService faturamentoLoteService;
	
	@Inject
	FaturamentoLoteRepository faturamentoLoteRepository;
	
	private FaturamentoLote lote;
	private List<FaturamentoLote> lotes;

	private Cliente cliente;
	private List<Cliente> clientes;
	private List<Projeto> projetos;
	private List<Projeto> projetosFiltro;
	
	private FaturamentoLoteFilter filtro;

	private boolean carregouRegistros;

	public FaturamentoLoteBean() {
		limparLote();
	}

	public void inicializar() {
		limparPesquisa();

		filtro = new FaturamentoLoteFilter();
		filtro.setCompetenciaDe(new Date());
		filtro.setCompetenciaAte(new Date());
		
        // Carrega todos os clientes cadastrados
        clientes = clienteRepository.clientes();
        
        // Carrega os projetos em aberto (para cadastrar novo lote via modal, filtrando pelo cliente selecionado)
        carregarProjetos();
		
        // Carrega os projetos em aberto (para o filtro de lotes da tela principal)
        ProjetoFilter projetoFilter = new ProjetoFilter();
        // Somente projetos em aberto
        EStatusProjeto[] statuses = {EStatusProjeto.COMECAR, EStatusProjeto.ANDAMENTO, EStatusProjeto.ATV_EXTERNA};
        projetoFilter.setStatuses(statuses);
        if (usuarioLogado.getFilialAtiva().getFiltrarProjetos().equals(ESimNao.SIM)) {
            projetoFilter.setFilial(usuarioLogado.getFilialAtiva());
        }
        projetoFilter.setPropriedadeOrdenacao("titulo");
        projetosFiltro = projetoRepository.filtrados(projetoFilter);		
	}

	public void limparPesquisa() {
		lotes = new ArrayList<>();
		setCarregouRegistros(false);
	}

	public void limparLote() {
		lote = new FaturamentoLote();
	}

	public void pesquisar() {
		// Faz a consulta na base de dados
		lotes = faturamentoLoteRepository.lotes(filtro);
		setCarregouRegistros(true);
	}
	
    public void carregarProjetos() {
        ProjetoFilter projetoFilter = new ProjetoFilter();
        // Somente projetos em aberto
        EStatusProjeto[] statuses = {EStatusProjeto.COMECAR, EStatusProjeto.ANDAMENTO, EStatusProjeto.ATV_EXTERNA};
        projetoFilter.setStatuses(statuses);
        if (usuarioLogado.getFilialAtiva().getFiltrarProjetos().equals(ESimNao.SIM)) {
            projetoFilter.setFilial(usuarioLogado.getFilialAtiva());
        }
        projetoFilter.setCliente(cliente);
        projetoFilter.setPropriedadeOrdenacao("titulo");
        projetos = projetoRepository.filtrados(projetoFilter);
    }	

	public void antesAbrirModalLote() {
		if (!isCarregouRegistros()) {
			throw new NegocioException("Clique primeiramente em Mostrar para carregar os registros.");
		}
		limparLote();
	}

	// Este método deve ser chamado ao abrir o cadastro de Lotes em modo de INCLUSÃO
	public void inicializarLote() {
		lote.setProjeto(filtro.getProjeto());
		lote.setDataCriacao(new Date());
		lote.setUsuario(usuarioLogado.getUsuario());
		lote.setStatus(EStatusLoteFaturamento.PENDENTE);
	}

	public void incluirLote() {
		if (lote.getDataInicio().getTime() > lote.getDataFim().getTime()) {
			throw new NegocioException("A data de término deve ser maior ou igual que a data de início");
		}

		// Grava o registro no banco de dados
		lote = faturamentoLoteRepository.guardarLoteTransactional(lote);

		if (!lotes.contains(lote)) {
			lotes.add(lote);
			limparLote();
		}
	}

	public void excluirLote() {
		if (lote != null) {
			validarExclusao();			
			faturamentoLoteRepository.remover(lote);

			lotes.remove(lote);
			FacesUtil.addInfoMessageGrowl("Registro excluído com sucesso.");
		}
	}

	public void validarExclusao() {
		if (!lote.getStatus().equals(EStatusLoteFaturamento.PENDENTE)) {
			throw new NegocioException("Somente lotes com status Pendente podem ser excluídos");
		}
	}

	public void processarLote() throws ParseException {
		executarAcaoLote(EAcaoLoteFaturamento.PROCESSAR);
	}

	public void fecharLote() throws ParseException {
		executarAcaoLote(EAcaoLoteFaturamento.FECHAR);
	}

	public void liberarLote() throws ParseException {
		executarAcaoLote(EAcaoLoteFaturamento.LIBERAR);
	}

	public void desfazerLiberacaoLote() throws ParseException {
		executarAcaoLote(EAcaoLoteFaturamento.DESFAZER_LIBERACAO);
	}

	public void reabrirLote() throws ParseException {
		executarAcaoLote(EAcaoLoteFaturamento.REABRIR);
	}
	
	public void desfazerLote() throws ParseException {
		executarAcaoLote(EAcaoLoteFaturamento.DESFAZER);
	}

	private void executarAcaoLote(EAcaoLoteFaturamento acao) throws ParseException {
		faturamentoLoteService.setLotes(lotes);
		faturamentoLoteService.executarAcaoLote(acao);
		
		// Pesquisa novamente para recarregar a grid
		pesquisar();

		FacesUtil.addInfoMessageGrowl("Operação realizada com sucesso.");
	}

	public void onChangeFiltro() {
		limparPesquisa();
	}
	
	public void onChangeCompetencia() {
		if (lote.getCompetencia() != null) {
			Calendar c = new GregorianCalendar();		
			c.setTime(lote.getCompetencia());
			
			if (lote.getDataInicio() == null) {
				// Primeiro dia do mês
				c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMinimum(Calendar.DAY_OF_MONTH));
				lote.setDataInicio(c.getTime());
			}
			if (lote.getDataFim() == null) {
				// Último dia do mês		
				c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMaximum(Calendar.DAY_OF_MONTH));
				lote.setDataFim(c.getTime());
			}
		}
	}
	
    public void onChangeCBCliente() {
    	lote.setProjeto(null);
        carregarProjetos();

        if (projetos.isEmpty()) {
            FacesUtil.addWarnMessage("O cliente informado não possui projetos cadastrados.");
        }
    }

	// Para carregar as opções do Enum no formulário
	public EStatusLoteFaturamento[] getStatuses() {
		return EStatusLoteFaturamento.values();
	}

	public FaturamentoLote getLote() {
		return lote;
	}

	public void setLote(FaturamentoLote lote) {
		this.lote = lote;
	}

	public List<FaturamentoLote> getLotes() {
		return lotes;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}	
	
	public List<Cliente> getClientes() {
		return clientes;
	}

	public List<Projeto> getProjetos() {
		return projetos;
	}
	
	public List<Projeto> getProjetosFiltro() {
		return projetosFiltro;
	}

	public FaturamentoLoteFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(FaturamentoLoteFilter filtro) {
		this.filtro = filtro;
	}

	public boolean isCarregouRegistros() {
		return carregouRegistros;
	}

	public void setCarregouRegistros(boolean carregouRegistros) {
		this.carregouRegistros = carregouRegistros;
	}

}

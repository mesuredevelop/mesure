package com.mesure.controller;

import com.mesure.enumerator.EAcaoLoteApuracaoHoras;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.EStatusLoteApuracaoHoras;
import com.mesure.model.ApuracaoHorasLote;
import com.mesure.model.Colaborador;
import com.mesure.repository.ApuracaoHorasLoteRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.filter.ApuracaoHorasLoteFilter;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.ApuracaoHorasLoteService;
import com.mesure.service.NegocioException;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class ApuracaoHorasLoteBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    @Inject
    ColaboradorRepository colaboradorRepository;

    @Inject
    ApuracaoHorasLoteService apuracaoHorasLoteService;

    @Inject
    ApuracaoHorasLoteRepository apuracaoHorasLoteRepository;

    private ApuracaoHorasLote lote;
    private List<ApuracaoHorasLote> lotes;

    private List<Colaborador> colaboradores;

    private ApuracaoHorasLoteFilter filtro;

    private boolean carregouRegistros;

    public ApuracaoHorasLoteBean() {
        limparLote();
    }

    public void inicializar() {
        limparPesquisa();

        filtro = new ApuracaoHorasLoteFilter();
        filtro.setCompetenciaDe(new Date());
        filtro.setCompetenciaAte(new Date());

        ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
        colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
        colaboradores = colaboradorRepository.colaboradores(colaboradorFilter);
    }

    public void limparPesquisa() {
        lotes = new ArrayList<>();
        setCarregouRegistros(false);
    }

    public void limparLote() {
        lote = new ApuracaoHorasLote();
    }

    public void pesquisar() {
        // Faz a consulta na base de dados
        lotes = apuracaoHorasLoteRepository.lotes(filtro);
        setCarregouRegistros(true);
    }

    public void antesAbrirModalLote() {
        if (!isCarregouRegistros()) {
            throw new NegocioException("Clique primeiramente em Mostrar para carregar os registros.");
        }
        limparLote();
    }

    // Este método deve ser chamado ao abrir o cadastro de Lotes em modo de INCLUSÃO
    public void inicializarLote() {
        lote.setColaborador(filtro.getColaborador());
        lote.setDataCriacao(new Date());
        lote.setUsuario(usuarioLogado.getUsuario());
        lote.setStatus(EStatusLoteApuracaoHoras.PENDENTE);
    }

    public void incluirLote() {
        if (lote.getDataInicio().getTime() > lote.getDataFim().getTime()) {
            throw new NegocioException("A data de término deve ser maior ou igual que a data de início");
        }

        if (lote.isTodosColaboradores()) {
            apuracaoHorasLoteService.incluirLoteTodosColaboradores(colaboradores, lotes, lote);
        } else {
            // Grava o registro no banco de dados
            lote = apuracaoHorasLoteRepository.guardarLoteTransactional(lote);

            if (!lotes.contains(lote)) {
                lotes.add(lote);
            }
        }
        limparLote();
    }

    public void marcarTodos() {
        if (!lotes.isEmpty()) {
            for (ApuracaoHorasLote a : lotes) {
                a.setFlag(true);
            }
        }
    }

    public void desmarcarTodos() {
        if (!lotes.isEmpty()) {
            for (ApuracaoHorasLote a : lotes) {
                a.setFlag(false);
            }
        }
    }

    public void excluirLote() {
        if (lote != null) {
            validarExclusao();
            apuracaoHorasLoteRepository.remover(lote);

            lotes.remove(lote);
            FacesUtil.addInfoMessageGrowl("Registro excluído com sucesso.");
        }
    }

    public void validarExclusao() {
        if (!lote.getStatus().equals(EStatusLoteApuracaoHoras.PENDENTE)) {
            throw new NegocioException("Somente lotes com status Pendente podem ser excluídos");
        }
    }

    public void processarLote() throws ParseException {
        executarAcaoLote(EAcaoLoteApuracaoHoras.PROCESSAR);
    }

    public void fecharLote() throws ParseException {
        executarAcaoLote(EAcaoLoteApuracaoHoras.FECHAR);
    }

    public void liberarLote() throws ParseException {
        executarAcaoLote(EAcaoLoteApuracaoHoras.LIBERAR);
    }

    public void desfazerLiberacaoLote() throws ParseException {
        executarAcaoLote(EAcaoLoteApuracaoHoras.DESFAZER_LIBERACAO);
    }

    public void reabrirLote() throws ParseException {
        executarAcaoLote(EAcaoLoteApuracaoHoras.REABRIR);
    }

    public void desfazerLote() throws ParseException {
        executarAcaoLote(EAcaoLoteApuracaoHoras.DESFAZER);
    }

    private void executarAcaoLote(EAcaoLoteApuracaoHoras acao) throws ParseException {
        apuracaoHorasLoteService.setLotes(lotes);
        apuracaoHorasLoteService.executarAcaoLote(acao);

        // Pesquisa novamente para recarregar a grid
        pesquisar();

        FacesUtil.addInfoMessageGrowl("Operação realizada com sucesso.");
    }

    public void onChangeFiltro() {
        limparPesquisa();
    }

    public void onChangeCompetencia() {
        if (lote.getCompetencia() != null) {
            Calendar c = new GregorianCalendar();
            c.setTime(lote.getCompetencia());

            if (lote.getDataInicio() == null) {
                // Primeiro dia do mês
                c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMinimum(Calendar.DAY_OF_MONTH));
                lote.setDataInicio(c.getTime());
            }
            if (lote.getDataFim() == null) {
                // Último dia do mês		
                c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lote.setDataFim(c.getTime());
            }
        }
    }

    // Para carregar as opções do Enum no formulário
    public EStatusLoteApuracaoHoras[] getStatuses() {
        return EStatusLoteApuracaoHoras.values();
    }

    public ApuracaoHorasLote getLote() {
        return lote;
    }

    public void setLote(ApuracaoHorasLote lote) {
        this.lote = lote;
    }

    public List<ApuracaoHorasLote> getLotes() {
        return lotes;
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }

    public ApuracaoHorasLoteFilter getFiltro() {
        return filtro;
    }

    public void setFiltro(ApuracaoHorasLoteFilter filtro) {
        this.filtro = filtro;
    }

    public boolean isCarregouRegistros() {
        return carregouRegistros;
    }

    public void setCarregouRegistros(boolean carregouRegistros) {
        this.carregouRegistros = carregouRegistros;
    }

}

package com.mesure.controller;

import com.mesure.enumerator.EHoraValor;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.model.Cliente;
import com.mesure.model.Colaborador;
import com.mesure.model.Filial;
import com.mesure.model.Projeto;
import com.mesure.repository.ClienteRepository;
import com.mesure.repository.ColaboradorPontoRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.EmpresaRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.EquipeService;
import com.mesure.util.jsf.FacesUtil;
import com.mesure.util.report.ExecutorRelatorio;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

@Named
@ViewScoped
public class RelatorioProjetosBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    ColaboradorRepository colaboradorRepository;

    @Inject
    ColaboradorPontoRepository colaboradorPontoRepository;

    @Inject
    ProjetoRepository projetoRepository;

    @Inject
    ClienteRepository clienteRepository;

    @Inject
    EmpresaRepository empresaRepository;

    @Inject
    EquipeService equipeService;

    @Inject
    Seguranca seguranca;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    @Inject
    FacesContext facesContext;

    @Inject
    HttpServletResponse respose;

    @Inject
    EntityManager manager;

    private List<Colaborador> colaboradores;
    private List<Projeto> projetos;
    private List<Cliente> clientes;
    private List<Filial> filiais;

    private Filial filtroFilial;
    private Colaborador filtroColaborador;
    private Projeto filtroProjeto;
    private Cliente filtroCliente;
    private EStatusProjeto filtroStatusProjeto;
    private EHoraValor tipoRelatorio;

    public void imprimirDadosProjeto() {
        if (tipoRelatorio != null) {
            switch (tipoRelatorio) {
                case HORAS:
                    imprimirDadosProjetoHoras();
                    break;
                case VALORES:
                    imprimirDadosProjetoValores();
                    break;
                case ETAPAS:
                    imprimirDadosProjetoEtapas();
                    break;
                default:
                    imprimirDadosProjetoCustos();
                    break;
            }
        }
    }

    public void imprimirDadosProjetoHoras() {
        Map<String, Object> parametros = new HashMap<>();

        if (this.filtroFilial != null && this.filtroFilial.getId() != null) {
            parametros.put("filial", this.filtroFilial.getId());
        }

        if (this.filtroColaborador != null && this.filtroColaborador.getId() != null) {
            parametros.put("colaborador", this.filtroColaborador.getId());
        }

        if (this.filtroProjeto != null && this.filtroProjeto.getId() != null) {
            parametros.put("projeto", this.filtroProjeto.getId());
        }

        if (this.filtroCliente != null && this.filtroCliente.getId() != null) {
            parametros.put("cliente", this.filtroCliente.getId());
        }

        if (this.filtroStatusProjeto != null && this.filtroStatusProjeto.toString() != null) {
            parametros.put("status", filtroStatusProjeto.toString());
        }

        ExecutorRelatorio executor = new ExecutorRelatorio("/relatorios/projetos/projetos001.jasper",
                this.respose, parametros, "Dados do Projeto (Horas).pdf");

        Session session = manager.unwrap(Session.class);
        session.doWork(executor);

        if (executor.isRelatorioGerado()) {
            facesContext.responseComplete();
        } else {
            FacesUtil.addErrorMessage("A execução do relatório não retornou dados.");
        }
    }

    public void imprimirDadosProjetoValores() {
        Map<String, Object> parametros = new HashMap<>();

        if (this.filtroFilial != null && this.filtroFilial.getId() != null) {
            parametros.put("filial", this.filtroFilial.getId());
        }

        if (this.filtroColaborador != null && this.filtroColaborador.getId() != null) {
            parametros.put("colaborador", this.filtroColaborador.getId());
        }

        if (this.filtroProjeto != null && this.filtroProjeto.getId() != null) {
            parametros.put("projeto", this.filtroProjeto.getId());
        }

        if (this.filtroCliente != null && this.filtroCliente.getId() != null) {
            parametros.put("cliente", this.filtroCliente.getId());
        }

        if (this.filtroStatusProjeto != null && this.filtroStatusProjeto.toString() != null) {
            parametros.put("status", filtroStatusProjeto.toString());
        }

        ExecutorRelatorio executor = new ExecutorRelatorio("/relatorios/projetos/projetos002.jasper",
                this.respose, parametros, "Dados do Projeto (Valores).pdf");

        Session session = manager.unwrap(Session.class);
        session.doWork(executor);

        if (executor.isRelatorioGerado()) {
            facesContext.responseComplete();
        } else {
            FacesUtil.addErrorMessage("A execução do relatório não retornou dados.");
        }
    }

    public void imprimirDadosProjetoEtapas() {
        Map<String, Object> parametros = new HashMap<>();

        if (this.filtroFilial != null && this.filtroFilial.getId() != null) {
            parametros.put("filial", this.filtroFilial.getId());
        }

        if (this.filtroColaborador != null && this.filtroColaborador.getId() != null) {
            parametros.put("colaborador", this.filtroColaborador.getId());
        }

        if (this.filtroProjeto != null && this.filtroProjeto.getId() != null) {
            parametros.put("projeto", this.filtroProjeto.getId());
        }

        if (this.filtroCliente != null && this.filtroCliente.getId() != null) {
            parametros.put("cliente", this.filtroCliente.getId());
        }

        if (this.filtroStatusProjeto != null && this.filtroStatusProjeto.toString() != null) {
            parametros.put("status", filtroStatusProjeto.toString());
        }

        parametros.put("local_subreport", "/relatorios/projetos/projetos003_sub.jasper");

        ExecutorRelatorio executor = new ExecutorRelatorio("/relatorios/projetos/projetos003.jasper",
                this.respose, parametros, "Dados do Projeto (Etapas).pdf");

        Session session = manager.unwrap(Session.class);
        session.doWork(executor);

        if (executor.isRelatorioGerado()) {
            facesContext.responseComplete();
        } else {
            FacesUtil.addErrorMessage("A execução do relatório não retornou dados.");
        }
    }

    public void imprimirDadosProjetoCustos() {
        Map<String, Object> parametros = new HashMap<>();

        if (this.filtroFilial != null && this.filtroFilial.getId() != null) {
            parametros.put("filial", this.filtroFilial.getId());
        }

        if (this.filtroColaborador != null && this.filtroColaborador.getId() != null) {
            parametros.put("colaborador", this.filtroColaborador.getId());
        }

        if (this.filtroProjeto != null && this.filtroProjeto.getId() != null) {
            parametros.put("projeto", this.filtroProjeto.getId());
        }

        if (this.filtroCliente != null && this.filtroCliente.getId() != null) {
            parametros.put("cliente", this.filtroCliente.getId());
        }

        if (this.filtroStatusProjeto != null && this.filtroStatusProjeto.toString() != null) {
            parametros.put("status", filtroStatusProjeto.toString());
        }

        ExecutorRelatorio executor = new ExecutorRelatorio("/relatorios/projetos/projetos004.jasper",
                this.respose, parametros, "Dados do Projeto (Custos).pdf");

        Session session = manager.unwrap(Session.class);
        session.doWork(executor);

        if (executor.isRelatorioGerado()) {
            facesContext.responseComplete();
        } else {
            FacesUtil.addErrorMessage("A execução do relatório não retornou dados.");
        }
    }

    public void inicializar() {
        filtroColaborador = usuarioLogado.getUsuario().getColaborador();

        // Carrega os colaboradores no qual o usuário é responsável além do próprio colaborador ativo
        colaboradores = equipeService.colaboradoresDoResponsavel(usuarioLogado.getUsuario().getColaborador(), ESimNao.SIM);

        // Carrega os projetos
        ProjetoFilter projetoFilter = new ProjetoFilter();
        // Somente projetos em aberto
        EStatusProjeto[] statuses = {EStatusProjeto.COMECAR, EStatusProjeto.ANDAMENTO, EStatusProjeto.ATV_EXTERNA};
        projetoFilter.setStatuses(statuses);
        if (usuarioLogado.getFilialAtiva().getFiltrarProjetos().equals(ESimNao.SIM)) {
            projetoFilter.setFilial(usuarioLogado.getFilialAtiva());
        }
        projetoFilter.setPropriedadeOrdenacao("titulo");
        projetos = projetoRepository.filtrados(projetoFilter);

        // Carrega todos os clientes cadastrados
        clientes = clienteRepository.clientes();

        // Carrega os registros para o combo de filiais
        filiais = empresaRepository.filiais();
    }

    public EStatusProjeto[] getStatusProjeto() {
        return EStatusProjeto.values();
    }

    public EHoraValor[] getHoraValor() {
        return EHoraValor.values();
    }

    public Colaborador getFiltroColaborador() {
        return filtroColaborador;
    }

    public void setFiltroColaborador(Colaborador filtroColaborador) {
        this.filtroColaborador = filtroColaborador;
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }

    public Projeto getFiltroProjeto() {
        return filtroProjeto;
    }

    public void setFiltroProjeto(Projeto filtroProjeto) {
        this.filtroProjeto = filtroProjeto;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public void setProjetos(List<Projeto> projetos) {
        this.projetos = projetos;
    }

    public Cliente getFiltroCliente() {
        return filtroCliente;
    }

    public void setFiltroCliente(Cliente filtroCliente) {
        this.filtroCliente = filtroCliente;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public void setColaboradores(List<Colaborador> colaboradores) {
        this.colaboradores = colaboradores;
    }

    public EStatusProjeto getFiltroStatusProjeto() {
        return filtroStatusProjeto;
    }

    public void setFiltroStatusProjeto(EStatusProjeto filtroStatusProjeto) {
        this.filtroStatusProjeto = filtroStatusProjeto;
    }

    public EHoraValor getTipoRelatorio() {
        return tipoRelatorio;
    }

    public void setTipoRelatorio(EHoraValor tipoRelatorio) {
        this.tipoRelatorio = tipoRelatorio;
    }

    public Filial getFiltroFilial() {
        return filtroFilial;
    }

    public void setFiltroFilial(Filial filtroFilial) {
        this.filtroFilial = filtroFilial;
    }

    public List<Filial> getFiliais() {
        return filiais;
    }

    public void setFiliais(List<Filial> filiais) {
        this.filiais = filiais;
    }

}

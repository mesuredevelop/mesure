package com.mesure.controller;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoAtividadePrevista;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.model.Cliente;
import com.mesure.model.ColaboradorPonto;
import com.mesure.model.ColaboradorPontoExtra;
import com.mesure.model.Projeto;
import com.mesure.model.ProjetoAtividadePrevista;
import com.mesure.model.ProjetoEtapa;
import com.mesure.model.ProjetoExecucao;
import com.mesure.model.TipoAtividadeExtra;
import com.mesure.repository.ClienteRepository;
import com.mesure.repository.ColaboradorPontoRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.ColaboradorPontoService;
import com.mesure.service.NegocioException;
import com.mesure.service.ProjetoService;
import com.mesure.util.Util;
import com.mesure.util.jpa.Transactional;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;

@Named
@ViewScoped
public class DetalhePontoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    ColaboradorPontoService colaboradorPontoService;

    @Inject
    ColaboradorPontoRepository colaboradorPontoRepository;

    @Inject
    ClienteRepository clienteRepository;

    @Inject
    ProjetoRepository projetoRepository;

    @Inject
    ProjetoService projetoService;

    @Inject
    private HttpServletRequest request;

    @Inject
    Seguranca seguranca;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    private List<ProjetoExecucao> execucoesProjetos;
    private List<ColaboradorPontoExtra> atividadesExtras;
    private List<TipoAtividadeExtra> tiposAtividadeExtra;
    private List<Cliente> clientes;
    private List<Projeto> projetos;
    private List<ProjetoAtividadePrevista> atividadesPrevistas;
    private List<ProjetoEtapa> etapasProjeto;

    private ColaboradorPonto colaboradorPonto;
    private ColaboradorPontoExtra colaboradorPontoExtra;
    private ProjetoExecucao projetoExecucao;
    private Cliente cliente;

    private boolean carregouRegistros;

    public DetalhePontoBean() {
        limparAtividadeExtra();
    }

    public void inicializar() {
        setCarregouRegistros(false);

        limparAtividadeExtra();
        limparExecucao();

        String param_ponto = request.getParameter("ponto");

        if (StringUtils.isBlank(param_ponto)) {
            colaboradorPonto = null;
        }

        if (!seguranca.isResponsavelEquipe() && colaboradorPonto != null) {
            if (!colaboradorPonto.getColaborador().equals(usuarioLogado.getUsuario().getColaborador())) {
                colaboradorPonto = null;
            }
        }

        if (colaboradorPonto == null) {
            throw new NegocioException("Não foi possível carregar as informações.");
        }

        // Carrega todas as execuções realizadas em projetos pelo colaborador na data indicada
        execucoesProjetos = colaboradorPontoService.execucoesRealizadasProjetos(colaboradorPonto);

        // Carrega todas as atividades extras apontadas pelo colaborador na data indicada
        atividadesExtras = colaboradorPontoService.atividadesExtras(colaboradorPonto);

        // Carrega os tipos de atividade extra existentes na base
        tiposAtividadeExtra = colaboradorPontoRepository.tiposAtividadeExtra();

        // Carrega todos os clientes cadastrados
        clientes = clienteRepository.clientes();

        // Carrega todos os projetos cadastrados (conforme filtros)
        carregarProjetos();

        setCarregouRegistros(true);
    }

    public void limparAtividadeExtra() {
        colaboradorPontoExtra = new ColaboradorPontoExtra();
    }

    // Este método deve ser chamado ao abrir o cadastro de Atividades Extras em modo de INCLUSÃO
    public void inicializarAtividadeExtra() {
        limparAtividadeExtra();
        colaboradorPontoExtra.setData(colaboradorPonto.getData());
        colaboradorPontoExtra.setColaborador(colaboradorPonto.getColaborador());
    }

    @Transactional
    public void incluirAtividadeExtra() throws ParseException {
        try {
            if (colaboradorPontoExtra.getHoraInicio().getTime() >= colaboradorPontoExtra.getHoraTermino().getTime()) {
                throw new NegocioException("Hora Término deve ser maior que Hora Início");
            }

            for (ColaboradorPontoExtra atvExtra : atividadesExtras) {
                if (!atvExtra.equals(colaboradorPontoExtra) && atvExtra.getColaborador().equals(colaboradorPontoExtra.getColaborador())) {
                    if (atvExtra.getData().getTime() == colaboradorPontoExtra.getData().getTime()) {

                        if (atvExtra.getHoraTermino() == null && colaboradorPontoExtra.getHoraTermino() == null) {
                            throw new NegocioException("Existe um conflito de horário com outra atividade extra (" + Util.formatarHora(atvExtra.getHoraInicio()) + " - 23:59)");
                        } else {
                            Date horaInicioAtu = colaboradorPontoExtra.getHoraInicio();
                            Date horaTerminoAtu = colaboradorPontoExtra.getHoraTermino();
                            Date horaInicioOld = atvExtra.getHoraInicio();
                            Date horaTerminoOld = atvExtra.getHoraTermino();

                            if (horaTerminoAtu == null) {
                                horaTerminoAtu = Util.montarHora("23:59");
                            }
                            if (horaTerminoOld == null) {
                                horaTerminoOld = Util.montarHora("23:59");
                            }

                            if (horaInicioOld.getTime() < horaTerminoAtu.getTime() && horaTerminoOld.getTime() > horaInicioAtu.getTime()) {
                                throw new NegocioException("Existe um conflito de horário com outra atividade extra (" + Util.formatarHora(horaInicioOld) + " - " + Util.formatarHora(horaTerminoOld) + ")");
                            }
                        }
                    }
                }
            }

            // Calcula o Total de Horas
            colaboradorPontoExtra.calcularTotalHora();

            // Grava o registro no banco de dados
            colaboradorPontoExtra = colaboradorPontoRepository.guardarAtividadeExtra(colaboradorPontoExtra);

            if (!atividadesExtras.contains(colaboradorPontoExtra)) {
                atividadesExtras.add(colaboradorPontoExtra);
            }
            limparAtividadeExtra();
        } catch (Exception e) {
            throw new NegocioException(StringUtils.join("Não foi possível registrar atividade extra. Motivo: ", e.getMessage()));
        }
    }

    public void excluirAtividadeExtra() {
        if (colaboradorPontoExtra != null) {
            colaboradorPontoRepository.removerAtividadeExtra(colaboradorPontoExtra);
            atividadesExtras.remove(colaboradorPontoExtra);

            FacesUtil.addInfoMessageGrowl("Registro excluído com sucesso.");
        }
    }

    public void limparExecucao() {
        projetoExecucao = new ProjetoExecucao();
        atividadesPrevistas = new ArrayList<>();
        etapasProjeto = new ArrayList<>();
    }

    // Este método deve ser chamado ao abrir o cadastro de Execuções em modo de INCLUSÃO
    public void inicializarExecucao() {
        limparExecucao();
        projetoExecucao.setData(colaboradorPonto.getData());
        projetoExecucao.setColaborador(colaboradorPonto.getColaborador());
        projetoExecucao.setRetrabalho(ESimNao.NAO);

        cliente = null;
    }

    @Transactional
    public void incluirExecucao() throws ParseException {
        try {
            // Faz as consistências necessárias
            projetoService.consistirExecucaoProjeto(projetoExecucao);

            // Calcula o Valor Hora, Total Horas e Valor Total
            projetoExecucao.calcularTotaisValorHora();

            // Grava o usuário e a data/hora que a execução foi incluída/alterada
            projetoExecucao.setUsuario(usuarioLogado.getUsuario());
            projetoExecucao.setDataAlteracao(new Date());

            // Grava o registro no banco de dados
            projetoExecucao = projetoRepository.guardarProjetoExecucao(projetoExecucao);

            // Calcula os campos 'Horas Executadas' e 'Percentual Executado' da etapa ligada à execução
            if (projetoExecucao.getEtapa() != null) {
                ProjetoEtapa projetoEtapa = projetoService.calcularHorasExecucaoEtapa(projetoExecucao.getEtapa());
                projetoRepository.guardarProjetoEtapa(projetoEtapa);
            }

            if (!execucoesProjetos.contains(projetoExecucao)) {
                execucoesProjetos.add(projetoExecucao);
            }
            limparExecucao();
        } catch (Exception e) {
            throw new NegocioException(StringUtils.join("Não foi possível registrar execução no projeto. Motivo: ", e.getMessage()));
        }
    }

    public void excluirExecucao() {
        if (projetoExecucao != null) {
            projetoRepository.removerProjetoExecucao(projetoExecucao);
            execucoesProjetos.remove(projetoExecucao);

            FacesUtil.addInfoMessageGrowl("Registro excluído com sucesso.");
        }
    }

    public void carregarProjetos() {
        ProjetoFilter projetoFilter = new ProjetoFilter();
        // Carrega os projetos no qual o usuário é participante ou responsável
        if (!seguranca.isResponsavelEquipe()) {
            projetoFilter.setColaborador(usuarioLogado.getUsuario().getColaborador());
        }
        // Somente projetos da filial ativa
        if (usuarioLogado.getFilialAtiva().getFiltrarProjetos().equals(ESimNao.SIM)) {
            projetoFilter.setFilial(usuarioLogado.getFilialAtiva());
        }
        // Somente projetos em aberto
        EStatusProjeto[] statuses = {EStatusProjeto.COMECAR, EStatusProjeto.ANDAMENTO, EStatusProjeto.ATV_EXTERNA};
        projetoFilter.setStatuses(statuses);
        projetoFilter.setCliente(cliente);
        projetoFilter.setPropriedadeOrdenacao("titulo");
        projetos = projetoRepository.filtrados(projetoFilter);

        carregarEtapasProjeto();
    }

    public void carregarAtividadesPrevistas() {
        atividadesPrevistas = new ArrayList<>();

        if (projetoExecucao.getProjeto() != null) {
            List<ProjetoAtividadePrevista> lista = projetoRepository.atividadesPrevistasDoProjeto(projetoExecucao.getProjeto());
            if (lista != null) {
                for (ProjetoAtividadePrevista atv : lista) {
                    // Se é a atividade já informada
                    if (projetoExecucao.getAtividadePrevista() != null && atv.equals(projetoExecucao.getAtividadePrevista())) {
                        atividadesPrevistas.add(atv);
                    } else {
                        if (atv.getColaborador().equals(colaboradorPonto.getColaborador()) && atv.getSituacao().equals(ESituacaoAtividadePrevista.PENDENTE)) {
                            atividadesPrevistas.add(atv);
                        }
                    }
                }
            }
        }
    }

    public void carregarEtapasProjeto() {
        etapasProjeto = new ArrayList<>();

        if (projetoExecucao.getProjeto() != null) {
            etapasProjeto = projetoRepository.etapasDoProjeto(projetoExecucao.getProjeto());
        }
    }

    // Para carregar as opções do Enum no formulário
    public ESimNao[] getOpcoesSimNao() {
        return ESimNao.values();
    }

    public void onChangeCBCliente() {
        projetoExecucao.setProjeto(null);
        projetoExecucao.setAtividadePrevista(null);
        projetoExecucao.setEtapa(null);

        // Carrega os projetos do cliente selecionado
        carregarProjetos();

        // Apenas para limpar os campos
        carregarAtividadesPrevistas();
        carregarEtapasProjeto();

        if (projetos.isEmpty()) {
            FacesUtil.addWarnMessage("O cliente informado não possui projetos cadastrados.");
        }
    }

    public void onChangeCBProjeto() {
        projetoExecucao.setAtividadePrevista(null);
        projetoExecucao.setEtapa(null);

        carregarAtividadesPrevistas();
        carregarEtapasProjeto();
    }

    public void onChangeCBAtividadeExecucao() {
        if (projetoExecucao.getAtividadePrevista() != null) {
            projetoExecucao.setEtapa(projetoExecucao.getAtividadePrevista().getEtapa());

            if (projetoExecucao.isEtapaRetrabalho()) {
                projetoExecucao.setRetrabalho(ESimNao.SIM);
            }
        }
    }

    public void onChangeCBEtapaExecucao() {
        if (projetoExecucao.isEtapaRetrabalho()) {
            projetoExecucao.setRetrabalho(ESimNao.SIM);
        }
    }

    public ColaboradorPonto getColaboradorPonto() {
        return colaboradorPonto;
    }

    public void setColaboradorPonto(ColaboradorPonto colaboradorPonto) {
        this.colaboradorPonto = colaboradorPonto;
    }

    public ColaboradorPontoExtra getColaboradorPontoExtra() {
        return colaboradorPontoExtra;
    }

    public void setColaboradorPontoExtra(ColaboradorPontoExtra colaboradorPontoExtra) {
        this.colaboradorPontoExtra = colaboradorPontoExtra;
    }

    public ProjetoExecucao getProjetoExecucao() {
        return projetoExecucao;
    }

    public void setProjetoExecucao(ProjetoExecucao projetoExecucao) {
        this.projetoExecucao = projetoExecucao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<ProjetoExecucao> getExecucoesProjetos() {
        return execucoesProjetos;
    }

    public List<ColaboradorPontoExtra> getAtividadesExtras() {
        return atividadesExtras;
    }

    public List<TipoAtividadeExtra> getTiposAtividadeExtra() {
        return tiposAtividadeExtra;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public List<ProjetoAtividadePrevista> getAtividadesPrevistas() {
        return atividadesPrevistas;
    }

    public List<ProjetoEtapa> getEtapasProjeto() {
        return etapasProjeto;
    }

    public boolean isCarregouRegistros() {
        return carregouRegistros;
    }

    public void setCarregouRegistros(boolean carregouRegistros) {
        this.carregouRegistros = carregouRegistros;
    }

}

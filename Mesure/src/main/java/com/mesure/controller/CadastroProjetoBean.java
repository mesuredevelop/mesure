package com.mesure.controller;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoAtividadePrevista;
import com.mesure.enumerator.ESituacaoEtapa;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.enumerator.ETipoEndereco;
import com.mesure.model.Atividade;
import com.mesure.model.Cliente;
import com.mesure.model.Colaborador;
import com.mesure.model.Empresa;
import com.mesure.model.EnderecoCliente;
import com.mesure.model.Etapa;
import com.mesure.model.Filial;
import com.mesure.model.ProgramacaoExecucao;
import com.mesure.model.Projeto;
import com.mesure.model.ProjetoAtividadePrevista;
import com.mesure.model.ProjetoColaborador;
import com.mesure.model.ProjetoEtapa;
import com.mesure.model.ProjetoExecucao;
import com.mesure.model.ProjetoHistorico;
import com.mesure.model.ProjetoProposta;
import com.mesure.model.ProjetoProrrogacao;
import com.mesure.model.TipoAtividadePrevista;
import com.mesure.model.TipoProjeto;
import com.mesure.repository.AtividadeRepository;
import com.mesure.repository.ClienteRepository;
import com.mesure.repository.ColaboradorPontoRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.EmpresaRepository;
import com.mesure.repository.ProgramacaoExecucaoRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.TipoAtividadePrevistaRepository;
import com.mesure.repository.TipoProjetoRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.repository.filter.EmpresaFilter;
import com.mesure.repository.filter.ProgramacaoExecucaoFilter;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.NegocioException;
import com.mesure.service.ProjetoService;
import com.mesure.util.Util;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;

@Named
@ViewScoped
public class CadastroProjetoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private ProjetoService projetoService;

    @Inject
    private ProjetoRepository projetoRepository;

    @Inject
    private ProgramacaoExecucaoRepository programacaoExecucaoRepository;

    @Inject
    EmpresaRepository empresaRepository;

    @Inject
    ClienteRepository clienteRepository;

    @Inject
    ColaboradorRepository colaboradorRepository;

    @Inject
    AtividadeRepository atividadeRepository;

    @Inject
    TipoAtividadePrevistaRepository atividadePrevistaRepository;

    @Inject
    TipoProjetoRepository tipoProjetoRepository;

    @Inject
    ColaboradorPontoRepository colaboradorPontoRepository;

    @Inject
    private HttpServletRequest request;

    @Inject
    Seguranca seguranca;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    private Projeto projeto;

    private Empresa empresa;

    private List<Empresa> empresas;
    private List<Filial> filiais;

    private List<Cliente> clientes;
    private List<Colaborador> colaboradores;
    private List<Colaborador> participantes;
    private List<TipoProjeto> tipos;
    private List<EnderecoCliente> enderecosObra;
    private List<Atividade> atividades;
    private List<TipoAtividadePrevista> tiposAtividadePrevistas;

    private ProjetoProposta projetoProposta;
    private ProjetoHistorico projetoHistorico;
    private ProjetoColaborador projetoColaborador;
    private ProjetoEtapa projetoEtapa;
    private ProjetoExecucao projetoExecucao;
    private ProjetoAtividadePrevista atividadePrevista;
    private ProjetoProrrogacao projetoProrrogacao;
    private Atividade atividade;

    private Colaborador participante;
    private Colaborador colaboradorAtivo;

    // Valor de cobrança por participante do projeto
    private BigDecimal valorCobranca;
    private BigDecimal valorCobrancaNoturna;

    // Totalizadores do projeto
    private BigDecimal totalHorasProjeto;
    private BigDecimal totalHorasProgramadas;
    private BigDecimal totalHorasExecutadas;
    private BigDecimal saldoHorasRestantes;
    private Integer saldoDiasRestantes;
    private BigDecimal perConclusaoProjeto;

    public CadastroProjetoBean() {
        limpar();
    }

    public void inicializar() {
        // Busca e armazena o colaborador vinculado ao usuário ativo
        colaboradorAtivo = usuarioLogado.getColaborador();

        if (colaboradorAtivo == null) {
            projeto = null;
            FacesUtil.addErrorMessage("O usuário ativo não está vinculado a um colaborador.");
        }

        String param_projeto = request.getParameter("projeto");

        if (StringUtils.isBlank(param_projeto)) {
            projeto = null;
        }

        // Se o usuário não tem permissão só poderá visualizar os projetos em que é responsável ou participante
        if (!seguranca.visualizaProjetos() && projeto != null) {
            boolean permite = false;
            try {
                if (colaboradorAtivo != null) {
                    if (colaboradorAtivo.equals(projeto.getResponsavel())) {
                        permite = true;
                    } else {
                        for (ProjetoColaborador prt : projeto.getParticipantes()) {
                            if (colaboradorAtivo.equals(prt.getColaborador()) && prt.getEncerrado() == ESimNao.NAO) {
                                permite = true;
                                break;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                projeto = null;
                FacesUtil.addErrorMessage("Ocorreu um erro ao carregar as informações");
            }
            if (!permite) {
                projeto = null;
            }
        }

        // Verifica se a filial do projeto é a filial ativa
        if (projeto != null && projeto.isEditando()) {
            if (!projeto.getFilial().equals(usuarioLogado.getFilialAtiva())) {
                projeto = null;
            }
        }

        if (projeto == null) {
            limpar();
        }

        // carrega todas as empresas cadastradas
        empresas = empresaRepository.empresas(new EmpresaFilter());

        // Carrega todos os clientes cadastrados
        clientes = clienteRepository.clientes();

        // Carrega todos os colaboradores cadastrados e ativos
        ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
        colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
        colaboradores = colaboradorRepository.colaboradores(colaboradorFilter);

        // Carrega todos os colaboradores cadastrados e ativos para seleção os participantes
        carregarParticipantesDisponiveis();

        // Carrega todas as atividades cadastradas
        atividades = atividadeRepository.atividades();

        // Carrega todos os tipos de atividades previstas cadastrados
        tiposAtividadePrevistas = atividadePrevistaRepository.tipos();

        // Carrega todos os tipos de projeto cadastrados
        tipos = tipoProjetoRepository.tipos();

        // Carrega os endereços do cliente relacionados a obra
        carregarEnderecosObra();

        // Se é um novo projeto, ou seja, modo de inclusão
        if (projeto != null && !projeto.isEditando()) {
            projeto.setStatus(EStatusProjeto.COMECAR);
            empresa = usuarioLogado.getFilialAtiva().getEmpresa();
            projeto.setFilial(usuarioLogado.getFilialAtiva());
        }

        // Carrega as filiais da empresa para apresentar no formulário
        if (empresa != null) {
            carregarFiliais();
        }

        // Calcula os totalizadores do projeto
        calcularTotalizadores();
    }

    private void limpar() {
        projeto = new Projeto();

        // Necessário para não dar erro ao abrir a tela em modo de inclusão
        projeto.setEnderecoObra(new EnderecoCliente());

        empresa = null;
        filiais = null;

        limparProposta();
        limparHistorico();
        limparParticipante();
        limparEtapa();
        limparAtividadePrevista();
        limparExecucao();
        limparProrrogacao();
    }

    public void salvar() {
        if (colaboradorAtivo == null) {
            throw new NegocioException("O usuário ativo não está vinculado a um colaborador.");
        }

        sugerirEtapasProjeto();
        calcularHorasExecucaoEtapas();

        projeto = projetoService.salvar(projeto);

        // Calcula os totalizadores do projeto
        calcularTotalizadores();

        FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
    }

    public boolean isHabilitaBotaoGravar() {
        return projeto.getStatus() != EStatusProjeto.CANCELADO
                && projeto.getStatus() != EStatusProjeto.FINALIZADO;
    }

    public boolean isParticipanteProjeto() {
        return isParticipanteProjeto(colaboradorAtivo);
    }

    public boolean isParticipanteProjeto(Colaborador colaborador) {
        return projeto.isParticipanteProjeto(colaborador);
    }

    public boolean isAdmOuResponsavel() {
        return seguranca.visualizaProjetos() || usuarioLogado.getColaborador().equals(projeto.getResponsavel());
    }

    public boolean isPodeAlterarSituacaoEtapa() {
        if (projeto != null && projeto.isNaoIniciado()) {
            return false;
        }
        if (!isAdmOuResponsavel()) {
            if (projetoEtapa != null && projetoEtapa.getSituacao().equals(ESituacaoEtapa.RETRABALHO)) {
                return false;
            }
        }
        return true;
    }

    public boolean isPodeAvancarEstagio() {
        return projeto.isEditando() && !projeto.getStatus().equals(EStatusProjeto.FINALIZADO);
    }

    public boolean isPodeRetrocederEstagio() {
        return projeto.isEditando() && !projeto.getStatus().equals(EStatusProjeto.CANCELADO);
    }

    public boolean isPodeEnviarAtividadeExterna() {
        return projeto.isEditando() && projeto.getStatus().equals(EStatusProjeto.ANDAMENTO);
    }

    public boolean isTemDataPrazo() {
        for (ProjetoProposta prop : projeto.getPropostas()) {
            if (prop.getDataAprovacao() != null && prop.getDataPrazoFim() != null) {
                return true;
            }
        }
        return (projeto.getProrrogacoes().size() > 0);
    }

    public void avancarEstagio() {
        projeto = projetoService.avancarEstagio(projeto);
        calcularTotalizadores();
        FacesUtil.addInfoMessageGrowl("O status do projeto foi alterado para '" + projeto.getStatus().getLabel() + "'");
    }

    public void retrocederEstagio() {
        projeto = projetoService.retrocederEstagio(projeto);
        calcularTotalizadores();
        FacesUtil.addInfoMessageGrowl("O status do projeto foi alterado para '" + projeto.getStatus().getLabel() + "'");
    }

    public void enviarAtividadeExterna() {
        projeto = projetoService.enviarAtividadeExterna(projeto);
        calcularTotalizadores();
        FacesUtil.addInfoMessageGrowl("O status do projeto foi alterado para '" + projeto.getStatus().getLabel() + "'");
    }

    private void carregarEnderecosObra() {
        if (enderecosObra == null) {
            enderecosObra = new ArrayList<>();
        }

        enderecosObra.clear();

        if (projeto.getCliente() != null) {
            enderecosObra = clienteRepository.enderecos(projeto.getCliente(), ETipoEndereco.OBRA);
        }
    }

    public void carregarFiliais() {
        filiais = null;

        if (empresa != null) {
            filiais = empresaRepository.filialDe(empresa);
        }
    }

    // Busca todas as etapas das atividades informadas e vincula no projeto
    private void sugerirEtapasProjeto() {

        // Editando o projeto
        if (projeto.isEditando()) {
            List<ProjetoEtapa> listaEtapas = new ArrayList<>();

            // Exclui as etapas que pertencem às atividades removidas do projeto
            if (projeto.getEtapas().size() > 0) {
                for (ProjetoEtapa etp : projeto.getEtapas()) {
                    boolean excluirEtapa = true;

                    if (etp.getSituacao() == ESituacaoEtapa.FINALIZADO || etp.getEtapa() == null) {
                        excluirEtapa = false;
                    } else {
                        // Percorre todas as atividades do projeto
                        for (Atividade atividadeTela : projeto.getAtividades()) {
                            if (!excluirEtapa) {
                                break;
                            }
                            Atividade atvEtapas = atividadeRepository.etapasDaAtividade(atividadeTela);
                            if (atvEtapas != null && atvEtapas.getEtapas() != null) {
                                for (Etapa e : atvEtapas.getEtapas()) {
                                    if (e.equals(etp.getEtapa())) {
                                        excluirEtapa = false;
                                        break;
                                    }
                                }
                            }
                        }

                        if (excluirEtapa) {
                            // Verifica se a etapa está vinculada a uma execução do projeto
                            for (ProjetoExecucao exe : projeto.getExecucoes()) {
                                if (exe.getEtapa() != null && exe.getEtapa().equals(etp)) {
                                    excluirEtapa = false;
                                    break;
                                }
                            }
                            // Verifica se a etapa está vinculada a uma atividade prevista do projeto
                            for (ProjetoAtividadePrevista atv : projeto.getAtividadesPrevistas()) {
                                if (atv.getEtapa() != null && atv.getEtapa().equals(etp)) {
                                    excluirEtapa = false;
                                    break;
                                }
                            }

                            if (excluirEtapa) {
                                listaEtapas.add(etp);
                            }
                        }
                    }
                }
            }

            // Remove as etapas do projeto
            for (ProjetoEtapa e : listaEtapas) {
                projeto.getEtapas().remove(e);
            }

            // Adiciona as etapas que pertencem às atividades incluídas no projeto
            listaEtapas = new ArrayList<>();

            Projeto projetoAux = projetoRepository.atividadesDoProjeto(projeto);

            // Percorre todas as atividades do projeto
            for (Atividade atividadeTela : projeto.getAtividades()) {
                boolean novaAtividade = true;
                if (projetoAux != null && !projetoAux.getAtividades().isEmpty()) {
                    for (Atividade atividadeBanco : projetoAux.getAtividades()) {
                        // Verifica se a atividade já estava vinculada no projeto
                        if (atividadeTela.equals(atividadeBanco)) {
                            novaAtividade = false;
                            break;
                        }
                    }
                }

                if (novaAtividade) {
                    Atividade atvEtapas = atividadeRepository.etapasDaAtividade(atividadeTela);
                    if (atvEtapas != null && atvEtapas.getEtapas() != null) {
                        for (Etapa e : atvEtapas.getEtapas()) {
                            boolean incluirEtapa = true;
                            // Verifica se a etapa já consta no projeto
                            for (ProjetoEtapa etp : projeto.getEtapas()) {
                                if (etp.getEtapa() != null && etp.getEtapa().equals(e)) {
                                    incluirEtapa = false;
                                    break;
                                }
                            }

                            if (incluirEtapa) {
                                listaEtapas.add(new ProjetoEtapa(projeto, e, e.getDescricao(), e.getQuantidadeHoras(), BigDecimal.ZERO, BigDecimal.ZERO, ESituacaoEtapa.PENDENTE));
                            }
                        }
                    }
                }
            }

            // Adiciona as etapas no projeto
            for (ProjetoEtapa e : listaEtapas) {
                projeto.getEtapas().add(e);
            }

            // Novo projeto
        } else {
            // Percorre todas as atividades do projeto
            for (Atividade atividadeTela : projeto.getAtividades()) {
                Atividade atvEtapas = atividadeRepository.etapasDaAtividade(atividadeTela);
                if (atvEtapas != null && atvEtapas.getEtapas() != null) {
                    // Adiciona as etapas no projeto
                    for (Etapa e : atvEtapas.getEtapas()) {
                        projeto.getEtapas().add(new ProjetoEtapa(projeto, e, e.getDescricao(), e.getQuantidadeHoras(), BigDecimal.ZERO, BigDecimal.ZERO, ESituacaoEtapa.PENDENTE));
                    }
                }
            }
        }
    }

    // Método que calcula os campos 'Horas Executadas' e 'Percentual Executado' das etapas do projeto
    public void calcularHorasExecucaoEtapas() {
        if (projeto.isEditando()) {

            BigDecimal horasExecutadas;
            // Percorre todas as etapas do projeto
            for (ProjetoEtapa etp : projeto.getEtapas()) {

                // Horas Executadas
                horasExecutadas = BigDecimal.ZERO;
                // Percorre todas as execuções do projeto
                for (ProjetoExecucao exe : projeto.getExecucoes()) {
                    // Se a etapa da execução é a etapa em questão
                    if (exe.getEtapa() != null && exe.getEtapa().equals(etp) && exe.getTotalHoras() != null) {
                        horasExecutadas = horasExecutadas.add(exe.getTotalHoras());
                    }
                }
                etp.setHorasExecutadas(horasExecutadas);

                // Percentual Executado
                float percentualExecutado = 0;
                // Verifica se a etapa possui previsão de horas
                if (etp.getQuantidadeHoras() != null && etp.getQuantidadeHoras().floatValue() > 0) {
                    // Verifica se a etapa possui horas executadas
                    if (etp.getHorasExecutadas() != null && etp.getHorasExecutadas().floatValue() > 0) {
                        // Fórmula: (100 / qtde horas da etapa) * qtde horas executadas
                        percentualExecutado = 100 / etp.getQuantidadeHoras().floatValue();
                        percentualExecutado = percentualExecutado * etp.getHorasExecutadas().floatValue();
                    }
                }
                etp.setPercentualExecutado(new BigDecimal(percentualExecutado));
            }
        }
    }

    public void carregarParticipantesDisponiveis() {
        // Carrega todos os colaboradores cadastrados e ativos
        ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
        colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
        participantes = colaboradorRepository.colaboradores(colaboradorFilter);

        // Remove os colaboradores já adicionados como participantes do projeto
        if (projeto != null && projeto.isEditando()) {
            for (ProjetoColaborador pc : projeto.getParticipantes()) {
                participantes.remove(pc.getColaborador());
            }
        }
    }

    public void onChangeCliente() {
        projeto.setEnderecoObra(null);

        carregarEnderecosObra();

        if (enderecosObra.isEmpty()) {
            FacesUtil.addWarnMessage("O cliente informado não possui um endereço do tipo OBRA cadastrado.");
        }
    }

    public void onChangeObra() {
        // Método criado apenas para ser chamado no formulário e atualizar as informações da guia Obra
    }

    public void onChangeAtividadeExecucao() {
        if (projetoExecucao.getAtividadePrevista() != null) {
            projetoExecucao.setData(projetoExecucao.getAtividadePrevista().getData());
            projetoExecucao.setEtapa(projetoExecucao.getAtividadePrevista().getEtapa());

            if (projetoExecucao.isEtapaRetrabalho()) {
                projetoExecucao.setRetrabalho(ESimNao.SIM);
            }
        }
    }

    public void onChangeEtapaExecucao() {
        if (projetoExecucao.isEtapaRetrabalho()) {
            projetoExecucao.setRetrabalho(ESimNao.SIM);
        }
    }

    public void onChangeEmpresa() {
        carregarFiliais();
    }

    public void incluirProposta() {
        if (projetoProposta.getDataInicio().getTime() > projetoProposta.getDataPrazoFim().getTime()) {
            throw new NegocioException("Data prazo fim deve ser maior ou igual que Data início");
        }

        if (projetoProposta.getDataEncerramento() != null) {
            if (projetoProposta.getDataAprovacao() == null) {
                throw new NegocioException("Para informar a data de encerramento é necessário que haja data de aprovação");
            }
            if (projetoProposta.getDataEncerramento().getTime() < projetoProposta.getDataAprovacao().getTime()) {
                throw new NegocioException("A data de encerramento deve ser maior ou igual que a data de aprovação");
            }
        }

        if (!projeto.getPropostas().contains(projetoProposta)) {
            projetoProposta.setProjeto(projeto);
            projeto.getPropostas().add(projetoProposta);
            limparProposta();
        }
    }

    public void limparProposta() {
        projetoProposta = new ProjetoProposta();
    }

    public void excluirProposta() {
        if (projetoProposta != null) {
            projeto.getPropostas().remove(projetoProposta);
        }
    }

    // Este método deve ser chamado ao abrir o cadastro de Historicos em modo de INCLUSÃO
    public void inicializarHistorico() {
        projetoHistorico.setData(new Date());
        projetoHistorico.setColaborador(colaboradorAtivo);
    }

    public void incluirHistorico() {
        if (!projeto.getHistoricos().contains(projetoHistorico)) {
            projetoHistorico.setProjeto(projeto);
            projeto.getHistoricos().add(projetoHistorico);
            limparHistorico();
        }
    }

    public void limparHistorico() {
        projetoHistorico = new ProjetoHistorico();
    }

    public void excluirHistorico() {
        if (projetoHistorico != null) {
            projeto.getHistoricos().remove(projetoHistorico);
        }
    }

    public void incluirAtividade() {
        if (atividade == null) {
            FacesUtil.addWarnMessage("Selecione uma atividade para adicionar");
        } else if (projeto.getAtividades().contains(atividade)) {
            FacesUtil.addWarnMessage("Atividade já vinculada ao projeto");
        } else {
            projeto.getAtividades().add(atividade);
            limparAtividade();
        }
    }

    public void limparAtividade() {
        atividade = new Atividade();
    }

    public void excluirAtividade() {
        if (atividade != null) {
            projeto.getAtividades().remove(atividade);
        }
    }

    public void incluirParticipante() {
        if (participante == null) {
            FacesUtil.addWarnMessage("Selecione um colaborador para adicionar");
        } else {

            boolean adicionado = false;
            for (ProjetoColaborador c : projeto.getParticipantes()) {
                if (c.getColaborador().equals(participante)) {
                    adicionado = true;
                    FacesUtil.addWarnMessage("Colaborador já vinculado ao projeto");
                    break;
                }
            }

            if (!adicionado) {
                projeto.getParticipantes().add(new ProjetoColaborador(ESimNao.NAO, null, participante, projeto));
                participantes.remove(participante);
                limparParticipante();
            }
        }
    }

    public void incluirTodosParticipantes() {
        for (Colaborador c : participantes) {
            boolean adicionado = false;
            for (ProjetoColaborador p : projeto.getParticipantes()) {
                if (p.getColaborador().equals(c)) {
                    adicionado = true;
                    break;
                }
            }
            if (!adicionado) {
                projeto.getParticipantes().add(new ProjetoColaborador(ESimNao.NAO, null, c, projeto));
            }
        }
        participantes.clear();
        limparParticipante();
    }

    public void limparParticipante() {
        participante = new Colaborador();
    }

    public void excluirParticipante() {
        if (projetoColaborador != null) {
            projeto.getParticipantes().remove(projetoColaborador);
            participantes.add(projetoColaborador.getColaborador());
        }
    }

    public void informarValorCobranca() {
        if ((valorCobranca != null && valorCobranca.floatValue() > 0)
                || (valorCobrancaNoturna != null && valorCobrancaNoturna.floatValue() > 0)) {
            for (ProjetoColaborador c : projeto.getParticipantes()) {
                // Valor cobrança
                if (valorCobranca != null && valorCobranca.floatValue() > 0) {
                    if (c.getValorCobranca() == null || c.getValorCobranca().floatValue() == 0) {
                        c.setValorCobranca(valorCobranca);
                    }
                }
                // Valor cobrança noturna
                if (valorCobrancaNoturna != null && valorCobrancaNoturna.floatValue() > 0) {
                    if (c.getValorCobrancaNoturna() == null || c.getValorCobrancaNoturna().floatValue() == 0) {
                        c.setValorCobrancaNoturna(valorCobrancaNoturna);
                    }
                }
            }
        }
        limparValorCobranca();
    }

    public void limparValorCobranca() {
        valorCobranca = null;
        valorCobrancaNoturna = null;
    }

    // Este método deve ser chamado ao abrir o cadastro de Execuções em modo de INCLUSÃO
    public void inicializarExecucao() {
        if (!projeto.isParticipanteProjeto(colaboradorAtivo)) {
            throw new NegocioException("Somente participantes do projeto podem incluir uma execução");
        }

        projetoExecucao.setData(new Date());
        projetoExecucao.setColaborador(colaboradorAtivo);
        projetoExecucao.setRetrabalho(ESimNao.NAO);
    }

    public void incluirExecucao() throws ParseException {

        projetoExecucao.setProjeto(projeto);

        // Faz as consistências necessárias
        projetoService.consistirExecucaoProjeto(projetoExecucao);

        // Calcula o Valor Hora, Total Horas e Valor Total
        projetoExecucao.calcularTotaisValorHora();

        // Grava o usuário e a data/hora que a execução foi incluída/alterada
        projetoExecucao.setUsuario(usuarioLogado.getUsuario());
        projetoExecucao.setDataAlteracao(new Date());

        // Caso os campos "Atividade Prevista" e "Hora Término" estejam preenchidos, altera a situação da atividade prevista para "Finalizado".
        if (projetoExecucao.getAtividadePrevista() != null && projetoExecucao.getHoraTermino() != null) {
            for (ProjetoAtividadePrevista atv : projeto.getAtividadesPrevistas()) {
                if (atv.equals(projetoExecucao.getAtividadePrevista())) {
                    atv.setSituacao(ESituacaoAtividadePrevista.FINALIZADO);
                    break;
                }
            }
        }

        if (!projeto.getExecucoes().contains(projetoExecucao)) {
            projeto.getExecucoes().add(projetoExecucao);
        }
        limparExecucao();
    }

    public void limparExecucao() {
        projetoExecucao = new ProjetoExecucao();
    }

    public void excluirExecucao() {
        if (projetoExecucao != null) {
            projeto.getExecucoes().remove(projetoExecucao);
        }
    }

    // Este método deve ser chamado ao abrir o cadastro de Atividades Previstas em modo de INCLUSÃO
    public void inicializarAtividadePrevista() {
        atividadePrevista.setData(new Date());
        atividadePrevista.setPausa(ESimNao.NAO);
        atividadePrevista.setSituacao(ESituacaoAtividadePrevista.PENDENTE);
        atividadePrevista.setColaborador(colaboradorAtivo);
    }

    public void incluirAtividadePrevista() throws ParseException {
        if (!projeto.getAtividadesPrevistas().contains(atividadePrevista)) {
            atividadePrevista.setProjeto(projeto);
            projeto.getAtividadesPrevistas().add(atividadePrevista);
            limparAtividadePrevista();
        }
    }

    public void limparAtividadePrevista() {
        atividadePrevista = new ProjetoAtividadePrevista();
    }

    public void excluirAtividadePrevista() {
        if (atividadePrevista != null) {

            // Verifica se a atividade prevista foi gerada através de uma programação
            if (atividadePrevista.getProgramacaoExecucao() != null) {
                throw new NegocioException("Essa atividade prevista não pode ser excluída pois foi gerada através de uma programação");
            }

            // Verifica se a atividade prevista está vinculada a uma execução do projeto
            for (ProjetoExecucao exec : projeto.getExecucoes()) {
                if (exec.getAtividadePrevista() != null && exec.getAtividadePrevista().equals(atividadePrevista)) {
                    throw new NegocioException("Essa atividade prevista não pode ser excluída pois já está sendo referenciada em uma execução do projeto");
                }
            }

            projeto.getAtividadesPrevistas().remove(atividadePrevista);
        }
    }

    public void incluirEtapa() {
        // Somente se o usuário for administrador ou responsável pelo projeto poderá alterar a situação de RETRABALHO para outra situação ou vice-versa 
        if (!isAdmOuResponsavel() && projetoEtapa.isEditando()) {

            // Busca a etapa no banco de dados
            ProjetoEtapa etapaOld = projetoRepository.porIdProjetoEtapa(projetoEtapa.getId());

            if (etapaOld != null) {
                if (etapaOld.getSituacao().equals(ESituacaoEtapa.RETRABALHO) && !projetoEtapa.getSituacao().equals(ESituacaoEtapa.RETRABALHO)) {
                    projetoEtapa.setSituacao(etapaOld.getSituacao());
                    throw new NegocioException("Usuário sem permissão para alterar a situação da etapa de Retrabalho para outra situação");
                }
                if (!etapaOld.getSituacao().equals(ESituacaoEtapa.RETRABALHO) && projetoEtapa.getSituacao().equals(ESituacaoEtapa.RETRABALHO)) {
                    projetoEtapa.setSituacao(etapaOld.getSituacao());
                    throw new NegocioException("Usuário sem permissão para alterar a situação da etapa para Retrabalho");
                }
            }
        }

        if (!projeto.getEtapas().contains(projetoEtapa)) {
            projetoEtapa.setProjeto(projeto);
            projeto.getEtapas().add(projetoEtapa);
        }
        limparEtapa();
    }

    public void limparEtapa() {
        projetoEtapa = new ProjetoEtapa();
        projetoEtapa.setSituacao(ESituacaoEtapa.PENDENTE);
    }

    public void excluirEtapa() {
        if (projetoEtapa != null) {

            // Verifica se a etapa está vinculada a uma execução do projeto
            for (ProjetoExecucao exec : projeto.getExecucoes()) {
                if (exec.getEtapa() != null && exec.getEtapa().equals(projetoEtapa)) {
                    throw new NegocioException("A etapa '" + projetoEtapa.getDescricao() + "' não pode ser excluída pois já está sendo referenciada em uma execução do projeto");
                }
            }

            // Verifica se a etapa está vinculada a uma atividade prevista do projeto
            for (ProjetoAtividadePrevista ativ : projeto.getAtividadesPrevistas()) {
                if (ativ.getEtapa() != null && ativ.getEtapa().equals(projetoEtapa)) {
                    throw new NegocioException("A etapa '" + projetoEtapa.getDescricao() + "' não pode ser excluída pois já está sendo referenciada em uma atividade prevista do projeto");
                }
            }

            projeto.getEtapas().remove(projetoEtapa);
        }
    }

    // Este método deve ser chamado ao abrir o cadastro de Prorrogações em modo de INCLUSÃO
    public void inicializarProrrogacao() {
        projetoProrrogacao.setData(new Date());
        projetoProrrogacao.setColaborador(colaboradorAtivo);

        if (isTemDataPrazo()) {
            Date prazoAtual = new Date(0);
            Date dataAux = new Date(0);

            // Verifica qual é a proposta vigente (maior data de aprovação) e seta como sendo o prazo atual
            for (ProjetoProposta prop : projeto.getPropostas()) {
                if (prop.getDataAprovacao() != null && prop.getDataAprovacao().getTime() > dataAux.getTime()) {
                    dataAux.setTime(prop.getDataAprovacao().getTime());
                    prazoAtual.setTime(prop.getDataPrazoFim().getTime());
                }
            }

            // Verifica se existe prorrogação com data superior à proposta vigente e seta como sendo o prazo atual
            for (ProjetoProrrogacao prog : projeto.getProrrogacoes()) {
                if (prog.getData().getTime() > dataAux.getTime()) {
                    dataAux.setTime(prog.getData().getTime());
                    prazoAtual.setTime(prog.getNovoPrazo().getTime());
                }
            }

            projetoProrrogacao.setPrazoAtual(prazoAtual);
        }
    }

    public void incluirProrrogacao() {
        if (projetoProrrogacao.getPrazoAtual() != null && projetoProrrogacao.getNovoPrazo().getTime() == projetoProrrogacao.getPrazoAtual().getTime()) {
            throw new NegocioException("Novo prazo deve ser diferente do Prazo atual");
        }

        if (!projeto.getProrrogacoes().contains(projetoProrrogacao)) {
            projetoProrrogacao.setProjeto(projeto);
            projeto.getProrrogacoes().add(projetoProrrogacao);
            limparProrrogacao();
        }
    }

    public void limparProrrogacao() {
        projetoProrrogacao = new ProjetoProrrogacao();
    }

    public void excluirProrrogacao() {
        if (projetoProrrogacao != null) {
            projeto.getProrrogacoes().remove(projetoProrrogacao);
        }
    }

    public boolean isEditaCadastroProjeto() {
        return seguranca.editaCadastroProjetos();
    }

    public boolean isNaoIniciado() {
        return (projeto.getStatus() == EStatusProjeto.COMECAR) && (isEditaCadastroProjeto());
    }

    // Somente deverá apresentar as atividades previstas para o colaborador ativo e com situação "Pendente",
    // além da atividade já informada no próprio registro de execução
    public List<ProjetoAtividadePrevista> getAtividadesPrevistasColaborador() {
        List<ProjetoAtividadePrevista> retorno = new ArrayList<>();

        if (projeto.getAtividadesPrevistas() != null) {
            for (ProjetoAtividadePrevista atv : projeto.getAtividadesPrevistas()) {
                if (projetoExecucao != null && projetoExecucao.getAtividadePrevista() != null && atv.equals(projetoExecucao.getAtividadePrevista())) {
                    retorno.add(atv);
                } else {
                    if (atv.getColaborador().equals(colaboradorAtivo) && atv.getSituacao().equals(ESituacaoAtividadePrevista.PENDENTE)) {
                        retorno.add(atv);
                    }
                }
            }
        }
        return retorno;
    }

    public void calcularTotalizadores() {

        setTotalHorasProjeto(BigDecimal.ZERO);
        setTotalHorasProgramadas(BigDecimal.ZERO);
        setTotalHorasExecutadas(BigDecimal.ZERO);
        setSaldoHorasRestantes(BigDecimal.ZERO);
        setSaldoDiasRestantes(0);
        setPerConclusaoProjeto(BigDecimal.ZERO);

        if (projeto.isEditando()) {

            // Total horas projeto
            for (ProjetoProposta p : projeto.getPropostas()) {
                if (p.getDataAprovacao() != null && p.getTotalHoras() != null) {
                    totalHorasProjeto = totalHorasProjeto.add(p.getTotalHoras());
                }
            }

            // Total horas programadas
            ProgramacaoExecucaoFilter filtro = new ProgramacaoExecucaoFilter();
            filtro.setProjeto(projeto);
            List<ProgramacaoExecucao> programacoes = programacaoExecucaoRepository.programacoes(filtro);

            for (ProgramacaoExecucao p : programacoes) {
                double total = Util.DiferencaEmHoras(p.getHoraInicio(), p.getHoraTermino());
                totalHorasProgramadas = totalHorasProgramadas.add(new BigDecimal(total));
            }

            // Total horas executadas
            for (ProjetoExecucao p : projeto.getExecucoes()) {
                if (p.getTotalHoras() != null) {
                    totalHorasExecutadas = totalHorasExecutadas.add(p.getTotalHoras());
                }
            }

            // Saldo horas restantes
            BigDecimal saldo = getTotalHorasProjeto().subtract(getTotalHorasExecutadas());
            if (saldo.floatValue() < 0) {
                saldo = BigDecimal.ZERO;
            }
            setSaldoHorasRestantes(saldo);

            // Saldo dias restantes
            if (!projeto.getStatus().equals(EStatusProjeto.FINALIZADO)) {
                Date dataAtual = new Date();
                for (ProjetoProposta p : projeto.getPropostas()) {
                    if (p.getDataAprovacao() != null && p.getTotalDias() != null) {
                        Integer saldoDias = (int) (p.getTotalDias() - Util.DiferencaEmDias(p.getDataInicio(), dataAtual));
                        saldoDiasRestantes += saldoDias;
                    }
                }
            }

            // Conclusão do projeto "%"
            float totalEtapas = 0;
            float etapasPendentes = 0;
            float etapasFinalizadas = 0;

            for (ProjetoEtapa p : projeto.getEtapas()) {
                if (p.getSituacao().equals(ESituacaoEtapa.PENDENTE) || p.getSituacao().equals(ESituacaoEtapa.EXECUCAO)) {
                    etapasPendentes = etapasPendentes + 1;
                }

                if (p.getSituacao().equals(ESituacaoEtapa.FINALIZADO)) {
                    etapasFinalizadas = etapasFinalizadas + 1;
                }

                if (!p.getSituacao().equals(ESituacaoEtapa.CANCELADO)) {
                    totalEtapas = totalEtapas + 1;
                }
            }

            float percentual = 0;
            if (totalEtapas != 0) {
                if (etapasPendentes == 0) {
                    percentual = 100;
                } else {
                    if (etapasPendentes == totalEtapas) {
                        percentual = 0;
                    } else {
                        percentual = (etapasFinalizadas / totalEtapas) * 100;
                    }
                }
            }
            setPerConclusaoProjeto(new BigDecimal(percentual));
        }
    }

    // Para carregar as opções do Enum no formulário
    public EStatusProjeto[] getStatuses() {
        return EStatusProjeto.values();
    }

    // Para carregar as opções do Enum no formulário
    public ESituacaoEtapa[] getSituacoesEtapa() {
        return ESituacaoEtapa.values();
    }

    // Para carregar as opções do Enum no formulário
    public ESituacaoAtividadePrevista[] getSituacoesAtividadePrevista() {
        return ESituacaoAtividadePrevista.values();
    }

    // Para carregar as opções do Enum no formulário
    public ESimNao[] getOpcoesSimNao() {
        return ESimNao.values();
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;

        if (this.projeto != null) {
            this.empresa = this.projeto.getFilial().getEmpresa();
        }
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public List<Filial> getFiliais() {
        return filiais;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }

    public List<Colaborador> getParticipantes() {
        return participantes;
    }

    public List<TipoProjeto> getTipos() {
        return tipos;
    }

    public List<EnderecoCliente> getEnderecosObra() {
        return enderecosObra;
    }

    public List<Atividade> getAtividades() {
        return atividades;
    }

    public List<TipoAtividadePrevista> getTiposAtividadePrevistas() {
        return tiposAtividadePrevistas;
    }

    public ProjetoProposta getProjetoProposta() {
        return projetoProposta;
    }

    public void setProjetoProposta(ProjetoProposta projetoProposta) {
        this.projetoProposta = projetoProposta;
    }

    public ProjetoHistorico getProjetoHistorico() {
        return projetoHistorico;
    }

    public void setProjetoHistorico(ProjetoHistorico projetoHistorico) {
        this.projetoHistorico = projetoHistorico;
    }

    public ProjetoColaborador getProjetoColaborador() {
        return projetoColaborador;
    }

    public void setProjetoColaborador(ProjetoColaborador projetoColaborador) {
        this.projetoColaborador = projetoColaborador;
    }

    public ProjetoEtapa getProjetoEtapa() {
        return projetoEtapa;
    }

    public void setProjetoEtapa(ProjetoEtapa projetoEtapa) {
        this.projetoEtapa = projetoEtapa;
    }

    public ProjetoExecucao getProjetoExecucao() {
        return projetoExecucao;
    }

    public void setProjetoExecucao(ProjetoExecucao projetoExecucao) {
        this.projetoExecucao = projetoExecucao;
    }

    public ProjetoAtividadePrevista getAtividadePrevista() {
        return atividadePrevista;
    }

    public void setAtividadePrevista(ProjetoAtividadePrevista atividadePrevista) {
        this.atividadePrevista = atividadePrevista;
    }

    public ProjetoProrrogacao getProjetoProrrogacao() {
        return projetoProrrogacao;
    }

    public void setProjetoProrrogacao(ProjetoProrrogacao projetoProrrogacao) {
        this.projetoProrrogacao = projetoProrrogacao;
    }

    public Atividade getAtividade() {
        return atividade;
    }

    public void setAtividade(Atividade atividade) {
        this.atividade = atividade;
    }

    public Colaborador getParticipante() {
        return participante;
    }

    public void setParticipante(Colaborador participante) {
        this.participante = participante;
    }

    public BigDecimal getValorCobranca() {
        return valorCobranca;
    }

    public void setValorCobranca(BigDecimal valorCobranca) {
        this.valorCobranca = valorCobranca;
    }

    public BigDecimal getValorCobrancaNoturna() {
        return valorCobrancaNoturna;
    }

    public void setValorCobrancaNoturna(BigDecimal valorCobrancaNoturna) {
        this.valorCobrancaNoturna = valorCobrancaNoturna;
    }

    public BigDecimal getTotalHorasProjeto() {
        return totalHorasProjeto;
    }

    public void setTotalHorasProjeto(BigDecimal totalHorasProjeto) {
        this.totalHorasProjeto = totalHorasProjeto;
    }

    public BigDecimal getTotalHorasProgramadas() {
        return totalHorasProgramadas;
    }

    public void setTotalHorasProgramadas(BigDecimal totalHorasProgramadas) {
        this.totalHorasProgramadas = totalHorasProgramadas;
    }

    public BigDecimal getTotalHorasExecutadas() {
        return totalHorasExecutadas;
    }

    public void setTotalHorasExecutadas(BigDecimal totalHorasExecutadas) {
        this.totalHorasExecutadas = totalHorasExecutadas;
    }

    public BigDecimal getSaldoHorasRestantes() {
        return saldoHorasRestantes;
    }

    public void setSaldoHorasRestantes(BigDecimal saldoHorasRestantes) {
        this.saldoHorasRestantes = saldoHorasRestantes;
    }

    public Integer getSaldoDiasRestantes() {
        return saldoDiasRestantes;
    }

    public void setSaldoDiasRestantes(Integer saldoDiasRestantes) {
        this.saldoDiasRestantes = saldoDiasRestantes;
    }

    public BigDecimal getPerConclusaoProjeto() {
        return perConclusaoProjeto;
    }

    public void setPerConclusaoProjeto(BigDecimal perConclusaoProjeto) {
        this.perConclusaoProjeto = perConclusaoProjeto;
    }

}

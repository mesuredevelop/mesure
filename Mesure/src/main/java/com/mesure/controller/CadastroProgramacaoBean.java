package com.mesure.controller;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.model.Cliente;
import com.mesure.model.Colaborador;
import com.mesure.model.Empresa;
import com.mesure.model.Equipe;
import com.mesure.model.Filial;
import com.mesure.model.ProgramacaoExecucao;
import com.mesure.model.ProgramacaoExecucaoColaborador;
import com.mesure.model.Projeto;
import com.mesure.model.ProjetoEtapa;
import com.mesure.model.TipoAtividadePrevista;
import com.mesure.model.Veiculo;
import com.mesure.model.VeiculoMovimento;
import com.mesure.repository.ClienteRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.EmpresaRepository;
import com.mesure.repository.EquipeRepository;
import com.mesure.repository.ProgramacaoExecucaoRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.TipoAtividadePrevistaRepository;
import com.mesure.repository.VeiculoRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.repository.filter.EmpresaFilter;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.repository.filter.VeiculoFilter;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.EquipeService;
import com.mesure.service.NegocioException;
import com.mesure.service.ProgramacaoExecucaoService;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;

@Named
@ViewScoped
public class CadastroProgramacaoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    ProgramacaoExecucaoService programacaoExecucaoService;

    @Inject
    ProgramacaoExecucaoRepository programacaoExecucaoRepository;

    @Inject
    private ProjetoRepository projetoRepository;

    @Inject
    EmpresaRepository empresaRepository;

    @Inject
    ClienteRepository clienteRepository;

    @Inject
    ColaboradorRepository colaboradorRepository;

    @Inject
    EquipeRepository equipeRepository;

    @Inject
    EquipeService equipeService;

    @Inject
    VeiculoRepository veiculoRepository;

    @Inject
    TipoAtividadePrevistaRepository atividadePrevistaRepository;

    @Inject
    private HttpServletRequest request;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    private Empresa empresa;

    private List<Empresa> empresas;
    private List<Filial> filiais;
    private List<Cliente> clientes;

    private List<Colaborador> colaboradores;
    private List<Veiculo> veiculos;
    private List<Projeto> projetos;
    private List<ProjetoEtapa> etapasProjeto;
    private List<TipoAtividadePrevista> atividadesPrevistas;
    private List<Equipe> equipes;

    private ProgramacaoExecucao programacao;
    private Colaborador participante;
    private Equipe equipe;
    private ProgramacaoExecucaoColaborador programacaoColaborador;
    private ProjetoEtapa etapa;
    private Cliente cliente;

    private Long idProjeto;

    public CadastroProgramacaoBean() {
        limpar();
    }

    public void inicializar() {
        String param_programacao = request.getParameter("programacao");
        idProjeto = new Long(0);

        if (StringUtils.isBlank(param_programacao)) {
            programacao = null;
        }

        // Verifica se a filial do programacao é a filial ativa
        if (programacao != null && programacao.isEditando()) {
            if (!programacao.getFilial().equals(usuarioLogado.getFilialAtiva())) {
                programacao = null;
            }
        }

        if (programacao == null) {
            limpar();
        }

        // Carrega todos os clientes cadastrados
        clientes = clienteRepository.clientes();

        // Carrega o cliente vinculado ao projeto, para que já fique selecionado na tela
        cliente = null;
        if (programacao.isEditando()) {
            cliente = programacao.getProjeto().getCliente();
        }

        // Carrega os projetos em aberto
        carregarProjetos();

        // Carrega os tipos de atividade prevista
        atividadesPrevistas = atividadePrevistaRepository.tipos();

        // carrega todas as empresas cadastradas
        empresas = empresaRepository.empresas(new EmpresaFilter());

        // Carrega os colaboradores para selecionar o participante e outros participantes da programação
        ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
        colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
        colaboradores = colaboradorRepository.colaboradores(colaboradorFilter);

        // Carrega os registros para o filtro de veículo
        veiculos = veiculoRepository.filtrados(new VeiculoFilter());

        // Carrega as equipes para inclusão de outros participantes na programação
        equipes = equipeRepository.equipes();

        // Se é uma nova programação, ou seja, modo de inclusão
        if (programacao != null) {
            if (programacao.isNovo()) {
                empresa = usuarioLogado.getFilialAtiva().getEmpresa();
                programacao.setFilial(usuarioLogado.getFilialAtiva());

                programacao.setData(new Date());
                programacao.setColaborador(usuarioLogado.getColaborador());
            } else {
                empresa = programacao.getFilial().getEmpresa();
            }
        }

        // Carrega as filiais da empresa para apresentar no formulário
        if (empresa != null) {
            carregarFiliais();
        }
    }

    private void limpar() {
        programacao = new ProgramacaoExecucao();
        programacao.setVeiculoProprio(ESimNao.NAO);

        idProjeto = new Long(0);

        empresa = null;
        filiais = null;

        limparParticipante();
        limparEquipe();
        limparEtapa();
    }

    public void salvar() throws ParseException {
        consistirDadosProgramacao();

        programacao = programacaoExecucaoService.salvar(programacao);

        limpar();
        FacesUtil.addInfoMessageGrowl("Programação gerada com sucesso.");
    }

    public void consistirDadosProgramacao() {

        // Verifica o horário de saída e retorno
        if (programacao.getHoraTermino() != null && programacao.getHoraInicio().getTime() >= programacao.getHoraTermino().getTime()) {
            throw new NegocioException("Horário de término deve ser maior que o horário de início");
        }

        // Verifica o horário de saída e retorno da empresa
        if (programacao.getRetornoEmpresa() != null && programacao.getSaidaEmpresa().getTime() >= programacao.getRetornoEmpresa().getTime()) {
            throw new NegocioException("Horário de retorno da empresa deve ser maior que o horário de saída");
        }

        // Quando for motorista deve informar o veículo
        if (programacao.getMotorista().equals(ESimNao.SIM) && programacao.getVeiculo() == null) {
            throw new NegocioException("Quando for motorista deve selecionar o veículo");
        }

        // Quando informar o veículo deve informar os campos Saída Empresa e Retorno Empresa
        if (programacao.getVeiculo() != null) {
            if (programacao.getSaidaEmpresa() == null || programacao.getRetornoEmpresa() == null) {
                throw new NegocioException("Quando um veículo é informado os campos de saída e retorno da empresa devem ser preenchidos");
            }
        }

        // Verifica a data de término da programação
        if (programacao.isNovo() && programacao.getDataTermino() != null) {
            if (programacao.getDataTermino().getTime() < programacao.getData().getTime()) {
                throw new NegocioException("A data de término da programação deve ser maior que a data de início");
            }
        }

        // Verifica a quantidade máxima de passageiros do veículo
        if (programacao.getVeiculo() != null && programacao.getVeiculo().getMaximoPassageiros() != null) {
            if (programacao.getVeiculo().getMaximoPassageiros() < programacao.getParticipantes().size() + 1) {
                throw new NegocioException("Quantidade máxima de passageiros do veículo foi excedida. Verifique a quantidade de participantes da programação.");
            }
        }

    }

    // Retorna se já foi gerado um movimento para o veículo da programação de execução
    public boolean isTemMovimentacaoVeiculo() {
        if (programacao != null && programacao.isEditando()) {
            VeiculoMovimento movimento = programacaoExecucaoRepository.movimentacaoVeiculoDaProgramacaoExecucao(programacao);
            if (movimento != null) {
                return true;
            }
        }
        return false;
    }

    public void carregarProjetos() {
        ProjetoFilter projetoFilter = new ProjetoFilter();
        // Somente projetos em aberto
        EStatusProjeto[] statuses = {EStatusProjeto.COMECAR, EStatusProjeto.ANDAMENTO, EStatusProjeto.ATV_EXTERNA};
        projetoFilter.setStatuses(statuses);
        if (usuarioLogado.getFilialAtiva().getFiltrarProjetos().equals(ESimNao.SIM)) {
            projetoFilter.setFilial(usuarioLogado.getFilialAtiva());
        }
        projetoFilter.setCliente(cliente);
        projetoFilter.setPropriedadeOrdenacao("titulo");
        projetos = projetoRepository.filtrados(projetoFilter);

        if (programacao != null) {
            if (programacao.getProjeto() != null && programacao.getProjeto().getId() != null) {
                idProjeto = programacao.getProjeto().getId();
            } else {
                idProjeto = new Long(0);
            }
            carregarEtapasProjeto();
        }
    }

    public void carregarEtapasProjeto() {
        etapasProjeto = new ArrayList<>();

        if (programacao.getProjeto() != null) {
            etapasProjeto = projetoRepository.etapasDoProjeto(programacao.getProjeto());
        }
    }

    public void incluirParticipante() {
        if (participante == null) {
            FacesUtil.addWarnMessage("Selecione um participante para adicionar");
        } else {

            boolean adicionado = false;
            for (ProgramacaoExecucaoColaborador c : programacao.getParticipantes()) {
                if (c.getColaborador().equals(participante)) {
                    adicionado = true;
                    FacesUtil.addWarnMessage("Participante já vinculado nesta programação");
                    break;
                }
            }

            if (!adicionado) {
                programacao.getParticipantes().add(new ProgramacaoExecucaoColaborador(programacao, participante, programacao.getHorasViagem()));
                limparParticipante();
            }
        }
    }

    public void incluirEquipe() {
        if (equipe == null) {
            FacesUtil.addWarnMessage("Selecione uma equipe para adicionar");
        } else {
            // Busca os colaboradores que fazem parte da equipe selecionada
            List<Colaborador> participantesEquipe = equipeService.colaboradoresDaEquipe(equipe, ESimNao.NAO);

            for (Colaborador p : participantesEquipe) {
                if (!programacao.getParticipantes().contains(p)) {
                    programacao.getParticipantes().add(new ProgramacaoExecucaoColaborador(programacao, p, programacao.getHorasViagem()));
                }
            }
            limparEquipe();
        }
    }

    public void incluirEtapa() {
        if (etapa == null) {
            throw new NegocioException("Selecione uma etapa para adicionar");
        } else if (programacao.getEtapas().contains(etapa)) {
            throw new NegocioException("Etapa já vinculada nesta programação");
        } else {
            programacao.getEtapas().add(etapa);
            limparEtapa();
        }
    }

    public void onChangeCBEmpresa() {
        carregarFiliais();
    }

    public void onChangeCBCliente() {
        programacao.setProjeto(null);
        carregarProjetos();

        if (projetos.isEmpty()) {
            FacesUtil.addWarnMessage("O cliente informado não possui projetos cadastrados.");
        }
    }

    public void onChangeCBProjeto() {
        if (programacao.getProjeto() != null) {
            idProjeto = programacao.getProjeto().getId();
        } else {
            idProjeto = new Long(0);
        }
        carregarEtapasProjeto();
    }

    public void onChangeIdProjeto() {
        consultarProjetoId();
        carregarEtapasProjeto();
    }

    public void onChangeSaidaEmpresa() {
        if (programacao.isNovo() && programacao.getSaidaEmpresa() != null && programacao.getHoraInicio() == null) {
            programacao.setHoraInicio(programacao.getSaidaEmpresa());
        }
    }

    public void onChangeRetornoEmpresa() {
        if (programacao.isNovo() && programacao.getRetornoEmpresa() != null && programacao.getHoraTermino() == null) {
            programacao.setHoraTermino(programacao.getRetornoEmpresa());
        }
    }

    public void onChangeCBVeiculoProprio() {
        if (programacao.getVeiculoProprio().equals(ESimNao.SIM)) {
            programacao.setVeiculo(null);
            programacao.setMotorista(ESimNao.NAO);
        }
    }

    public boolean isPodeAlterarVeiculo() {
        return !(isTemMovimentacaoVeiculo() || programacao.getVeiculoProprio().equals(ESimNao.SIM));
    }

    public boolean isPodeAlterarMotorista() {
        return !(isTemMovimentacaoVeiculo() || programacao.getVeiculoProprio().equals(ESimNao.SIM));
    }

    public boolean isPodeAlterarVeiculoProprio() {
        return programacao.isNovo();
    }

    public void consultarProjetoId() {
        if (idProjeto != null && idProjeto != 0) {
            programacao.setProjeto(projetoRepository.porId(idProjeto));

            if (programacao.getProjeto() == null || (!projetos.contains(programacao.getProjeto()))) {
                idProjeto = new Long(0);
                programacao.setProjeto(null);
            }
        } else {
            idProjeto = new Long(0);
            programacao.setProjeto(null);
        }
    }

    public void carregarFiliais() {
        filiais = null;

        if (empresa != null) {
            filiais = empresaRepository.filialDe(empresa);
        }
    }

    public void limparParticipante() {
        participante = new Colaborador();
    }

    public void limparEquipe() {
        equipe = new Equipe();
    }

    public void limparEtapa() {
        this.etapa = new ProjetoEtapa();
    }

    public void excluirParticipante() {
        if (programacaoColaborador != null) {
            programacao.getParticipantes().remove(programacaoColaborador);
        }
    }

    public void excluirEtapa() {
        if (etapa != null) {
            programacao.getEtapas().remove(etapa);
        }
    }

    // Para carregar as opções do Enum no formulário
    public ESimNao[] getOpcoesSimNao() {
        return ESimNao.values();
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public List<Filial> getFiliais() {
        return filiais;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }

    public Colaborador getParticipante() {
        return participante;
    }

    public void setParticipante(Colaborador participante) {
        this.participante = participante;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public ProgramacaoExecucaoColaborador getProgramacaoColaborador() {
        return programacaoColaborador;
    }

    public void setProgramacaoColaborador(ProgramacaoExecucaoColaborador programacaoColaborador) {
        this.programacaoColaborador = programacaoColaborador;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Veiculo> getVeiculos() {
        return veiculos;
    }

    public void setVeiculos(List<Veiculo> veiculos) {
        this.veiculos = veiculos;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public void setProjetos(List<Projeto> projetos) {
        this.projetos = projetos;
    }

    public List<Equipe> getEquipes() {
        return equipes;
    }

    public void setEquipes(List<Equipe> equipes) {
        this.equipes = equipes;
    }

    public List<ProjetoEtapa> getEtapasProjeto() {
        return etapasProjeto;
    }

    public void setEtapasProjeto(List<ProjetoEtapa> etapasProjeto) {
        this.etapasProjeto = etapasProjeto;
    }

    public List<TipoAtividadePrevista> getAtividadesPrevistas() {
        return atividadesPrevistas;
    }

    public void setAtividadesPrevistas(List<TipoAtividadePrevista> atividadesPrevistas) {
        this.atividadesPrevistas = atividadesPrevistas;
    }

    public ProgramacaoExecucao getProgramacao() {
        return programacao;
    }

    public void setProgramacao(ProgramacaoExecucao programacao) {
        this.programacao = programacao;
    }

    public ProjetoEtapa getEtapa() {
        return etapa;
    }

    public void setEtapa(ProjetoEtapa etapa) {
        this.etapa = etapa;
    }

    public Long getIdProjeto() {
        return idProjeto;
    }

    public void setIdProjeto(Long idProjeto) {
        this.idProjeto = idProjeto;
    }

}

package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.DispositivoSeguranca;
import com.mesure.repository.DispositivoSegurancaRepository;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroDispositivoSegurancaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	DispositivoSegurancaRepository tipoRepository;
	
	private List<DispositivoSeguranca> tiposFiltrados;	
	private DispositivoSeguranca tipoSelecionado;
	
	public  CadastroDispositivoSegurancaBean() {
		limpar();
	}
	
	public void salvar() {
		tipoSelecionado = tipoRepository.guardar(tipoSelecionado);
		limpar();
	}	
	
	public void excluir() {
		tipoRepository.remover(tipoSelecionado);
		tiposFiltrados.remove(tipoSelecionado);
		
		FacesUtil.addInfoMessage("Registro '" + tipoSelecionado.getDescricao() + "' excluído com sucesso.");
	}
	
	public void pesquisar() {
		tiposFiltrados = tipoRepository.dispositivos();
	}

	public List<DispositivoSeguranca> getTiposFiltrados() {
		return tiposFiltrados;
	}	
	
	public DispositivoSeguranca getTipoSelecionado() {
		return tipoSelecionado;
	}
	
	public void setTipoSelecionado(DispositivoSeguranca tipoSelecionado) {
		this.tipoSelecionado = tipoSelecionado;
	}	
	
	public void limpar() {
		this.tipoSelecionado = new DispositivoSeguranca();
	}

}


package com.mesure.controller;

import com.mesure.enumerator.EFormaAlerta;
import com.mesure.enumerator.EPeriodicidadeAlerta;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ETipoAlerta;
import com.mesure.model.Alerta;
import com.mesure.model.AlertaEmail;
import com.mesure.model.Colaborador;
import com.mesure.model.Configuracao;
import com.mesure.repository.AlertaRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.ConfiguracaoRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.AlertaService;
import com.mesure.service.NegocioException;
import com.mesure.util.jsf.FacesUtil;
import com.mesure.util.mail.Mailer;
import com.outjected.email.api.MailMessage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.tools.generic.NumberTool;
import org.hibernate.validator.constraints.Email;

@Named
@ViewScoped
public class ConfiguracaoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private ConfiguracaoRepository configuracaoRepository;

    @Inject
    AlertaRepository alertaRepository;

    @Inject
    AlertaService alertaService;

    @Inject
    private ColaboradorRepository colaboradorRepository;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    @Inject
    private Mailer mailer;

    private Configuracao configuracao;
    private Alerta alerta;
    private AlertaEmail alertaEmail;

    private List<Alerta> alertas;
    private List<Colaborador> colaboradores;

    private String mailPassword;
    private String mailTestUsername;

    private Colaborador colaborador;

    private Date dataAtual;

    public void inicializar() {
        // Busca as configurações cadastradas para a filial ativa
        configuracao = configuracaoRepository.obterConfiguracao(usuarioLogado.getFilialAtiva());

        // Se ainda não possui configuração para a filial ativa então cria um novo registro
        if (configuracao == null) {
            configuracao = new Configuracao();
        }

        // Busca os alertas cadastrados para a filial ativa
        alertas = alertaRepository.obterAlertas(usuarioLogado.getFilialAtiva());

        // Carrega todos os colaboradores cadastrados e ativos
        ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
        colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
        colaboradores = colaboradorRepository.colaboradores(colaboradorFilter);

        // Inicializa alguns atributos evitando erros ao carregar a página
        alerta = new Alerta();
        alertaEmail = new AlertaEmail();
        colaborador = new Colaborador();

        // Data atual
        setDataAtual(new Date());
    }

    public void salvar() {

        if (configuracao.isNovo()) {
            if (StringUtils.isEmpty(mailPassword)) {
                throw new NegocioException("É necessário informar a senha para configuração do envio de e-mail");
            }
            configuracao.setMailPassword(getMailPassword());
            configuracao.setFilial(usuarioLogado.getFilialAtiva());
        } else {
            if (!StringUtils.isEmpty(mailPassword)) {
                configuracao.setMailPassword(getMailPassword());
            }
        }

        configuracao = configuracaoRepository.guardar(configuracao);
        FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
    }
    
    private void consistirConfiguracoesEmail() {
    	// Host
    	if (StringUtils.isBlank(configuracao.getMailServerHost())) {
    		throw new NegocioException("Host para envio de e-mail não foi informado.");
    	}
    	// Porta
    	if (configuracao.getMailServerPort() == null || configuracao.getMailServerPort() == 0) {
    		throw new NegocioException("Porta para envio de e-mail não foi informado.");
    	}
    	// E-mail padrão
    	if (StringUtils.isBlank(configuracao.getMailUsername())) {
    		throw new NegocioException("E-mail padrão para envio de e-mail não foi informado.");
    	}
    }

    public void testarEnvioEmail() {
        try {
        	consistirConfiguracoesEmail();
        	
            MailMessage message = mailer.novaMensagem();

            message.to(getMailTestUsername())
                    .subject("Teste envio de e-mail pelo sistema Mesure")
                    .bodyText("Enviado pelo sistema Mesure")
                    .put("numberTool", new NumberTool())
                    .put("locale", new Locale("pt", "BR"))
                    .send();

            setMailTestUsername("");
            FacesUtil.addInfoMessage("E-mail enviado com sucesso para o destinatário.");
        } catch (Exception e) {
            FacesUtil.addErrorMessage("Ocorreram erros ao enviar o e-mail. Motivo: " + e.getMessage());
        }
    }

    // Método executado ao abrir o modal em modo de INCLUSÃO
    public void inicializarAlertaInclusao() {
        alerta = new Alerta();
        alerta.setFilial(usuarioLogado.getFilialAtiva());
        alerta.setInicio(new Date());
    }

    // Método executado ao abrir o modal em modo de ALTERAÇÃO
    public void inicializarAlertaAlteracao() {
        // Carrega os e-mails vinculados ao alerta
        if (alerta != null && alerta.isEditando()) {
            alerta.setEmails(alertaRepository.emailsDoAlerta(alerta));
        }
    }

    // Método executado ao clicar Ok no modal, tanto para INCLUSÃO quanto ALTERAÇÃO
    public void incluirAlerta() {
        if (alerta.getTermino() != null && alerta.getTermino().getTime() < alerta.getInicio().getTime()) {
            throw new NegocioException("Data de término deve ser maior ou igual que data de início");
        }

        alerta = alertaRepository.guardar(alerta);

        if (!alertas.contains(alerta)) {
            alertas.add(alerta);
        }

        FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
    }

    public void excluirAlerta() {
        if (alerta != null) {
            alertaRepository.remover(alerta);
            alertas.remove(alerta);

            FacesUtil.addInfoMessageGrowl("Registro excluído com sucesso.");
        }
    }

    public void incluirAlertaEmail() {
        if (colaborador == null) {
            FacesUtil.addWarnMessage("Selecione um colaborador para adicionar");
        } else {

            boolean adicionado = false;
            for (AlertaEmail a : alerta.getEmails()) {
                if (a.getColaborador().equals(colaborador)) {
                    adicionado = true;
                    FacesUtil.addWarnMessage("Colaborador já adicionado ao alerta");
                    break;
                }
            }

            if (!adicionado) {
                String email = "";
                // Busca o e-mail de login ou e-mail comercial do colaborador
                if (colaborador.getUsuario() != null) {
                    email = colaborador.getUsuario().getEmail();
                }
                if (StringUtils.isBlank(email)) {
                    email = colaborador.getEmailComercial();
                }
                if (StringUtils.isBlank(email)) {
                    FacesUtil.addWarnMessage("Colaborador não possui e-mail informado");
                } else {
                    alerta.getEmails().add(new AlertaEmail(colaborador, email, alerta));
                    colaborador = new Colaborador();
                }
            }
        }
    }

    public void excluirAlertaEmail() {
        if (alertaEmail != null) {
            alerta.getEmails().remove(alertaEmail);
        }
    }

    // Executar alerta específico
    public void executarAlerta() {
        if (alerta == null) {
            throw new NegocioException("Nenhum alerta para executar");
        }
        
        consistirConfiguracoesEmail();

        try {
            // Verifica se o alerta está válido na data atual
            if (alerta.getInicio().getTime() <= getDataAtual().getTime()
                    && (alerta.getTermino() == null || alerta.getTermino().getTime() >= getDataAtual().getTime())) {
                alertaService.executarTipoAlerta(alerta);
                FacesUtil.addInfoMessageGrowl("Alerta executado com sucesso");
            } else {
                throw new NegocioException("Este alerta não está válido na data atual");
            }
        } catch (NegocioException e) {
            FacesUtil.addErrorMessage(e.getMessage());
        } catch (Exception e) {
            FacesUtil.addErrorMessage("Ocorreram erros ao executar o alerta. Motivo: " + e.getMessage());
        }
    }

    // Executar todos os alertas
    public void executarAlertas(boolean filialAtiva) {
    	
    	List<Alerta> alertasList = new ArrayList<>();
    	
    	if (filialAtiva) {
    		alertasList = alertas;
    	} else {
    		alertasList = alertaRepository.obterAlertas(null);
    	}
    	
        if (alertasList.isEmpty()) {
            throw new NegocioException("Nenhum alerta para executar");
        }
        
        consistirConfiguracoesEmail();
        
        int executados = 0;
        
        for (Alerta a : alertasList) {
            try {
                // Verifica se o alerta está válido na data atual
                if (a.getInicio().getTime() <= getDataAtual().getTime()
                        && (a.getTermino() == null || a.getTermino().getTime() >= getDataAtual().getTime())) {
                    alertaService.executarTipoAlerta(a);
                    executados++;
                }
            } catch (Exception e) {
                // apenas não executa o alerta e vai para o próximo
            }
        }
        if (executados == 0) {
            FacesUtil.addWarnMessage("Nenhum alerta foi executado");
        } else {
            FacesUtil.addInfoMessageGrowl(executados + " alerta(s) executado(s) com sucesso");
        }
    }

    public boolean isPossuiAlertas() {
        return (alertas != null && alertas.size() > 0);
    }

    // Para carregar as opções do Enum no formulário
    public ETipoAlerta[] getTiposAlerta() {
        return ETipoAlerta.values();
    }

    // Para carregar as opções do Enum no formulário
    public EFormaAlerta[] getFormasAlerta() {
        return EFormaAlerta.values();
    }

    // Para carregar as opções do Enum no formulário
    public EPeriodicidadeAlerta[] getPeriodicidades() {
        return EPeriodicidadeAlerta.values();
    }

    public Configuracao getConfiguracao() {
        return configuracao;
    }

    public void setConfiguracao(Configuracao configuracao) {
        this.configuracao = configuracao;
    }

    public String getMailPassword() {
        return mailPassword;
    }

    public void setMailPassword(String mailPassword) {
        this.mailPassword = mailPassword;
    }

    @Email
    public String getMailTestUsername() {
        return mailTestUsername;
    }

    public void setMailTestUsername(String mailTestUsername) {
        this.mailTestUsername = mailTestUsername;
    }

    public Alerta getAlerta() {
        return alerta;
    }

    public void setAlerta(Alerta alerta) {
        this.alerta = alerta;
    }

    public AlertaEmail getAlertaEmail() {
        return alertaEmail;
    }

    public void setAlertaEmail(AlertaEmail alertaEmail) {
        this.alertaEmail = alertaEmail;
    }

    public List<Alerta> getAlertas() {
        return alertas;
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public Date getDataAtual() {
        return dataAtual;
    }

    public void setDataAtual(Date dataAtual) {
        this.dataAtual = dataAtual;
    }

}

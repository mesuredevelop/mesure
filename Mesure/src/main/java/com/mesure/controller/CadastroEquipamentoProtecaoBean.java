package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.EquipamentoProtecao;
import com.mesure.repository.EquipamentoProtecaoRepository;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroEquipamentoProtecaoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	EquipamentoProtecaoRepository tipoRepository;
	
	private List<EquipamentoProtecao> tiposFiltrados;	
	private EquipamentoProtecao tipoSelecionado;
	
	public  CadastroEquipamentoProtecaoBean() {
		limpar();
	}
	
	public void salvar() {
		tipoSelecionado = tipoRepository.guardar(tipoSelecionado);
		limpar();
	}	
	
	public void excluir() {
		tipoRepository.remover(tipoSelecionado);
		tiposFiltrados.remove(tipoSelecionado);
		
		FacesUtil.addInfoMessage("Registro '" + tipoSelecionado.getDescricao() + "' excluído com sucesso.");
	}
	
	public void pesquisar() {
		tiposFiltrados = tipoRepository.equipamentos();
	}

	public List<EquipamentoProtecao> getTiposFiltrados() {
		return tiposFiltrados;
	}	
	
	public EquipamentoProtecao getTipoSelecionado() {
		return tipoSelecionado;
	}
	
	public void setTipoSelecionado(EquipamentoProtecao tipoSelecionado) {
		this.tipoSelecionado = tipoSelecionado;
	}	
	
	public void limpar() {
		this.tipoSelecionado = new EquipamentoProtecao();
	}

}


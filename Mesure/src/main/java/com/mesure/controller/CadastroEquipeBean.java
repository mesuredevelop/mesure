package com.mesure.controller;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Colaborador;
import com.mesure.model.Equipe;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.EquipeRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.service.EquipeService;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class CadastroEquipeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    EquipeService equipeService;

    @Inject
    EquipeRepository equipeRepository;

    @Inject
    ColaboradorRepository colaboradorRepository;

    private List<Colaborador> responsaveis;
    private List<Colaborador> participantes;

    private Equipe equipe;
    private Colaborador participanteEquipe;

    public CadastroEquipeBean() {
        limpar();
    }

    public void inicializar() {
        if (this.equipe == null) {
            limpar();
        }

        // Carrega todos os colaboradores cadastrados e ativos
        ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
        colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
        responsaveis = colaboradorRepository.colaboradores(colaboradorFilter);

        // Carrega os registros para o combo de colaboradores
        carregarResponsaveisDisponiveis();
    }

    private void limpar() {
        this.equipe = new Equipe();
        limparParticipanteEquipe();
    }

    public void salvar() {
        equipe = equipeService.salvar(equipe);
        limpar();

        carregarResponsaveisDisponiveis();

        FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
    }

    public void incluirParticipanteEquipe() {
        if (participanteEquipe == null) {
            FacesUtil.addWarnMessage("Selecione um colaborador para adicionar");
        } else if (equipe.getColaboradores().contains(participanteEquipe)) {
            FacesUtil.addWarnMessage("Colaborador já vinculado nesta equipe");
        } else {
            equipe.getColaboradores().add(participanteEquipe);
            participantes.remove(participanteEquipe);
            limparParticipanteEquipe();
        }
    }

    public void incluirTodosParticipantes() {
        for (Colaborador c : participantes) {
            if (!equipe.getColaboradores().contains(c)) {
                equipe.getColaboradores().add(c);
            }
        }
        participantes.clear();
        limparParticipanteEquipe();
    }

    public void limparParticipanteEquipe() {
        this.participanteEquipe = new Colaborador();
    }

    public void excluirParticipanteEquipe() {
        if (participanteEquipe != null) {
            equipe.getColaboradores().remove(participanteEquipe);
            participantes.add(participanteEquipe);
            limparParticipanteEquipe();
        }
    }

    public void carregarResponsaveisDisponiveis() {
        // Carrega todos os colaboradores cadastrados e ativos
        ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
        colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
        participantes = colaboradorRepository.colaboradores(colaboradorFilter);

        // Remove os colaboradores já adicionados como participantes da equipe
        if (equipe != null && equipe.isEditando()) {
            participantes.removeAll(equipe.getColaboradores());
        }
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public List<Colaborador> getResponsaveis() {
        return responsaveis;
    }

    public List<Colaborador> getParticipantes() {
        return participantes;
    }

    public Colaborador getParticipanteEquipe() {
        return participanteEquipe;
    }

    public void setParticipanteEquipe(Colaborador participanteEquipe) {
        this.participanteEquipe = participanteEquipe;
    }

}

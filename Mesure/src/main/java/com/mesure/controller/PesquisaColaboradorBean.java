package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Colaborador;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaColaboradorBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	ColaboradorRepository colaboradorRepository;
	
	@Inject
	@UsuarioLogado
	private UsuarioSistema usuarioLogado;
	
	private List<Colaborador> colaboradoresFiltrados;
	
	private Colaborador colaboradorSelecionado;
	
	public void excluir() {
		colaboradorRepository.remover(colaboradorSelecionado);
		colaboradoresFiltrados.remove(colaboradorSelecionado);
		
		FacesUtil.addInfoMessage("Colaborador '" + colaboradorSelecionado.getNome() + "' excluído com sucesso.");
	}
	
	public void pesquisar() {
		ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
		colaboradorFilter.setSomenteAtivos(ESimNao.NAO);
		colaboradoresFiltrados = colaboradorRepository.colaboradores(colaboradorFilter);
	}
		
	public List<Colaborador> getColaboradorsFiltrados() {
		return colaboradoresFiltrados;
	}

	public Colaborador getColaboradorSelecionado() {
		return colaboradorSelecionado;		
	}

	public void setColaboradorSelecionado(Colaborador colaboradorSelecionado) {
		this.colaboradorSelecionado = colaboradorSelecionado;
	}	

}


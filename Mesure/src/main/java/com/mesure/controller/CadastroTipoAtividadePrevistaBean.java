package com.mesure.controller;

import java.io.Serializable;

import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.TipoAtividadePrevista;
import com.mesure.repository.TipoAtividadePrevistaRepository;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroTipoAtividadePrevistaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	TipoAtividadePrevistaRepository tipoRepository;
	
	private List<TipoAtividadePrevista> tiposFiltrados;	
	private TipoAtividadePrevista tipoSelecionado;
	
	public  CadastroTipoAtividadePrevistaBean() {
		limpar();
	}
	
	public void salvar() {
		tipoSelecionado = tipoRepository.guardar(tipoSelecionado);
		limpar();
	}	
	
	public void excluir() {
		tipoRepository.remover(tipoSelecionado);
		tiposFiltrados.remove(tipoSelecionado);
		
		FacesUtil.addInfoMessage("Registro '" + tipoSelecionado.getDescricao() + "' excluído com sucesso.");
	}
	
	public void pesquisar() {
		tiposFiltrados = tipoRepository.tipos();
	}

	public List<TipoAtividadePrevista> getTiposFiltrados() {
		return tiposFiltrados;
	}	
	
	public TipoAtividadePrevista getTipoSelecionado() {
		return tipoSelecionado;
	}
	
	public void setTipoSelecionado(TipoAtividadePrevista tipoSelecionado) {
		this.tipoSelecionado = tipoSelecionado;
	}	
	
	public void limpar() {
		this.tipoSelecionado = new TipoAtividadePrevista();
	}

}

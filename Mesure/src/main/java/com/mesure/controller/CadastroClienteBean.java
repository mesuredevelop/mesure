package com.mesure.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.EEstado;
import com.mesure.enumerator.ETipoEndereco;
import com.mesure.enumerator.ETipoPessoa;
import com.mesure.model.Cliente;
import com.mesure.model.EnderecoCliente;
import com.mesure.service.ClienteService;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroClienteBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ClienteService clienteService;

	private Cliente cliente;	
	private EnderecoCliente endereco;
	
	private boolean pessoaJuridica;
	
	public CadastroClienteBean() {		
		limpar();		
	}
	
	public void inicializar() {
		if (this.cliente == null) {
			limpar();
		}
		onChangeTipo();
	}
	
	private void limpar() {
		this.cliente = new Cliente();
		this.cliente.setTipo(ETipoPessoa.FISICA);
		limparEndereco();
	}
	
	public void limparEndereco() {
		this.endereco = new EnderecoCliente();
	}
	
	public void salvar() {
		cliente = clienteService.salvar(cliente);
		limpar();

		FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
	}
	
	public void incluirEndereco() {
		if (!cliente.getEnderecos().contains(endereco)) {
			endereco.setCliente(cliente);
			cliente.getEnderecos().add(endereco);
			limparEndereco();
		}		
	}	
		
	public void excluirEndereco() {
		// TODO - testar quando o endereço estiver sendo referenciado
		if (endereco != null) {
			cliente.getEnderecos().remove(endereco);	
		}		
	}
	
	public boolean isPessoaJuridica() {
		return pessoaJuridica;
	}	
	
	public void onChangeTipo() {
		pessoaJuridica = (cliente.getTipo().equals(ETipoPessoa.JURIDICA));
		
		if (pessoaJuridica) {
			cliente.setCpf(null);
			cliente.setRg(null);
		} else {
			cliente.setCnpj(null);
			cliente.setInscricaoEstadual(null);
		}
	}	
	
	// Para carregar as opções do Enum no formulário
	public ETipoPessoa[] getTiposPessoa() {
		return ETipoPessoa.values();
	}
	
	// Para carregar as opções do Enum no formulário
	public EEstado[] getEstados() {
		return EEstado.values();
	}
	
	// Para carregar as opções do Enum no formulário
	public ETipoEndereco[] getTiposEndereco() {
		return ETipoEndereco.values();
	}		
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public EnderecoCliente getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoCliente endereco) {
		this.endereco = endereco;
	}
}

package com.mesure.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.EEstado;
import com.mesure.model.Fornecedor;
import com.mesure.service.FornecedorService;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroFornecedorBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private FornecedorService fornecedorService;	
	
	private Fornecedor fornecedor;	
	
	public CadastroFornecedorBean() {
		limpar();		
	}
	
	public void inicializar() {
		if (this.fornecedor == null) {
			limpar();
		}
	}
	
	private void limpar() {
		this.fornecedor = new Fornecedor();
	}
	
	public void salvar() {
		fornecedor = fornecedorService.salvar(fornecedor);		
		limpar();

		FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
	}
	
	// Para carregar as opções do Enum no formulário
	public EEstado[] getEstados() {
		return EEstado.values();
	}	
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
}


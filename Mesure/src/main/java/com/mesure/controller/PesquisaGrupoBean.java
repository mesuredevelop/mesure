package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.Grupo;
import com.mesure.model.Permissao;
import com.mesure.repository.GrupoRepository;
import com.mesure.service.GrupoService;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaGrupoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	GrupoService grupoService;
	
	@Inject
	GrupoRepository grupoRepository;
	
	private List<Grupo> grupos;	
	private List<Permissao> permissoes;
	
	private Grupo grupoSelecionado;
	private Permissao permissaoGrupo;
	
	public PesquisaGrupoBean() {
		limpar();
	}
	
	// Método executado ao abrir o modal em modo de INCLUSÃO
	public void inicializarGrupo() {
		limpar();
		carregarPermissoesDisponiveis();
	}
	
	public void salvar() {		
		grupoSelecionado = grupoService.salvar(grupoSelecionado);
		limpar();
	}	
	
	public void excluir() {
		grupoRepository.remover(grupoSelecionado);
		grupos.remove(grupoSelecionado);
		
		FacesUtil.addInfoMessage("Grupo '" + grupoSelecionado.getNome() + "' excluído com sucesso.");
	}
	
	public void pesquisar() {
		grupos = grupoRepository.todos();
		
		// Faz isso apenas para carregar os dados e evitar o erro LazyInitializationException		
		for (Grupo g : grupos) {
			g.getPermissoes().size();
		}
	}
	
	public void incluirPermissaoGrupo() {
		if (permissaoGrupo == null) {
			FacesUtil.addWarnMessage("Selecione uma permissão para adicionar");
		} else if (grupoSelecionado.getPermissoes().contains(permissaoGrupo)) {
			FacesUtil.addWarnMessage("Permissão já vinculada neste grupo");			
		} else {
			grupoSelecionado.getPermissoes().add(permissaoGrupo);
			permissoes.remove(permissaoGrupo);
			limparPermissaoGrupo();
		}
	}
	
	public void incluirTodasPermissoesGrupo() {
		for (Permissao p : permissoes) {
			if (!grupoSelecionado.getPermissoes().contains(p)) {
				grupoSelecionado.getPermissoes().add(p);
			}
		}
		permissoes.clear();
		limparPermissaoGrupo();
	}
	
	public void excluirPermissaoGrupo() {
		if (permissaoGrupo != null) {
			grupoSelecionado.getPermissoes().remove(permissaoGrupo);
			permissoes.add(permissaoGrupo);
			limparPermissaoGrupo();
		}
	}
	
	public void limpar() {
		grupoSelecionado = new Grupo();
	}
	
	public void limparPermissaoGrupo() {
		permissaoGrupo = new Permissao();
	}
	
	public void carregarPermissoesDisponiveis() {
		// Carrega todas as permissões disponíveis no sistema
		permissoes = grupoRepository.permissoes();
		
		// Remove as permissões já vinculadas ao grupo
		if (grupoSelecionado != null && grupoSelecionado.isEditando()) {
			permissoes.removeAll(grupoSelecionado.getPermissoes());
		}
	}	    
    
	public List<Grupo> getGrupos() {
		return grupos;
	}

	public List<Permissao> getPermissoes() {
		return permissoes;
	}
	
	public Grupo getGrupoSelecionado() {
		return grupoSelecionado;
	}
	
	public void setGrupoSelecionado(Grupo grupoSelecionado) {
		this.grupoSelecionado = grupoSelecionado;
		carregarPermissoesDisponiveis();
	}
	
	public Permissao getPermissaoGrupo() {
		return permissaoGrupo;
	}

	public void setPermissaoGrupo(Permissao permissaoGrupo) {
		this.permissaoGrupo = permissaoGrupo;
	}
	
}

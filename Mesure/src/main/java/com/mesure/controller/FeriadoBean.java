package com.mesure.controller;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.EEstado;
import com.mesure.enumerator.ETipoFeriado;
import com.mesure.model.Feriado;
import com.mesure.model.Filial;
import com.mesure.repository.EmpresaRepository;
import com.mesure.repository.FeriadoRepository;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.NegocioException;
import com.mesure.util.Util;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class FeriadoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	FeriadoRepository feriadoRepository;
	
	@Inject
	private EmpresaRepository empresaRepository;	
	
	@Inject
	@UsuarioLogado
	private UsuarioSistema usuarioLogado;	
	
	private List<Feriado> feriados;	
	private List<Filial> filiais;
	private Feriado feriado;
	
	Date dataDuplicar;
	
	public FeriadoBean() {
		limpar();
	}
	
	public void salvar() {
		feriado.setFilial(usuarioLogado.getFilialAtiva());
		feriado = feriadoRepository.guardar(feriado);
		
		if (!feriados.contains(feriado)) {
			feriados.add(feriado);
		}
		
		limpar();		
		FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
	}	
	
	public void excluir() {
		if (feriado != null) {
			feriadoRepository.remover(feriado);
			feriados.remove(feriado);
			
			FacesUtil.addInfoMessageGrowl("Registro excluído com sucesso.");
		}
	}
	
	public void duplicarFeriado() {
		boolean registroDuplicado = false;
		
		if (dataDuplicar != null) {
			for (Feriado f : feriados) {
				if (f.isFlag()) {
					long dif = 0;
					
					if (dataDuplicar.getTime() > f.getData().getTime()) {
						dif = Util.DiferencaEmAnos(dataDuplicar, f.getData());
						
						for (int i = 1; i <= dif; i++) {
							GregorianCalendar c = new GregorianCalendar();
							c.setTime(f.getData());
							c.add(Calendar.YEAR, i);

							if (feriadoRepository.porDataTipo(c.getTime(), f.getTipo(), f.getFilial()).isEmpty()) {
								Feriado novoFeriado = new Feriado();
								novoFeriado.setFilial(f.getFilial());
								novoFeriado.setDescricao(f.getDescricao());
								novoFeriado.setTipo(f.getTipo());
								novoFeriado.setUf(f.getUf());
								novoFeriado.setCidade(f.getCidade());
								novoFeriado.setFlag(false);
								novoFeriado.setData(c.getTime());
								
								feriadoRepository.guardar(novoFeriado);
								registroDuplicado = true;	

							}														
						}
					}	
				}
			}			
		}
		
		if (registroDuplicado) {
			FacesUtil.addInfoMessageGrowl("Registro duplicado com sucesso.");
		} else {
			throw new NegocioException("Nenhum registro foi processado");
		}
		dataDuplicar = null;
	}
	
	public void duplicarFeriadoFiliais() {
		boolean registroDuplicado = false;		
		
		for (Filial fi : filiais) {
			if (fi.isFlag()) {
				for (Feriado f : feriados) {
					if (f.isFlag()) {
						if (feriadoRepository.porDataTipo(f.getData(), f.getTipo(), fi).isEmpty()) {
							Feriado novoFeriado = new Feriado();						
							novoFeriado.setCidade(f.getCidade());
							novoFeriado.setData(f.getData());
							novoFeriado.setDescricao(f.getDescricao());
							novoFeriado.setFilial(fi);
							novoFeriado.setFlag(false);
							novoFeriado.setTipo(f.getTipo());
							novoFeriado.setUf(f.getUf());
							
							feriadoRepository.guardar(novoFeriado);
							registroDuplicado = true;	
						}
					}
				}
			}
		}
		
		if (registroDuplicado) {
			FacesUtil.addInfoMessageGrowl("Registro duplicado com sucesso.");
		} else {
			throw new NegocioException("Nenhum registro foi processado");
		}
	}
	
	public void validarSelecionados() {
		if (feriados == null || feriados.size() == 0) {
			throw new NegocioException("Nenhum registro para processar");
		}			
		
		boolean selecionado = false;
		
		for (Feriado f : feriados) {
			if (f.isFlag()) {
				selecionado = true;
				break;
			}
		}
		
		if (!selecionado) {
			throw new NegocioException("Nenhum feriado selecionado.");
		}
	}
	
	public void pesquisar() {
		feriados = feriadoRepository.obterFeriados(usuarioLogado.getFilialAtiva());
		filiais = empresaRepository.outrasFiliais(usuarioLogado.getFilialAtiva());
	}
	
	// Para carregar as opções do Enum no formulário
	public ETipoFeriado[] getTiposFeriado() {
		return ETipoFeriado.values();
	}	
	
	// Para carregar as opções do Enum no formulário
	public EEstado[] getEstados() {
		return EEstado.values();
	}	
	
	public String getFilialAtiva() {
		String filialAtiva;
		
		if (feriado.getFilial() == null) {
			filialAtiva = usuarioLogado.getFilialAtiva().getNome();
		} else {
			filialAtiva = feriado.getFilial().getNome();
		}
		return filialAtiva;
	}

	public List<Feriado> getFeriados() {
		return feriados;
	}

	public void setFeriados(List<Feriado> feriados) {
		this.feriados = feriados;
	}

	public List<Filial> getFiliais() {
		return filiais;
	}

	public void setFiliais(List<Filial> filiais) {
		this.filiais = filiais;
	}

	public Feriado getFeriado() {
		feriado.setFilial(usuarioLogado.getFilialAtiva());
		return feriado;
	}

	public void setFeriado(Feriado feriado) {
		this.feriado = feriado;
	}

	public Date getDataDuplicar() {
		return dataDuplicar;
	}

	public void setDataDuplicar(Date dataDuplicar) {
		this.dataDuplicar = dataDuplicar;
	}

	public void limpar() {
		this.feriado = new Feriado();
	}

}


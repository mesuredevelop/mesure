package com.mesure.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.mesure.enumerator.EAcaoLoteFaturamento;
import com.mesure.enumerator.EStatusLoteFaturamento;
import com.mesure.model.FaturamentoLote;
import com.mesure.repository.FaturamentoLoteRepository;
import com.mesure.service.FaturamentoLoteService;
import com.mesure.service.NegocioException;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class DetalheLoteFaturamentoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private HttpServletRequest request;
	
	@Inject
	FaturamentoLoteService faturamentoLoteService;
	
	@Inject
	FaturamentoLoteRepository faturamentoLoteRepository;
	
	private FaturamentoLote lote;
	
	private boolean carregouRegistros;
	
	public void inicializar() {
		setCarregouRegistros(false);
		
		String param_lote = request.getParameter("lote");
		
		if (StringUtils.isBlank(param_lote)) {
			lote = null;
		} 
				
		if (lote == null) {
			throw new NegocioException("Não foi possível carregar as informações.");
		}
		
		if (lote.getStatus().equals(EStatusLoteFaturamento.PENDENTE)) {
			throw new NegocioException("Este lote ainda não foi processado");
		}
		
		setCarregouRegistros(true);
	}
	
	public void fecharLote() throws ParseException {
		lote.setFlag(true);
		List<FaturamentoLote> lotes = new ArrayList<>();		
		lotes.add(lote);
		faturamentoLoteService.setLotes(lotes);
		faturamentoLoteService.executarAcaoLote(EAcaoLoteFaturamento.FECHAR);
		lote = faturamentoLoteRepository.porId(lote.getId());

		FacesUtil.addInfoMessageGrowl("Operação realizada com sucesso.");		
	}
	
	public void liberarLote() throws ParseException {
		lote.setFlag(true);
		List<FaturamentoLote> lotes = new ArrayList<>();		
		lotes.add(lote);
		faturamentoLoteService.setLotes(lotes);
		faturamentoLoteService.executarAcaoLote(EAcaoLoteFaturamento.LIBERAR);
		lote = faturamentoLoteRepository.porId(lote.getId());

		FacesUtil.addInfoMessageGrowl("Operação realizada com sucesso.");	
	}
	
	public boolean isPodeFecharLote() {
		return lote.getStatus().equals(EStatusLoteFaturamento.PROCESSADO);
	}
	
	public boolean isPodeLiberarLote() {
		return lote.getStatus().equals(EStatusLoteFaturamento.FECHADO);
	}

	public FaturamentoLote getLote() {
		return lote;
	}

	public void setLote(FaturamentoLote lote) {
		this.lote = lote;
	}

	public boolean isCarregouRegistros() {
		return carregouRegistros;
	}

	public void setCarregouRegistros(boolean carregouRegistros) {
		this.carregouRegistros = carregouRegistros;
	}
	
}

package com.mesure.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.FacesException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.Cliente;
import com.mesure.repository.ClienteRepository;
import com.mesure.repository.filter.ClienteFilter;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaClienteBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	ClienteRepository clienteRepository;
	
	private ClienteFilter filtro;
	private List<Cliente> clientesFiltrados;
	
	private Cliente clienteSelecionado;
	
	public PesquisaClienteBean() {
		filtro = new ClienteFilter();
	}	
	
	public void redirect() {
		String page = "/clientes/CadastroCliente.xhtml";
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			String contextPath = externalContext.getRequestContextPath();
	
			externalContext.redirect(contextPath + page);
			facesContext.responseComplete();
		} catch (IOException e) {
			throw new FacesException(e);
		}
	}	
	
	public String redirect2() {
		return "/clientes/CadastroCliente.xhtml";
	}
	
	public void excluir() {
		clienteRepository.remover(clienteSelecionado);
		clientesFiltrados.remove(clienteSelecionado);
		
		FacesUtil.addInfoMessage("Cliente '" + clienteSelecionado.getNome() + "' excluído com sucesso.");
	}
	
	public void pesquisar() {
		clientesFiltrados = clienteRepository.filtrados(filtro);
	}
		
	public ClienteFilter getFiltro() {
		return filtro;
	}

	public List<Cliente> getClientesFiltrados() {
		return clientesFiltrados;
	}

	public Cliente getClienteSelecionado() {
		return clienteSelecionado;		
	}

	public void setClienteSelecionado(Cliente clienteSelecionado) {
		this.clienteSelecionado = clienteSelecionado;
	}	
}

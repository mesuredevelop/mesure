package com.mesure.controller;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Grupo;
import com.mesure.model.Usuario;
import com.mesure.repository.GrupoRepository;
import com.mesure.service.NegocioException;
import com.mesure.service.UsuarioService;
import com.mesure.util.Util;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroUsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	GrupoRepository grupoRepository; 
	
	@Inject
	private UsuarioService usuarioService;
	
	private Usuario usuario;
	
	private Grupo grupo;
	
	private List<Grupo> gruposDisponiveis;	
	
	private String senha;
	private String confirmacaoSenha;
	
	private String selecione;
	
	public String getSelecione() {
		return selecione;
	}

	public void setSelecione(String selecione) {
		this.selecione = selecione;
	}

	public CadastroUsuarioBean() {		
		limpar();		
	}
	
	public void inicializar() {
		if (this.usuario == null) {
			limpar();
		}
		gruposDisponiveis = grupoRepository.todos();
	}
		
	private void limpar() {
		usuario = new Usuario();		
		grupo = new Grupo();
		senha = "";
		confirmacaoSenha = "";
	}
	
	public void salvar() {
		
		if (usuario.isNovo()) {
			if (StringUtils.isEmpty(senha)) {
				throw new NegocioException("É necessário informar Senha");
			}
			
			consistirConfirmacaoSenha(senha, confirmacaoSenha);
			consistirTamanhoSenha(senha);
			setSenhaUsuario(senha);
		} else {
			if (!StringUtils.isEmpty(senha)) {
				consistirConfirmacaoSenha(senha, confirmacaoSenha);
				consistirTamanhoSenha(senha);
				setSenhaUsuario(senha);
			}
		}			
		
		usuario = usuarioService.salvar(usuario);
		limpar();

		FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
		
		//FacesUtil.addInfoMessage("Gravação efetuada com sucesso.");
		//FacesUtil.addInfoMessageGrowl2("Gravação efetuada com sucesso.");
	}
	
	private void consistirTamanhoSenha(String strSenha) {
		int size = strSenha.length();
		if ((size < 6) || (size > 14)) {
			throw new NegocioException("Senha deve conter entre 6 e 14 caracteres");
		}
	}
	
	private void consistirConfirmacaoSenha(String strSenha, String strConfirmacaoSenha) {
		if (StringUtils.isEmpty(strConfirmacaoSenha)) {
			throw new NegocioException("É necessário informar Confirmação Senha");
		}
		
		if (!strSenha.equals(strConfirmacaoSenha)) {
			throw new NegocioException("Senha e Confirmação Senha não conferem");
		}
	}
	
	private void setSenhaUsuario(String strSenha) {
		try {
			usuario.setSenha(Util.encryptMD5(strSenha));
		} catch (NoSuchAlgorithmException e) {
			throw new NegocioException("Erro ao processar senha");
		}
	}
	
	public void incluirGrupo() {
		if (grupo == null) {
			FacesUtil.addWarnMessage("Selecione um grupo para adicionar");
		} else if (usuario.getGrupos().contains(grupo)) {
			FacesUtil.addWarnMessage("Grupo já vinculado ao usuário");			
		} else {
			usuario.getGrupos().add(grupo);
			grupo = new Grupo();
		}
	}
			
	public void excluirGrupo() {
		if (grupo != null) {
			usuario.getGrupos().remove(grupo);
		}		
	}
	
	// Para carregar as opções do Enum no formulário
	public ESimNao[] getOpcoesSimNao() {
		return ESimNao.values();
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Grupo getGrupo() {
		return grupo;		
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public List<Grupo> getGruposDisponiveis() {
		return this.gruposDisponiveis;
	}
	
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getConfirmacaoSenha() {
		return confirmacaoSenha;
	}

	public void setConfirmacaoSenha(String confirmacaoSenha) {
		this.confirmacaoSenha = confirmacaoSenha;
	}

	public boolean isEditando() {
		return this.usuario.getId() != null;
	}		
}

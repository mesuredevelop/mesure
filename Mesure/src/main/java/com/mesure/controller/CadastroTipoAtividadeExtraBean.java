package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.TipoAtividadeExtra;
import com.mesure.repository.TipoAtividadeExtraRepository;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroTipoAtividadeExtraBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	TipoAtividadeExtraRepository tipoRepository;
	
	private List<TipoAtividadeExtra> tiposFiltrados;	
	private TipoAtividadeExtra tipoSelecionado;
	
	public  CadastroTipoAtividadeExtraBean() {
		limpar();
	}
	
	public void salvar() {
		tipoSelecionado = tipoRepository.guardar(tipoSelecionado);
		limpar();
	}	
	
	public void excluir() {
		tipoRepository.remover(tipoSelecionado);
		tiposFiltrados.remove(tipoSelecionado);
		
		FacesUtil.addInfoMessage("Registro '" + tipoSelecionado.getDescricao() + "' excluído com sucesso.");
	}
	
	public void pesquisar() {
		tiposFiltrados = tipoRepository.tipos();
	}
	
	// Para carregar as opções do Enum no formulário
	public ESimNao[] getOpcoesSimNao() {
		return ESimNao.values();
	}

	public List<TipoAtividadeExtra> getTiposFiltrados() {
		return tiposFiltrados;
	}	
	
	public TipoAtividadeExtra getTipoSelecionado() {
		return tipoSelecionado;
	}
	
	public void setTipoSelecionado(TipoAtividadeExtra tipoSelecionado) {
		this.tipoSelecionado = tipoSelecionado;
	}	
	
	public void limpar() {
		this.tipoSelecionado = new TipoAtividadeExtra();
	}

}


package com.mesure.controller;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Colaborador;
import com.mesure.model.Projeto;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class PesquisaProjetoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    ProjetoRepository projetoRepository;

    @Inject
    ColaboradorRepository colaboradorRepository;

    @Inject
    Seguranca seguranca;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    private ProjetoFilter filtro;
    private List<Projeto> projetosFiltrados;

    private List<Colaborador> colaboradores;

    public PesquisaProjetoBean() {
        filtro = new ProjetoFilter();
    }

    @PostConstruct
    public void inicializar() {

    }

    public void pesquisar() {
        if (filtro == null) {
            filtro = new ProjetoFilter();
        }

        // Para trazer somente projetos no qual o usuário é participante ou responsável
        if (!seguranca.visualizaProjetos()) {
            filtro.setColaborador(usuarioLogado.getUsuario().getColaborador());
        }

        // Para trazer somente projetos da filial ativa
        if (usuarioLogado.getFilialAtiva().getFiltrarProjetos().equals(ESimNao.SIM)) {
            filtro.setFilial(usuarioLogado.getFilialAtiva());
        }

        projetosFiltrados = projetoRepository.filtrados(filtro);
    }

    public ProjetoFilter getFiltro() {
        return filtro;
    }

    public List<Projeto> getProjetosFiltrados() {
        return projetosFiltrados;
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }

}

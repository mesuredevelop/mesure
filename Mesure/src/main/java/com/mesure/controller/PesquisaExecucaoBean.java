package com.mesure.controller;

import com.mesure.consult.repository.CNHorasFaltantesRepository;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoAtividadePrevista;
import com.mesure.enumerator.ESituacaoEtapa;
import com.mesure.model.Atividade;
import com.mesure.model.Colaborador;
import com.mesure.model.ColaboradorPonto;
import com.mesure.model.DispositivoSeguranca;
import com.mesure.model.EquipamentoProtecao;
import com.mesure.model.Etapa;
import com.mesure.model.ProgramacaoExecucao;
import com.mesure.model.ProgramacaoExecucaoColaborador;
import com.mesure.model.ProjetoAtividadePrevista;
import com.mesure.model.ProjetoAtividadePrevistaDispositivo;
import com.mesure.model.ProjetoAtividadePrevistaEquipamento;
import com.mesure.model.ProjetoEtapa;
import com.mesure.model.ProjetoExecucao;
import com.mesure.model.Veiculo;
import com.mesure.model.VeiculoMovimento;
import com.mesure.repository.AtividadeRepository;
import com.mesure.repository.ColaboradorPontoRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.DispositivoSegurancaRepository;
import com.mesure.repository.EquipamentoProtecaoRepository;
import com.mesure.repository.ExecucaoRepository;
import com.mesure.repository.ProgramacaoExecucaoRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.VeiculoRepository;
import com.mesure.repository.filter.ExecucaoFilter;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.ColaboradorPontoService;
import com.mesure.service.EquipeService;
import com.mesure.service.NegocioException;
import com.mesure.service.ProjetoService;
import com.mesure.util.Util;
import com.mesure.util.jpa.Transactional;
import com.mesure.util.jsf.FacesUtil;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class PesquisaExecucaoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    ExecucaoRepository execucaoRepository;

    @Inject
    ColaboradorRepository colaboradorRepository;

    @Inject
    VeiculoRepository veiculoRepository;

    @Inject
    AtividadeRepository atividadeRepository;

    @Inject
    ProgramacaoExecucaoRepository programacaoExecucaoRepository;

    @Inject
    ColaboradorPontoService colaboradorPontoService;

    @Inject
    ColaboradorPontoRepository colaboradorPontoRepository;

    @Inject
    CNHorasFaltantesRepository cnhorasFaltantesRepository;
    
    @Inject
    EquipamentoProtecaoRepository equipamentosRepository;

    @Inject
    DispositivoSegurancaRepository dispositivosRepository;

    @Inject
    EquipeService equipeService;

    @Inject
    ProjetoService projetoService;

    @Inject
    Seguranca seguranca;

    @Inject
    ProjetoRepository projetoRepository;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    private ProjetoAtividadePrevista atividadePrevista;
    private Colaborador novoMotorista;
    private Date saidaEmpresa;
    private Date retornoEmpresa;
    private Date horaInicio;
    private Date horaTermino;
    private Date horaPausa;
    private Date horaRetomada;
    private Integer kmVeiculo;
    private String motivoPausa;
    private boolean checkEPI;
    private boolean checkTrocarMotorista;
    private boolean pausaAlmoco;
    private List<Colaborador> participantes;
    private List<ProjetoEtapa> etapasExecucao;
    private List<ProjetoEtapa> etapasProjeto;
    private List<EquipamentoProtecao> equipamentosProtecao;
    private List<DispositivoSeguranca> dispositivosSeguranca;

    private ExecucaoFilter filtro;
    private List<ProjetoAtividadePrevista> atvPrevistaFiltrados;
    private List<Colaborador> colaboradores;
    private List<Colaborador> motoristas;

    private List<EquipamentoProtecao> equipamentosDisponiveis;
    private List<DispositivoSeguranca> dispositivosDisponiveis;

    private ProjetoAtividadePrevistaEquipamento equipamentoAtividade;
    private ProjetoAtividadePrevistaDispositivo dispositivoAtividade;

    private EquipamentoProtecao equipamentoSelecionado;
    private DispositivoSeguranca dispositivoSelecionado;
    private ProjetoEtapa etapaSelecionada;

    // filtro de pesquisa
    private Colaborador filtroColaborador;
    private Date filtroCompetencia;

    public PesquisaExecucaoBean() {
        filtro = new ExecucaoFilter();
    }

    public void inicializar() {
        limparPesquisa();

        filtroColaborador = seguranca.getUsuarioLogado().getUsuario().getColaborador();
        filtroCompetencia = new Date();

        // Carrega os colaboradores no qual o usuário é responsável além do próprio colaborador ativo
        colaboradores = equipeService.colaboradoresDoResponsavel(usuarioLogado.getUsuario().getColaborador(), ESimNao.SIM);

        // Carrega os EPIs para mostrar no combo
        equipamentosDisponiveis = equipamentosRepository.equipamentos();

        // Carrega os EPCs para mostrar no combo
        dispositivosDisponiveis = dispositivosRepository.dispositivos();
    }

    public void inicializarExecucaoSaida() {

        if (atividadePrevista == null) {
            throw new NegocioException("Não foi possível carregar os dados da atividade prevista");
        }

        if (atividadePrevista.getPausa().equals(ESimNao.SIM)) {
            throw new NegocioException("Esta ação não é permitida pois a execução se encontra em pausa.");
        }

        ProjetoExecucao projetoExecucao = null;

        // Busca as execuções vinculadas à atividade prevista
        List<ProjetoExecucao> execucoes = execucaoRepository.execucoesDaAtividadePrevista(atividadePrevista);

        // Se possui duas ou mais execuções não abre o modal de saída
        if (execucoes.size() >= 2) {
            throw new NegocioException("Esta ação não é permitida pois a execução já está em andamento.");
        } else if (execucoes.size() == 1) {
            // Pega a execução vinculada à atividade prevista
            projetoExecucao = execucoes.get(0);

            // Se a execução já possui hora de término não abre o modal de saída
            if (projetoExecucao.getHoraTermino() != null) {
                throw new NegocioException("Esta ação não é permitida pois a execução já está em andamento.");
            }
        }

        saidaEmpresa = new Date();
        horaInicio = new Date();

        // Ainda não foi gerada a execução p/ a atividade prevista (inclusão)
        if (projetoExecucao == null || projetoExecucao.isNovo()) {
            if (temProgramacao()) {
                // Saída Empresa
                saidaEmpresa = atividadePrevista.getProgramacaoExecucao().getSaidaEmpresa();
                // Hora Início
                horaInicio = atividadePrevista.getProgramacaoExecucao().getHoraInicio();
            }
        } else { // Já foi gerada a execução p/ a atividade prevista (alteração)
            // Saída Empresa
            saidaEmpresa = projetoExecucao.getSaidaEmpresa();
            // Hora Início
            horaInicio = projetoExecucao.getHoraInicio();
        }

        // Km veículo
        kmVeiculo = 0;
        Veiculo veiculo = getVeiculo();
        if (veiculo != null) {
            kmVeiculo = veiculoRepository.maiorKmFinal(veiculo);
        }

        // CheckBox "Estou ciente da utilização destes EPI's e EPC's"
        checkEPI = false;

        // Participantes
        participantes = new ArrayList<>();
        if (temProgramacao()) {
            ProgramacaoExecucao programacaoP = programacaoExecucaoRepository.participantesDaProgramacao(atividadePrevista.getProgramacaoExecucao());
            if (programacaoP != null && programacaoP.getParticipantes() != null) {
                for (ProgramacaoExecucaoColaborador c : programacaoP.getParticipantes()) {
                    participantes.add(c.getColaborador());
                }
            }
        }

        // Etapas
        carregarEtapas();

        // EPI's e EPC's
        equipamentosProtecao = new ArrayList<>();
        dispositivosSeguranca = new ArrayList<>();

        for (ProjetoEtapa projetoEtapa : etapasExecucao) {
            Etapa etapa = projetoEtapa.getEtapa();

            if (etapa != null) {

                // Equipamentos de Proteção (EPI) da etapa
                Etapa etapaEPI = atividadeRepository.equipamentosDaEtapa(etapa);
                if (etapaEPI != null && etapaEPI.getEquipamentos() != null) {
                    for (EquipamentoProtecao epi : etapaEPI.getEquipamentos()) {
                        if (!equipamentosProtecao.contains(epi)) {
                            equipamentosProtecao.add(epi);
                        }
                    }
                }

                // Dispositivos de Segurança (EPC) da etapa
                Etapa etapaEPC = atividadeRepository.dispositivosDaEtapa(etapa);
                if (etapaEPC != null && etapaEPC.getDispositivos() != null) {
                    for (DispositivoSeguranca epc : etapaEPC.getDispositivos()) {
                        if (!dispositivosSeguranca.contains(epc)) {
                            dispositivosSeguranca.add(epc);
                        }
                    }
                }

                // Equipamentos de Proteção (EPI) da atividade
                Atividade atividadeEPI = atividadeRepository.equipamentosDaAtividade(etapa.getAtividade());
                if (atividadeEPI != null && atividadeEPI.getEquipamentos() != null) {
                    for (EquipamentoProtecao epi : atividadeEPI.getEquipamentos()) {
                        if (!equipamentosProtecao.contains(epi)) {
                            equipamentosProtecao.add(epi);
                        }
                    }
                }

                // Dispositivos de Segurança (EPC) da atividade
                Atividade atividadeEPC = atividadeRepository.dispositivosDaAtividade(etapa.getAtividade());
                if (atividadeEPC != null && atividadeEPC.getDispositivos() != null) {
                    for (DispositivoSeguranca epc : atividadeEPC.getDispositivos()) {
                        if (!dispositivosSeguranca.contains(epc)) {
                            dispositivosSeguranca.add(epc);
                        }
                    }
                }
            }
        }

        // Ainda não foi gerada a execução p/ a atividade prevista (inclusão)
        if (projetoExecucao == null || projetoExecucao.isNovo()) {
            atividadePrevista.setEquipamentos(new ArrayList<>());
            atividadePrevista.setDispositivos(new ArrayList<>());

            // Vincula os equipamentos à atividade prevista
            for (EquipamentoProtecao epi : equipamentosProtecao) {
                ProjetoAtividadePrevistaEquipamento atividadeEPI = new ProjetoAtividadePrevistaEquipamento();
                atividadeEPI.setAtividadePrevista(atividadePrevista);
                atividadeEPI.setEquipamentoProtecao(epi);
                atividadePrevista.getEquipamentos().add(atividadeEPI);
            }

            // Vincula os dispositivos à atividade prevista
            for (DispositivoSeguranca epc : dispositivosSeguranca) {
                ProjetoAtividadePrevistaDispositivo atividadeEPC = new ProjetoAtividadePrevistaDispositivo();
                atividadeEPC.setAtividadePrevista(atividadePrevista);
                atividadeEPC.setDispositivoSeguranca(epc);
                atividadePrevista.getDispositivos().add(atividadeEPC);
            }
        }
    }

    public void inicializarExecucaoRetorno() {

        if (atividadePrevista == null) {
            throw new NegocioException("Não foi possível carregar os dados da atividade prevista");
        }

        if (atividadePrevista.getPausa().equals(ESimNao.SIM)) {
            throw new NegocioException("Esta ação não é permitida pois a execução se encontra em pausa.");
        }

        // Busca a última execução vinculada à atividade prevista
        ProjetoExecucao projetoExecucao = execucaoRepository.ultimaExecucaoDaAtividadePrevista(atividadePrevista);

        // O usuário ainda não gerou a execução (saída) p/ a atividade prevista
        if (projetoExecucao == null || projetoExecucao.isNovo()) {
            throw new NegocioException("Essa atividade ainda não possui a execução de saída");
        }

        // Carrega as etapas do projeto
        etapasProjeto = projetoRepository.etapasDoProjeto(atividadePrevista.getProjeto());

        // Km veículo
        kmVeiculo = 0;
        Veiculo veiculo = getVeiculo();
        if (veiculo != null) {
            kmVeiculo = veiculoRepository.maiorKmFinal(veiculo);
        }

        horaTermino = new Date();
        retornoEmpresa = new Date();

        if (temProgramacao()) {
            // Hora Término
            horaTermino = atividadePrevista.getProgramacaoExecucao().getHoraTermino();
            // Retorno Empresa
            retornoEmpresa = atividadePrevista.getProgramacaoExecucao().getRetornoEmpresa();
        }

        // CheckBox "Trocar motorista"
        checkTrocarMotorista = false;

        // Outros participantes da programação (que podem ser selecionados como motorista)
        carregarMotoristas();

        // Etapas		
        carregarEtapas();
    }

    @Transactional
    public void gerarExecucaoSaida() {

        if (atividadePrevista == null) {
            throw new NegocioException("Não foi possível carregar os dados da atividade prevista");
        }

        if (saidaEmpresa == null) {
            throw new NegocioException("O horário de saída da empresa deve ser informado");
        }

        if (horaInicio == null) {
            throw new NegocioException("O horário de início da atividade deve ser informado");
        }

        if (horaInicio.getTime() < saidaEmpresa.getTime()) {
            throw new NegocioException("O horário de início da atividade deve ser maior ou igual que o horário de saída da empresa");
        }

        ProjetoEtapa etapa = null;
        if (temProgramacao()) {
            etapa = atividadePrevista.getProgramacaoExecucao().getEtapa();
        } else {
            etapa = atividadePrevista.getEtapa();
        }

        // Busca a última execução vinculada à atividade prevista
        ProjetoExecucao projetoExecucao = execucaoRepository.ultimaExecucaoDaAtividadePrevista(atividadePrevista);

        // Verifica se está inserindo ou alterando a execução no projeto
        if (projetoExecucao == null || projetoExecucao.isNovo()) {
            projetoExecucao = new ProjetoExecucao();
            projetoExecucao.setData(atividadePrevista.getData());
            projetoExecucao.setAtividadePrevista(atividadePrevista);
            projetoExecucao.setColaborador(atividadePrevista.getColaborador());
            projetoExecucao.setProjeto(atividadePrevista.getProjeto());
            projetoExecucao.setRetrabalho(ESimNao.NAO);
            
            // Verifica se permite apontamentos atrasados
            cnhorasFaltantesRepository.verificarApontamentosAtrasados(projetoExecucao.getColaborador(), projetoExecucao.getData());
            
        }
        projetoExecucao.setHoraInicio(horaInicio);
        projetoExecucao.setSaidaEmpresa(saidaEmpresa);
        projetoExecucao.setEtapa(etapa);

        // Verifica se o colaborador está treinado/qualificado para executar a atividade
        if (!projetoService.validarTreinamentosColaboradorParaExecutarAtividade(projetoExecucao)) {
            throw new NegocioException("Colaborador " + projetoExecucao.getColaborador().getNome() + " não possui treinamento/qualificação para executar esta etapa.");
        }

        // Grava a atividade prevista por conta dos EPIs/EPCs que podem ter sido incluídos/removidos pelo usuário
        atividadePrevista = projetoRepository.guardarAtividadePrevista(atividadePrevista);

        // Busca o registro de ponto do colaborador na data da execução
        ColaboradorPonto colaboradorPonto = colaboradorPontoRepository.pontoColaborador(projetoExecucao.getColaborador(), projetoExecucao.getData());

        // Caso ainda não tenha o registro, cria o mesmo
        if (colaboradorPonto == null) {
            colaboradorPonto = new ColaboradorPonto();
            colaboradorPonto.setData(projetoExecucao.getData());
            colaboradorPonto.setDiaSemana(Util.diaDaSemanaEnum(colaboradorPonto.getData()));
            colaboradorPonto.setColaborador(projetoExecucao.getColaborador());
            colaboradorPonto.setHoraEntrada(projetoExecucao.getSaidaEmpresa());
            // Sugere o tipo de registro
            colaboradorPonto.setTipoRegistro(colaboradorPontoService.sugerirTipoRegistroPonto(colaboradorPonto));
            colaboradorPontoRepository.guardar(colaboradorPonto);
        }

        // Grava o usuário e a data/hora que a execução foi incluída/alterada
        projetoExecucao.setUsuario(usuarioLogado.getUsuario());
        projetoExecucao.setDataAlteracao(new Date());

        // Grava a execução
        projetoRepository.guardarProjetoExecucao(projetoExecucao);

        // Atualiza a movimentação do veículo ligada à programação
        VeiculoMovimento veiculoMovimento = getVeiculoMovimento();

        if (veiculoMovimento != null) {
            veiculoMovimento.setHoraSaida(saidaEmpresa);

            if (kmVeiculo != null && kmVeiculo > 0) {
                veiculoMovimento.setKmInicial(kmVeiculo);
            }

            // Grava a movimentação do veículo
            veiculoRepository.guardarMovimento(veiculoMovimento);
        }

        // Recarrega a grid de atividades previstas
        pesquisar();

        FacesUtil.addInfoMessageGrowl("Execução gerada com sucesso");
    }

    @Transactional
    public void gerarExecucaoRetorno() throws ParseException {

        if (atividadePrevista == null) {
            throw new NegocioException("Não foi possível carregar os dados da atividade prevista");
        }

        if (atividadePrevista.getSituacao().equals(ESituacaoAtividadePrevista.FINALIZADO)) {
            throw new NegocioException("Essa atividade já está finalizada");
        }

        // Busca a última execução vinculada à atividade prevista
        ProjetoExecucao projetoExecucao = execucaoRepository.ultimaExecucaoDaAtividadePrevista(atividadePrevista);

        // O usuário ainda não gerou a execução (saída) p/ a atividade prevista
        if (projetoExecucao == null || projetoExecucao.isNovo()) {
            throw new NegocioException("Essa atividade ainda não possui a execução de saída");
        }

        if (horaTermino == null) {
            throw new NegocioException("O horário de término da atividade deve ser informado");
        }

        if (retornoEmpresa == null) {
            throw new NegocioException("O horário de retorno na empresa deve ser informado");
        }

        if (horaTermino.getTime() > retornoEmpresa.getTime()) {
            throw new NegocioException("O horário de término da atividade deve ser menor ou igual que o horário de retorno na empresa");
        }

        if (horaTermino.getTime() <= projetoExecucao.getHoraInicio().getTime()) {
            throw new NegocioException("O horário de término deve ser maior que o início da última execução: " + Util.formatarHora(projetoExecucao.getHoraInicio()));
        }

        if (isMostrarCheckTrocarMotorista() && checkTrocarMotorista && novoMotorista == null) {
            throw new NegocioException("É necessário selecionar o novo motorista");
        }

        // Somente se o usuário for administrador ou responsável pelo projeto poderá alterar a situação de RETRABALHO para outra situação ou vice-versa
        if (!isAdmOuResponsavel()) {
            // Percorre todas as etapas da execução (que são apresentadas na grid)
            for (ProjetoEtapa e : etapasExecucao) {
                // Busca a etapa no banco de dados
                ProjetoEtapa etapaOld = projetoRepository.porIdProjetoEtapa(e.getId());

                if (etapaOld != null) {
                    if (etapaOld.getSituacao().equals(ESituacaoEtapa.RETRABALHO) && !e.getSituacao().equals(ESituacaoEtapa.RETRABALHO)) {
                        throw new NegocioException("Usuário sem permissão para alterar a situação da etapa de Retrabalho para outra situação");
                    }
                    if (!etapaOld.getSituacao().equals(ESituacaoEtapa.RETRABALHO) && e.getSituacao().equals(ESituacaoEtapa.RETRABALHO)) {
                        throw new NegocioException("Usuário sem permissão para alterar a situação da etapa para Retrabalho");
                    }
                }
            }
        }

        // Busca a movimentação do veiculo ligada à programação
        VeiculoMovimento veiculoMovimento = getVeiculoMovimento();

        // Atualiza a km final, horário de retorno e novo motorista (quando selecionado) da movimentação 
        if (veiculoMovimento != null) {
            if (kmVeiculo != null && kmVeiculo > 0) {
                if (veiculoMovimento.getKmInicial() >= kmVeiculo) {
                    throw new NegocioException("A Km final deve ser maior que a Km inicial (" + veiculoMovimento.getKmInicial().toString() + ") desta movimentação");
                }
                veiculoMovimento.setKmFinal(kmVeiculo);
            }

            if (isMostrarCheckTrocarMotorista() && checkTrocarMotorista && novoMotorista != null) {
                veiculoMovimento.setColaborador(novoMotorista);
            }

            veiculoMovimento.setHoraRetorno(retornoEmpresa);

            // Grava a movimentação do veículo
            veiculoRepository.guardarMovimento(veiculoMovimento);
        }

        // Atualiza a execução com a hora de término da atividade e hora de retorno à empresa
        projetoExecucao.setHoraTermino(horaTermino);
        projetoExecucao.setRetornoEmpresa(retornoEmpresa);
        // Calcula o Valor Hora, Total Horas e Valor Total
        projetoExecucao.calcularTotaisValorHora();

        // Grava o usuário e a data/hora que a execução foi incluída/alterada
        projetoExecucao.setUsuario(usuarioLogado.getUsuario());
        projetoExecucao.setDataAlteracao(new Date());

        // Grava a execução
        projetoExecucao = projetoRepository.guardarProjetoExecucao(projetoExecucao);

        // Atualiza a situação das etapas do projeto
        for (ProjetoEtapa e : etapasExecucao) {
            // Calcula os campos 'Horas Executadas' e 'Percentual Executado' da etapa ligada à execução
            if (projetoExecucao.getEtapa() != null && projetoExecucao.getEtapa().equals(e)) {
                e = projetoService.calcularHorasExecucaoEtapa(e);
            }
            projetoRepository.guardarProjetoEtapa(e);
        }

        // Atualiza a situação da atividade para Finalizado
        atividadePrevista.setSituacao(ESituacaoAtividadePrevista.FINALIZADO);

        // Grava a atividade prevista
        projetoRepository.guardarAtividadePrevista(atividadePrevista);

        // Busca o registro de ponto do colaborador na data da execução
        ColaboradorPonto colaboradorPonto = colaboradorPontoRepository.pontoColaborador(projetoExecucao.getColaborador(), projetoExecucao.getData());

        // Caso tenha o registro, grava a hora de retorno à empresa como sendo a hora fim do ponto
        if (colaboradorPonto != null) {
            // TODO - quando já possui a hora de saída informada, manter ou atualizar?
            colaboradorPonto.setHoraSaida(projetoExecucao.getRetornoEmpresa());

            // Total de horas executadas em projetos
            colaboradorPonto = colaboradorPontoService.calcularHorasExecucoesProjetos(colaboradorPonto);
            // Total de horas extras lançadas
            colaboradorPonto = colaboradorPontoService.calcularHorasAtividadesExtras(colaboradorPonto);

            // Calcula os campos de totais
            colaboradorPonto.calcularTotais();

            // Grava o registro no banco de dados
            colaboradorPontoRepository.guardar(colaboradorPonto);
        }

        // Recarrega a grid de atividades previstas
        pesquisar();

        FacesUtil.addInfoMessageGrowl("Retorno da execução gerado com sucesso");
    }

    public void inicializarPausa() {

        if (atividadePrevista == null) {
            throw new NegocioException("Não foi possível carregar os dados da atividade prevista");
        }

        if (atividadePrevista.getPausa().equals(ESimNao.SIM)) {
            throw new NegocioException("Esta ação não é permitida pois a execução se encontra em pausa.");
        }

        // Busca a última execução vinculada à atividade prevista
        ProjetoExecucao projetoExecucao = execucaoRepository.ultimaExecucaoDaAtividadePrevista(atividadePrevista);

        // O usuário ainda não gerou a execução (saída) p/ a atividade prevista
        if (projetoExecucao == null || projetoExecucao.isNovo()) {
            throw new NegocioException("Essa atividade ainda não possui a execução de saída");
        }

        horaPausa = null;
        pausaAlmoco = false;
    }

    public void inicializarRetomada() {

        if (atividadePrevista == null) {
            throw new NegocioException("Não foi possível carregar os dados da atividade prevista");
        }

        if (atividadePrevista.getPausa().equals(ESimNao.NAO)) {
            throw new NegocioException("Esta ação não é permitida pois a execução não se encontra em pausa.");
        }

        // Busca a última execução vinculada à atividade prevista
        ProjetoExecucao projetoExecucao = execucaoRepository.ultimaExecucaoDaAtividadePrevista(atividadePrevista);

        // O usuário ainda não gerou a execução (saída) p/ a atividade prevista
        if (projetoExecucao == null || projetoExecucao.isNovo()) {
            throw new NegocioException("Essa atividade ainda não possui a execução de saída");
        }

        horaRetomada = null;
    }

    @Transactional
    public void pausarExecucao() {

        if (atividadePrevista == null) {
            throw new NegocioException("Não foi possível carregar os dados da atividade prevista");
        }

        // Busca a última execução vinculada à atividade prevista
        ProjetoExecucao projetoExecucao = execucaoRepository.ultimaExecucaoDaAtividadePrevista(atividadePrevista);

        // O usuário ainda não gerou a execução (saída) p/ a atividade prevista
        if (projetoExecucao == null || projetoExecucao.isNovo()) {
            throw new NegocioException("Essa atividade ainda não possui a execução de saída");
        }

        // Consiste a hora da pausa informada pelo usuário
        if (horaPausa.getTime() <= projetoExecucao.getHoraInicio().getTime()) {
            throw new NegocioException("A pausa deve iniciar após o início da última execução: " + Util.formatarHora(projetoExecucao.getHoraInicio()));
        }

        // Grava a hora término da execução aberta
        projetoExecucao.setHoraTermino(horaPausa);
        // Calcula o Valor Hora, Total Horas e Valor Total
        projetoExecucao.calcularTotaisValorHora();

        // Motivo da pausa
        if (motivoPausa != null) {
            projetoExecucao.setObservacao(motivoPausa);
        }

        // Grava o usuário e a data/hora que a execução foi incluída/alterada
        projetoExecucao.setUsuario(usuarioLogado.getUsuario());
        projetoExecucao.setDataAlteracao(new Date());

        // Grava a execução
        projetoRepository.guardarProjetoExecucao(projetoExecucao);

        // Grava a atividade prevista
        atividadePrevista.setPausa(ESimNao.SIM);
        projetoRepository.guardarAtividadePrevista(atividadePrevista);

        // Se for pausa para o almoço atualiza no ponto
        if (isPausaAlmoco()) {
            // Busca o registro de ponto do colaborador na data da execução
            ColaboradorPonto colaboradorPonto = colaboradorPontoRepository.pontoColaborador(atividadePrevista.getColaborador(), atividadePrevista.getData());

            if (colaboradorPonto != null) {
                colaboradorPonto.setHoraSaidaAlmoco(horaPausa);
                colaboradorPontoRepository.guardar(colaboradorPonto);
            }
        }

        // Recarrega a grid de atividades previstas
        pesquisar();

        FacesUtil.addInfoMessageGrowl("Pausa da execução gerada com sucesso");
    }

    @Transactional
    public void retomarExecucao() {

        if (atividadePrevista == null) {
            throw new NegocioException("Não foi possível carregar os dados da atividade prevista");
        }

        // Busca a última execução vinculada à atividade prevista
        ProjetoExecucao projetoExecucao = execucaoRepository.ultimaExecucaoDaAtividadePrevista(atividadePrevista);

        // O usuário ainda não gerou a execução (saída) p/ a atividade prevista
        if (projetoExecucao == null || projetoExecucao.isNovo()) {
            throw new NegocioException("Essa atividade ainda não possui a execução de saída");
        }

        // Consiste a hora de retomada informada pelo usuário
        if (horaRetomada.getTime() <= projetoExecucao.getHoraTermino().getTime()) {
            throw new NegocioException("A retomada deve iniciar após o término da última execução: " + Util.formatarHora(projetoExecucao.getHoraTermino()));
        }

        ProjetoExecucao novaExecucao = new ProjetoExecucao();
        novaExecucao.setData(atividadePrevista.getData());
        novaExecucao.setAtividadePrevista(atividadePrevista);
        novaExecucao.setColaborador(atividadePrevista.getColaborador());
        novaExecucao.setProjeto(atividadePrevista.getProjeto());
        novaExecucao.setRetrabalho(ESimNao.NAO);
        novaExecucao.setHoraInicio(horaRetomada);
        novaExecucao.setSaidaEmpresa(projetoExecucao.getSaidaEmpresa());
        novaExecucao.setEtapa(projetoExecucao.getEtapa());

        // Grava o usuário e a data/hora que a execução foi incluída/alterada
        novaExecucao.setUsuario(usuarioLogado.getUsuario());
        novaExecucao.setDataAlteracao(new Date());

        // Grava a nova execução
        projetoRepository.guardarProjetoExecucao(novaExecucao);

        // Grava a atividade prevista
        atividadePrevista.setPausa(ESimNao.NAO);
        projetoRepository.guardarAtividadePrevista(atividadePrevista);

        // Se for retorno do almoço atualiza no ponto
        if (isPausaAlmoco()) {
            // Busca o registro de ponto do colaborador na data da execução
            ColaboradorPonto colaboradorPonto = colaboradorPontoRepository.pontoColaborador(atividadePrevista.getColaborador(), atividadePrevista.getData());

            if (colaboradorPonto != null) {
                colaboradorPonto.setHoraRetornoAlmoco(horaRetomada);
                colaboradorPontoRepository.guardar(colaboradorPonto);
            }
        }

        // Recarrega a grid de atividades previstas
        pesquisar();

        FacesUtil.addInfoMessageGrowl("Retomada da execução gerada com sucesso");
    }

    public void incluirEquipamentoAtividadePrevista() {
        if (equipamentoSelecionado == null) {
            FacesUtil.addWarnMessage("Selecione um equipamento para adicionar");
        } else {
            boolean adicionado = false;

            for (ProjetoAtividadePrevistaEquipamento epi : atividadePrevista.getEquipamentos()) {
                if (epi.getEquipamentoProtecao().equals(equipamentoSelecionado)) {
                    adicionado = true;
                    FacesUtil.addWarnMessage("Equipamento já vinculado na atividade prevista");
                    break;
                }
            }

            if (!adicionado) {
                atividadePrevista.getEquipamentos().add(new ProjetoAtividadePrevistaEquipamento(atividadePrevista, equipamentoSelecionado));
                equipamentoSelecionado = new EquipamentoProtecao();
            }
        }
    }

    public void excluirEquipamentoAtividadePrevista() {
        if (equipamentoAtividade != null) {
            boolean podeExcluir = true;

            // Somente poderá excluir os equipamentos que o próprio usuário incluiu
            for (EquipamentoProtecao epi : equipamentosProtecao) {
                if (equipamentoAtividade.getEquipamentoProtecao().equals(epi)) {
                    podeExcluir = false;
                    FacesUtil.addWarnMessage("Este equipamento não pode ser excluído pois é de uso obrigatório");
                    break;
                }
            }

            if (podeExcluir) {
                atividadePrevista.getEquipamentos().remove(equipamentoAtividade);
            }
        }
    }

    public void incluirDispositivoAtividadePrevista() {
        if (dispositivoSelecionado == null) {
            FacesUtil.addWarnMessage("Selecione um dispositivo para adicionar");
        } else {
            boolean adicionado = false;

            for (ProjetoAtividadePrevistaDispositivo epc : atividadePrevista.getDispositivos()) {
                if (epc.getDispositivoSeguranca().equals(dispositivoSelecionado)) {
                    adicionado = true;
                    FacesUtil.addWarnMessage("Dispositivo já vinculado na atividade prevista");
                    break;
                }
            }

            if (!adicionado) {
                atividadePrevista.getDispositivos().add(new ProjetoAtividadePrevistaDispositivo(atividadePrevista, dispositivoSelecionado));
                dispositivoSelecionado = new DispositivoSeguranca();
            }
        }
    }

    public void excluirDispositivoAtividadePrevista() {
        if (dispositivoAtividade != null) {
            boolean podeExcluir = true;

            // Somente poderá excluir os dispositivos que o próprio usuário incluiu
            for (DispositivoSeguranca epc : dispositivosSeguranca) {
                if (dispositivoAtividade.getDispositivoSeguranca().equals(epc)) {
                    podeExcluir = false;
                    FacesUtil.addWarnMessage("Este dispositivo não pode ser excluído pois é de uso obrigatório");
                    break;
                }
            }

            if (podeExcluir) {
                atividadePrevista.getDispositivos().remove(dispositivoAtividade);
            }
        }
    }

    public void incluirEtapaExecucao() {
        if (etapaSelecionada == null) {
            FacesUtil.addWarnMessage("Selecione uma etapa para adicionar");
        } else {
            if (etapasExecucao.contains(etapaSelecionada)) {
                FacesUtil.addWarnMessage("Etapa já adicionada na execução");
            } else {
                etapaSelecionada.setSituacao(ESituacaoEtapa.PENDENTE);
                etapasExecucao.add(etapaSelecionada);
                etapaSelecionada = new ProjetoEtapa();
            }
        }
    }

    private void carregarEtapas() {
        etapasExecucao = new ArrayList<>();

        if (temProgramacao()) {
            // Etapa da programação
            if (atividadePrevista.getProgramacaoExecucao().getEtapa() != null) {
                etapasExecucao.add(atividadePrevista.getProgramacaoExecucao().getEtapa());
            }
            // Outras Etapas da programação
            ProgramacaoExecucao programacaoE = programacaoExecucaoRepository.etapasDaProgramacao(atividadePrevista.getProgramacaoExecucao());
            if (programacaoE != null && programacaoE.getEtapas() != null) {
                for (ProjetoEtapa e : programacaoE.getEtapas()) {
                    etapasExecucao.add(e);
                }
            }
        } else {
            // Etapa da atividade prevista
            if (atividadePrevista.getEtapa() != null) {
                etapasExecucao.add(atividadePrevista.getEtapa());
            }
        }
    }

    private void carregarMotoristas() {
        motoristas = new ArrayList<>();

        ProgramacaoExecucao programacao = atividadePrevista.getProgramacaoExecucao();

        // Verifica se tem programação e se o colaborador é motorista
        if (programacao != null && programacao.getMotorista().equals(ESimNao.SIM)) {
            ProgramacaoExecucao programacaoP = programacaoExecucaoRepository.participantesDaProgramacao(atividadePrevista.getProgramacaoExecucao());
            if (programacaoP != null && programacaoP.getParticipantes() != null) {
                for (ProgramacaoExecucaoColaborador c : programacaoP.getParticipantes()) {
                    motoristas.add(c.getColaborador());
                }
            }
        }
    }

    public boolean isAdmOuResponsavel() {
        if (seguranca.visualizaProjetos()) {
            return true;
        }
        if (atividadePrevista != null && usuarioLogado.getColaborador().equals(atividadePrevista.getProjeto().getResponsavel())) {
            return true;
        }
        return false;
    }

    // Para carregar as opções do Enum no formulário
    public ESituacaoEtapa[] getSituacoesEtapa() {
        return ESituacaoEtapa.values();
    }

    public void limparPesquisa() {
        atvPrevistaFiltrados = new ArrayList<>();
    }

    public void pesquisar() {
        if (filtro == null) {
            filtro = new ExecucaoFilter();
        }

        filtro.setColaborador(filtroColaborador);
        filtro.setData(filtroCompetencia);
        filtro.setSituacao(ESituacaoAtividadePrevista.PENDENTE);

        atvPrevistaFiltrados = execucaoRepository.atividadesPrevisas(filtro);

        // Faz isso apenas para carregar os dados e evitar o erro LazyInitializationException		
        for (ProjetoAtividadePrevista a : atvPrevistaFiltrados) {
            a.getEquipamentos().size();
            a.getDispositivos().size();
        }
    }

    public void onChangeFiltro() {
        limparPesquisa();
    }

    public void onChangeSituacaoEtapa(ProjetoEtapa etapa) {
        // Somente se o usuário for administrador ou responsável pelo projeto poderá alterar a situação de RETRABALHO para outra situação ou vice-versa
        if (!isAdmOuResponsavel() && etapa != null) {
            // Busca a etapa no banco de dados
            ProjetoEtapa etapaOld = projetoRepository.porIdProjetoEtapa(etapa.getId());

            if (etapaOld != null) {
                if (etapaOld.getSituacao().equals(ESituacaoEtapa.RETRABALHO) && !etapa.getSituacao().equals(ESituacaoEtapa.RETRABALHO)) {
                    etapa.setSituacao(etapaOld.getSituacao());
                    throw new NegocioException("Usuário sem permissão para alterar a situação da etapa de Retrabalho para outra situação");
                }
                if (!etapaOld.getSituacao().equals(ESituacaoEtapa.RETRABALHO) && etapa.getSituacao().equals(ESituacaoEtapa.RETRABALHO)) {
                    etapa.setSituacao(etapaOld.getSituacao());
                    throw new NegocioException("Usuário sem permissão para alterar a situação da etapa para Retrabalho");
                }
            }
        }
    }

    public Veiculo getVeiculo() {
        if (temProgramacao()) {
            return atividadePrevista.getProgramacaoExecucao().getVeiculo();
        }
        return null;
    }

    public String getInfoVeiculo() {
        Veiculo veiculo = getVeiculo();
        if (veiculo != null) {
            return veiculo.getModelo().getDescricao() + " - " + veiculo.getPlaca();
        }
        return "";
    }

    public VeiculoMovimento getVeiculoMovimento() {
        if (temProgramacao()) {
            return programacaoExecucaoRepository.movimentacaoVeiculoDaProgramacaoExecucao(atividadePrevista.getProgramacaoExecucao());
        }
        return null;
    }

    public boolean temProgramacao() {
        return (atividadePrevista != null && atividadePrevista.getProgramacaoExecucao() != null);
    }

    public boolean temEquipamentoProtecao() {
        return equipamentosProtecao != null && !equipamentosProtecao.isEmpty();
    }

    public boolean temDispositivoSeguranca() {
        return dispositivosSeguranca != null && !dispositivosSeguranca.isEmpty();
    }

    public boolean isPossuiVeiculo() {
        return (temProgramacao() && atividadePrevista.getProgramacaoExecucao().getVeiculo() != null);
    }

    public boolean isPossuiVeiculoMovimento() {
        return (getVeiculoMovimento() != null);
    }

    public boolean isMostrarCheckEPI() {
        return (temEquipamentoProtecao() || temDispositivoSeguranca());
    }

    public boolean isMostrarCheckTrocarMotorista() {
        if (temProgramacao()) {
            return atividadePrevista.getProgramacaoExecucao().getMotorista().equals(ESimNao.SIM);
        }
        return false;
    }

    public ProjetoAtividadePrevista getAtividadePrevista() {
        return atividadePrevista;
    }

    public void setAtividadePrevista(ProjetoAtividadePrevista atividadePrevista) {
        this.atividadePrevista = atividadePrevista;
    }

    public Colaborador getNovoMotorista() {
        return novoMotorista;
    }

    public void setNovoMotorista(Colaborador novoMotorista) {
        this.novoMotorista = novoMotorista;
    }

    public Date getSaidaEmpresa() {
        return saidaEmpresa;
    }

    public void setSaidaEmpresa(Date saidaEmpresa) {
        this.saidaEmpresa = saidaEmpresa;
    }

    public Date getRetornoEmpresa() {
        return retornoEmpresa;
    }

    public void setRetornoEmpresa(Date retornoEmpresa) {
        this.retornoEmpresa = retornoEmpresa;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Date getHoraTermino() {
        return horaTermino;
    }

    public void setHoraTermino(Date horaTermino) {
        this.horaTermino = horaTermino;
    }

    public Date getHoraPausa() {
        return horaPausa;
    }

    public void setHoraPausa(Date horaPausa) {
        this.horaPausa = horaPausa;
    }

    public Date getHoraRetomada() {
        return horaRetomada;
    }

    public void setHoraRetomada(Date horaRetomada) {
        this.horaRetomada = horaRetomada;
    }

    public Integer getKmVeiculo() {
        return kmVeiculo;
    }

    public void setKmVeiculo(Integer kmVeiculo) {
        this.kmVeiculo = kmVeiculo;
    }

    public String getMotivoPausa() {
        return motivoPausa;
    }

    public void setMotivoPausa(String motivoPausa) {
        this.motivoPausa = motivoPausa;
    }

    public boolean isCheckEPI() {
        return checkEPI;
    }

    public void setCheckEPI(boolean checkEPI) {
        this.checkEPI = checkEPI;
    }

    public boolean isCheckTrocarMotorista() {
        return checkTrocarMotorista;
    }

    public void setCheckTrocarMotorista(boolean checkTrocarMotorista) {
        this.checkTrocarMotorista = checkTrocarMotorista;
    }

    public boolean isPausaAlmoco() {
        return pausaAlmoco;
    }

    public void setPausaAlmoco(boolean pausaAlmoco) {
        this.pausaAlmoco = pausaAlmoco;
    }

    public List<Colaborador> getParticipantes() {
        return participantes;
    }

    public List<ProjetoEtapa> getEtapasExecucao() {
        return etapasExecucao;
    }

    public List<ProjetoEtapa> getEtapasProjeto() {
        return etapasProjeto;
    }

    public List<EquipamentoProtecao> getEquipamentosProtecao() {
        return equipamentosProtecao;
    }

    public List<DispositivoSeguranca> getDispositivosSeguranca() {
        return dispositivosSeguranca;
    }

    public ExecucaoFilter getFiltro() {
        return filtro;
    }

    public void setFiltro(ExecucaoFilter filtro) {
        this.filtro = filtro;
    }

    public List<ProjetoAtividadePrevista> getAtvPrevistaFiltrados() {
        return atvPrevistaFiltrados;
    }

    public void setAtvPrevistaFiltrados(List<ProjetoAtividadePrevista> atvPrevistaFiltrados) {
        this.atvPrevistaFiltrados = atvPrevistaFiltrados;
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }

    public List<Colaborador> getMotoristas() {
        return motoristas;
    }

    public List<EquipamentoProtecao> getEquipamentosDisponiveis() {
        return equipamentosDisponiveis;
    }

    public List<DispositivoSeguranca> getDispositivosDisponiveis() {
        return dispositivosDisponiveis;
    }

    public ProjetoAtividadePrevistaEquipamento getEquipamentoAtividade() {
        return equipamentoAtividade;
    }

    public void setEquipamentoAtividade(ProjetoAtividadePrevistaEquipamento equipamentoAtividade) {
        this.equipamentoAtividade = equipamentoAtividade;
    }

    public ProjetoAtividadePrevistaDispositivo getDispositivoAtividade() {
        return dispositivoAtividade;
    }

    public void setDispositivoAtividade(ProjetoAtividadePrevistaDispositivo dispositivoAtividade) {
        this.dispositivoAtividade = dispositivoAtividade;
    }

    public EquipamentoProtecao getEquipamentoSelecionado() {
        return equipamentoSelecionado;
    }

    public void setEquipamentoSelecionado(EquipamentoProtecao equipamentoSelecionado) {
        this.equipamentoSelecionado = equipamentoSelecionado;
    }

    public DispositivoSeguranca getDispositivoSelecionado() {
        return dispositivoSelecionado;
    }

    public void setDispositivoSelecionado(DispositivoSeguranca dispositivoSelecionado) {
        this.dispositivoSelecionado = dispositivoSelecionado;
    }

    public ProjetoEtapa getEtapaSelecionada() {
        return etapaSelecionada;
    }

    public void setEtapaSelecionada(ProjetoEtapa etapaSelecionada) {
        this.etapaSelecionada = etapaSelecionada;
    }

    public Colaborador getFiltroColaborador() {
        return filtroColaborador;
    }

    public void setFiltroColaborador(Colaborador filtroColaborador) {
        this.filtroColaborador = filtroColaborador;
    }

    public Date getFiltroCompetencia() {
        return filtroCompetencia;
    }

    public void setFiltroCompetencia(Date filtroCompetencia) {
        this.filtroCompetencia = filtroCompetencia;
    }

}

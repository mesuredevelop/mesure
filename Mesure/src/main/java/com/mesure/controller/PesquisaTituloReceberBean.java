package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.model.TituloReceber;
import com.mesure.repository.TituloReceberRepository;
import com.mesure.repository.filter.TituloReceberFilter;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;

@Named
@ViewScoped
public class PesquisaTituloReceberBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    TituloReceberRepository tituloReceberRepository;

    @Inject
    Seguranca seguranca;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    private TituloReceberFilter filtro;
    private List<TituloReceber> tituloReceberFiltrados;

    public PesquisaTituloReceberBean() {
        filtro = new TituloReceberFilter();
    }

    @PostConstruct
    public void inicializar() {

    }

    public void pesquisar() {
        if (filtro == null) {
            filtro = new TituloReceberFilter();
        }

        // Para trazer somente titulos da filial ativa
        filtro.setFilial(usuarioLogado.getFilialAtiva());

        tituloReceberFiltrados = tituloReceberRepository.filtrados(filtro);
    }

    public TituloReceberFilter getFiltro() {
        return filtro;
    }

    public List<TituloReceber> getTituloReceberFiltrados() {
        return tituloReceberFiltrados;
    }

}

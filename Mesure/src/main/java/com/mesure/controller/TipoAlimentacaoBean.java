package com.mesure.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mesure.enumerator.EEstado;
import com.mesure.enumerator.EPeriodoAlimentacao;
import com.mesure.model.TipoAlimentacao;
import com.mesure.repository.TipoAlimentacaoRepository;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.util.jsf.FacesUtil;

@Named
@ViewScoped
public class TipoAlimentacaoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	TipoAlimentacaoRepository tipoAlimentacaoRepository;
	
	@Inject
	@UsuarioLogado
	private UsuarioSistema usuarioLogado;	
	
	private List<TipoAlimentacao> tipos;	
	private TipoAlimentacao tipo;
	
	public TipoAlimentacaoBean() {
		limpar();
	}
	
	public void salvar() {
		tipo.setFilial(usuarioLogado.getFilialAtiva());
		tipo = tipoAlimentacaoRepository.guardar(tipo);
		
		if (!tipos.contains(tipo)) {
			tipos.add(tipo);
		}
		
		limpar();		
		FacesUtil.addInfoMessageGrowl("Gravação efetuada com sucesso.");
	}	
	
	public void excluir() {
		if (tipo != null) {
			tipoAlimentacaoRepository.remover(tipo);
			tipos.remove(tipo);
			
			FacesUtil.addInfoMessageGrowl("Registro excluído com sucesso.");
		}
	}
	
	public void pesquisar() {
		tipos = tipoAlimentacaoRepository.obterTiposAlimentacao(usuarioLogado.getFilialAtiva(), null);
	}
	
	// Para carregar as opções do Enum no formulário
	public EPeriodoAlimentacao[] getperiodos() {
		return EPeriodoAlimentacao.values();
	}	
	
	// Para carregar as opções do Enum no formulário
	public EEstado[] getEstados() {
		return EEstado.values();
	}	
	
	public String getFilialAtiva() {
		String filialAtiva;
		
		if (tipo.getFilial() == null) {
			filialAtiva = usuarioLogado.getFilialAtiva().getNome();
		} else {
			filialAtiva = tipo.getFilial().getNome();
		}
		return filialAtiva;
	}

	public List<TipoAlimentacao> getTipos() {
		return tipos;
	}

	public void setTipos(List<TipoAlimentacao> tipos) {
		this.tipos = tipos;
	}

	public TipoAlimentacao getTipo() {
		tipo.setFilial(usuarioLogado.getFilialAtiva());
		return tipo;
	}

	public void setTipo(TipoAlimentacao tipo) {
		this.tipo = tipo;
	}

	public void limpar() {
		this.tipo = new TipoAlimentacao();
	}

}

package com.mesure.controller;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.inject.Inject;
import javax.inject.Named;

@Named
@ApplicationScoped
public class UtilBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String contextPath;

	@Inject
	FacesContext facesContext;
	
	@Inject
	ExternalContext externalContext;
	
	public UtilBean() {
		facesContext = FacesContext.getCurrentInstance();
		externalContext = facesContext.getExternalContext();
		
		setContextPath(externalContext.getRequestContextPath());
	}
	
	public String getUrlLogo() {
		return getContextPath() + "/resources/images/logo-mesure-new.png";
	}
	
	public String getUrlHome() {
		return getContextPath() + "/Home.xhtml";
	}	
	
	public String getUrlLogout() {
		return getContextPath() + "/logout";
	}
	
	public String getDataTableCustonLang() {
		return getContextPath() + "/resources/json/Portuguese-Brasil.json";
	}
	
	public String getTimeZone() {
		return "";
	}
	
	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
	
}


package com.mesure.controller;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.model.Cliente;
import com.mesure.model.Projeto;
import com.mesure.model.ProjetoAtividadePrevista;
import com.mesure.repository.ClienteRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class KanBanAtividadesProjetoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    ClienteRepository clienteRepository;

    @Inject
    ProjetoRepository projetoRepository;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    private List<Cliente> clientes;
    private List<Projeto> projetos;

    private List<ProjetoAtividadePrevista> atividadesPendentes;
    private List<ProjetoAtividadePrevista> atividadesEmExecucao;
    private List<ProjetoAtividadePrevista> atividadesFinalizadas;

    // filtro de pesquisa
    private Cliente filtroCliente;
    private Projeto filtroProjeto;

    public void inicializar() {
        // Carrega os clientes para o filtro
        clientes = clienteRepository.clientes();

        // Carrega os projetos para o filtro
        carregarFiltroProjetos();

        atividadesPendentes = new ArrayList<>();
        atividadesEmExecucao = new ArrayList<>();
        atividadesFinalizadas = new ArrayList<>();
    }

    private void carregarFiltroProjetos() {
        ProjetoFilter projetoFilter = new ProjetoFilter();
        // Somente projetos em aberto
        EStatusProjeto[] statuses = {EStatusProjeto.COMECAR, EStatusProjeto.ANDAMENTO, EStatusProjeto.ATV_EXTERNA};
        projetoFilter.setStatuses(statuses);
        if (usuarioLogado.getFilialAtiva().getFiltrarProjetos().equals(ESimNao.SIM)) {
            projetoFilter.setFilial(usuarioLogado.getFilialAtiva());
        }
        projetoFilter.setCliente(filtroCliente);
        projetoFilter.setPropriedadeOrdenacao("titulo");
        projetos = projetoRepository.filtrados(projetoFilter);
    }

    public void pesquisar() {
        atividadesPendentes = projetoRepository.atividadesPendentesDoProjeto(filtroProjeto);
        atividadesEmExecucao = projetoRepository.atividadesEmExecucaoDoProjeto(filtroProjeto);
        atividadesFinalizadas = projetoRepository.atividadesFinalizadasDoProjeto(filtroProjeto);
    }

    public void onChangeCliente() {
        carregarFiltroProjetos();
        filtroProjeto = new Projeto();
    }

    public Cliente getFiltroCliente() {
        return filtroCliente;
    }

    public void setFiltroCliente(Cliente filtroCliente) {
        this.filtroCliente = filtroCliente;
    }

    public Projeto getFiltroProjeto() {
        return filtroProjeto;
    }

    public void setFiltroProjeto(Projeto filtroProjeto) {
        this.filtroProjeto = filtroProjeto;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public List<ProjetoAtividadePrevista> getAtividadesPendentes() {
        return atividadesPendentes;
    }

    public void setAtividadesPendentes(List<ProjetoAtividadePrevista> atividadesPendentes) {
        this.atividadesPendentes = atividadesPendentes;
    }

    public List<ProjetoAtividadePrevista> getAtividadesEmExecucao() {
        return atividadesEmExecucao;
    }

    public void setAtividadesEmExecucao(List<ProjetoAtividadePrevista> atividadesEmExecucao) {
        this.atividadesEmExecucao = atividadesEmExecucao;
    }

    public List<ProjetoAtividadePrevista> getAtividadesFinalizadas() {
        return atividadesFinalizadas;
    }

    public void setAtividadesFinalizadas(List<ProjetoAtividadePrevista> atividadesFinalizadas) {
        this.atividadesFinalizadas = atividadesFinalizadas;
    }

}

package com.mesure.util;

import com.mesure.enumerator.EDiaSemana;
import com.mesure.enumerator.EPeriodoDia;
import com.mesure.model.Feriado;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Years;

public class Util implements Serializable {

    private static final long serialVersionUID = 1L;

    public static String formatString(String string, String mask) throws java.text.ParseException {
        javax.swing.text.MaskFormatter mf = new javax.swing.text.MaskFormatter(mask);
        mf.setValueContainsLiteralCharacters(false);
        return mf.valueToString(string);
    }

    public static String formatCPF(String cpf) throws ParseException {
        return formatString(cpf, "AAA.AAA.AAA-AA");
    }

    public static String formatCNPJ(String cnpj) throws ParseException {
        return formatString(cnpj, "AA.AAA.AAA/AAAA-AA");
    }

    public static boolean validarCPF(String cpf) {
        if (cpf.length() != 11) {
            return false;
        }
        return validarInteiro(cpf);
    }

    public static boolean validarCNPJ(String cnpj) {
        if (cnpj.length() != 14) {
            return false;
        }
        return validarInteiro(cnpj);
    }

    public static boolean validarInteiro(String texto) {
        try {
            Long.parseLong(texto);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean validarDouble(String texto) {
        try {
            Double.parseDouble(texto);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean validarData(String texto) {
        if (texto.length() != 10) {
            return false;
        }
        try {
            SimpleDateFormat fmtData = new SimpleDateFormat("dd/MM/yyyy");
            fmtData.parse(texto);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public static boolean validarHora(String texto) {
        if (texto.length() != 5) {
            return false;
        }
        try {
            SimpleDateFormat fmtHora = new SimpleDateFormat("HH:mm");
            fmtHora.parse(texto);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public static String onlyNumbers(String str) {
        if (str != null) {
            return str.replaceAll("\\D", "");
        } else {
            return "";
        }
    }

    // Retorna o dia da semana conforme a data passada por parâmetro
    public static String diaDaSemana(Date data) {
        Calendar c = new GregorianCalendar();
        c.setTime(data);

        int dia = c.get(Calendar.DAY_OF_WEEK);

        switch (dia) {
            case Calendar.SUNDAY:
                return "Domingo";
            case Calendar.MONDAY:
                return "Segunda-feira";
            case Calendar.TUESDAY:
                return "Terça-feira";
            case Calendar.WEDNESDAY:
                return "Quarta-feira";
            case Calendar.THURSDAY:
                return "Quinta-feira";
            case Calendar.FRIDAY:
                return "Sexta-feira";
            case Calendar.SATURDAY:
                return "Sábado";
        }
        return "";
    }

    // Retorna o dia da semana conforme a data passada por parâmetro
    public static int diaDaSemanaCalendar(Date data) {
        Calendar c = new GregorianCalendar();
        c.setTime(data);
        return c.get(Calendar.DAY_OF_WEEK);
    }

    // Retorna o dia da semana conforme a data passada por parâmetro
    public static EDiaSemana diaDaSemanaEnum(Date data) {
        Calendar c = new GregorianCalendar();
        c.setTime(data);

        int dia = c.get(Calendar.DAY_OF_WEEK);

        switch (dia) {
            case Calendar.SUNDAY:
                return EDiaSemana.DOMINGO;
            case Calendar.MONDAY:
                return EDiaSemana.SEGUNDA_FEIRA;
            case Calendar.TUESDAY:
                return EDiaSemana.TERCA_FEIRA;
            case Calendar.WEDNESDAY:
                return EDiaSemana.QUARTA_FEIRA;
            case Calendar.THURSDAY:
                return EDiaSemana.QUINTA_FEIRA;
            case Calendar.FRIDAY:
                return EDiaSemana.SEXTA_FEIRA;
            case Calendar.SATURDAY:
                return EDiaSemana.SABADO;
        }
        return null;
    }

    // Retorna o período (Matutino / Vespertino / Noturno) conforme o horário passado por parâmetro
    public static EPeriodoDia periodoDia(Date horario) throws ParseException {
        DateFormat df = new SimpleDateFormat("HH:mm");

        Date horaMatIni = df.parse("05:01");
        Date horaMatFim = df.parse("12:00");
        Date horaVesIni = df.parse("12:01");
        Date horaVesFim = df.parse("18:00");

        if (horario.getTime() >= horaMatIni.getTime() && horario.getTime() <= horaMatFim.getTime()) {
            return EPeriodoDia.MATUTINO;
        }
        if (horario.getTime() >= horaVesIni.getTime() && horario.getTime() <= horaVesFim.getTime()) {
            return EPeriodoDia.VESPERTINO;
        }
        return EPeriodoDia.NOTURNO;
    }

    public static int DiferencaEmAnos(Date dataMenor, Date dataMaior) {
        LocalDate dt1 = new LocalDate(dataMenor);
        LocalDate dt2 = new LocalDate(dataMaior);
        Years m = Years.yearsBetween(dt2, dt1);
        return (m.getYears());
    }

    public static int DiferencaEmMeses(Date dataMenor, Date dataMaior) {
        LocalDate dt1 = new LocalDate(dataMenor);
        LocalDate dt2 = new LocalDate(dataMaior);
        Months m = Months.monthsBetween(dt1, dt2);
        return (m.getMonths());
    }

    public static long DiferencaEmDias(Date dataMenor, Date dataMaior) {
        long DAY = 24L * 60L * 60L * 1000L;
        return ((dataMaior.getTime() - dataMenor.getTime()) / DAY);
    }

    // Retorna no formato centesimal (exemplo: 1,50)
    public static double DiferencaEmHoras(Date horaMenor, Date horaMaior) {
        double HOUR = 60L * 60L * 1000L;
        return ((horaMaior.getTime() - horaMenor.getTime()) / HOUR);
    }

    // Retorna no formato de horas (exemplo: 1:30) 
    public static Date DiferencaEmHoras2(Date horaMenor, Date horaMaior) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        Calendar c3 = Calendar.getInstance();

        c1.setTime(horaMenor);
        c2.setTime(horaMaior);

        long diff = c2.getTimeInMillis() - c1.getTimeInMillis();
        long hours = (60 * 60 * 1000);
        long diffHoras = diff / hours;
        long diffHorasMinutos = (diff % hours) / (60 * 1000);

        c3.set(c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH), (int) diffHoras, (int) diffHorasMinutos, 0);

        return c3.getTime();
    }

    public static float converteHoraParaCentesimal(Date hora) throws ParseException {
        return converteHoraParaCentesimal(retornaHoraMSExata(hora));
    }

    public static float converteHoraParaCentesimal(float hora) {
        long HOUR = 60L * 60L * 1000L;
        return (float) (hora / HOUR);
    }

    public static Date converteCentesimalParaHora(float hora) throws ParseException {
        double d = Util.arredondarDouble(hora, 2);
        // vamos converter para segundos primeiro
        long s = (long) (d * 3600);
        // Agora vamos calcular horas, minutos e segundos
        long h = s / 3600;
        long m = (s - 3600 * h) / 60;
        s = s % 60;

        Calendar c = new GregorianCalendar(0, 0, 0, (int) h, (int) m, (int) s);
        return c.getTime();
    }

    public static float retornaHoraMSExata(Date hora) throws ParseException {
        DateFormat df = new SimpleDateFormat("HH:mm");
        Date horaZero = df.parse("00:00");
        return (hora.getTime() - horaZero.getTime());
    }

    public static Date montarHora(String hora) throws ParseException {
        DateFormat df = new SimpleDateFormat("HH:mm");
        return df.parse(hora);
    }

    public static Date montarData(String data) throws ParseException {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.parse(data);
    }

    public static String formatarData(Date data) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.format(data);
    }

    public static String formatarData(Date data, String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(data);
    }

    public static String formatarHora(Date hora) {
        DateFormat df = new SimpleDateFormat("HH:mm");
        return df.format(hora);
    }

    public static String formatarDouble(double valor) {
        DecimalFormat df = new DecimalFormat("#0.00");
        return df.format(valor);
    }

    public static double arredondarDouble(double valor, int escala) {
        return new BigDecimal(valor).setScale(escala, RoundingMode.HALF_UP).floatValue();
    }

    public static String noSpaces(String str) {
        return str.replaceAll(" ", "");
    }

    public static double StrToDouble(String str) {
        str = str.replaceAll("\\.", "");
        str = str.replaceAll(",", ".");
        return Double.parseDouble(str);
    }

    public static String encryptMD5(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        BigInteger hash = new BigInteger(1, md.digest(password.getBytes()));
        return hash.toString(16);
    }

    public static boolean isFeriado(List<Feriado> feriados, Date data) {
        for (Feriado feriado : feriados) {
            if (feriado.getData().equals(data)) {
                return true;
            }
        }
        return false;
    }
}

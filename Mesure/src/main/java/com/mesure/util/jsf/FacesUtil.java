package com.mesure.util.jsf;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import net.bootsfaces.utils.FacesMessages;

public class FacesUtil {

	public static boolean isPostback() {
		return FacesContext.getCurrentInstance().isPostback();
	}
	
	public static boolean isNotPostback() {
		return !isPostback();
	}
	
	public static void addInfoMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));
	}
	
	public static void addWarnMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, message, message));
	}
	
	public static void addErrorMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
	}	
	
	public static void addFatalMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, message, message));
	}
	
	public static void addInfoMessageGrowl(String message) {
		FacesContext context = FacesContext.getCurrentInstance();		
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, message, message);		
		context.addMessage("message-growl", msg);
	}
	
	public static void addInfoMessageGrowl2(String message) {
		FacesMessages.info("frmCadastro:message-growl", message);
	}
	
}

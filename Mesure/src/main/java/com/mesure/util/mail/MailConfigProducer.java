package com.mesure.util.mail;

import java.io.IOException;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.mesure.model.Configuracao;
import com.mesure.repository.ConfiguracaoRepository;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.outjected.email.api.SessionConfig;
import com.outjected.email.impl.SimpleMailConfig;

public class MailConfigProducer {
	
	@Inject
	private ConfiguracaoRepository configuracaoRepository;
	
	@Inject
	@UsuarioLogado
	private UsuarioSistema usuarioLogado;
	
	@Produces	
	@RequestScoped
	//@ApplicationScoped
	public SessionConfig getMailConfig() throws IOException {
		
		SimpleMailConfig config = new SimpleMailConfig();		
		
		Configuracao configuracao = configuracaoRepository.obterConfiguracao(usuarioLogado.getFilialAtiva());
		
		if (configuracao != null) {
			config.setServerHost(configuracao.getMailServerHost());
			config.setServerPort(configuracao.getMailServerPort());
			config.setEnableSsl(Boolean.parseBoolean(configuracao.getMailEnableSsl()));
			config.setAuth(Boolean.parseBoolean(configuracao.getMailAuth()));
			config.setUsername(configuracao.getMailUsername());
			config.setPassword(configuracao.getMailPassword());			
		}		
		
		return config;
	}
	
}
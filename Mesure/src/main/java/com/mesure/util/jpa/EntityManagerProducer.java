package com.mesure.util.jpa;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.tenant.jpa.TenantEntityManagerProducer;

@RequestScoped
public class EntityManagerProducer {
	
	@Inject
	private TenantEntityManagerProducer tenantEntityManagerProducer; 

	@Inject
	@UsuarioLogado
	@RequestScoped
	private UsuarioSistema usuarioSistema;


	@Default
	@Produces
	@RequestScoped
	public EntityManager createEntityManager() {
		return tenantEntityManagerProducer.getEntityManager(usuarioSistema.getUsuario().getTenantId());
	}
	
	public void closeEntityManager(@Disposes @Default EntityManager manager) {
		if (manager.isOpen()) {
			manager.close();
		}
	}

}
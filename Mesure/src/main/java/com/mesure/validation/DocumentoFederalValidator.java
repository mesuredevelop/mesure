package com.mesure.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import com.mesure.util.Util;

public class DocumentoFederalValidator implements ConstraintValidator<DocumentoFederal, String> {
	
	@Override
	public void initialize(DocumentoFederal constraintAnnotation) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		String valueNumber = Util.onlyNumbers(value);
		
		if (StringUtils.isEmpty(valueNumber))
			return true;
		
		if ((valueNumber.length() == 11))
			return CNP.isValidCPF(valueNumber);
		else if ((valueNumber.length() == 14))
			return CNP.isValidCNPJ(valueNumber);
		else
			return false;
	}
}


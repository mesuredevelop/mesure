package com.mesure.enumerator;

public enum EStatusLoteFaturamento {
	PENDENTE("Pendente"),
	PROCESSADO("Processado"),	
	FECHADO("Fechado"),
	LIBERADO("Liberado");
	
	private String label;

	private EStatusLoteFaturamento(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	
}

package com.mesure.enumerator;

public enum EStatusProjeto {
	
	COMECAR("A começar", "#ffff66"),
	ANDAMENTO("Em andamento", "#4da6ff"),	
	FINALIZADO("Finalizado", "#006600"),
	CANCELADO("Cancelado", "#999966"),
	ATV_EXTERNA("Atividade externa","#ffa64d");
	
	private String label;
	private String cor;

	private EStatusProjeto(String label, String cor) {
		this.label = label;
		this.cor = cor;
	}

	public String getLabel() {
		return label;
	}	
	
	public String getCor() {
		return cor;
	}

}


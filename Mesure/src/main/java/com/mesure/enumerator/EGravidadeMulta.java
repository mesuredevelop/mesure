package com.mesure.enumerator;

public enum EGravidadeMulta {
	
	LEVE("Leve"),
	MEDIA("Média"),	
	GRAVE("Grave"),
	GRAVISSIMA("Gravíssima");
	
	private String label;

	private EGravidadeMulta(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	

}


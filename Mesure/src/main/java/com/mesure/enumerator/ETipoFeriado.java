package com.mesure.enumerator;

public enum ETipoFeriado {
	MUNICIPAL("Municipal"),	
	ESTADUAL("Estadual"),
	NACIONAL("Nacional");
	
	private String label;

	private ETipoFeriado(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}

package com.mesure.enumerator;

public enum ETipoContaBancaria {
	CAIXA("Caixa"),	
	CORRENTE("Conta corrente"),
        APLICACAO("Conta aplicação"),
        POUPANCA("Conta poupança"),
        OUTRO("Outras");
	
	private String label;

	private ETipoContaBancaria(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}

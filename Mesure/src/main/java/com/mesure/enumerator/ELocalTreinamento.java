package com.mesure.enumerator;

public enum ELocalTreinamento {
	
	INTERNO("Interno"),
	EXTERNO("Externo");
	
	private String label;

	private ELocalTreinamento(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}

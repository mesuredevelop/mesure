package com.mesure.enumerator;

public enum ECategoriaVeiculo {

	CARRO("Carro"),
	CAMINHAO("Caminhão"),
	CAMINHONETE("Caminhonete"),
	ONIBUS("Ônibus"),
	MICRO_ONIBUS("Microônibus"),
	MOTOCICLETA("Motocicleta"),
	UTILITARIO("Utilitário"),
	OUTRO("Outro");
	
	private String label;

	private ECategoriaVeiculo(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	
	
}


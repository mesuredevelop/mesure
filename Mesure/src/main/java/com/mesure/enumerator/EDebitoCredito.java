package com.mesure.enumerator;

public enum EDebitoCredito {
	DEBITO("Debito"),
	CREDITO("Credito");
	
	private String label;

	private EDebitoCredito(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}

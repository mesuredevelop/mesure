package com.mesure.enumerator;

public enum ETipoRegistroPonto {
	NORMAL("Normal"),
	ATESTADO("Atestado"),	
	FALTA("Falta"),
	FOLGA("Folga"),
	FERIADO("Feriado"),
	FDSEMANA("Final de semana");
	
	private String label;

	private ETipoRegistroPonto(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}

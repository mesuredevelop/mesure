package com.mesure.enumerator;

public enum ETipoEndereco {
	
	OBRA("Obra"),
	COMERCIAL("Comercial"),	
	FINANCEIRO("Financeiro");
	
	private String label;

	private ETipoEndereco(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}		

}


package com.mesure.enumerator;

public enum EPeriodicidadeAlerta {

	DIARIO("Diário"),
	SEMANAL("Semanal"),
	QUINZENAL("Quinzenal"),
	MENSAL("Mensal");
	
	private String label;

	private EPeriodicidadeAlerta(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}

package com.mesure.enumerator;

public enum EPeriodoDia {

	MATUTINO("Matutino"),
	VESPERTINO("Vespertino"),	
	NOTURNO("Noturno");
	
	private String label;

	private EPeriodoDia(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	
	
}


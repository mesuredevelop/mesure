package com.mesure.enumerator;

public enum ESituacaoColaborador {

	TRABALHANDO("Trabalhando"),
	FERIAS("Férias"),
	DEMITIDO("Demitido"),
	AFASTADO("Afastado"),
	TRANSFERIDO("Transferido"),
	OUTROS("Outra Situação");
	
	private String label;

	private ESituacaoColaborador(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}		
	
}


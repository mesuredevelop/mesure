package com.mesure.enumerator;

public enum ESituacaoDetran {
	
	OK("Ok"),
	MULTA("Multa"),	
	LICENCIAMENTO("Licenciamento pendente"),
	TRANSFERENCIA("Transferência");
	
	private String label;

	private ESituacaoDetran(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	
	
}


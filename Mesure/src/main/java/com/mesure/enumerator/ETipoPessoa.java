package com.mesure.enumerator;

public enum ETipoPessoa {
	FISICA("Física"),	
	JURIDICA("Jurídica");
	
	private String label;

	private ETipoPessoa(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	
}


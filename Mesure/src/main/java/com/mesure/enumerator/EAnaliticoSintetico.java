package com.mesure.enumerator;

public enum EAnaliticoSintetico {

	ANALITICO("Apontamento de horas detalhado"),
	SINTETICO("Apontamento de horas consolidado");	
	
	private String label;

	private EAnaliticoSintetico(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	
	
}


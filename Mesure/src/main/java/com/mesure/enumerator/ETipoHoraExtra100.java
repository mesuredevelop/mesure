package com.mesure.enumerator;

public enum ETipoHoraExtra100 {
	
	SAB_DOM_FER("Sábado, domingo e feriados"),
	DOM_FER("Domingo e feriados"),	
	FER("Feriados");
	
	private String label;

	private ETipoHoraExtra100(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}

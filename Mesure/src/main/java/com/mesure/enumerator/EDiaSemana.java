package com.mesure.enumerator;

public enum EDiaSemana {
	
	SEGUNDA_FEIRA("Segunda-feira"),
	TERCA_FEIRA("Terça-feira"),
	QUARTA_FEIRA("Quarta-feira"),
	QUINTA_FEIRA("Quinta-feira"),
	SEXTA_FEIRA("Sexta-feira"),
	SABADO("Sábado"),
	DOMINGO("Domingo");
	
	private String label;

	private EDiaSemana(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	

}

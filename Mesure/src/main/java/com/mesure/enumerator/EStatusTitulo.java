package com.mesure.enumerator;

public enum EStatusTitulo {
	
	ABERTO("Aberto", "#ffff99"),	
	LIQUIDADO("Liquidado", "#006600"),
	CANCELADO("Cancelado", "#999966");
	
	private final String label;
	private final String cor;

	private EStatusTitulo(String label, String cor) {
		this.label = label;
		this.cor = cor;
	}

	public String getLabel() {
		return label;
	}	
	
	public String getCor() {
		return cor;
	}

}


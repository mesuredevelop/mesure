package com.mesure.enumerator;

public enum EAcaoLoteApuracaoHoras {
	PROCESSAR("Processar"),
	FECHAR("Fechar"),	
	LIBERAR("Liberar"),
	DESFAZER_LIBERACAO("Desfazer liberação"),
	REABRIR("Reabrir"),
	DESFAZER("Desfazer");
	
	private String label;

	private EAcaoLoteApuracaoHoras(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}

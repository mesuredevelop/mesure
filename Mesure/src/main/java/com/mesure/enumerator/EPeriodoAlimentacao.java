package com.mesure.enumerator;

public enum EPeriodoAlimentacao {
	CAFE_MANHA("Café da manhã"),
	ALMOCO("Almoço"),
	JANTA("Janta");
	
	private String label;

	private EPeriodoAlimentacao(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}

package com.mesure.enumerator;

public enum ESituacaoBancoHoras {
	
	POSITIVO("Positivo", "#0000cc"),
	NEGATIVO("Negativo", "#ff0000"),
	ZERADO("Zerado", "#000000");
	
	private String label;
	private String cor;

	private ESituacaoBancoHoras(String label, String cor) {
		this.label = label;
		this.cor = cor;
	}

	public String getLabel() {
		return label;
	}

	public String getCor() {
		return cor;
	}
	
}

package com.mesure.enumerator;

public enum EAcaoLoteFaturamento {
	PROCESSAR("Processar"),
	FECHAR("Fechar"),	
	LIBERAR("Liberar"),
	DESFAZER_LIBERACAO("Desfazer liberação"),
	REABRIR("Reabrir"),
	DESFAZER("Desfazer");
	
	private String label;

	private EAcaoLoteFaturamento(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}

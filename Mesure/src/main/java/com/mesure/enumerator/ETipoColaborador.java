package com.mesure.enumerator;

public enum ETipoColaborador {

	INTERNO("Interno"),
	EXTERNO("Externo");
	
	private String label;

	private ETipoColaborador(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}		
	
}


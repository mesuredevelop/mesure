package com.mesure.enumerator;

public enum ESituacaoManutencao {

	PENDENTE("Pendente"),
	FINALIZADO("Finalizado");
	
	private String label;

	private ESituacaoManutencao(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}


package com.mesure.enumerator;

public enum ESituacaoMulta {

	PENDENTE("Pendente"),
	PAGO("Pago");	
	
	private String label;

	private ESituacaoMulta(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	

}


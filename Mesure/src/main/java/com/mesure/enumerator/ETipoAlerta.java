package com.mesure.enumerator;

public enum ETipoAlerta {
	
	HRS_NAO_APT("Horas não apontadas"),
	PRG_DIA("Programação diária"),
	PRG_FDS("Programação final de semana"),
	CON_HRS_PRJ("Consumo de horas do projeto"),
	IND_APT_HRS("Índice de apontamento de horas"),
	PRJ_SEM_TRT("Projetos sem tratativas"),
	PRJ_ATV_EXT("Projetos em atividade externa"),
	MAN_VEI("Manutenção de veículos");
	
	private String label;

	private ETipoAlerta(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

}

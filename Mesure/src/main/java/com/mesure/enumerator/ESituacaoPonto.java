package com.mesure.enumerator;

public enum ESituacaoPonto {
	
	OK("Ok", ""),
	HORA_EXTRA("Hora Extra", "#4da6ff"),	
	HORA_FALTA("Hora Falta", "#fc5c4e");
	
	private String label;
	private String cor;

	private ESituacaoPonto(String label, String cor) {
		this.label = label;
		this.cor = cor;
	}

	public String getLabel() {
		return label;
	}	
	
	public String getCor() {
		return cor;
	}

}

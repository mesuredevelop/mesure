package com.mesure.enumerator;

public enum ETipoRelatorioColaborador {

	PONTO("Registro de ponto"),
	PROJETO("Colaborador x projeto");	
	
	private String label;

	private ETipoRelatorioColaborador(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	
	
}


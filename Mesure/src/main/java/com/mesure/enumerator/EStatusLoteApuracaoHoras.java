package com.mesure.enumerator;

public enum EStatusLoteApuracaoHoras {
	PENDENTE("Pendente"),
	PROCESSADO("Processado"),	
	FECHADO("Fechado"),
	LIBERADO("Liberado");
	
	private String label;

	private EStatusLoteApuracaoHoras(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	
}

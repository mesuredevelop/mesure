package com.mesure.enumerator;

public enum ESituacaoAtividadePrevista {

	PENDENTE("Pendente"),
	FINALIZADO("Finalizado");
	
	private String label;

	private ESituacaoAtividadePrevista(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}

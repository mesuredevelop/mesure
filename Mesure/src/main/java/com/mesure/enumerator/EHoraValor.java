package com.mesure.enumerator;

public enum EHoraValor {

	HORAS("Horas registradas no projeto"),
	VALORES("Lucratividade do projeto"),	
	ETAPAS("Detalhamento e evolução das etapas do projeto"),
	CUSTOS("Custos do projeto");
	
	private String label;

	private EHoraValor(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	
	
}


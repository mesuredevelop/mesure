package com.mesure.enumerator;

public enum ESimNao {

	SIM("Sim"),
	NAO("Não");	
	
	private String label;

	private ESimNao(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	
	
}


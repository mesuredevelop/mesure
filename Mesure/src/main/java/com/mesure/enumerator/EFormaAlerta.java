package com.mesure.enumerator;

public enum EFormaAlerta {

	EMAIL("Email"),
	SISTEMA("Sistema");	
	
	private String label;

	private EFormaAlerta(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}

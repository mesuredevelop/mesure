package com.mesure.enumerator;

public enum ESituacaoEtapa {

	PENDENTE("Pendente"),
	EXECUCAO("Em execução"),
	FINALIZADO("Finalizado"),
	CANCELADO("Cancelado"),
	RETRABALHO("Retrabalho");
	
	private String label;

	private ESituacaoEtapa(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	
	
}


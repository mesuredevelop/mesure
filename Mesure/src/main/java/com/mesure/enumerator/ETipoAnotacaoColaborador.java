package com.mesure.enumerator;

public enum ETipoAnotacaoColaborador {
	
	ADVERTENCIA("Advertência"),
	SUSPENSAO("Suspensão");
	
	private String label;

	private ETipoAnotacaoColaborador(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}	

}

package com.mesure.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import com.mesure.model.ContaBancaria;
import com.mesure.model.Filial;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class ContaBancariaRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    @Transactional
    public ContaBancaria guardar(ContaBancaria contaBancaria) {
        return manager.merge(contaBancaria);
    }

    @Transactional
    public void remover(ContaBancaria contaBancaria) {
        try {
            contaBancaria = porId(contaBancaria.getId());
            manager.remove(contaBancaria);
            manager.flush();
        } catch (PersistenceException e) {
            throw new NegocioException("Registro não pode ser excluído.");
        }
    }

    public ContaBancaria porId(Long id) {
        return manager.find(ContaBancaria.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<ContaBancaria> obterContasBancarias(Filial filial) {
        List<ContaBancaria> retorno = new ArrayList<>();

        try {
            String sql = "select f from ContaBancaria f";
            if (filial != null) {
                sql += " where f.filial = :filial";
            }
            Query query = manager.createQuery(sql, ContaBancaria.class);
            if (filial != null) {
                query.setParameter("filial", filial);
            }
            retorno = query.getResultList();
        } catch (NoResultException e) {
            // nenhum registro encontrado
            retorno = new ArrayList<>();
        }
        return retorno;
    }

}

package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mesure.model.FaturamentoDetalhe;
import com.mesure.model.FaturamentoLote;
import com.mesure.repository.filter.FaturamentoLoteFilter;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class FaturamentoLoteRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public FaturamentoLote guardarLote(FaturamentoLote faturamentoLote) {
		return manager.merge(faturamentoLote);
	}
	
	@Transactional
	public FaturamentoLote guardarLoteTransactional(FaturamentoLote faturamentoLote) {
		return manager.merge(faturamentoLote);
	}
	
	public FaturamentoDetalhe guardarDetalhe(FaturamentoDetalhe faturamentoDetalhe) {
		return manager.merge(faturamentoDetalhe);
	}
	
	@Transactional
	public FaturamentoDetalhe guardarDetalheTransactional(FaturamentoDetalhe faturamentoDetalhe) {
		return manager.merge(faturamentoDetalhe);
	}
	
	public FaturamentoLote porId(Long id) {
		return manager.find(FaturamentoLote.class, id);
	}
	
	@Transactional
	public void remover(FaturamentoLote faturamentoLote) {
		try {
			faturamentoLote = porId(faturamentoLote.getId());
			manager.remove(faturamentoLote);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Registro não pode ser excluído.");
		}
	}
	
	private Criteria criarCriteriaParaFiltro(FaturamentoLoteFilter filtro) {
		Session session = this.manager.unwrap(Session.class);
		
		Criteria criteria = session.createCriteria(FaturamentoLote.class);
		
		if (filtro.getProjeto() != null) {
			criteria.add(Restrictions.eq("projeto", filtro.getProjeto()));
		}
		
		if (filtro.getStatus() != null) {
			criteria.add(Restrictions.eq("status", filtro.getStatus()));
		}
		
		if (filtro.getCompetenciaDe() != null) {
			criteria.add(Restrictions.ge("competencia", filtro.getCompetenciaDe()));
		}
		
		if (filtro.getCompetenciaAte() != null) {
			criteria.add(Restrictions.le("competencia", filtro.getCompetenciaAte()));
		}
		return criteria;
	}
	
	@SuppressWarnings("unchecked")
	public List<FaturamentoLote> lotes(FaturamentoLoteFilter filtro) {
		Criteria criteria = criarCriteriaParaFiltro(filtro);		
		criteria.addOrder(Order.desc("competencia"));
		
		return criteria.list();		
	}	

}

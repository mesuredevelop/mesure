package com.mesure.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import com.mesure.enumerator.ETipoFeriado;
import com.mesure.model.Feriado;
import com.mesure.model.Filial;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class FeriadoRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	@Transactional
	public Feriado guardar(Feriado feriado) {
		return manager.merge(feriado);
	}
	
	@Transactional
	public void remover(Feriado feriado) {
		try {
			feriado = porId(feriado.getId());
			manager.remove(feriado);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Registro não pode ser excluído.");
		}
	}
	
	public Feriado porId(Long id) {
		return manager.find(Feriado.class, id);
	}
	
	public List<Feriado> porData(Date data, Filial filial) {
		List<Feriado> retorno = new ArrayList<>();
		try {
			retorno = manager.createQuery("select f from Feriado f where f.filial = :filial and f.data = :data", Feriado.class)
					.setParameter("filial", filial)
					.setParameter("data", data)
					.getResultList();
		} catch (NoResultException e) {
			retorno = new ArrayList<>();
		}
		return retorno;
	}
	
	public List<Feriado> porDataTipo(Date data, ETipoFeriado tipo, Filial filial) {
		List<Feriado> retorno = new ArrayList<>();
		try {
			retorno = manager.createQuery("select f from Feriado f where f.filial = :filial and f.data = :data and f.tipo = :tipo", Feriado.class)
					.setParameter("filial", filial)
					.setParameter("data", data)
					.setParameter("tipo", tipo)
					.getResultList();
		} catch (NoResultException e) {
			retorno = new ArrayList<>();
		}
		return retorno;
	}
	
	@SuppressWarnings("unchecked")
	public List<Feriado> obterFeriados(Filial filial) {
		List<Feriado> retorno = new ArrayList<>();
		
		try {
			String sql = "select f from Feriado f";
			if (filial != null) {
				sql += " where f.filial = :filial";
			}
			Query query = manager.createQuery(sql, Feriado.class);
			if (filial != null) {
				query.setParameter("filial", filial);
			}				
			retorno = query.getResultList();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			retorno = new ArrayList<>();
		}
		return retorno;		
	}
	
}

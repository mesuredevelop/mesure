package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Usuario;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class UsuarioRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public UsuarioRepository() {
	}

	public UsuarioRepository(EntityManager manager) {
		this.manager = manager;
	}

	public Usuario guardar(Usuario usuario) {
		return manager.merge(usuario);
	}

	@Transactional
	public void remover(Usuario usuario) {
		try {
			usuario = porId(usuario.getId());
			manager.remove(usuario);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Usuario não pode ser excluído.");
		}
	}

	public Usuario porId(Long id) {
		return manager.find(Usuario.class, id);
	}

	public Usuario porEmail(String email, ESimNao somenteAtivo) throws NoResultException, NonUniqueResultException {
		Usuario usuario = null;

		if (somenteAtivo.equals(ESimNao.SIM)) {
			usuario = manager.createQuery("from Usuario where lower(email) = :email and ativo = :ativo", Usuario.class)
					.setParameter("email", email.toLowerCase()).setParameter("ativo", ESimNao.SIM).getSingleResult();
		} else {
			usuario = manager.createQuery("from Usuario where lower(email) = :email", Usuario.class)
					.setParameter("email", email.toLowerCase()).getSingleResult();
		}

		return usuario;
	}

	public List<Usuario> ativos() {
		try {
			return manager.createQuery("from Usuario where ativo = :ativo order by nome", Usuario.class)
					.setParameter("ativo", ESimNao.SIM).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> filtrados() {
		Session session = manager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Usuario.class);

		return criteria.addOrder(Order.asc("nome")).list();
	}

}

package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import com.mesure.model.VeiculoMarca;
import com.mesure.model.VeiculoModelo;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class VeiculoMarcaRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	public VeiculoMarca guardar(VeiculoMarca veiculoMarca) {
		return manager.merge(veiculoMarca);
	}
	
	@Transactional
	public void remover(VeiculoMarca veiculoMarca) {
		try {
			veiculoMarca = marcaPorId(veiculoMarca.getId());
			manager.remove(veiculoMarca);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Marca não pode ser excluída.");
		}
	}
	
	public VeiculoMarca marcaPorId(Long id) {
		return manager.find(VeiculoMarca.class, id);
	}
	
	public VeiculoModelo modeloPorId(Long id) {
		return manager.find(VeiculoModelo.class, id);
	}	
	
	public List<VeiculoMarca> marcas() {
		try {
			return manager.createQuery("from VeiculoMarca order by descricao", VeiculoMarca.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}		
	}
	
	public List<VeiculoModelo> modelos() {
		try {
			return manager.createQuery("select md from VeiculoModelo md join fetch md.marca mr order by mr.descricao, md.descricao", VeiculoModelo.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}		
	}
	
	public List<VeiculoModelo> modelosDe(VeiculoMarca marca) {
		return manager.createQuery("from VeiculoModelo where marca = :marca", VeiculoModelo.class).setParameter("marca", marca).getResultList();
	}	

}


package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import com.mesure.model.Colaborador;
import com.mesure.model.Equipe;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class EquipeRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Equipe guardar(Equipe equipe) {
		return manager.merge(equipe);
	}
	
	@Transactional
	public void remover(Equipe equipe) {
		try {			
			equipe = porId(equipe.getId());
			manager.remove(equipe);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Equipe não pode ser excluída.");
		}
	}
	
	public Equipe porId(Long id) {
		return manager.find(Equipe.class, id);
	}	
	
	public List<Equipe> equipes() {
		try {
			return manager.createQuery("from Equipe order by descricao", Equipe.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Equipe equipeComParticipantes(Equipe equipe) {
		try {
			return manager.createQuery("from Equipe e JOIN FETCH e.colaboradores where e = :equipe", Equipe.class)
					.setParameter("equipe", equipe)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}	
	
	public List<Equipe> equipesReponsavelSemParticipantes(Colaborador colaborador) {
		try {
			return manager.createQuery("from Equipe where colaborador = :colaborador", Equipe.class)
				.setParameter("colaborador", colaborador)
				.getResultList();
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	public List<Equipe> equipesReponsavelComParticipantes(Colaborador colaborador) {
		try {
			return manager.createQuery("from Equipe e JOIN FETCH e.colaboradores where e.colaborador = :colaborador", Equipe.class)
				.setParameter("colaborador", colaborador)
				.getResultList();
		} catch (NoResultException e) {
			return null;
		}	
	}
	
}


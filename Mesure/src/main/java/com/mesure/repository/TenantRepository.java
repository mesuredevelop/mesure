package com.mesure.repository;

import java.io.Serializable;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.hibernate.exception.SQLGrammarException;
import org.hibernate.validator.constraints.Email;

import com.mesure.tenant.cdi.Mesure;
import com.mesure.tenant.model.Tenant;

public class TenantRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Mesure
	@Inject
	private EntityManager em;

	public Tenant obter(@Email String email) throws NoResultException, NonUniqueResultException, SQLGrammarException {

		String domain = email.split("@")[1];
		Tenant tenant = em
				.createQuery("select t from Tenant t JOIN t.emailDomains ed ON ed.nome = :domain AND ed.ativo = :emailDomainAtivo WHERE t.ativo = :ativo", Tenant.class)
				.setParameter("domain", domain)
				.setParameter("emailDomainAtivo", true)
				.setParameter("ativo", true)
				.getSingleResult();

		return tenant;
	}
}

package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import com.mesure.model.DispositivoSeguranca;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class DispositivoSegurancaRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	@Transactional
	public DispositivoSeguranca guardar(DispositivoSeguranca dispositivoSeguranca) {
		return manager.merge(dispositivoSeguranca);
	}
	
	@Transactional
	public void remover(DispositivoSeguranca dispositivoSeguranca) {
		try {			
			dispositivoSeguranca = porId(dispositivoSeguranca.getId());
			manager.remove(dispositivoSeguranca);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Dispositivo de segurança não pode ser excluído.");
		}
	}
	
	public DispositivoSeguranca porId(Long id) {
		return manager.find(DispositivoSeguranca.class, id);
	}	
	
	public List<DispositivoSeguranca> dispositivos() {
		try {
			return manager.createQuery("from DispositivoSeguranca order by descricao", DispositivoSeguranca.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
}

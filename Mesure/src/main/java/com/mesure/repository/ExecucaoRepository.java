package com.mesure.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mesure.model.ProjetoAtividadePrevista;
import com.mesure.model.ProjetoExecucao;
import com.mesure.repository.filter.ExecucaoFilter;

public class ExecucaoRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	private Criteria criarCriteriaParaFiltro(ExecucaoFilter filtro) {
		Session session = this.manager.unwrap(Session.class);
		
		Criteria criteria = session.createCriteria(ProjetoAtividadePrevista.class);
		
		if (filtro.getColaborador() != null) {
			criteria.add(Restrictions.eq("colaborador", filtro.getColaborador()));
		}	
		
		if (filtro.getData() != null) {
			criteria.add(Restrictions.eq("data", filtro.getData()));
		}		
		
		if (filtro.getSituacao() != null) {
			criteria.add(Restrictions.eq("situacao", filtro.getSituacao()));
		}	
		
		return criteria;
	}	
	
	@SuppressWarnings("unchecked")
	public List<ProjetoAtividadePrevista> atividadesPrevisas(ExecucaoFilter filtro) {
		Criteria criteria = criarCriteriaParaFiltro(filtro);
		criteria.addOrder(Order.asc("data"));
		
		return criteria.list();		
	}
	
	public ProjetoExecucao ultimaExecucaoDaAtividadePrevista(ProjetoAtividadePrevista atividade) {
		try {
			return manager.createQuery("from ProjetoExecucao exe where exe.atividadePrevista = :atividade and exe.horaInicio = "
					+ "(select max(exe2.horaInicio) from ProjetoExecucao exe2 where exe2.atividadePrevista = :atividade)", ProjetoExecucao.class)
				.setParameter("atividade", atividade)
				.getSingleResult();
		} catch (NoResultException e) {
			return null; // nenhum registro encontrado
		} catch (Exception e) {
			return null; // caso retorne mais de um registro
		}
	}
	
	public List<ProjetoExecucao> execucoesDaAtividadePrevista(ProjetoAtividadePrevista atividade) {
		try {
			return manager.createQuery("from ProjetoExecucao where atividadePrevista = :atividade", ProjetoExecucao.class)
				.setParameter("atividade", atividade)
				.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>(); // nenhum registro encontrado
		}
	}
	
}


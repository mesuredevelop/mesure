package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import com.mesure.model.Atividade;
import com.mesure.model.Etapa;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class AtividadeRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	@Transactional
	public Atividade guardar(Atividade atividade) {
		return manager.merge(atividade);
	}
	
	@Transactional
	public void remover(Atividade atividade) {
		try {			
			atividade = porId(atividade.getId());
			manager.remove(atividade);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Atividade não pode ser excluída.");
		}
	}
	
	public Atividade porId(Long id) {
		return manager.find(Atividade.class, id);
	}	
	
	public List<Atividade> atividades() {
		try {
			return manager.createQuery("from Atividade order by descricao", Atividade.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Atividade equipamentosDaAtividade(Atividade atividade) {
		try {
			Query query = manager.createQuery("SELECT a FROM Atividade AS a JOIN FETCH a.equipamentos WHERE a = :atividade");
			query.setParameter("atividade", atividade);
			return (Atividade) query.getSingleResult();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			return null;
		}		
	}
	
	public Atividade dispositivosDaAtividade(Atividade atividade) {
		try {
			Query query = manager.createQuery("SELECT a FROM Atividade AS a JOIN FETCH a.dispositivos WHERE a = :atividade");
			query.setParameter("atividade", atividade);
			return (Atividade) query.getSingleResult();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			return null;
		}		
	}
	
	public Etapa equipamentosDaEtapa(Etapa etapa) {
		try {
			Query query = manager.createQuery("SELECT e FROM Etapa AS e JOIN FETCH e.equipamentos WHERE e = :etapa");
			query.setParameter("etapa", etapa);
			return (Etapa) query.getSingleResult();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			return null;
		}		
	}
	
	public Etapa dispositivosDaEtapa(Etapa etapa) {
		try {
			Query query = manager.createQuery("SELECT e FROM Etapa AS e JOIN FETCH e.dispositivos WHERE e = :etapa");
			query.setParameter("etapa", etapa);
			return (Etapa) query.getSingleResult();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			return null;
		}		
	}
	
	public Atividade etapasDaAtividade(Atividade atividade) {
		try {
			Query query = manager.createQuery("SELECT a FROM Atividade AS a JOIN FETCH a.etapas WHERE a = :atividade");
			query.setParameter("atividade", atividade);
			return (Atividade) query.getSingleResult();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			return null;
		}	
	}
	
	public Atividade treinamentosDaAtividade(Atividade atividade) {
		try {
			Query query = manager.createQuery("SELECT a FROM Atividade AS a JOIN FETCH a.treinamentos WHERE a = :atividade");
			query.setParameter("atividade", atividade);
			return (Atividade) query.getSingleResult();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			return null;
		}	
	}
}


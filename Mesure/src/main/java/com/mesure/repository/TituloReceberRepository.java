package com.mesure.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mesure.model.TituloReceber;
import com.mesure.model.TituloReceberBaixa;
import com.mesure.repository.filter.TituloReceberFilter;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;
import java.math.BigDecimal;
import javax.persistence.Query;

public class TituloReceberRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public TituloReceber guardar(TituloReceber tituloReceber) {
        return manager.merge(tituloReceber);
    }

    public TituloReceberBaixa guardarTituloReceberBaixa(TituloReceberBaixa tituloReceberBaixa) {
        return manager.merge(tituloReceberBaixa);
    }

    @Transactional
    public void remover(TituloReceber tituloReceber) {
        try {
            tituloReceber = porId(tituloReceber.getId());
            manager.remove(tituloReceber);
            manager.flush();
        } catch (PersistenceException e) {
            throw new NegocioException("Titulo a receber não pode ser excluído.");
        }
    }

    @Transactional
    public void removerBaixaTitulo(TituloReceberBaixa tituloReceberBaixa) {
        try {
            tituloReceberBaixa = porIdTituloReceberBaixa(tituloReceberBaixa.getId());
            manager.remove(tituloReceberBaixa);
            manager.flush();
        } catch (PersistenceException e) {
            throw new NegocioException("Baixa não pode ser excluído.");
        }
    }

    public TituloReceber porId(Long id) {
        return manager.find(TituloReceber.class, id);
    }

    public TituloReceberBaixa porIdTituloReceberBaixa(Long id) {
        return manager.find(TituloReceberBaixa.class, id);
    }

    public List<TituloReceber> titulosReceber() {
        try {
            return manager.createQuery("from TituloReceber order by id", TituloReceber.class).getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    private Criteria criarCriteriaParaFiltro(TituloReceberFilter filtro) {
        Session session = this.manager.unwrap(Session.class);

        Criteria criteria = session.createCriteria(TituloReceber.class);

        if (filtro.getFilial() != null) {
            criteria.add(Restrictions.eq("filial", filtro.getFilial()));
        }

        if (filtro.getCliente() != null) {
            criteria.add(Restrictions.eq("cliente", filtro.getCliente()));
        }

        return criteria;
    }

    @SuppressWarnings("unchecked")
    public List<TituloReceber> filtrados(TituloReceberFilter filtro) {
        Criteria criteria = criarCriteriaParaFiltro(filtro);

        if (filtro.isAscendente() && filtro.getPropriedadeOrdenacao() != null) {
            criteria.addOrder(Order.asc(filtro.getPropriedadeOrdenacao()));
        } else if (filtro.getPropriedadeOrdenacao() != null) {
            criteria.addOrder(Order.desc(filtro.getPropriedadeOrdenacao()));
        }

        return criteria.list();
    }

    public List<TituloReceberBaixa> baixasDoTitulo(TituloReceber tituloReceber) {
        List<TituloReceberBaixa> retorno = new ArrayList<>();

        try {
            retorno = manager.createQuery("from TituloReceberBaixa where titulo = :tituloReceber", TituloReceberBaixa.class)
                    .setParameter("titulo", tituloReceber)
                    .getResultList();

        } catch (NoResultException e) {
            // nenhum registro encontrado
        }
        return retorno;
    }

    public BigDecimal totalValorLiquidoBaixasTituloReceber(TituloReceber tituloReceber) {
        BigDecimal totalBaixas = new BigDecimal(0);

        try {
            Query query = manager.createQuery("select sum(e.valorLiquido) from TituloReceberBaixa e where e.titulo = :tituloReceber", BigDecimal.class)
                    .setParameter("tituloReceber", tituloReceber);
            totalBaixas = (BigDecimal) query.getSingleResult();

        } catch (NoResultException e) {
            // nenhum registro encontrado
        }

        if (totalBaixas == null) {
            totalBaixas = new BigDecimal(0);
        }
        return totalBaixas;
    }

    public BigDecimal totalValorBaixasTituloReceber(TituloReceber tituloReceber) {
        BigDecimal totalBaixas = new BigDecimal(0);

        try {
            Query query = manager.createQuery("select sum(e.valorBaixa) from TituloReceberBaixa e where e.titulo = :tituloReceber", BigDecimal.class)
                    .setParameter("tituloReceber", tituloReceber);
            totalBaixas = (BigDecimal) query.getSingleResult();

        } catch (NoResultException e) {
            // nenhum registro encontrado
        }

        if (totalBaixas == null) {
            totalBaixas = BigDecimal.ZERO;
        }
        return totalBaixas;
    }

}

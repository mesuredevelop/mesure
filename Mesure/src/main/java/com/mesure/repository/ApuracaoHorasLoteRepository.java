package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mesure.model.ApuracaoHorasDetalhe;
import com.mesure.model.ApuracaoHorasLote;
import com.mesure.repository.filter.ApuracaoHorasLoteFilter;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class ApuracaoHorasLoteRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	public ApuracaoHorasLote guardarLote(ApuracaoHorasLote apuracaoHorasLote) {
		return manager.merge(apuracaoHorasLote);
	}
	
	@Transactional
	public ApuracaoHorasLote guardarLoteTransactional(ApuracaoHorasLote apuracaoHorasLote) {
		return manager.merge(apuracaoHorasLote);
	}
	
	public ApuracaoHorasDetalhe guardarDetalhe(ApuracaoHorasDetalhe apuracaoHorasDetalhe) {
		return manager.merge(apuracaoHorasDetalhe);
	}
	
	@Transactional
	public ApuracaoHorasDetalhe guardarDetalheTransactional(ApuracaoHorasDetalhe apuracaoHorasDetalhe) {
		return manager.merge(apuracaoHorasDetalhe);
	}		
	
	public ApuracaoHorasLote porId(Long id) {
		return manager.find(ApuracaoHorasLote.class, id);
	}
	
	@Transactional
	public void remover(ApuracaoHorasLote apuracaoHorasLote) {
		try {
			apuracaoHorasLote = porId(apuracaoHorasLote.getId());
			manager.remove(apuracaoHorasLote);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Registro não pode ser excluído.");
		}
	}
	
	private Criteria criarCriteriaParaFiltro(ApuracaoHorasLoteFilter filtro) {
		Session session = this.manager.unwrap(Session.class);
		
		Criteria criteria = session.createCriteria(ApuracaoHorasLote.class);
		
		if (filtro.getColaborador() != null) {
			criteria.add(Restrictions.eq("colaborador", filtro.getColaborador()));
		}
		
		if (filtro.getStatus() != null) {
			criteria.add(Restrictions.eq("status", filtro.getStatus()));
		}
		
		if (filtro.getCompetenciaDe() != null) {
			criteria.add(Restrictions.ge("competencia", filtro.getCompetenciaDe()));
		}
		
		if (filtro.getCompetenciaAte() != null) {
			criteria.add(Restrictions.le("competencia", filtro.getCompetenciaAte()));
		}
		return criteria;
	}
	
	@SuppressWarnings("unchecked")
	public List<ApuracaoHorasLote> lotes(ApuracaoHorasLoteFilter filtro) {
		Criteria criteria = criarCriteriaParaFiltro(filtro);		
		criteria.addOrder(Order.desc("competencia"));
		
		return criteria.list();		
	}
	
}

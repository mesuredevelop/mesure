package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mesure.enumerator.ESituacaoAtividadePrevista;
import com.mesure.model.ProgramacaoExecucao;
import com.mesure.model.ProjetoAtividadePrevista;
import com.mesure.model.VeiculoMovimento;
import com.mesure.repository.filter.ProgramacaoExecucaoFilter;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class ProgramacaoExecucaoRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	public ProgramacaoExecucao guardar(ProgramacaoExecucao programacaoExecucao) {
		return manager.merge(programacaoExecucao);
	}
	
	public ProgramacaoExecucao porId(Long id) {
		return manager.find(ProgramacaoExecucao.class, id);
	}
	
	@Transactional
	public void remover(ProgramacaoExecucao programacaoExecucao) {
		try {
			programacaoExecucao = porId(programacaoExecucao.getId());
			manager.remove(programacaoExecucao);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Registro não pode ser excluído.");
		}
	}
	
	private Criteria criarCriteriaParaFiltro(ProgramacaoExecucaoFilter filtro) {
		Session session = this.manager.unwrap(Session.class);
		
		Criteria criteria = session.createCriteria(ProgramacaoExecucao.class);
		
		if (filtro.getFilial() != null) {
			criteria.add(Restrictions.eq("filial", filtro.getFilial()));
		}
		
		if (filtro.getColaborador() != null) {
			criteria.add(Restrictions.eq("colaborador", filtro.getColaborador()));
		}		
		
		if (filtro.getVeiculo() != null) {
			criteria.add(Restrictions.eq("veiculo", filtro.getVeiculo()));
		}
		
		if (filtro.getProjeto() != null) {
			criteria.add(Restrictions.eq("projeto", filtro.getProjeto()));
		}
		
		if (filtro.getDataDe() != null) {
			criteria.add(Restrictions.ge("data", filtro.getDataDe()));
		}
		
		if (filtro.getDataAte() != null) {
			criteria.add(Restrictions.le("data", filtro.getDataAte()));
		}
		return criteria;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProgramacaoExecucao> programacoes(ProgramacaoExecucaoFilter filtro) {
		Criteria criteria = criarCriteriaParaFiltro(filtro);
		
		if (filtro.isAscendente() && filtro.getPropriedadeOrdenacao() != null) {
			criteria.addOrder(Order.asc(filtro.getPropriedadeOrdenacao()));
		} else if (filtro.getPropriedadeOrdenacao() != null) {
			criteria.addOrder(Order.desc(filtro.getPropriedadeOrdenacao()));
		}		
		
		return criteria.list();		
	}
	
	// Esse método retorna as programações cuja atividade prevista está com situação 'PENDENTE' e ainda não possui execução lançada
	public List<ProgramacaoExecucao> programacoesSemExecucao() {
		try {
			return manager.createQuery("SELECT prg FROM ProgramacaoExecucao prg INNER JOIN prg.atividadePrevista atv " +
										"WHERE atv.situacao = :situacao " +
										  "AND NOT EXISTS (SELECT exe FROM ProjetoExecucao exe WHERE exe.atividadePrevista = atv) " +
										"ORDER BY prg.data", ProgramacaoExecucao.class)
					.setParameter("situacao", ESituacaoAtividadePrevista.PENDENTE)
					.getResultList();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	public ProjetoAtividadePrevista atividadePrevistaDaProgramacaoExecucao(ProgramacaoExecucao programacaoExecucao) {
		ProjetoAtividadePrevista atividade = null;
		
		try {
			atividade = manager.createQuery("from ProjetoAtividadePrevista where programacaoExecucao = :programacao", ProjetoAtividadePrevista.class)
				.setParameter("programacao", programacaoExecucao)
				.getSingleResult();
		} catch (NoResultException e) {
			return null; // nenhum registro encontrado
		} catch (Exception e) {
			return null; // caso retorne mais de um registro
		}
		return atividade;		
	}
	
	public VeiculoMovimento movimentacaoVeiculoDaProgramacaoExecucao(ProgramacaoExecucao programacaoExecucao) {
		VeiculoMovimento movimento = null;
		
		try {
			movimento = manager.createQuery("from VeiculoMovimento where programacaoExecucao = :programacao", VeiculoMovimento.class)
				.setParameter("programacao", programacaoExecucao)
				.getSingleResult();
		} catch (NoResultException e) {
			return null; // nenhum registro encontrado
		} catch (Exception e) {
			return null; // caso retorne mais de um registro
		}
		return movimento;		
	}
	
	public ProgramacaoExecucao participantesDaProgramacao(ProgramacaoExecucao programacaoExecucao) {
		try {
			Query query = manager.createQuery("SELECT p FROM ProgramacaoExecucao AS p JOIN FETCH p.participantes WHERE p = :programacao");
			query.setParameter("programacao", programacaoExecucao);
			return (ProgramacaoExecucao) query.getSingleResult();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			return null;
		}		
	}
	
	public ProgramacaoExecucao etapasDaProgramacao(ProgramacaoExecucao programacaoExecucao) {
		try {
			Query query = manager.createQuery("SELECT p FROM ProgramacaoExecucao AS p JOIN FETCH p.etapas WHERE p = :programacao");
			query.setParameter("programacao", programacaoExecucao);
			return (ProgramacaoExecucao) query.getSingleResult();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			return null;
		}		
	}
	
}

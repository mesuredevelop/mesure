package com.mesure.repository;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoAtividadePrevista;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.model.Colaborador;
import com.mesure.model.Projeto;
import com.mesure.model.ProjetoAtividadePrevista;
import com.mesure.model.ProjetoAtividadePrevistaDispositivo;
import com.mesure.model.ProjetoAtividadePrevistaEquipamento;
import com.mesure.model.ProjetoColaborador;
import com.mesure.model.ProjetoEtapa;
import com.mesure.model.ProjetoExecucao;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class ProjetoRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public Projeto guardar(Projeto projeto) {
        return manager.merge(projeto);
    }

    public ProjetoAtividadePrevista guardarAtividadePrevista(ProjetoAtividadePrevista projetoAtividadePrevista) {
        return manager.merge(projetoAtividadePrevista);
    }

    public ProjetoExecucao guardarProjetoExecucao(ProjetoExecucao projetoExecucao) {
        return manager.merge(projetoExecucao);
    }

    @Transactional
    public ProjetoExecucao guardarProjetoExecucaoTransactional(ProjetoExecucao projetoExecucao) {
        return guardarProjetoExecucao(projetoExecucao);
    }

    public ProjetoEtapa guardarProjetoEtapa(ProjetoEtapa projetoEtapa) {
        return manager.merge(projetoEtapa);
    }

    @Transactional
    public ProjetoEtapa guardarProjetoEtapaTransactional(ProjetoEtapa projetoEtapa) {
        return guardarProjetoEtapa(projetoEtapa);
    }

    @Transactional
    public void remover(Projeto projeto) {
        try {
            projeto = porId(projeto.getId());
            manager.remove(projeto);
            manager.flush();
        } catch (PersistenceException e) {
            throw new NegocioException("Projeto não pode ser excluído.");
        }
    }

    @Transactional
    public void removerProjetoExecucao(ProjetoExecucao projetoExecucao) {
        try {
            projetoExecucao = porIdProjetoExecucao(projetoExecucao.getId());
            manager.remove(projetoExecucao);
            manager.flush();
        } catch (PersistenceException e) {
            throw new NegocioException("Registro não pode ser excluído.");
        }
    }

    public Projeto porId(Long id) {
        return manager.find(Projeto.class, id);
    }

    public ProjetoExecucao porIdProjetoExecucao(Long id) {
        return manager.find(ProjetoExecucao.class, id);
    }

    public ProjetoEtapa porIdProjetoEtapa(Long id) {
        return manager.find(ProjetoEtapa.class, id);
    }

    public List<Projeto> getProjetos() {
        try {
            return manager.createQuery("from Projeto order by id", Projeto.class).getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    private Criteria criarCriteriaParaFiltro(ProjetoFilter filtro) {
        Session session = this.manager.unwrap(Session.class);

        Criteria criteria = session.createCriteria(Projeto.class);

        if (StringUtils.isNotBlank(filtro.getTitulo())) {
            criteria.add(Restrictions.ilike("titulo", filtro.getTitulo(), MatchMode.ANYWHERE));
        }

        if (filtro.getStatuses() != null && filtro.getStatuses().length > 0) {
            // adicionamos uma restrição "in", passando um array de constantes da enum EStatusProjeto
            criteria.add(Restrictions.in("status", filtro.getStatuses()));
        }

        if (filtro.getFilial() != null) {
            criteria.add(Restrictions.eq("filial", filtro.getFilial()));
        }

        if (filtro.getCliente() != null) {
            criteria.add(Restrictions.eq("cliente", filtro.getCliente()));
        }
        
        if (filtro.getResponsavel() != null) {
        	criteria.add(Restrictions.eq("responsavel", filtro.getResponsavel()));
        }

        return criteria;
    }

    @SuppressWarnings("unchecked")
    public List<Projeto> filtrados(ProjetoFilter filtro) {
        Criteria criteria = criarCriteriaParaFiltro(filtro);

        if (filtro.isAscendente() && filtro.getPropriedadeOrdenacao() != null) {
            criteria.addOrder(Order.asc(filtro.getPropriedadeOrdenacao()));
        } else if (filtro.getPropriedadeOrdenacao() != null) {
            criteria.addOrder(Order.desc(filtro.getPropriedadeOrdenacao()));
        }

        // Filtra os projetos no qual o colaborador é PARTICIPANTE ou RESPONSÁVEL
        if (filtro.getColaborador() != null) {
            List<Projeto> resultado = new ArrayList<>();
            List<Projeto> resultadoTmp = criteria.list();

            for (Projeto prj : resultadoTmp) {
                if (filtro.getColaborador().equals(prj.getResponsavel())) {
                    resultado.add(prj);
                } else {
                    for (ProjetoColaborador c : prj.getParticipantes()) {
                        if (filtro.getColaborador().equals(c.getColaborador()) && c.getEncerrado() == ESimNao.NAO) {
                            resultado.add(prj);
                            break;
                        }
                    }
                }
            }
            return resultado;
        // Filtra os projetos no qual o colaborador é PARTICIPANTE
        } else if (filtro.getParticipante() != null) {
            List<Projeto> resultado = new ArrayList<>();
            List<Projeto> resultadoTmp = criteria.list();

            for (Projeto prj : resultadoTmp) {
            	for (ProjetoColaborador c : prj.getParticipantes()) {
            		if (filtro.getParticipante().equals(c.getColaborador())) {
            			resultado.add(prj);
                        break;
                    }
            	}
            }
            return resultado;
        }
        return criteria.list();
    }

    public List<Projeto> projetosIniciadosPorPeriodo(Date dataInicial, Date dataFinal) {
        List<Projeto> retorno = new ArrayList<>();
        try {
            retorno = manager.createQuery("from Projeto where status <> :cancelado and dataInicio between :dataInicial and :dataFinal order by dataInicio", Projeto.class)
                    .setParameter("cancelado", EStatusProjeto.CANCELADO)
                    .setParameter("dataInicial", dataInicial)
                    .setParameter("dataFinal", dataFinal)
                    .getResultList();

        } catch (NoResultException e) {
            retorno = new ArrayList<>();
        }
        return retorno;
    }

    public List<Projeto> projetosFinalizadosPorPeriodo(Date dataInicial, Date dataFinal) {
        List<Projeto> retorno = new ArrayList<>();
        try {
            retorno = manager.createQuery("from Projeto where status = :finalizado and dataEncerramento between :dataInicial and :dataFinal order by dataEncerramento", Projeto.class)
                    .setParameter("finalizado", EStatusProjeto.FINALIZADO)
                    .setParameter("dataInicial", dataInicial)
                    .setParameter("dataFinal", dataFinal)
                    .getResultList();

        } catch (NoResultException e) {
            retorno = new ArrayList<>();
        }
        return retorno;
    }

    public List<ProjetoEtapa> etapasDoProjeto(Projeto projeto) {
        List<ProjetoEtapa> retorno = new ArrayList<>();

        try {
            retorno = manager.createQuery("from ProjetoEtapa where projeto = :projeto", ProjetoEtapa.class)
                    .setParameter("projeto", projeto)
                    .getResultList();

        } catch (NoResultException e) {
            // nenhum registro encontrado
        }
        return retorno;
    }

    public List<ProjetoAtividadePrevista> atividadesPrevistasDoProjeto(Projeto projeto) {
        List<ProjetoAtividadePrevista> retorno = new ArrayList<>();

        try {
            retorno = manager.createQuery("from ProjetoAtividadePrevista where projeto = :projeto", ProjetoAtividadePrevista.class)
                    .setParameter("projeto", projeto)
                    .getResultList();

        } catch (NoResultException e) {
            // nenhum registro encontrado
        }
        return retorno;
    }

    // Busca as atividades previstas com o status PENDENTE e que NÃO possuem execução vinculada
    public List<ProjetoAtividadePrevista> atividadesPendentesDoProjeto(Projeto projeto) {
        List<ProjetoAtividadePrevista> retorno = new ArrayList<>();

        try {
            retorno = manager.createQuery("from ProjetoAtividadePrevista atv where atv.projeto = :projeto AND atv.situacao = :situacao AND NOT EXISTS (SELECT exe FROM ProjetoExecucao exe WHERE exe.atividadePrevista = atv)", ProjetoAtividadePrevista.class)
                    .setParameter("projeto", projeto)
                    .setParameter("situacao", ESituacaoAtividadePrevista.PENDENTE)
                    .getResultList();

        } catch (NoResultException e) {
            // nenhum registro encontrado
        }
        return retorno;
    }

    // Busca as atividades previstas com o status PENDENTE e que possuem execução vinculada
    public List<ProjetoAtividadePrevista> atividadesEmExecucaoDoProjeto(Projeto projeto) {
        List<ProjetoAtividadePrevista> retorno = new ArrayList<>();

        try {
            retorno = manager.createQuery("from ProjetoAtividadePrevista atv where atv.projeto = :projeto AND atv.situacao = :situacao AND EXISTS (SELECT exe FROM ProjetoExecucao exe WHERE exe.atividadePrevista = atv)", ProjetoAtividadePrevista.class)
                    .setParameter("projeto", projeto)
                    .setParameter("situacao", ESituacaoAtividadePrevista.PENDENTE)
                    .getResultList();

        } catch (NoResultException e) {
            // nenhum registro encontrado
        }
        return retorno;
    }

    // Busca as atividades previstas com o status FINALIZADO
    public List<ProjetoAtividadePrevista> atividadesFinalizadasDoProjeto(Projeto projeto) {
        List<ProjetoAtividadePrevista> retorno = new ArrayList<>();

        try {
            retorno = manager.createQuery("from ProjetoAtividadePrevista atv where atv.projeto = :projeto AND atv.situacao = :situacao)", ProjetoAtividadePrevista.class)
                    .setParameter("projeto", projeto)
                    .setParameter("situacao", ESituacaoAtividadePrevista.FINALIZADO)
                    .getResultList();

        } catch (NoResultException e) {
            // nenhum registro encontrado
        }
        return retorno;
    }

    public List<ProjetoExecucao> ExecucoesDoProjeto(Projeto projeto) {
        List<ProjetoExecucao> retorno = new ArrayList<>();

        try {
            retorno = manager.createQuery("from ProjetoExecucao where projeto = :projeto", ProjetoExecucao.class)
                    .setParameter("projeto", projeto)
                    .getResultList();

        } catch (NoResultException e) {
            // nenhum registro encontrado
        }
        return retorno;
    }

    public Projeto atividadesDoProjeto(Projeto projeto) {
        try {
            Query query = manager.createQuery("SELECT p FROM Projeto AS p JOIN FETCH p.atividades WHERE p = :projeto");
            query.setParameter("projeto", projeto);
            return (Projeto) query.getSingleResult();
        } catch (NoResultException e) {
            // nenhum registro encontrado
            return null;
        }
    }

    public BigDecimal horasExecucoesEtapaProjeto(ProjetoEtapa projetoEtapa) {
        BigDecimal horas = new BigDecimal(0);

        try {
            Query query = manager.createQuery("select sum(e.totalHoras) from ProjetoExecucao e where e.etapa = :projetoEtapa", BigDecimal.class)
                    .setParameter("projetoEtapa", projetoEtapa);
            horas = (BigDecimal) query.getSingleResult();

        } catch (NoResultException e) {
            // nenhum registro encontrado
        }

        if (horas == null) {
            horas = new BigDecimal(0);
        }
        return horas;
    }

    // Busca as execuções realizadas para o colaborador e data, ordenando por hora início
    public List<ProjetoExecucao> execucoesColaboradorData(Colaborador colaborador, Date data) {
        List<ProjetoExecucao> retorno = new ArrayList<>();
        try {
            retorno = manager.createQuery("from ProjetoExecucao where colaborador = :colaborador and data = :data order by horaInicio ", ProjetoExecucao.class)
                    .setParameter("colaborador", colaborador)
                    .setParameter("data", data)
                    .getResultList();
        } catch (NoResultException e) {
            // nenhum registro encontrado
            retorno = new ArrayList<>();
        }
        return retorno;
    }

    // Busca as execuções realizadas para o projeto e período, ordenando por data, colaborador e hora início
    public List<ProjetoExecucao> execucoesProjetoPeriodo(Projeto projeto, Date dataInicio, Date dataFim) {
        List<ProjetoExecucao> retorno = new ArrayList<>();
        try {
            retorno = manager.createQuery("from ProjetoExecucao where projeto = :projeto and data between :dataInicio and :dataFim order by data, colaborador, horaInicio", ProjetoExecucao.class)
                    .setParameter("projeto", projeto)
                    .setParameter("dataInicio", dataInicio)
                    .setParameter("dataFim", dataFim)
                    .getResultList();
        } catch (NoResultException e) {
            // nenhum registro encontrado
            retorno = new ArrayList<>();
        }
        return retorno;
    }

    public List<ProjetoAtividadePrevistaEquipamento> equipamentosDaAtividadePrevista(ProjetoAtividadePrevista atividadePrevista) {
        List<ProjetoAtividadePrevistaEquipamento> retorno = new ArrayList<>();
        try {
            retorno = manager.createQuery("from ProjetoAtividadePrevistaEquipamento where atividadePrevista = :atividadePrevista", ProjetoAtividadePrevistaEquipamento.class)
                    .setParameter("atividadePrevista", atividadePrevista)
                    .getResultList();

        } catch (NoResultException e) {
            // nenhum registro encontrado
            retorno = new ArrayList<>();
        }
        return retorno;
    }

    public List<ProjetoAtividadePrevistaDispositivo> dispositivosDaAtividadePrevista(ProjetoAtividadePrevista atividadePrevista) {
        List<ProjetoAtividadePrevistaDispositivo> retorno = new ArrayList<>();

        try {
            retorno = manager.createQuery("from ProjetoAtividadePrevistaDispositivo where atividadePrevista = :atividadePrevista", ProjetoAtividadePrevistaDispositivo.class)
                    .setParameter("atividadePrevista", atividadePrevista)
                    .getResultList();

        } catch (NoResultException e) {
            // nenhum registro encontrado
            retorno = new ArrayList<>();
        }
        return retorno;
    }
}

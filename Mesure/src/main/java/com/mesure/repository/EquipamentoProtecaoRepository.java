package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import com.mesure.model.EquipamentoProtecao;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class EquipamentoProtecaoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	@Transactional
	public EquipamentoProtecao guardar(EquipamentoProtecao equipamentoProtecao) {
		return manager.merge(equipamentoProtecao);
	}
	
	@Transactional
	public void remover(EquipamentoProtecao equipamentoProtecao) {
		try {			
			equipamentoProtecao = porId(equipamentoProtecao.getId());
			manager.remove(equipamentoProtecao);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Equipamento não pode ser excluído.");
		}
	}
	
	public EquipamentoProtecao porId(Long id) {
		return manager.find(EquipamentoProtecao.class, id);
	}	
	
	public List<EquipamentoProtecao> equipamentos() {
		try {
			return manager.createQuery("from EquipamentoProtecao order by descricao", EquipamentoProtecao.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
}

package com.mesure.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoColaborador;
import com.mesure.model.Alerta;
import com.mesure.model.AlertaEmail;
import com.mesure.model.AlertaLog;
import com.mesure.model.Colaborador;
import com.mesure.model.ColaboradorPonto;
import com.mesure.model.Filial;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class AlertaRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	@Transactional
	public Alerta guardar(Alerta alerta) {
		return manager.merge(alerta);
	}
	
	@Transactional
	public AlertaLog guardarAlertaLog(AlertaLog alertaLog) {
		return manager.merge(alertaLog);
	}
	
	@Transactional
	public void remover(Alerta alerta) {
		try {
			alerta = porId(alerta.getId());
			manager.remove(alerta);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Registro não pode ser excluído.");
		}
	}
	
	public Alerta porId(Long id) {
		return manager.find(Alerta.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Alerta> obterAlertas(Filial filial) {
		List<Alerta> retorno = new ArrayList<>();
		
		try {
			String sql = "select a from Alerta a";
			if (filial != null) {
				sql += " where a.filial = :filial";
			}
			sql += " order by a.filial";
			Query query = manager.createQuery(sql, Alerta.class);
			if (filial != null) {
				query.setParameter("filial", filial);
			}
			retorno = query.getResultList();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			retorno = new ArrayList<>();
		}
		return retorno;		
	}
	
	public List<AlertaEmail> emailsDoAlerta(Alerta alerta) {
		List<AlertaEmail> retorno = new ArrayList<>();
		try {
			retorno = manager.createQuery("from AlertaEmail where alerta = :alerta", AlertaEmail.class)
				.setParameter("alerta", alerta)
				.getResultList();
			
		} catch (NoResultException e) {
			retorno = new ArrayList<>();
		}
		return retorno;		
	}
	
	public List<AlertaLog> logsDoAlerta(Alerta alerta) {
		List<AlertaLog> retorno = new ArrayList<>();
		try {
			retorno = manager.createQuery("from AlertaLog where alerta = :alerta", AlertaLog.class)
				.setParameter("alerta", alerta)
				.getResultList();
			
		} catch (NoResultException e) {
			retorno = new ArrayList<>();
		}
		return retorno;	
	}
	
	public AlertaLog ultimoLogAlerta(Alerta alerta) {
		AlertaLog alertaLog = null;		
		try {
			alertaLog = manager.createQuery("select l from AlertaLog l where l.id = (select max(l1.id) from AlertaLog l1 where l1.alerta = :alerta)", AlertaLog.class)
					.setParameter("alerta", alerta)
					.getSingleResult();
		} catch (Exception e) {
			alertaLog = null;
		}
		return alertaLog;		
	}
	
	public AlertaLog ultimoLogAlertaData(Alerta alerta, Date data) {
		AlertaLog alertaLog = null;		
		try {
			alertaLog = manager.createQuery("select l from AlertaLog l where l.id = (select max(l1.id) from AlertaLog l1 where l1.alerta = :alerta and l1.data = :data)", AlertaLog.class)
					.setParameter("alerta", alerta)
					.setParameter("data", data)
					.getSingleResult();
		} catch (Exception e) {
			alertaLog = null;
		}
		return alertaLog;		
	}
	
	// Alerta: Horas não apontadas: registraram ponto mas faltou apontamento de horas
	public List<ColaboradorPonto> horasNaoApontadas(Filial filial, Date dataMenor, Date dataMaior) {
		List<ColaboradorPonto> retorno = new ArrayList<>();
		try {
			retorno = manager.createQuery("select cp from ColaboradorPonto cp INNER JOIN cp.colaborador c where c.registraPonto = :registraPonto and c.apontamento = :apontamento and c.filial = :filial and cp.data between :dataMenor and :dataMaior and cp.totalHorasFalta > 0 order by c.nome, cp.data", ColaboradorPonto.class)
				.setParameter("registraPonto", ESimNao.SIM)
				.setParameter("apontamento", ESimNao.SIM)
				.setParameter("filial", filial)
				.setParameter("dataMenor", dataMenor)
				.setParameter("dataMaior", dataMaior)
				.getResultList();
		} catch (Exception e) {
			retorno = new ArrayList<>();
		}
		return retorno;		
	}
	
	// Alerta: Horas não apontadas: não registraram ponto
	public List<Colaborador> colaboradoresSemRegistroPonto(Filial filial, Date data) {
		List<Colaborador> retorno = new ArrayList<>();
		try {
			retorno = manager.createQuery("select c from Colaborador c where c.registraPonto = :registraPonto and c.filial = :filial and c.situacao <> :demitido and not exists (select cp from ColaboradorPonto cp WHERE cp.colaborador = c and cp.data = :data) order by c.nome", Colaborador.class)
				.setParameter("registraPonto", ESimNao.SIM)
				.setParameter("filial", filial)
				.setParameter("demitido", ESituacaoColaborador.DEMITIDO)
				.setParameter("data", data)
				.getResultList();
		} catch (Exception e) {
			retorno = new ArrayList<>();
		}
		return retorno;		
	}

}

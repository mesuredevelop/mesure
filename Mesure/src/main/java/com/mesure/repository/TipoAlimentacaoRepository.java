package com.mesure.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import com.mesure.enumerator.EEstado;
import com.mesure.model.Filial;
import com.mesure.model.TipoAlimentacao;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class TipoAlimentacaoRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	@Transactional
	public TipoAlimentacao guardar(TipoAlimentacao tipoAlimentacao) {
		return manager.merge(tipoAlimentacao);
	}
	
	@Transactional
	public void remover(TipoAlimentacao tipoAlimentacao) {
		try {
			tipoAlimentacao = porId(tipoAlimentacao.getId());
			manager.remove(tipoAlimentacao);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Registro não pode ser excluído.");
		}
	}
	
	public TipoAlimentacao porId(Long id) {
		return manager.find(TipoAlimentacao.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoAlimentacao> obterTiposAlimentacao(Filial filial, EEstado estado) {
		List<TipoAlimentacao> retorno = new ArrayList<>();
		
		try {
			String sql = "select t from TipoAlimentacao t where (1=1)";
			if (filial != null) {
				sql += " and t.filial = :filial";
			}
			if (estado != null) {
				sql += " and t.estado = :estado";
			}			
			Query query = manager.createQuery(sql, TipoAlimentacao.class);
			if (filial != null) {
				query.setParameter("filial", filial);
			}
			if (estado != null) {
				query.setParameter("estado", estado);
			}
			retorno = query.getResultList();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			retorno = new ArrayList<>();
		}
		return retorno;		
	}

}

package com.mesure.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mesure.model.Veiculo;
import com.mesure.model.VeiculoMovimento;
import com.mesure.repository.filter.VeiculoFilter;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class VeiculoRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	public Veiculo guardar(Veiculo veiculo) {
		return manager.merge(veiculo);
	}
	
	public VeiculoMovimento guardarMovimento(VeiculoMovimento veiculoMovimento) {
		return manager.merge(veiculoMovimento);
	}
	
	@Transactional
	public void remover(Veiculo veiculo) {
		try {
			veiculo = porId(veiculo.getId());
			manager.remove(veiculo);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Veículo não pode ser excluído.");
		}
	}
	
	public void removerMovimento(VeiculoMovimento veiculoMovimento) {
		try {
			veiculoMovimento = porIdVeiculoMovimento(veiculoMovimento.getId());
			manager.remove(veiculoMovimento);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Registro não pode ser excluído.");
		}
	}
	
	public Veiculo porId(Long id) {
		return manager.find(Veiculo.class, id);
	}
	
	public VeiculoMovimento porIdVeiculoMovimento(Long id) {
		return manager.find(VeiculoMovimento.class, id);
	}
	
	public Veiculo porPlaca(String placa) {
		Veiculo veiculo = null;
		
		try {
			veiculo = manager.createQuery("from Veiculo where lower(placa) = :placa", Veiculo.class)
				.setParameter("placa", placa.toLowerCase()).getSingleResult();
		} catch (NoResultException e) {
			// nenhum registro encontrado
		}
		return veiculo;
	}
	
	public Veiculo porRenavan(String renavan) {
		Veiculo veiculo = null;
		
		try {
			veiculo = manager.createQuery("from Veiculo where lower(renavan) = :renavan", Veiculo.class)
				.setParameter("renavan", renavan.toLowerCase()).getSingleResult();
		} catch (NoResultException e) {
			// nenhum registro encontrado
		}
		return veiculo;
	}
	
	@SuppressWarnings("unchecked")
	public List<VeiculoMovimento> movimentacoes(Veiculo veiculo, Date dataDe, Date dataAte) {
		Session session = manager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(VeiculoMovimento.class);
		
		if (veiculo != null) {
			criteria.add(Restrictions.eq("veiculo", veiculo));
		}
		
		if (dataDe != null) {
			criteria.add(Restrictions.ge("dataMovimento", dataDe));
		}
		
		if (dataAte != null) {
			criteria.add(Restrictions.le("dataMovimento", dataAte));
		}
		
		return criteria.addOrder(Order.asc("horaSaida")).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Veiculo> filtrados(VeiculoFilter filtro) {
		Session session = manager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Veiculo.class);
		
		// criado uma associação (join) com Modelo e nomeamos como "md"
		criteria.createAlias("modelo", "md");
		
		return criteria.addOrder(Order.asc("md.descricao")).list();
	}
	
	public Integer maiorKmInicial(Veiculo veiculo) {		
		try {
			Query query = manager.createQuery("select max(m.kmInicial) from VeiculoMovimento m where m.veiculo = :veiculo", Integer.class)
				.setParameter("veiculo", veiculo);				
			return (Integer)query.getSingleResult();
			
		} catch (NoResultException e) {			
			return 0;
		}
	}	
	
	public Integer maiorKmFinal(Veiculo veiculo) {		
		try {
			Query query = manager.createQuery("select max(m.kmFinal) from VeiculoMovimento m where m.veiculo = :veiculo", Integer.class)
				.setParameter("veiculo", veiculo);				
			return (Integer)query.getSingleResult();
			
		} catch (NoResultException e) {			
			return 0;
		}
	}
	
}


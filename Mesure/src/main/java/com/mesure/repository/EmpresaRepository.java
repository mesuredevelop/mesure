package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mesure.model.Empresa;
import com.mesure.model.Filial;
import com.mesure.repository.filter.EmpresaFilter;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class EmpresaRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Empresa guardar(Empresa empresa) {
		return manager.merge(empresa);
	}
	
	@Transactional
	public void remover(Empresa empresa) {
		try {
			empresa = porId(empresa.getId());
			manager.remove(empresa);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Empresa não pode ser excluída.");
		}
	}
	
	public Empresa porId(Long id) {
		return manager.find(Empresa.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Empresa> empresas(EmpresaFilter filtro) {
		Session session = manager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Empresa.class);
		
		if (StringUtils.isNotBlank(filtro.getRazaoSocial())) {
			criteria.add(Restrictions.ilike("razaoSocial", filtro.getRazaoSocial(), MatchMode.ANYWHERE));
		}
		return criteria.addOrder(Order.asc("razaoSocial")).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Filial> filiais() {
		Session session = manager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Filial.class);
		
		// criado uma associação (join) com Empresa e nomeamos como "emp"
		criteria.createAlias("empresa", "emp");
		
		return criteria.addOrder(Order.asc("emp.id")).list();
	}
	
	public List<Filial> filialDe(Empresa empresa) {
		return manager.createQuery("from Filial where empresa = :empresa", Filial.class)
				.setParameter("empresa", empresa).getResultList();
	}	
	
	public List<Filial> outrasFiliais(Filial filial) {
		return manager.createQuery("from Filial f where f <> :filial", Filial.class)
				.setParameter("filial", filial).getResultList();
	}	
}


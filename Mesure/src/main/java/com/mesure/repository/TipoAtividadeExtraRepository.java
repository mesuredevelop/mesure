package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import com.mesure.model.TipoAtividadeExtra;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class TipoAtividadeExtraRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	@Transactional
	public TipoAtividadeExtra guardar(TipoAtividadeExtra tipoAtividadeExtra) {
		return manager.merge(tipoAtividadeExtra);
	}
	
	@Transactional
	public void remover(TipoAtividadeExtra tipoAtividadeExtra) {
		try {			
			tipoAtividadeExtra = porId(tipoAtividadeExtra.getId());
			manager.remove(tipoAtividadeExtra);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Tipo não pode ser excluído.");
		}
	}
	
	public TipoAtividadeExtra porId(Long id) {
		return manager.find(TipoAtividadeExtra.class, id);
	}	
	
	public List<TipoAtividadeExtra> tipos() {
		try {
			return manager.createQuery("from TipoAtividadeExtra order by descricao", TipoAtividadeExtra.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
}

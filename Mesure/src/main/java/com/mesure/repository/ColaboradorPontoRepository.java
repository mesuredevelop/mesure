package com.mesure.repository;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mesure.model.Colaborador;
import com.mesure.model.ColaboradorPonto;
import com.mesure.model.ColaboradorPontoExtra;
import com.mesure.model.ProjetoExecucao;
import com.mesure.model.TipoAtividadeExtra;
import com.mesure.repository.filter.ColaboradorPontoFilter;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class ColaboradorPontoRepository implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	public ColaboradorPonto guardar(ColaboradorPonto colaboradorPonto) {
		return manager.merge(colaboradorPonto);
	}
	
	public ColaboradorPontoExtra guardarAtividadeExtra(ColaboradorPontoExtra colaboradorPontoExtra) {
		return manager.merge(colaboradorPontoExtra);
	}	
	
	public ColaboradorPonto porId(Long id) {
		return manager.find(ColaboradorPonto.class, id);
	}
	
	public ColaboradorPontoExtra porIdAtividadeExtra(Long id) {
		return manager.find(ColaboradorPontoExtra.class, id);
	}
	
	@Transactional
	public void remover(ColaboradorPonto colaboradorPonto) {
		try {
			colaboradorPonto = porId(colaboradorPonto.getId());
			manager.remove(colaboradorPonto);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Registro não pode ser excluído.");
		}
	}
	
	@Transactional
	public void removerAtividadeExtra(ColaboradorPontoExtra colaboradorPontoExtra) {
		try {
			colaboradorPontoExtra = porIdAtividadeExtra(colaboradorPontoExtra.getId());
			manager.remove(colaboradorPontoExtra);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Registro não pode ser excluído.");
		}
	}	
	
	private Criteria criarCriteriaParaFiltro(ColaboradorPontoFilter filtro) {
		Session session = this.manager.unwrap(Session.class);
		
		Criteria criteria = session.createCriteria(ColaboradorPonto.class);
		
		if (filtro.getColaborador() != null) {
			criteria.add(Restrictions.eq("colaborador", filtro.getColaborador()));
		}		
		
		if (filtro.getDataPontoDe() != null) {
			criteria.add(Restrictions.ge("data", filtro.getDataPontoDe()));
		}
		
		if (filtro.getDataPontoAte() != null) {
			criteria.add(Restrictions.le("data", filtro.getDataPontoAte()));
		}
		return criteria;
	}
	
	@SuppressWarnings("unchecked")
	public List<ColaboradorPonto> marcacoes(ColaboradorPontoFilter filtro) {
		Criteria criteria = criarCriteriaParaFiltro(filtro);		
		criteria.addOrder(Order.desc("data"));
		
		return criteria.list();		
	}
	
	public ColaboradorPonto pontoColaborador(Colaborador colaborador, Date data) {
		try {
			return manager.createQuery("from ColaboradorPonto where colaborador = :colaborador and data = :data", ColaboradorPonto.class)
				.setParameter("colaborador", colaborador)
				.setParameter("data", data)
				.getSingleResult();
		} catch (NoResultException e) {
			return null; // nenhum registro encontrado
		} catch (Exception e) {
			return null; // caso retorne mais de um registro
		}
	}
	
	public List<TipoAtividadeExtra> tiposAtividadeExtra() {
		try {	
			return manager.createQuery("from TipoAtividadeExtra order by descricao", TipoAtividadeExtra.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}	
	
	public List<ProjetoExecucao> execucoesRealizadasProjetos(Colaborador colaborador, Date data) {
		try {
			return manager.createQuery("from ProjetoExecucao e where e.colaborador = :colaborador and e.data = :data", ProjetoExecucao.class)
					.setParameter("colaborador", colaborador)
					.setParameter("data", data)
					.getResultList();				
		} catch (NoResultException e) {
			return null;
		}
	}	
	
	public BigDecimal horasExecucoesProjetos(Colaborador colaborador, Date data) {		
		BigDecimal horas = new BigDecimal(0);
		
		try {
			Query query = manager.createQuery("select sum(e.totalHoras) from ProjetoExecucao e where e.colaborador = :colaborador and e.data = :data", BigDecimal.class)
				.setParameter("colaborador", colaborador)
				.setParameter("data", data);
			horas = (BigDecimal)query.getSingleResult();
			
		} catch (NoResultException e) {			
			// nenhum registro encontrado
		}
		
		if (horas == null) {
			horas = new BigDecimal(0);
		}
		return horas;
	}
	
	public List<ColaboradorPontoExtra> atividadesExtras(Colaborador colaborador, Date data) {
		try {
			return manager.createQuery("from ColaboradorPontoExtra a where a.colaborador = :colaborador and a.data = :data", ColaboradorPontoExtra.class)
					.setParameter("colaborador", colaborador)
					.setParameter("data", data)
					.getResultList();				
		} catch (NoResultException e) {
			return null;
		}
	}	
	
	public BigDecimal horasAtividadesExtras(Colaborador colaborador, Date data) {		
		BigDecimal horas = new BigDecimal(0);
		
		try {
			Query query = manager.createQuery("select sum(a.totalHoras) from ColaboradorPontoExtra a where a.colaborador = :colaborador and a.data = :data", BigDecimal.class)
				.setParameter("colaborador", colaborador)
				.setParameter("data", data);
			horas = (BigDecimal)query.getSingleResult();
			
		} catch (NoResultException e) {
			// nenhum registro encontrado
		}
		
		if (horas == null) {
			horas = new BigDecimal(0);
		}
		return horas;
	}
	
}

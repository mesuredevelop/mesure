package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mesure.model.Fornecedor;
import com.mesure.repository.filter.FornecedorFilter;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;	

public class FornecedorRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Fornecedor guardar(Fornecedor fornecedor) {
		return manager.merge(fornecedor);
	}
	
	@Transactional
	public void remover(Fornecedor fornecedor) {
		try {
			fornecedor = porId(fornecedor.getId());
			manager.remove(fornecedor);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Fornecedor não pode ser excluído.");
		}
	}
	
	public Fornecedor porId(Long id) {
		return manager.find(Fornecedor.class, id);
	}
	
	public List<Fornecedor> fornecedores() {
		try {
			return manager.createQuery("from Fornecedor order by nome", Fornecedor.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}		
	
	@SuppressWarnings("unchecked")
	public List<Fornecedor> filtrados(FornecedorFilter filtro) {
		Session session = manager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Fornecedor.class);
		
		if (StringUtils.isNotBlank(filtro.getNome())) {
			criteria.add(Restrictions.ilike("nome", filtro.getNome(), MatchMode.ANYWHERE));
		}
		return criteria.addOrder(Order.asc("nome")).list();
	}	
}


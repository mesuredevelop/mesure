package com.mesure.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import com.mesure.model.TipoAtividadePrevista;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class TipoAtividadePrevistaRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	@Transactional
	public TipoAtividadePrevista guardar(TipoAtividadePrevista tipoAtividadePrevista) {
		return manager.merge(tipoAtividadePrevista);
	}
	
	@Transactional
	public void remover(TipoAtividadePrevista tipoAtividadePrevista) {
		try {			
			tipoAtividadePrevista = porId(tipoAtividadePrevista.getId());
			manager.remove(tipoAtividadePrevista);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Tipo não pode ser excluído.");
		}
	}
	
	public TipoAtividadePrevista porId(Long id) {
		return manager.find(TipoAtividadePrevista.class, id);
	}	
	
	public List<TipoAtividadePrevista> tipos() {
		try {
			return manager.createQuery("from TipoAtividadePrevista order by descricao", TipoAtividadePrevista.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
}

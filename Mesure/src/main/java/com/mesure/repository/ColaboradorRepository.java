package com.mesure.repository;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoColaborador;
import com.mesure.model.Colaborador;
import com.mesure.model.Usuario;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

public class ColaboradorRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public Colaborador guardar(Colaborador colaborador) {
        return manager.merge(colaborador);
    }

    @Transactional
    public void remover(Colaborador colaborador) {
        try {
            colaborador = porId(colaborador.getId());
            manager.remove(colaborador);
            manager.flush();
        } catch (PersistenceException e) {
            throw new NegocioException("Colaborador não pode ser excluído.");
        }
    }

    public Colaborador porId(Long id) {
        return manager.find(Colaborador.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Colaborador> colaboradores(ColaboradorFilter filtro) {
        try {
            String sql = "select c from Colaborador c where (1=1)";
            if (filtro.getSomenteAtivos().equals(ESimNao.SIM)) {
                sql += " and c.situacao <> :situacao";
            }
            if (filtro.getSomenteRegistraPonto().equals(ESimNao.SIM)) {
                sql += " and c.registraPonto = :registraPonto";
            }
            if (filtro.getFilial() != null) {
                sql += " and c.filial = :filial";
            }
            sql += " order by c.nome";

            Query query = manager.createQuery(sql, Colaborador.class);

            if (filtro.getSomenteAtivos().equals(ESimNao.SIM)) {
                query.setParameter("situacao", ESituacaoColaborador.DEMITIDO);
            }
            if (filtro.getSomenteRegistraPonto().equals(ESimNao.SIM)) {
                query.setParameter("registraPonto", ESimNao.SIM);
            }
            if (filtro.getFilial() != null) {
                query.setParameter("filial", filtro.getFilial());
            }
            return query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

    public Colaborador porUsuario(Usuario usuario) {
        Colaborador colaborador = null;

        try {
            colaborador = manager.createQuery("from Colaborador where usuario = :usuario", Colaborador.class)
                    .setParameter("usuario", usuario)
                    .getSingleResult();
        } catch (NoResultException e) {
            // nenhum colaborador encontrado com o usuário informado
        }
        return colaborador;
    }

    @SuppressWarnings("unchecked")
    public List<String> cargosAutoComplete() {
        try {
            return manager.createQuery("select distinct c.cargo from Colaborador c order by c.cargo").getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Colaborador treinamentosDoColaborador(Colaborador colaborador) {
        try {
            Query query = manager.createQuery("SELECT c FROM Colaborador AS c JOIN FETCH c.treinamentos WHERE c = :colaborador");
            query.setParameter("colaborador", colaborador);
            return (Colaborador) query.getSingleResult();
        } catch (NoResultException e) {
            // nenhum registro encontrado
            return null;
        }
    }

}

package com.mesure.repository;

import com.mesure.model.MovimentoFinanceiro;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

public class MovimentoFinanceiroRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public MovimentoFinanceiro guardar(MovimentoFinanceiro movimento) {
        return manager.merge(movimento);
    }

    @Transactional
    public void remover(MovimentoFinanceiro movimento) {
        try {
            movimento = porId(movimento.getId());
            manager.remove(movimento);
            manager.flush();
        } catch (PersistenceException e) {
            throw new NegocioException("Movimento financeiro não pode ser excluído.");
        }
    }

    public MovimentoFinanceiro porId(Long id) {
        return manager.find(MovimentoFinanceiro.class, id);
    }

}

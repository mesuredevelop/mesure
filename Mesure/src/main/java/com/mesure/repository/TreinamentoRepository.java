package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import com.mesure.model.Treinamento;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class TreinamentoRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	@Transactional
	public Treinamento guardar(Treinamento treinamento) {
		return manager.merge(treinamento);
	}
	
	@Transactional
	public void remover(Treinamento treinamento) {
		try {			
			treinamento = porId(treinamento.getId());
			manager.remove(treinamento);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Treinamento não pode ser excluído.");
		}
	}
	
	public Treinamento porId(Long id) {
		return manager.find(Treinamento.class, id);
	}	
	
	public List<Treinamento> treinamentos() {
		try {
			return manager.createQuery("from Treinamento order by descricao", Treinamento.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

}

package com.mesure.repository.filter;

import java.io.Serializable;
import java.util.Date;

import com.mesure.model.Colaborador;

public class ColaboradorPontoFilter implements Serializable {

	private static final long serialVersionUID = 1L;
	private Date dataPontoDe;
	private Date dataPontoAte;
	private Colaborador colaborador;

	public Date getDataPontoDe() {
		return dataPontoDe;
	}

	public void setDataPontoDe(Date dataPontoDe) {
		this.dataPontoDe = dataPontoDe;
	}

	public Date getDataPontoAte() {
		return dataPontoAte;
	}

	public void setDataPontoAte(Date dataPontoAte) {
		this.dataPontoAte = dataPontoAte;
	}

	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

}


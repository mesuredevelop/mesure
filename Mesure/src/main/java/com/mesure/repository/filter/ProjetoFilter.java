package com.mesure.repository.filter;

import java.io.Serializable;

import com.mesure.enumerator.EStatusProjeto;
import com.mesure.model.Cliente;
import com.mesure.model.Colaborador;
import com.mesure.model.Filial;

public class ProjetoFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private String titulo;
	private Colaborador colaborador;
	private Colaborador responsavel;
	private Colaborador participante;
	private Cliente cliente;
	private Filial filial;
	private EStatusProjeto[] statuses;

	private String propriedadeOrdenacao = "id";
	private boolean ascendente = true;
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public Colaborador getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}

	public Colaborador getParticipante() {
		return participante;
	}

	public void setParticipante(Colaborador participante) {
		this.participante = participante;
	}

	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	public EStatusProjeto[] getStatuses() {
		return statuses;
	}

	public void setStatuses(EStatusProjeto[] statuses) {
		this.statuses = statuses;
	}

	public String getPropriedadeOrdenacao() {
		return propriedadeOrdenacao;
	}

	public void setPropriedadeOrdenacao(String propriedadeOrdenacao) {
		this.propriedadeOrdenacao = propriedadeOrdenacao;
	}

	public boolean isAscendente() {
		return ascendente;
	}

	public void setAscendente(boolean ascendente) {
		this.ascendente = ascendente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}


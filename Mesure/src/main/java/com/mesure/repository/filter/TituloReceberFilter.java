package com.mesure.repository.filter;

import java.io.Serializable;
import com.mesure.model.Cliente;
import com.mesure.model.Filial;

public class TituloReceberFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private Cliente cliente;
	private Filial filial;

	private String propriedadeOrdenacao = "id";
	private boolean ascendente = true;
		
	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	public String getPropriedadeOrdenacao() {
		return propriedadeOrdenacao;
	}

	public void setPropriedadeOrdenacao(String propriedadeOrdenacao) {
		this.propriedadeOrdenacao = propriedadeOrdenacao;
	}

	public boolean isAscendente() {
		return ascendente;
	}

	public void setAscendente(boolean ascendente) {
		this.ascendente = ascendente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}


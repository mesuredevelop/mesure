package com.mesure.repository.filter;

import java.io.Serializable;

public class EmpresaFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private String razaoSocial;

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
}


package com.mesure.repository.filter;

import java.io.Serializable;
import java.util.Date;

import com.mesure.enumerator.EStatusLoteFaturamento;
import com.mesure.model.Projeto;

public class FaturamentoLoteFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date competenciaDe;
	private Date competenciaAte;
	private Projeto projeto;
	private EStatusLoteFaturamento status;

	public Date getCompetenciaDe() {
		return competenciaDe;
	}

	public void setCompetenciaDe(Date competenciaDe) {
		this.competenciaDe = competenciaDe;
	}

	public Date getCompetenciaAte() {
		return competenciaAte;
	}

	public void setCompetenciaAte(Date competenciaAte) {
		this.competenciaAte = competenciaAte;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public EStatusLoteFaturamento getStatus() {
		return status;
	}

	public void setStatus(EStatusLoteFaturamento status) {
		this.status = status;
	}

}

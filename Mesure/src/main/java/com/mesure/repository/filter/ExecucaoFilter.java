package com.mesure.repository.filter;

import java.io.Serializable;
import java.util.Date;

import com.mesure.enumerator.ESituacaoAtividadePrevista;
import com.mesure.model.Colaborador;

public class ExecucaoFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date data;
	private Colaborador colaborador;
	private ESituacaoAtividadePrevista situacao;

	public ESituacaoAtividadePrevista getSituacao() {
		return situacao;
	}

	public void setSituacao(ESituacaoAtividadePrevista situacao) {
		this.situacao = situacao;
	}

	private String propriedadeOrdenacao = "id";
	private boolean ascendente = true;
		
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public String getPropriedadeOrdenacao() {
		return propriedadeOrdenacao;
	}

	public void setPropriedadeOrdenacao(String propriedadeOrdenacao) {
		this.propriedadeOrdenacao = propriedadeOrdenacao;
	}

	public boolean isAscendente() {
		return ascendente;
	}

	public void setAscendente(boolean ascendente) {
		this.ascendente = ascendente;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
}


package com.mesure.repository.filter;

import java.io.Serializable;
import java.util.Date;

import com.mesure.enumerator.EStatusLoteApuracaoHoras;
import com.mesure.model.Colaborador;

public class ApuracaoHorasLoteFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date competenciaDe;
	private Date competenciaAte;
	private Colaborador colaborador;
	private EStatusLoteApuracaoHoras status;

	public Date getCompetenciaDe() {
		return competenciaDe;
	}

	public void setCompetenciaDe(Date competenciaDe) {
		this.competenciaDe = competenciaDe;
	}

	public Date getCompetenciaAte() {
		return competenciaAte;
	}

	public void setCompetenciaAte(Date competenciaAte) {
		this.competenciaAte = competenciaAte;
	}

	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public EStatusLoteApuracaoHoras getStatus() {
		return status;
	}

	public void setStatus(EStatusLoteApuracaoHoras status) {
		this.status = status;
	}
	
}

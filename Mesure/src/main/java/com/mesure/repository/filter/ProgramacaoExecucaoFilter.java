package com.mesure.repository.filter;

import java.io.Serializable;
import java.util.Date;

import com.mesure.model.Colaborador;
import com.mesure.model.Filial;
import com.mesure.model.Projeto;
import com.mesure.model.Veiculo;

public class ProgramacaoExecucaoFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date dataDe;
	private Date dataAte;	
	private Veiculo veiculo;
	private Colaborador colaborador;
	private Filial filial;
	private Projeto projeto;
	
	private String propriedadeOrdenacao = "data";
	private boolean ascendente = true;

	public Date getDataDe() {
		return dataDe;
	}

	public void setDataDe(Date dataDe) {
		this.dataDe = dataDe;
	}

	public Date getDataAte() {
		return dataAte;
	}

	public void setDataAte(Date dataAte) {
		this.dataAte = dataAte;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public String getPropriedadeOrdenacao() {
		return propriedadeOrdenacao;
	}

	public void setPropriedadeOrdenacao(String propriedadeOrdenacao) {
		this.propriedadeOrdenacao = propriedadeOrdenacao;
	}

	public boolean isAscendente() {
		return ascendente;
	}

	public void setAscendente(boolean ascendente) {
		this.ascendente = ascendente;
	}
	
}

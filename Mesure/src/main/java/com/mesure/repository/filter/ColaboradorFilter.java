package com.mesure.repository.filter;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Filial;
import java.io.Serializable;

public class ColaboradorFilter implements Serializable {

    private static final long serialVersionUID = 1L;

    private Filial filial;
    private ESimNao somenteAtivos;
    private ESimNao somenteRegistraPonto;

    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }

    public ESimNao getSomenteAtivos() {
        return somenteAtivos;
    }

    public void setSomenteAtivos(ESimNao somenteAtivos) {
        this.somenteAtivos = somenteAtivos;
    }

    public ESimNao getSomenteRegistraPonto() {
        if (somenteRegistraPonto == null) {
            return ESimNao.NAO;
        }
        return somenteRegistraPonto;
    }

    public void setSomenteRegistraPonto(ESimNao somenteRegistraPonto) {
        this.somenteRegistraPonto = somenteRegistraPonto;
    }

}

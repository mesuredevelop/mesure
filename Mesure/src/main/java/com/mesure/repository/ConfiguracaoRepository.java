package com.mesure.repository;

import java.io.Serializable;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import com.mesure.model.Configuracao;
import com.mesure.model.Filial;
import com.mesure.util.jpa.Transactional;

public class ConfiguracaoRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
		
	@Transactional
	public Configuracao guardar(Configuracao configuracao) {
		return manager.merge(configuracao);
	}
		
	public Configuracao obterConfiguracao(Filial filial) {
		Configuracao configuracao = null;		
		try {
			configuracao = manager.createQuery("select c from Configuracao c where c.filial = :filial", Configuracao.class)
					.setParameter("filial", filial)
					.getSingleResult();
		} catch (NoResultException e) {
			// nenhum registro encontrado
			configuracao = null;
		} catch (Exception e) {
			// outra exceção
			configuracao = null;
		}
		return configuracao;		
	}
	
}

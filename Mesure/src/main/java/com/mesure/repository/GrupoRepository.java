package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import com.mesure.model.Grupo;
import com.mesure.model.Permissao;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class GrupoRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	public Grupo porId(Long id) {
		return manager.find(Grupo.class, id);
	}
	
	public Grupo guardar(Grupo grupo) {
		return manager.merge(grupo);
	}
	
	@Transactional
	public void remover(Grupo grupo) {
		try {
			grupo = porId(grupo.getId());
			manager.remove(grupo);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Grupo não pode ser excluído.");
		}
	}
	
	public List<Grupo> todos() {
		try {
			return manager.createQuery("from Grupo order by nome asc", Grupo.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}		
	}
	
	public List<Permissao> permissoes() {
		try {
			return manager.createQuery("from Permissao order by descricao asc", Permissao.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}		
	}
	
}


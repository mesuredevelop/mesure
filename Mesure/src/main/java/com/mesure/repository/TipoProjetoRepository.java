package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import com.mesure.model.TipoProjeto;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class TipoProjetoRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	@Transactional
	public TipoProjeto guardar(TipoProjeto tipoProjeto) {
		return manager.merge(tipoProjeto);
	}
	
	@Transactional
	public void remover(TipoProjeto tipoProjeto) {
		try {			
			tipoProjeto = porId(tipoProjeto.getId());
			manager.remove(tipoProjeto);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Tipo não pode ser excluído.");
		}
	}
	
	public TipoProjeto porId(Long id) {
		return manager.find(TipoProjeto.class, id);
	}	
	
	public List<TipoProjeto> tipos() {
		try {
			return manager.createQuery("from TipoProjeto order by descricao", TipoProjeto.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
}


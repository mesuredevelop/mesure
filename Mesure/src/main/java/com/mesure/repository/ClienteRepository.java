package com.mesure.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mesure.enumerator.ETipoEndereco;
import com.mesure.model.Cliente;
import com.mesure.model.EnderecoCliente;
import com.mesure.repository.filter.ClienteFilter;
import com.mesure.service.NegocioException;
import com.mesure.util.jpa.Transactional;

public class ClienteRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	public Cliente guardar(Cliente cliente) {
		return manager.merge(cliente);
	}
	
	@Transactional
	public void remover(Cliente cliente) {
		try {
			cliente = porId(cliente.getId());
			manager.remove(cliente);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Cliente não pode ser excluído.");
		}
	}
	
	public Cliente porId(Long id) {
		return manager.find(Cliente.class, id);
	}	

	public List<Cliente> clientes() {
		try {
			return manager.createQuery("from Cliente order by nome", Cliente.class).getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}	
	
	@SuppressWarnings("unchecked")
	public List<Cliente> filtrados(ClienteFilter filtro) {
		
		Session session = manager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Cliente.class);
		
		if (StringUtils.isNotBlank(filtro.getNome())) {
			criteria.add(Restrictions.ilike("nome", filtro.getNome(), MatchMode.ANYWHERE));
		}
		
		return criteria.addOrder(Order.asc("nome")).list();
	}
	
	@Transactional
	public void executeUpdate() {
		System.out.println("passo 1");
		Query query = manager.createNativeQuery("SET @@auto_increment_increment = :incremento");
		System.out.println("passo 2");
		query.setParameter("incremento", 1);
		System.out.println("passo 3");
		query.executeUpdate();
		System.out.println("passo 4");		
	}
	
	@SuppressWarnings("unchecked")
	public List<EnderecoCliente> enderecos(Cliente cliente, ETipoEndereco tipoEndereco) {
		Session session = manager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(EnderecoCliente.class);
		
		if (cliente != null) {
			criteria.add(Restrictions.eq("cliente", cliente));	
		}
		
		if (tipoEndereco != null) {
			criteria.add(Restrictions.eq("tipoEndereco", tipoEndereco));	
		}		
		
		return criteria.addOrder(Order.asc("logradouro")).list();
	}
}


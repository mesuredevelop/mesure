package com.mesure.consult;

import java.io.Serializable;
import java.math.BigDecimal;

import com.mesure.model.Colaborador;

public class CNApontamentoDetalhe implements Serializable {

	private static final long serialVersionUID = 1L;

	private Colaborador colaborador;
	private Integer diasTrabalhados;
	private BigDecimal horasProgramadas;
	private BigDecimal horasExecutadas;

	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public Integer getDiasTrabalhados() {
		return diasTrabalhados;
	}

	public void setDiasTrabalhados(Integer diasTrabalhados) {
		this.diasTrabalhados = diasTrabalhados;
	}

	public BigDecimal getHorasProgramadas() {
		return horasProgramadas;
	}

	public void setHorasProgramadas(BigDecimal horasProgramadas) {
		this.horasProgramadas = horasProgramadas;
	}

	public BigDecimal getHorasExecutadas() {
		return horasExecutadas;
	}

	public void setHorasExecutadas(BigDecimal horasExecutadas) {
		this.horasExecutadas = horasExecutadas;
	}

}

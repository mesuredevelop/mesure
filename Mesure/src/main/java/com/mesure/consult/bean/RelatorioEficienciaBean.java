package com.mesure.consult.bean;

import com.mesure.consult.CNEficiencia;
import com.mesure.consult.repository.CNEficienciaRepository;
import com.mesure.consult.repository.filter.CNEficienciaFilter;
import com.mesure.enumerator.ESimNao;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class RelatorioEficienciaBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    Seguranca seguranca;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    @Inject
    CNEficienciaRepository cneficienciaRepository;

    private CNEficienciaFilter filtro;
    private List<CNEficiencia> eficiencia;

    public RelatorioEficienciaBean() {
        filtro = new CNEficienciaFilter();
    }

    public void pesquisar() {
        if (filtro == null) {
            filtro = new CNEficienciaFilter();
        }

        // Para trazer somente projetos da filial ativa
        if (usuarioLogado.getFilialAtiva().getFiltrarProjetos().equals(ESimNao.SIM)) {
            filtro.setFilial(usuarioLogado.getFilialAtiva());
        }

        eficiencia = cneficienciaRepository.pesquisar(filtro);
    }

    public void limparPesquisa() {
        eficiencia = new ArrayList<>();
    }

    public List<CNEficiencia> getEficiencia() {
        return eficiencia;
    }

    public void setEficiencia(List<CNEficiencia> eficiencia) {
        this.eficiencia = eficiencia;
    }

}

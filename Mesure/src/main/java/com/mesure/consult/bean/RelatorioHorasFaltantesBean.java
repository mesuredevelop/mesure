package com.mesure.consult.bean;

import com.mesure.consult.CNHorasFaltantes;
import com.mesure.consult.repository.CNHorasFaltantesRepository;
import com.mesure.consult.repository.filter.CNHorasFaltantesFilter;
import com.mesure.enumerator.ESimNao;
import com.mesure.model.Colaborador;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.service.NegocioException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class RelatorioHorasFaltantesBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private CNHorasFaltantesRepository cnhorasFaltantesRepository;
    
    @Inject
    ColaboradorRepository colaboradorRepository;

    private CNHorasFaltantesFilter filtro;
    
    private List<Colaborador> colaboradores;
    private List<CNHorasFaltantes> horasFaltantes;

    public RelatorioHorasFaltantesBean() {

    }

    public void inicializar() {
        filtro = new CNHorasFaltantesFilter();
        
        ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
        colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
        colaboradorFilter.setSomenteRegistraPonto(ESimNao.SIM);
        colaboradores = colaboradorRepository.colaboradores(colaboradorFilter);        
    }

    public void pesquisar() {
        horasFaltantes = cnhorasFaltantesRepository.pesquisar(filtro);

        if (horasFaltantes == null || horasFaltantes.isEmpty()) {
            throw new NegocioException("Nenhum registro encontrado");
        }
    }

    public void limparPesquisa() {
        horasFaltantes = new ArrayList<>();
    }

    public void limparFiltros() {
        filtro.setDataDe(null);
        filtro.setDataAte(null);
        filtro.setColaborador(null);
    }

    public void atualizarFiltros() {

    }
    
    public CNHorasFaltantesFilter getFiltro() {
        return filtro;
    }
    
    public List<Colaborador> getColaboradores() {
		return colaboradores;
	}

	public List<CNHorasFaltantes> getHorasFaltantes() {
        return horasFaltantes;
    }

}

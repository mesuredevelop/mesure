package com.mesure.consult.bean;

import com.mesure.consult.CNApontamento;
import com.mesure.consult.CNApontamentoDetalhe;
import com.mesure.consult.repository.CNApontamentoRepository;
import com.mesure.consult.repository.filter.CNApontamentoFilter;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.model.Cliente;
import com.mesure.model.Colaborador;
import com.mesure.model.Filial;
import com.mesure.model.Projeto;
import com.mesure.repository.ClienteRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.EmpresaRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.security.Seguranca;
import com.mesure.service.NegocioException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class RelatorioApontamentoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    Seguranca seguranca;

    @Inject
    CNApontamentoRepository cnApontamentoRepository;

    @Inject
    ProjetoRepository projetoRepository;

    @Inject
    EmpresaRepository empresaRepository;

    @Inject
    ClienteRepository clienteRepository;

    @Inject
    ColaboradorRepository colaboradorRepository;

    private List<CNApontamento> apontamentos;
    private List<CNApontamentoDetalhe> detalhes;

    private Projeto projetoDetalhe;
    private boolean carregouRegistros;

    // Dados para o filtro
    private CNApontamentoFilter filtro;
    private List<Projeto> projetos;
    private List<Filial> filiais;
    private List<Cliente> clientes;
    private List<Colaborador> colaboradores;

    public RelatorioApontamentoBean() {

    }

    public void inicializar() {

        setCarregouRegistros(false);

        // Se projetoDetalhe estiver nulo significa que está visualizando o relatório de apontamentos,
        // caso contrário está visualizando o detalhamento
        if (projetoDetalhe == null) {

            filtro = new CNApontamentoFilter();

            // Carrega todos os projetos em aberto
            ProjetoFilter projetoFilter = new ProjetoFilter();
            EStatusProjeto[] statuses = {EStatusProjeto.COMECAR, EStatusProjeto.ANDAMENTO, EStatusProjeto.ATV_EXTERNA};
            projetoFilter.setStatuses(statuses);
            projetoFilter.setPropriedadeOrdenacao("titulo");
            projetos = projetoRepository.filtrados(projetoFilter);

            // Carrega todas as filiais
            filiais = empresaRepository.filiais();

            // Carrega todos os clientes
            clientes = clienteRepository.clientes();

            // Carrega todos os colaboradores ativos
            ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
            colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
            colaboradorFilter.setSomenteRegistraPonto(ESimNao.SIM);
            colaboradores = colaboradorRepository.colaboradores(colaboradorFilter);

            // Detalhes
        } else {
            detalhes = cnApontamentoRepository.pesquisarDetalhe(projetoDetalhe);
            setCarregouRegistros(true);
        }

    }

    public void pesquisar() {
        apontamentos = cnApontamentoRepository.pesquisar(filtro);

        if (apontamentos == null || apontamentos.isEmpty()) {
            throw new NegocioException("Nenhum registro encontrado");
        }
    }

    public void limparPesquisa() {
        apontamentos = new ArrayList<>();
    }

    public void limparFiltros() {
        filtro.setFilial(null);
        filtro.setCliente(null);
        filtro.setProjeto(null);
        filtro.setResponsavel(null);
        filtro.setStatus(null);
    }

    public void atualizarFiltros() {

    }

    // Para carregar as opções do Enum no formulário
    public EStatusProjeto[] getStatuses() {
        return EStatusProjeto.values();
    }

    public Projeto getProjetoDetalhe() {
        return projetoDetalhe;
    }

    public void setProjetoDetalhe(Projeto projetoDetalhe) {
        this.projetoDetalhe = projetoDetalhe;
    }

    public boolean isCarregouRegistros() {
        return carregouRegistros;
    }

    public void setCarregouRegistros(boolean carregouRegistros) {
        this.carregouRegistros = carregouRegistros;
    }

    public CNApontamentoFilter getFiltro() {
        return filtro;
    }

    public void setFiltro(CNApontamentoFilter filtro) {
        this.filtro = filtro;
    }

    public List<CNApontamento> getApontamentos() {
        return apontamentos;
    }

    public List<CNApontamentoDetalhe> getDetalhes() {
        return detalhes;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public List<Filial> getFiliais() {
        return filiais;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }

}

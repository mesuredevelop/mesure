package com.mesure.consult;

import com.mesure.enumerator.EDiaSemana;
import com.mesure.enumerator.ETipoRegistroPonto;
import com.mesure.model.Colaborador;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CNHorasFaltantes implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date data;
    private EDiaSemana diaSemana;
    private ETipoRegistroPonto tipoRegistro;
    private Colaborador colaborador;
    private BigDecimal cargaHoraria;
    private BigDecimal horasApontadas;
    private BigDecimal horasExecutadas;
    private BigDecimal horasFaltantes;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public EDiaSemana getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(EDiaSemana diaSemana) {
        this.diaSemana = diaSemana;
    }

    public ETipoRegistroPonto getTipoRegistro() {
        return tipoRegistro;
    }

    public void setTipoRegistro(ETipoRegistroPonto tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public BigDecimal getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(BigDecimal cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public BigDecimal getHorasApontadas() {
        return horasApontadas;
    }

    public void setHorasApontadas(BigDecimal horasApontadas) {
        this.horasApontadas = horasApontadas;
    }
    
	public BigDecimal getHorasExecutadas() {
		return horasExecutadas;
	}

	public void setHorasExecutadas(BigDecimal horasExecutadas) {
		this.horasExecutadas = horasExecutadas;
	}

	public BigDecimal getHorasFaltantes() {
        return horasFaltantes;
    }

    public void setHorasFaltantes(BigDecimal horasFaltantes) {
        this.horasFaltantes = horasFaltantes;
    }

}

package com.mesure.consult;

import com.mesure.model.Projeto;
import java.io.Serializable;
import java.math.BigDecimal;

public class CNEficiencia implements Serializable {

    private static final long serialVersionUID = 1L;

    private Projeto projeto;
    private BigDecimal horasOrcadas;
    private BigDecimal horasRealizadas;
    private BigDecimal percentualHoras;
    private BigDecimal percentualCusto;

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public BigDecimal getHorasOrcadas() {
        return horasOrcadas;
    }

    public void setHorasOrcadas(BigDecimal horasOrcadas) {
        this.horasOrcadas = horasOrcadas;
    }

    public BigDecimal getHorasRealizadas() {
        return horasRealizadas;
    }

    public void setHorasRealizadas(BigDecimal horasRealizadas) {
        this.horasRealizadas = horasRealizadas;
    }

    public BigDecimal getPercentualHoras() {
        return percentualHoras;
    }

    public void setPercentualHoras(BigDecimal percentualHoras) {
        this.percentualHoras = percentualHoras;
    }

    public BigDecimal getPercentualCusto() {
        return percentualCusto;
    }

    public void setPercentualCusto(BigDecimal percentualCusto) {
        this.percentualCusto = percentualCusto;
    }

}

package com.mesure.consult.repository;

import com.mesure.consult.CNEficiencia;
import com.mesure.consult.repository.filter.CNEficienciaFilter;
import com.mesure.model.Projeto;
import com.mesure.model.ProjetoExecucao;
import com.mesure.model.ProjetoProposta;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.filter.ProjetoFilter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class CNEficienciaRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private ProjetoRepository projetoRepository;

    public List<CNEficiencia> pesquisar(CNEficienciaFilter filtro) {
        List<CNEficiencia> eficiencias = new ArrayList<>();
        List<Projeto> projetos = projetoRepository.filtrados(getProjetoFilter(filtro));

        for (Projeto projeto : projetos) {
            CNEficiencia cneficiencia = new CNEficiencia();
            cneficiencia.setProjeto(projeto);
            cneficiencia.setHorasOrcadas(getHorasOrcadas(projeto.getPropostas()));
            cneficiencia.setHorasRealizadas(getHorasRealizadas(projeto.getExecucoes()));
            cneficiencia.setPercentualHoras(getPercentualHoras(cneficiencia));
            cneficiencia.setPercentualCusto(getPercentualCusto(projeto));
            eficiencias.add(cneficiencia);
        }

        return eficiencias;
    }

    private ProjetoFilter getProjetoFilter(CNEficienciaFilter filtro) {
        ProjetoFilter projetoFilter = new ProjetoFilter();
        projetoFilter.setFilial(filtro.getFilial());
        return projetoFilter;
    }

    private BigDecimal getHorasOrcadas(List<ProjetoProposta> propostas) {
        BigDecimal totalHorasOrcadas = BigDecimal.ZERO;
        for (ProjetoProposta proposta : propostas) {
            if (proposta.getTotalHoras() != null) {
                totalHorasOrcadas = totalHorasOrcadas.add(proposta.getTotalHoras());
            }
        }
        return totalHorasOrcadas;
    }

    private BigDecimal getHorasRealizadas(List<ProjetoExecucao> execucoes) {
        BigDecimal totalHorasExecutadas = BigDecimal.ZERO;
        for (ProjetoExecucao execucao : execucoes) {
            if (execucao.getTotalHoras() != null) {
                totalHorasExecutadas = totalHorasExecutadas.add(execucao.getTotalHoras());
            }
        }
        return totalHorasExecutadas;
    }

    private BigDecimal getPercentualHoras(CNEficiencia cneficiencia) {
        float percentualHoras = 0;
        if (cneficiencia.getHorasOrcadas().floatValue() > 0 && cneficiencia.getHorasRealizadas().floatValue() > 0) {
            percentualHoras = 100 / cneficiencia.getHorasOrcadas().floatValue();
            percentualHoras = percentualHoras * cneficiencia.getHorasRealizadas().floatValue();
        }
        return new BigDecimal(percentualHoras);
    }

    private BigDecimal getPercentualCusto(Projeto projeto) {
        float percentualCusto = 0;
        BigDecimal valorOrcado = getValorOrcado(projeto.getPropostas());
        BigDecimal valorCusto = getValorCusto(projeto.getExecucoes());
        if (valorCusto.floatValue() > 0 && valorOrcado.floatValue() > 0) {
            percentualCusto = 100 / valorOrcado.floatValue();
            percentualCusto = percentualCusto * valorCusto.floatValue();
        }
        return new BigDecimal(percentualCusto);
    }

    private BigDecimal getValorOrcado(List<ProjetoProposta> propostas) {
        BigDecimal valorOrcado = BigDecimal.ZERO;
        for (ProjetoProposta proposta : propostas) {
            valorOrcado = valorOrcado.add(proposta.getValorProjeto());
        }
        return valorOrcado;
    }

    private BigDecimal getValorCusto(List<ProjetoExecucao> execucoes) {
        BigDecimal valorCusto = BigDecimal.ZERO;
        for (ProjetoExecucao execucao : execucoes) {
            if (execucao.getValorTotal() != null) {
                valorCusto = valorCusto.add(execucao.getValorTotal());
            }
        }
        return valorCusto;
    }

}

package com.mesure.consult.repository;

import com.mesure.consult.CNHorasFaltantes;
import com.mesure.consult.repository.filter.CNHorasFaltantesFilter;
import com.mesure.enumerator.EDiaSemana;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ETipoRegistroPonto;
import com.mesure.model.Colaborador;
import com.mesure.model.ColaboradorPonto;
import com.mesure.model.Feriado;
import com.mesure.repository.ColaboradorPontoRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.FeriadoRepository;
import com.mesure.repository.filter.ColaboradorFilter;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.service.ColaboradorPontoService;
import com.mesure.service.NegocioException;
import com.mesure.util.Util;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.inject.Inject;

public class CNHorasFaltantesRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private ColaboradorRepository colaboradorRepository;

    @Inject
    ColaboradorPontoRepository colaboradorPontoRepository;
    
    @Inject
    ColaboradorPontoService colaboradorPontoService;

    @Inject
    FeriadoRepository feriadoRepository;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    public List<CNHorasFaltantes> pesquisar(CNHorasFaltantesFilter filtro) {
        if (filtro == null) {
            filtro = new CNHorasFaltantesFilter();
        }

        if (filtro.getDataDe() == null) {
            Calendar dataMenor = Calendar.getInstance();
            dataMenor.add(Calendar.DATE, -1);
            filtro.setDataDe(dataMenor.getTime());
        }

        if (filtro.getDataAte() == null) {
            Calendar dataMaior = Calendar.getInstance();
            dataMaior.add(Calendar.DATE, -1);
            filtro.setDataAte(dataMaior.getTime());
        }

        if (filtro.getDataAte().before(filtro.getDataDe())) {
            throw new NegocioException("A data do filtro \"De\" deve ser menor ou igual que a data do filtro \"Até\".");
        }

        List<CNHorasFaltantes> horasFaltantesList = new ArrayList<>();
        List<Feriado> feriados = feriadoRepository.obterFeriados(usuarioLogado.getFilialAtiva());
        List<Colaborador> colaboradores = getColaboradores(filtro);

        Calendar dataAux = new GregorianCalendar();
        dataAux.setTime(filtro.getDataDe());
        
        boolean verificar;

        long dif = Util.DiferencaEmDias(filtro.getDataDe(), filtro.getDataAte()) + 1;
        for (int i = 1; i <= dif; i++) {
            for (Colaborador colaborador : colaboradores) {
            	
            	// Controle para não verificar dias anteriores à admissão do colaborador
            	if (dataAux.getTime().getTime() >= colaborador.getDataAdmissao().getTime()) {
            		
                    CNHorasFaltantes horasFaltantes = new CNHorasFaltantes();
                    horasFaltantes.setData(dataAux.getTime());
                    horasFaltantes.setDiaSemana(Util.diaDaSemanaEnum(dataAux.getTime()));
                    horasFaltantes.setColaborador(colaborador);
                    
                	// Possíveis tipos de registro de ponto:
                    // NORMAL, ATESTADO, FALTA, FOLGA, FERIADO, FDSEMANA
                    
                    verificar = false;
                    
                    ColaboradorPonto ponto = colaboradorPontoRepository.pontoColaborador(colaborador, dataAux.getTime());

                    if (ponto == null) {
                        if (Util.isFeriado(feriados, dataAux.getTime())) {
                            horasFaltantes.setTipoRegistro(ETipoRegistroPonto.FERIADO);
                            horasFaltantes.setCargaHoraria(BigDecimal.ZERO);
                            horasFaltantes.setHorasApontadas(BigDecimal.ZERO);
                            horasFaltantes.setHorasExecutadas(BigDecimal.ZERO);
                            horasFaltantes.setHorasFaltantes(BigDecimal.ZERO);
                            
                        } else if (horasFaltantes.getDiaSemana() == EDiaSemana.SABADO 
                        		|| horasFaltantes.getDiaSemana() == EDiaSemana.DOMINGO) {
                            horasFaltantes.setTipoRegistro(ETipoRegistroPonto.FDSEMANA);
                            horasFaltantes.setCargaHoraria(BigDecimal.ZERO);
                            horasFaltantes.setHorasApontadas(BigDecimal.ZERO);
                            horasFaltantes.setHorasExecutadas(BigDecimal.ZERO);
                            horasFaltantes.setHorasFaltantes(BigDecimal.ZERO);
                        }

                        // Se não é FINAL DE SEMANA nem FERIADO, então é FALTA
                        if (horasFaltantes.getTipoRegistro() == null) {
                            horasFaltantes.setTipoRegistro(ETipoRegistroPonto.FALTA);
                            horasFaltantes.setCargaHoraria(colaborador.getHorasTrabalhoDiario());
                            horasFaltantes.setHorasApontadas(BigDecimal.ZERO);
                            horasFaltantes.setHorasExecutadas(BigDecimal.ZERO);
                            horasFaltantes.setHorasFaltantes(colaborador.getHorasTrabalhoDiario());
                            verificar = true;
                        }
                    } else {
                        horasFaltantes.setTipoRegistro(ponto.getTipoRegistro());
                        
                        // Quando for ponto Normal, Feriado ou Final de semana, deve considerar as horas que o usuário apontou
                        if (horasFaltantes.getTipoRegistro().equals(ETipoRegistroPonto.NORMAL)) { 
                        	horasFaltantes.setCargaHoraria(colaborador.getHorasTrabalhoDiario());
                        	horasFaltantes.setHorasApontadas(ponto.getTotalHoras());
                        	horasFaltantes.setHorasExecutadas(getHorasExecutadas(horasFaltantes));
                        	horasFaltantes.setHorasFaltantes(getHorasFaltantes(horasFaltantes));
                        	verificar = true;
                        	
                        } else if (horasFaltantes.getTipoRegistro().equals(ETipoRegistroPonto.FERIADO) 
                        		|| horasFaltantes.getTipoRegistro().equals(ETipoRegistroPonto.FDSEMANA)) {
                        	horasFaltantes.setCargaHoraria(BigDecimal.ZERO);
                        	horasFaltantes.setHorasApontadas(ponto.getTotalHoras());
                        	horasFaltantes.setHorasExecutadas(getHorasExecutadas(horasFaltantes));
                        	horasFaltantes.setHorasFaltantes(getHorasFaltantes(horasFaltantes));
                        	verificar = true;
                        	
                        } else if (horasFaltantes.getTipoRegistro().equals(ETipoRegistroPonto.FALTA)) {
                        	horasFaltantes.setCargaHoraria(colaborador.getHorasTrabalhoDiario());
                        	horasFaltantes.setHorasApontadas(BigDecimal.ZERO);
                        	horasFaltantes.setHorasExecutadas(BigDecimal.ZERO);
                        	horasFaltantes.setHorasFaltantes(colaborador.getHorasTrabalhoDiario());
                        	verificar = true;
                        	
                        } else { // ATESTADO, FOLGA
                        	horasFaltantes.setCargaHoraria(BigDecimal.ZERO);
                        	horasFaltantes.setHorasApontadas(BigDecimal.ZERO);
                        	horasFaltantes.setHorasExecutadas(BigDecimal.ZERO);
                        	horasFaltantes.setHorasFaltantes(BigDecimal.ZERO);
                        }
                    }
                    
                    if (verificar) {
                        String horasStr = Util.formatarDouble(horasFaltantes.getHorasFaltantes().floatValue());
                		
                		// Só mostra o registro se há horas faltantes            		
                		if (!Util.onlyNumbers(horasStr).equalsIgnoreCase("000")) {
                        	horasFaltantesList.add(horasFaltantes);
                        }
                    }
            	}
            }
            dataAux.add(Calendar.DAY_OF_MONTH, 1);
        }
        return horasFaltantesList;
    }

	private List<Colaborador> getColaboradores(CNHorasFaltantesFilter filtro) {
		if (filtro.getColaborador() != null) {
			List<Colaborador> retorno = new ArrayList<>();
			retorno.add(filtro.getColaborador());
			return retorno;
		} else {
	        ColaboradorFilter colaboradorFilter = new ColaboradorFilter();
	        colaboradorFilter.setSomenteAtivos(ESimNao.SIM);
	        colaboradorFilter.setSomenteRegistraPonto(ESimNao.SIM);
	        return colaboradorRepository.colaboradores(colaboradorFilter);
		}
    }
	
    private BigDecimal getHorasExecutadas(CNHorasFaltantes horasFaltantes) {
    	BigDecimal retorno = BigDecimal.ZERO;
    	
    	BigDecimal horasExecucoesProjetos = colaboradorPontoService.calcularHorasExecucoesProjetos(horasFaltantes.getColaborador(), horasFaltantes.getData());
    	BigDecimal horasAtividadesExtras = colaboradorPontoService.calcularHorasAtividadesExtras(horasFaltantes.getColaborador(), horasFaltantes.getData());
    	
    	retorno = retorno.add(horasExecucoesProjetos);
    	retorno = retorno.add(horasAtividadesExtras);
    	
    	return retorno;
	}

    private BigDecimal getHorasFaltantes(CNHorasFaltantes horasFaltantes) {
    	
    	BigDecimal retorno = BigDecimal.ZERO;
    	
		// Horas Apontadas x Horas Executadas
		retorno = horasFaltantes.getHorasApontadas().subtract(horasFaltantes.getHorasExecutadas());
    	
    	if (retorno.floatValue() < 0) {
    		retorno = BigDecimal.ZERO;
    	}
        return retorno;
    }
    
    public void verificarApontamentosAtrasados(Colaborador colaborador, Date data) {
    	// Verifica se permite apontamentos atrasados e quantos dias deve verificar
        if (colaborador.getPermiteApontamentoAtrasado().equals(ESimNao.NAO) 
        		&& colaborador.getFilial().getDiasApontamentoAtrasado() != null) {
        	
        	int diasApontamentoAtrasado = colaborador.getFilial().getDiasApontamentoAtrasado();
        	
        	if (diasApontamentoAtrasado > 0) {
                CNHorasFaltantesFilter filtro = new CNHorasFaltantesFilter();
                filtro.setColaborador(colaborador);

                // Data de
                Calendar dataFiltro = Calendar.getInstance();
                dataFiltro.add(Calendar.DATE, -diasApontamentoAtrasado);
                filtro.setDataDe(dataFiltro.getTime());
                
                // Data até (dia anterior)
                dataFiltro = Calendar.getInstance();
                dataFiltro.add(Calendar.DATE, -1);
                filtro.setDataAte(dataFiltro.getTime());
                
                List<CNHorasFaltantes> horasFaltantes = pesquisar(filtro);
                
                if (horasFaltantes != null && !horasFaltantes.isEmpty()) {
                	String msg = "";
                	for (CNHorasFaltantes hf : horasFaltantes) {
                		if (hf.getData().getTime() < data.getTime()) {
                    		msg += (msg.isEmpty()) ? "Foram encontradas pendências de ponto e/ou execução para o(s) dia(s): " + Util.formatarData(hf.getData()) : ", " + Util.formatarData(hf.getData());
                		}
                	}
                	if (!msg.isEmpty()) {
                    	msg += ". É necessário resolver estas pendências para então registrar um novo ponto/execução.";
                    	throw new NegocioException(msg);
                	}
                }
        	}
        }
    }    
}

package com.mesure.consult.repository.filter;

import com.mesure.enumerator.EStatusProjeto;
import com.mesure.model.Cliente;
import com.mesure.model.Colaborador;
import com.mesure.model.Filial;
import com.mesure.model.Projeto;

import java.io.Serializable;

public class CNApontamentoFilter implements Serializable {

    private static final long serialVersionUID = 1L;

    private Filial filial;
    private Cliente cliente;
    private Projeto projeto;
    private Colaborador responsavel;
    private EStatusProjeto status;

    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public Colaborador getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}

	public EStatusProjeto getStatus() {
		return status;
	}

	public void setStatus(EStatusProjeto status) {
		this.status = status;
	}
    

}

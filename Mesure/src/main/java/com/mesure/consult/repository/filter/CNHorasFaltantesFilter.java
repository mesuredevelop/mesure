package com.mesure.consult.repository.filter;

import java.io.Serializable;
import java.util.Date;

import com.mesure.model.Colaborador;

public class CNHorasFaltantesFilter implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date dataDe;
    private Date dataAte;
    private Colaborador colaborador;

    public Date getDataDe() {
        return dataDe;
    }

    public void setDataDe(Date dataDe) {
        this.dataDe = dataDe;
    }

    public Date getDataAte() {
        return dataAte;
    }

    public void setDataAte(Date dataAte) {
        this.dataAte = dataAte;
    }

	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
    
}

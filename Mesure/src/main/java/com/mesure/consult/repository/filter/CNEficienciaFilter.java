package com.mesure.consult.repository.filter;

import com.mesure.model.Filial;
import java.io.Serializable;

public class CNEficienciaFilter implements Serializable {

    private static final long serialVersionUID = 1L;

    private Filial filial;

    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }

}

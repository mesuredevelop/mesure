package com.mesure.consult.repository;

import com.mesure.consult.CNApontamento;
import com.mesure.consult.CNApontamentoDetalhe;
import com.mesure.consult.repository.filter.CNApontamentoFilter;
import com.mesure.enumerator.ESituacaoEtapa;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.model.Colaborador;
import com.mesure.model.ProgramacaoExecucao;
import com.mesure.model.Projeto;
import com.mesure.model.ProjetoEtapa;
import com.mesure.model.ProjetoExecucao;
import com.mesure.model.ProjetoProposta;
import com.mesure.repository.ProgramacaoExecucaoRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.filter.ProgramacaoExecucaoFilter;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.util.Util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class CNApontamentoRepository implements Serializable {

    private static final long serialVersionUID = 1L;
    
	@Inject
	private EntityManager manager;

    @Inject
    private ProjetoRepository projetoRepository;
    
    @Inject
    private ProgramacaoExecucaoRepository programacaoExecucaoRepository;
    

    public List<CNApontamento> pesquisar(CNApontamentoFilter filtro) {
        List<CNApontamento> apontamentos = new ArrayList<>();
        List<Projeto> projetos = new ArrayList<>();
        
        if (filtro.getProjeto() != null) {
        	Projeto p = projetoRepository.porId(filtro.getProjeto().getId());
        	if (p != null) {
        		projetos.add(p);
        	}
        } else {
        	projetos = projetoRepository.filtrados(getProjetoFilter(filtro));	
        }
        
        for (Projeto projeto : projetos) {
            CNApontamento cnApontamento = new CNApontamento();

            // Total de horas programadas
            BigDecimal totalHorasProgramadas = getTotalHorasProgramadas(projeto);
            // Total de horas executadas
            BigDecimal totalHorasExecutadas = getTotalHorasExecutadas(projeto);
            // Total de horas previstas
            BigDecimal totalHorasProjeto = getTotalHorasProjeto(projeto);

            // Saldo de horas restantes
            BigDecimal saldoHorasRestantes = totalHorasProjeto.subtract(totalHorasExecutadas);
            if (saldoHorasRestantes.floatValue() < 0) {
            	saldoHorasRestantes = BigDecimal.ZERO;
            }            
            // Saldo de dias restantes
            Integer saldoDiasRestantes = getSaldoDiasRestantes(projeto);
            // Percentual de conclusão do projeto
            BigDecimal perConclusaoProjeto = getPerConclusaoProjeto(projeto);
            
            cnApontamento.setProjeto(projeto);
            cnApontamento.setTotalHorasProjeto(totalHorasProjeto);//
            cnApontamento.setTotalHorasProgramadas(totalHorasProgramadas);
            cnApontamento.setTotalHorasExecutadas(totalHorasExecutadas);//
            cnApontamento.setSaldoHorasRestantes(saldoHorasRestantes);
            cnApontamento.setSaldoDiasRestantes(saldoDiasRestantes);
            cnApontamento.setPerConclusaoProjeto(perConclusaoProjeto);            
            apontamentos.add(cnApontamento);
        }
        return apontamentos;
    }
    
    private BigDecimal getTotalHorasProjeto(Projeto projeto) {
    	BigDecimal retorno = BigDecimal.ZERO;
        for (ProjetoProposta p : projeto.getPropostas()) {
            if (p.getDataAprovacao() != null && p.getTotalHoras() != null) {
            	retorno = retorno.add(p.getTotalHoras());
            }
        }
    	return retorno;
    }
    
    private BigDecimal getTotalHorasProgramadas(Projeto projeto) {
    	BigDecimal retorno = BigDecimal.ZERO;
        ProgramacaoExecucaoFilter filtro = new ProgramacaoExecucaoFilter();
        filtro.setProjeto(projeto);
        List<ProgramacaoExecucao> programacoes = programacaoExecucaoRepository.programacoes(filtro);

        for (ProgramacaoExecucao p : programacoes) {
            double total = Util.DiferencaEmHoras(p.getHoraInicio(), p.getHoraTermino());
            retorno = retorno.add(new BigDecimal(total));
        }
    	return retorno;
    }
    
    private BigDecimal getTotalHorasExecutadas(Projeto projeto) {
    	BigDecimal retorno = BigDecimal.ZERO;
        for (ProjetoExecucao p : projeto.getExecucoes()) {
            if (p.getTotalHoras() != null) {
            	retorno = retorno.add(p.getTotalHoras());
            }
        }
    	return retorno;
    }
    
    private Integer getSaldoDiasRestantes(Projeto projeto) {
    	Integer retorno = 0;
        if (!projeto.getStatus().equals(EStatusProjeto.FINALIZADO)) {
            Date dataAtual = new Date();
            for (ProjetoProposta p : projeto.getPropostas()) {
                if (p.getDataAprovacao() != null && p.getTotalDias() != null) {
                    Integer saldoDias = (int) (p.getTotalDias() - Util.DiferencaEmDias(p.getDataInicio(), dataAtual));
                    retorno += saldoDias;
                }
            }
        }
    	return retorno;
    }
    
    private BigDecimal getPerConclusaoProjeto(Projeto projeto) {
    	BigDecimal retorno = BigDecimal.ZERO;
        float totalEtapas = 0;
        float etapasPendentes = 0;
        float etapasFinalizadas = 0;

        for (ProjetoEtapa p : projeto.getEtapas()) {
            if (p.getSituacao().equals(ESituacaoEtapa.PENDENTE) || p.getSituacao().equals(ESituacaoEtapa.EXECUCAO)) {
                etapasPendentes = etapasPendentes + 1;
            }

            if (p.getSituacao().equals(ESituacaoEtapa.FINALIZADO)) {
                etapasFinalizadas = etapasFinalizadas + 1;
            }

            if (!p.getSituacao().equals(ESituacaoEtapa.CANCELADO)) {
                totalEtapas = totalEtapas + 1;
            }
        }

        float percentual = 0;
        if (totalEtapas != 0) {
            if (etapasPendentes == 0) {
                percentual = 100;
            } else {
                if (etapasPendentes == totalEtapas) {
                    percentual = 0;
                } else {
                    percentual = (etapasFinalizadas / totalEtapas) * 100;
                }
            }
        }
        retorno = new BigDecimal(percentual);
    	return retorno;
    }

    private ProjetoFilter getProjetoFilter(CNApontamentoFilter filtro) {
        ProjetoFilter projetoFilter = new ProjetoFilter();
        projetoFilter.setFilial(filtro.getFilial());
        projetoFilter.setCliente(filtro.getCliente());
        projetoFilter.setResponsavel(filtro.getResponsavel());
        if (filtro.getStatus() != null) {
            EStatusProjeto[] statuses = {filtro.getStatus()};
            projetoFilter.setStatuses(statuses);        	
        }
        return projetoFilter;
    }
    
    /*----------------------------- Detalhes do apontamento -----------------------------*/
    
    public List<CNApontamentoDetalhe> pesquisarDetalhe(Projeto projeto) {
    	List<CNApontamentoDetalhe> detalhes = new ArrayList<>();
    	
    	List<Colaborador> colaboradores = colaboradoresApontamento(projeto);
    	
    	for (Colaborador c : colaboradores) {
    		CNApontamentoDetalhe detalhe = new CNApontamentoDetalhe();
    		
    		// Colaborador
    		detalhe.setColaborador(c);
    		// Dias trabalhados
    		List<Date> datas = datasApontamento(projeto, c);    		
    		detalhe.setDiasTrabalhados(datas.size());
    		// Total horas programadas
    		detalhe.setHorasProgramadas(horasProgramadasApontamento(projeto, c));
    		// Total horas executadas
    		detalhe.setHorasExecutadas(horasExecutadasApontamento(projeto, c));
    		
    		detalhes.add(detalhe);
    	}
    	return detalhes;
    }
    
	@SuppressWarnings("unchecked")
	private List<Colaborador> colaboradoresApontamento(Projeto projeto) {
		try {
			//return manager.createQuery("select distinct exe.colaborador as col from ProjetoExecucao exe where exe.projeto = :projeto order by col.nome")
			return manager.createQuery("select distinct exe.colaborador from ProjetoExecucao exe where exe.projeto = :projeto")
					 .setParameter("projeto", projeto)
					 .getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Date> datasApontamento(Projeto projeto, Colaborador colaborador) {
		try {
			//return manager.createQuery("select distinct exe.data dat from ProjetoExecucao exe where exe.projeto = :projeto and exe.colaborador = :colaborador order by dat")
			return manager.createQuery("select distinct exe.data from ProjetoExecucao exe where exe.projeto = :projeto and exe.colaborador = :colaborador")
					 .setParameter("projeto", projeto)
					 .setParameter("colaborador", colaborador)
					 .getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}
	
	public BigDecimal horasProgramadasApontamento(Projeto projeto, Colaborador colaborador) {		
		BigDecimal horas = new BigDecimal(0);		
		try {
			Query query = manager.createQuery("select sum(exe.totalHoras) from ProjetoExecucao exe where exe.projeto = :projeto and exe.colaborador = :colaborador and exe.atividadePrevista is not null", BigDecimal.class)
				.setParameter("projeto", projeto)
				.setParameter("colaborador", colaborador);
			horas = (BigDecimal)query.getSingleResult();
		} catch (NoResultException e) {			
			// nenhum registro encontrado
		}
		if (horas == null) {
			horas = new BigDecimal(0);
		}
		return horas;
	}
	
	public BigDecimal horasExecutadasApontamento(Projeto projeto, Colaborador colaborador) {		
		BigDecimal horas = new BigDecimal(0);		
		try {
			Query query = manager.createQuery("select sum(exe.totalHoras) from ProjetoExecucao exe where exe.projeto = :projeto and exe.colaborador = :colaborador", BigDecimal.class)
				.setParameter("projeto", projeto)
				.setParameter("colaborador", colaborador);
			horas = (BigDecimal)query.getSingleResult();
		} catch (NoResultException e) {			
			// nenhum registro encontrado
		}
		if (horas == null) {
			horas = new BigDecimal(0);
		}
		return horas;
	}	
	
}

package com.mesure.consult;

import com.mesure.model.Projeto;
import java.io.Serializable;
import java.math.BigDecimal;

public class CNApontamento implements Serializable {

    private static final long serialVersionUID = 1L;

    private Projeto projeto;
    private BigDecimal totalHorasProjeto;
    private BigDecimal totalHorasProgramadas;
    private BigDecimal totalHorasExecutadas;
    private BigDecimal saldoHorasRestantes;
    private Integer saldoDiasRestantes;
    private BigDecimal perConclusaoProjeto;

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

	public BigDecimal getTotalHorasProjeto() {
		return totalHorasProjeto;
	}

	public void setTotalHorasProjeto(BigDecimal totalHorasProjeto) {
		this.totalHorasProjeto = totalHorasProjeto;
	}

	public BigDecimal getTotalHorasProgramadas() {
		return totalHorasProgramadas;
	}

	public void setTotalHorasProgramadas(BigDecimal totalHorasProgramadas) {
		this.totalHorasProgramadas = totalHorasProgramadas;
	}

	public BigDecimal getTotalHorasExecutadas() {
		return totalHorasExecutadas;
	}

	public void setTotalHorasExecutadas(BigDecimal totalHorasExecutadas) {
		this.totalHorasExecutadas = totalHorasExecutadas;
	}

	public BigDecimal getSaldoHorasRestantes() {
		return saldoHorasRestantes;
	}

	public void setSaldoHorasRestantes(BigDecimal saldoHorasRestantes) {
		this.saldoHorasRestantes = saldoHorasRestantes;
	}

	public Integer getSaldoDiasRestantes() {
		return saldoDiasRestantes;
	}

	public void setSaldoDiasRestantes(Integer saldoDiasRestantes) {
		this.saldoDiasRestantes = saldoDiasRestantes;
	}

	public BigDecimal getPerConclusaoProjeto() {
		return perConclusaoProjeto;
	}

	public void setPerConclusaoProjeto(BigDecimal perConclusaoProjeto) {
		this.perConclusaoProjeto = perConclusaoProjeto;
	}

}

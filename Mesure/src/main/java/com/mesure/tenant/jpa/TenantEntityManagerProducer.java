package com.mesure.tenant.jpa;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Persistence;

import org.hibernate.exception.SQLGrammarException;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.hibernate.validator.constraints.Email;

import com.mesure.repository.TenantRepository;
import com.mesure.tenant.cdi.Mesure;
import com.mesure.tenant.model.Tenant;

@ApplicationScoped
public class TenantEntityManagerProducer {

	private EntityManagerFactory mesureFactory;
	private Map<Long, EntityManagerFactory> tenantFactories = new HashMap<>();
	@Inject
	private TenantRepository tenantRepository;

	public TenantEntityManagerProducer() {
		mesureFactory = Persistence.createEntityManagerFactory("MesurePU");
	}

	@Mesure
	@Produces
	@RequestScoped
	public EntityManager createMesureEM() {
		return mesureFactory.createEntityManager();
	}

	public TenantEntityManager createTenantEM(@Email String email) throws NoResultException, NonUniqueResultException, SQLGrammarException {

		EntityManager tenantEM;
		Tenant tenant = tenantRepository.obter(email);
		Long tenantId = tenant.getId();

		if (tenantFactories.containsKey(tenantId)) {
			tenantEM = tenantFactories.get(tenantId).createEntityManager();
		} else {
			EntityManagerFactory entityManagerFactory = createEntityManagerFactory(tenant);
			tenantFactories.put(tenantId, entityManagerFactory);
			tenantEM = entityManagerFactory.createEntityManager();
		}

		return new TenantEntityManager(tenantId, tenantEM);
	}

	private EntityManagerFactory createEntityManagerFactory(Tenant tenant) {
		EntityManagerFactory entityManagerFactory;
		TenantPersistenceUnitInfo tenantPersistenceUnitInfo = new TenantPersistenceUnitInfo(tenant);
		entityManagerFactory = new HibernatePersistenceProvider()
				.createContainerEntityManagerFactory(tenantPersistenceUnitInfo, null);
		return entityManagerFactory;
	}

	public void closeEntityManager(@Disposes @Mesure EntityManager manager) {
		if (manager.isOpen()) {
			manager.close();
		}
	}

	public EntityManager getEntityManager(Long tenantId) {
		EntityManagerFactory entityManagerFactory = tenantFactories.get(tenantId);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		return entityManager;
	}
}
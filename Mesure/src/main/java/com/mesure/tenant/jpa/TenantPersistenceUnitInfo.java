package com.mesure.tenant.jpa;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;

import org.reflections.Reflections;

import com.mesure.tenant.model.ConnectionDatabase;
import com.mesure.tenant.model.Tenant;

public class TenantPersistenceUnitInfo implements PersistenceUnitInfo {

	private Tenant tenant;
	private Properties tenantProperties;

	public TenantPersistenceUnitInfo(Tenant tenant) {
		this.tenant = tenant;
		ConnectionDatabase connectionDatabase = tenant.getConnectionDatabase();

		tenantProperties = new Properties();
		configAutenticacaoBanco(connectionDatabase);
		// Configurações específicas do MySql e Hibernate.
		configVendors(connectionDatabase);
		configPoolConexoes(connectionDatabase);
	}

	/**
	 * Configura a autenticação no banco
	 * 
	 * @param connectionDatabase
	 */
	private void configAutenticacaoBanco(ConnectionDatabase connectionDatabase) {
		tenantProperties.setProperty("javax.persistence.jdbc.user", connectionDatabase.getUsuario());
		tenantProperties.setProperty("javax.persistence.jdbc.password", connectionDatabase.getSenha());
	}

	/**
	 * Configurações específicas do driver do banco de dados e da implementação
	 * JPA.
	 * 
	 * @param urlJDBC
	 */
	private void configVendors(ConnectionDatabase connectionDatabase) {
		String urlJDBC = "jdbc:mysql://%s:%s/%s";
		urlJDBC = String.format(urlJDBC, connectionDatabase.getHost(), connectionDatabase.getPorta().toString(), connectionDatabase.getNome());

		tenantProperties.setProperty("javax.persistence.jdbc.url", urlJDBC);
		tenantProperties.setProperty("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
		tenantProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		tenantProperties.setProperty("hibernate.hbm2ddl.auto", connectionDatabase.getHbm2ddlAuto());
		tenantProperties.setProperty("hibernate.show_sql", "true");
		tenantProperties.setProperty("hibernate.connection.provider_class",	"org.hibernate.connection.C3P0ConnectionProvider");
		tenantProperties.setProperty("hibernate.archive.autodetection", "false");
	}

	private void configPoolConexoes(ConnectionDatabase connectionDatabase) {
		tenantProperties.setProperty("hibernate.c3p0.max_size", connectionDatabase.getConexoesMaximas().toString());
		tenantProperties.setProperty("hibernate.c3p0.min_size", connectionDatabase.getConexoesMinimas().toString());
		tenantProperties.setProperty("hibernate.c3p0.acquire_increment", connectionDatabase.getIncremento().toString());
		tenantProperties.setProperty("hibernate.c3p0.idle_test_period",	connectionDatabase.getIdleTestPeriod().toString());
		tenantProperties.setProperty("hibernate.c3p0.max_statements", connectionDatabase.getStatementsMaximas().toString());
		tenantProperties.setProperty("hibernate.c3p0.timeout", connectionDatabase.getTimeout().toString());
	}

	@Override
	public String getPersistenceUnitName() {
		return tenant.getNome() + "PU";
	}

	@Override
	public String getPersistenceProviderClassName() {
		return "org.hibernate.jpa.HibernatePersistenceProvider";
	}

	@Override
	public PersistenceUnitTransactionType getTransactionType() {
		return PersistenceUnitTransactionType.RESOURCE_LOCAL;
	}

	@Override
	public DataSource getJtaDataSource() {
		return null;
	}

	@Override
	public DataSource getNonJtaDataSource() {
		return null;
	}

	@Override
	public List<String> getMappingFileNames() {
		return Collections.emptyList();
	}

	@Override
	public List<URL> getJarFileUrls() {
		if (excludeUnlistedClasses()) {
			return null;
		} else {
			try {
				return Collections.list(this.getClass().getClassLoader().getResources(""));
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}
		}
	}

	@Override
	public URL getPersistenceUnitRootUrl() {
		return null;
	}

	@Override
	public List<String> getManagedClassNames() {
		Reflections reflections = new Reflections("com.mesure.model");
		Set<Class<? extends Object>> allClasses = reflections.getTypesAnnotatedWith(Entity.class);
		List<String> entidadesGerenciadas = new ArrayList<>();

		allClasses.forEach(clazz -> {
			entidadesGerenciadas.add(clazz.getName());
			System.out.println(clazz.getName());
		});

		return entidadesGerenciadas;
	}

	@Override
	public boolean excludeUnlistedClasses() {
		return true;
	}

	@Override
	public SharedCacheMode getSharedCacheMode() {
		return null;
	}

	@Override
	public ValidationMode getValidationMode() {
		return null;
	}

	@Override
	public Properties getProperties() {
		return tenantProperties;
	}

	@Override
	public String getPersistenceXMLSchemaVersion() {
		return null;
	}

	@Override
	public ClassLoader getClassLoader() {
		return null;
	}

	@Override
	public void addTransformer(ClassTransformer transformer) {

	}

	@Override
	public ClassLoader getNewTempClassLoader() {
		return null;
	}

}

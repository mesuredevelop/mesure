package com.mesure.tenant.jpa;

import javax.persistence.EntityManager;

public class TenantEntityManager {
	
	private Long tenantId;
	private EntityManager em;
	
	public TenantEntityManager(Long tenantId, EntityManager em) {
		super();
		this.tenantId = tenantId;
		this.em = em;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

}

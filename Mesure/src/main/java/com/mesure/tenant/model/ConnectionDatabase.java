package com.mesure.tenant.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class ConnectionDatabase implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	// Dados de conexão com o banco de dados
	private String nome;
	private String host;
	private Integer porta;
	private String usuario;
	private String senha;
	
	// Configuração do pool de conexões com C3P0
	private Integer conexoesMaximas = 3;
	private Integer conexoesMinimas = 2;
	private Integer incremento = 1;
	private Integer idleTestPeriod = 300;
	private Integer timeout = 120;
	private Integer statementsMaximas = 50;
	
	// Configurações específicas do driver do banco de dados e da implementação JPA
	private String hbm2ddlAuto = "false";
 
	private Boolean ativo = Boolean.TRUE;
	private Tenant tenant;

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	@Size(max = 60)
	@Column(nullable = false, length = 60)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@NotBlank
	@Size(max = 60)
	@Column(nullable = false, length = 60)
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	@Min(0)
	public Integer getPorta() {
		return porta;
	}

	public void setPorta(Integer porta) {
		this.porta = porta;
	}

	@NotBlank
	@Size(max = 60)
	@Column(nullable = false, length = 60)
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@NotBlank
	@Size(max = 60)
	@Column(nullable = false, length = 60)
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Column(nullable = false)
	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@NotNull
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "tenant_id", foreignKey = @ForeignKey(name = "FK_connectiondatabase_tenant"), nullable = false, unique = true)
	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}
	
	/**
	 * Número máximo de conexões que serão abertas para a aplicação.
	 * @return
	 */
	@Min(3)
	@Max(100)
	@Column(name = "conexoes_maximas")
	public Integer getConexoesMaximas() {
		return (conexoesMaximas!= null)? conexoesMaximas: 3;
	}

	/**
	 * Número máximo de conexões que serão abertas para a aplicação.
	 * @param conexoesMaximas
	 */
	public void setConexoesMaximas(Integer conexoesMaximas) {
		this.conexoesMaximas = conexoesMaximas;
	}

	/**
	 * Número mínimo de conexões que deverão ficar abertas para a aplicação.
	 * @return
	 */
	@Min(1)
	@Max(10)
	@Column(name = "conexoes_minimas")
	public Integer getConexoesMinimas() {
		return (conexoesMinimas!= null)? conexoesMinimas: 2;
	}

	/**
	 * Número mínimo de conexões que deverão ficar abertas para a aplicação.
	 * @param conexoesMinimas
	 */
	public void setConexoesMinimas(Integer conexoesMinimas) {
		this.conexoesMinimas = conexoesMinimas;
	}

	/**
	 * Quando precisar de mais conexões além do mínimo, especifica quantas
	 * conexões novas serão abertas/incrementadas.
	 * @return
	 */
	@Min(1)
	@Max(10)
	public Integer getIncremento() {
		return (incremento!= null)? incremento: 1;
	}

	/**
	 * Quando precisar de mais conexões além do mínimo, especifica quantas
	 * conexões novas serão abertas/incrementadas.
	 * @param incremento
	 */
	public void setIncremento(Integer incremento) {
		this.incremento = incremento;
	}

	/**
	 * Especifica de quanto em quanto tempo leva para testar e restabelecer
	 * as conexões quando abaixo no mínimo.
	 * @return
	 */
	@Min(100)
	@Column(name = "idle_test_period")
	public Integer getIdleTestPeriod() {
		return (idleTestPeriod!= null)? idleTestPeriod: 300;
	}

	/**
	 * Especifica de quanto em quanto tempo leva para testar e restabelecer
	 * as conexões quando abaixo no mínimo.
	 * @param idleTestPeriod
	 */
	public void setIdleTestPeriod(Integer idleTestPeriod) {
		this.idleTestPeriod = idleTestPeriod;
	}

	/**
	 * Especifica de quanto em quanto tempo leva para verificar e fechar
 	 * as conexões abertas acima do mínimo.
	 * @return
	 */
	@Min(100)
	public Integer getTimeout() {
		return (timeout!= null)? timeout: 120;
	}

	/**
	 * Especifica de quanto em quanto tempo leva para verificar e fechar
 	 * as conexões abertas acima do mínimo.
	 * @param timeout
	 */
	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	/**
	 * Número de prepared statements para fazer cache.
	 * @return
	 */
	@Min(0)
	@Column(name = "statements_maximas")
	public Integer getStatementsMaximas() {
		return (statementsMaximas!= null)? statementsMaximas: 50;
	}

	/**
	 * Número de prepared statements para fazer cache.
	 * @param statementsMaximas
	 */
	public void setStatementsMaximas(Integer statementsMaximas) {
		this.statementsMaximas = statementsMaximas;
	}
	
	/**
	 * Define se o Hibernate deve atualizar a base de dados.
	 * Valores possíveis:
	 * - update: atualiza a base de dados com as novas tabelas e campos
	 * - false: não faz nenhuma operação
	 * - (NÃO USAR) validate: valida a base, mas não faz mudanças
     * - (NÃO USAR) create: cria a base, destruindo dados anteriores
     * - (NÃO USAR) create-drop: exclui a base ao final da sessão
	 * @return
	 */
	@NotBlank
	@Size(max = 15)
	@Column(name = "hbm2ddl_auto", nullable = false, length = 15)
	public String getHbm2ddlAuto() {
		return hbm2ddlAuto;
	}

	/**
	 * Define se o Hibernate deve atualizar a base de dados
	 * @param hbm2ddlAuto
	 */
	public void setHbm2ddlAuto(String hbm2ddlAuto) {
		this.hbm2ddlAuto = hbm2ddlAuto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConnectionDatabase other = (ConnectionDatabase) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}

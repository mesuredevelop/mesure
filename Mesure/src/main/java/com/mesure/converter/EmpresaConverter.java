package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.Empresa;
import com.mesure.repository.EmpresaRepository;

//@FacesConverter(forClass = Empresa.class)
@FacesConverter("empresaConverter")
public class EmpresaConverter implements Converter {
	
	@Inject
	private EmpresaRepository empresaRepository;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Empresa retorno = null;
		
		if (StringUtils.isNotEmpty(value)) {
			retorno = empresaRepository.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Empresa empresa = (Empresa) value;
			return empresa.getId() == null ? null : empresa.getId().toString();
		}
		return "";
	}	
}


package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.ColaboradorPonto;
import com.mesure.repository.ColaboradorPontoRepository;

//@FacesConverter(forClass = ColaboradorPonto.class)
@FacesConverter("colaboradorPontoConverter")
public class ColaboradorPontoConverter implements Converter {

	@Inject
	private ColaboradorPontoRepository colaboradorPontoRepository; 
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		ColaboradorPonto retorno = null;
		
		if (StringUtils.isNotEmpty(value)) {
			retorno = colaboradorPontoRepository.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			ColaboradorPonto colaboradorPonto = (ColaboradorPonto) value;
			return colaboradorPonto.getId() == null ? null : colaboradorPonto.getId().toString();
		}
		return "";
	}
}


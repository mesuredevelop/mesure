package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.Fornecedor;
import com.mesure.repository.FornecedorRepository;

//@FacesConverter(forClass = Fornecedor.class)
@FacesConverter("fornecedorConverter")
public class FornecedorConverter implements Converter {

	@Inject
	private FornecedorRepository fornecedorRepository;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Fornecedor retorno = null;
		
		if (StringUtils.isNotEmpty(value)) {
			retorno = fornecedorRepository.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Fornecedor fornecedor = (Fornecedor) value;
			return fornecedor.getId() == null ? null : fornecedor.getId().toString();
		}
		return "";
	}

}


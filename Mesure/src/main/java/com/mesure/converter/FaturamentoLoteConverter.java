package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.FaturamentoLote;
import com.mesure.repository.FaturamentoLoteRepository;

//@FacesConverter(forClass = FaturamentoLote.class)
@FacesConverter("faturamentoLoteConverter")
public class FaturamentoLoteConverter implements Converter {

	@Inject
	private FaturamentoLoteRepository faturamentoLoteRepository; 
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		FaturamentoLote retorno = null;
		
		if (StringUtils.isNotEmpty(value)) {
			retorno = faturamentoLoteRepository.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			FaturamentoLote lote = (FaturamentoLote) value;
			return lote.getId() == null ? null : lote.getId().toString();
		}
		return "";
	}
}


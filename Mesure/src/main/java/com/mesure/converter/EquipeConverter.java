package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.Equipe;
import com.mesure.repository.EquipeRepository;

@FacesConverter("equipeConverter")
public class EquipeConverter implements Converter {

	@Inject
	private EquipeRepository equipeRepository;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Equipe retorno = null;
		
		if (StringUtils.isNotEmpty(value)) {
			retorno = equipeRepository.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Equipe equipe = (Equipe) value;
			return equipe.getId() == null ? null : equipe.getId().toString();
		}
		return "";
	}
	
}


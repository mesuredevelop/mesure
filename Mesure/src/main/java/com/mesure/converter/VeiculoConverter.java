package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.Veiculo;
import com.mesure.repository.VeiculoRepository;

//@FacesConverter(forClass = Veiculo.class)
@FacesConverter("veiculoConverter")
public class VeiculoConverter implements Converter {
	
	@Inject
	private VeiculoRepository veiculoRepository;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Veiculo retorno = null;
		
		if (StringUtils.isNotEmpty(value)) {
			retorno = veiculoRepository.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Veiculo veiculo = (Veiculo) value;
			return veiculo.getId() == null ? null : veiculo.getId().toString();
		}
		return "";
	}
	
}


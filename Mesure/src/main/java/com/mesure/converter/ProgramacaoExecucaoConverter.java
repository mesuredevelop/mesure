package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.ProgramacaoExecucao;
import com.mesure.repository.ProgramacaoExecucaoRepository;

@FacesConverter("programacaoExecucaoConverter")
public class ProgramacaoExecucaoConverter implements Converter {
	
	@Inject
	private ProgramacaoExecucaoRepository programacaoRepository;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		ProgramacaoExecucao retorno = null;
		
		if (StringUtils.isNotEmpty(value)) {
			retorno = programacaoRepository.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			ProgramacaoExecucao programacao = (ProgramacaoExecucao) value;
			return programacao.getId() == null ? null : programacao.getId().toString();
		}
		return "";
	}
	
}


package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.TituloReceber;
import com.mesure.repository.TituloReceberRepository;

@FacesConverter("tituloReceberConverter")
public class TituloReceberConverter implements Converter {

    @Inject
    private TituloReceberRepository tituloReceberRepository;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        TituloReceber retorno = null;

        if (StringUtils.isNotEmpty(value)) {
            retorno = tituloReceberRepository.porId(new Long(value));
        }
        return retorno;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null) {
            TituloReceber tituloReceber = (TituloReceber) value;
            return tituloReceber.getId() == null ? null : tituloReceber.getId().toString();
        }
        return "";
    }
}

package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.Atividade;
import com.mesure.repository.AtividadeRepository;

//@FacesConverter(forClass = Atividade.class)
@FacesConverter("atividadeConverter")
public class AtividadeConverter implements Converter {

	@Inject
	private AtividadeRepository atividadeRepository;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Atividade retorno = null;
		
		if (StringUtils.isNotEmpty(value)) {
			retorno = atividadeRepository.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Atividade atividade = (Atividade) value;
			return atividade.getId() == null ? null : atividade.getId().toString();
		}
		return "";
	}
	
}


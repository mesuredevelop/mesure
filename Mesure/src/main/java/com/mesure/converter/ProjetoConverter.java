package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.Projeto;
import com.mesure.repository.ProjetoRepository;

//@FacesConverter(forClass = Projeto.class)
@FacesConverter("projetoConverter")
public class ProjetoConverter implements Converter {

	@Inject
	private ProjetoRepository projetoRepository;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Projeto retorno = null;
		
		if (StringUtils.isNotEmpty(value)) {
			retorno = projetoRepository.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Projeto projeto = (Projeto) value;
			return projeto.getId() == null ? null : projeto.getId().toString();
		}
		return "";
	}
}


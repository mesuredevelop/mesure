package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.VeiculoMarca;
import com.mesure.repository.VeiculoMarcaRepository;

//@FacesConverter(forClass = VeiculoMarca.class)
@FacesConverter("veiculoMarcaConverter")
public class VeiculoMarcaConverter implements Converter {
	
	@Inject
	private VeiculoMarcaRepository veiculoMarcaRepository;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		VeiculoMarca retorno = null;
		
		if (StringUtils.isNotEmpty(value)) {
			retorno = veiculoMarcaRepository.marcaPorId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			VeiculoMarca veiculoMarca = (VeiculoMarca) value;
			return veiculoMarca.getId() == null ? null : veiculoMarca.getId().toString();
		}
		return "";
	}	

}


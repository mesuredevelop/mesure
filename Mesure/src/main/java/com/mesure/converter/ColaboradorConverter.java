package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.Colaborador;
import com.mesure.repository.ColaboradorRepository;

//@FacesConverter(forClass = Colaborador.class)
@FacesConverter("colaboradorConverter")
public class ColaboradorConverter implements Converter {

	@Inject
	private ColaboradorRepository colaboradorRepository;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Colaborador retorno = null;
		if (StringUtils.isNotEmpty(value)) {
			retorno = colaboradorRepository.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Colaborador colaborador = (Colaborador) value;
			return colaborador.getId() == null ? null : colaborador.getId().toString();
		}
		return "";
	}
	
}

package com.mesure.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.mesure.model.ApuracaoHorasLote;
import com.mesure.repository.ApuracaoHorasLoteRepository;

//@FacesConverter(forClass = ApuracaoHorasLote.class)
@FacesConverter("apuracaoHorasLoteConverter")
public class ApuracaoHorasLoteConverter implements Converter {

	@Inject
	private ApuracaoHorasLoteRepository apuracaoHorasLoteRepository; 
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		ApuracaoHorasLote retorno = null;
		
		if (StringUtils.isNotEmpty(value)) {
			retorno = apuracaoHorasLoteRepository.porId(new Long(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			ApuracaoHorasLote lote = (ApuracaoHorasLote) value;
			return lote.getId() == null ? null : lote.getId().toString();
		}
		return "";
	}
}


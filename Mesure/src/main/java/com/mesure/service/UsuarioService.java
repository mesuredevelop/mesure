package com.mesure.service;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Usuario;
import com.mesure.repository.UsuarioRepository;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

public class UsuarioService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private UsuarioRepository usuarioRepository;

    @Transactional
    public Usuario salvar(Usuario usuario) {
        if (usuario.getGrupos().isEmpty()) {
            throw new NegocioException("O usuário deve pertencer a no mínino 1 (um) grupo.");
        }

        try {
            Usuario usuarioExistente = usuarioRepository.porEmail(usuario.getEmail(), ESimNao.NAO);
            if (!usuarioExistente.equals(usuario)) {
                throw new NegocioException("Já existe um usuário com o Email informado.");
            }
        } catch (NonUniqueResultException e) {
            throw new NegocioException("Já existe um usuário com o Email informado.");
        } catch (NoResultException nre) {
        }

        return usuarioRepository.guardar(usuario);
    }
}

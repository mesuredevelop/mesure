package com.mesure.service;

import com.ibm.icu.text.DecimalFormat;
import com.mesure.enumerator.EFormaAlerta;
import com.mesure.enumerator.ETipoAlerta;
import com.mesure.model.Alerta;
import com.mesure.model.AlertaEmail;
import com.mesure.model.AlertaLog;
import com.mesure.model.Colaborador;
import com.mesure.model.ColaboradorPonto;
import com.mesure.model.Feriado;
import com.mesure.model.ProgramacaoExecucao;
import com.mesure.model.vo.DataColaborador;
import com.mesure.repository.AlertaRepository;
import com.mesure.repository.FeriadoRepository;
import com.mesure.repository.ProgramacaoExecucaoRepository;
import com.mesure.repository.filter.ProgramacaoExecucaoFilter;
import com.mesure.util.Util;
import com.mesure.util.mail.Mailer;
import com.outjected.email.api.MailMessage;
import com.outjected.email.impl.templating.velocity.VelocityTemplate;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;

public class AlertaService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    AlertaRepository alertaRepository;

    @Inject
    ProgramacaoExecucaoRepository programacaoExecucaoRepository;

    @Inject
    FeriadoRepository feriadoRepository;

    @Inject
    private Mailer mailer;

    private List<ColaboradorPonto> pontos = new ArrayList<>();
    private List<ProgramacaoExecucao> programacoes = new ArrayList<>();
    private List<DataColaborador> colaboradores = new ArrayList<>();
    private List<Feriado> feriados = new ArrayList<>();

//    public void executarAlertas() {
//
//        Date dataAtual = new Date();
//        List<Alerta> alertas = alertaRepository.obterAlertas(null);
//
//        for (Alerta alerta : alertas) {
//            // Verifica se o alerta está válido na data atual
//            if (alerta.getInicio().getTime() <= dataAtual.getTime()
//                    && (alerta.getTermino() == null || alerta.getTermino().getTime() >= dataAtual.getTime())) {
//
//                AlertaLog log = alertaRepository.ultimoLogAlerta(alerta);
//
//                // Verifica quantos dias faz que o alerta foi executado pela última vez
//                long diferenca = Long.MAX_VALUE;
//                if (log != null) {
//                    diferenca = Util.DiferencaEmDias(log.getData(), dataAtual);
//                }
//
//                // Verifica se o alerta deve ser executado de acordo com a sua periodicidade 
//                if (alerta.getPeriodicidade().equals(EPeriodicidadeAlerta.DIARIO)) {
//                    if (diferenca > 0) {
//                        executarTipoAlerta(alerta);
//                    }
//                }
//            }
//        }
//    }

    public void executarTipoAlerta(Alerta alerta) {

        // Busca os e-mails que o alerta deve ser enviado
        if (alerta.getForma().equals(EFormaAlerta.EMAIL)) {
            alerta.setEmails(alertaRepository.emailsDoAlerta(alerta));

            if (alerta.getEmails().isEmpty()) {
                throw new NegocioException("Não foi possível obter os e-mails do alerta");
            }
        }

        Date dataAtual = new Date();

        if (alerta.getForma().equals(EFormaAlerta.EMAIL)) {

            // Horas não apontadas
            switch (alerta.getTipo()) {
                case HRS_NAO_APT: {
                    // Padrão 5 dias para execução
                    int diasExecucao = 5;
                    if (alerta.getDiasExecucao() != null) {
                        diasExecucao = alerta.getDiasExecucao();
                    }
                    Calendar dataMenor = Calendar.getInstance();
                    dataMenor.add(Calendar.DATE, diasExecucao * -1);
                    Calendar dataMaior = Calendar.getInstance();
                    dataMaior.add(Calendar.DATE, -1);
                    List<ColaboradorPonto> lista = horasNaoApontadas(alerta, dataMenor.getTime(), dataMaior.getTime());
                    setPontos(lista);
                    List<DataColaborador> colaboradorList = colaboradoresSemRegistroPonto(alerta, dataMenor.getTime(), dataMaior.getTime());
                    setColaboradores(colaboradorList);
                    if (lista.size() > 0 || colaboradorList.size() > 0) {
                        enviarEmailAlerta(alerta);
                    }
                    break;
                }
                // Programação diária
                case PRG_DIA: {
                    // Busca as programações do dia
                    List<ProgramacaoExecucao> lista = programacaoDiaria(alerta);
                    if (lista.size() > 0) {
                        setProgramacoes(lista);
                        enviarEmailAlerta(alerta);
                    }
                    break;
                }
                // Programação final de semana
                case PRG_FDS: {
                    Calendar dataMenor = Calendar.getInstance();
                    Calendar dataMaior = Calendar.getInstance();
                    int diaSemana = Util.diaDaSemanaCalendar(dataAtual);
                    // Busca o sábado e domingo conforme a data atual.
                    switch (diaSemana) {
                        case Calendar.SUNDAY:
                            dataMenor.add(Calendar.DATE, 6);
                            dataMaior.add(Calendar.DATE, 7);
                            break;
                        case Calendar.MONDAY:
                            dataMenor.add(Calendar.DATE, 5);
                            dataMaior.add(Calendar.DATE, 6);
                            break;
                        case Calendar.TUESDAY:
                            dataMenor.add(Calendar.DATE, 4);
                            dataMaior.add(Calendar.DATE, 5);
                            break;
                        case Calendar.WEDNESDAY:
                            dataMenor.add(Calendar.DATE, 3);
                            dataMaior.add(Calendar.DATE, 4);
                            break;
                        case Calendar.THURSDAY:
                            dataMenor.add(Calendar.DATE, 2);
                            dataMaior.add(Calendar.DATE, 3);
                            break;
                        case Calendar.FRIDAY:
                            dataMenor.add(Calendar.DATE, 1);
                            dataMaior.add(Calendar.DATE, 2);
                            break;
                        case Calendar.SATURDAY:
                            dataMaior.add(Calendar.DATE, 1);
                            break;
                    }       // Busca as programações do final de semana
                    List<ProgramacaoExecucao> lista = programacaoFinalDeSemana(alerta, dataMenor.getTime(), dataMaior.getTime());
                    if (lista.size() > 0) {
                        setProgramacoes(lista);
                        enviarEmailAlerta(alerta);
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }

    private void enviarEmailAlerta(Alerta alerta) {
        try {
            MailMessage message = mailer.novaMensagem();

            // Destinatários do alerta
            List<String> lista = new ArrayList<>();
            for (AlertaEmail e : alerta.getEmails()) {
                lista.add(e.getEmail());
            }
            String[] emails = (String[]) lista.toArray(new String[lista.size()]);

            message = message.to(emails)
                    .subject(getSubjectEmail(alerta.getTipo()))
                    .bodyHtml(new VelocityTemplate(getClass().getResourceAsStream(getTemplateEmail(alerta.getTipo()))))
                    .put("numberTool", new NumberTool())
                    .put("dateTool", new DateTool())
                    .put("locale", new Locale("pt", "BR"))
                    .put("df", new DecimalFormat("#0.00"));

            message = setObjetosEmail(alerta.getTipo(), message);
            message.send();

            gerarLog(alerta, new Date());
        } catch (Exception e) {
            throw new NegocioException("Ocorreu um erro ao enviar o alerta por e-mail");
        }
    }

    // Alerta: Horas não apontadas: registraram ponto mas faltou apontamento de horas
    private List<ColaboradorPonto> horasNaoApontadas(Alerta alerta, Date dataMenor, Date dataMaior) {
        return alertaRepository.horasNaoApontadas(alerta.getFilial(), dataMenor, dataMaior);
    }

    // Alerta: Horas não apontadas: não registraram ponto
    private List<DataColaborador> colaboradoresSemRegistroPonto(Alerta alerta, Date dataMenor, Date dataMaior) {
        List<DataColaborador> retorno = new ArrayList<>();
        feriados = feriadoRepository.obterFeriados(alerta.getFilial());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dataMenor);

        while (calendar.getTime().getTime() <= dataMaior.getTime()) {
            int diaSemana = Util.diaDaSemanaCalendar(calendar.getTime());

            if (diaSemana != Calendar.SATURDAY && diaSemana != Calendar.SUNDAY) {
                // apenas verifica feriado se não é fim de semana.
                if (!Util.isFeriado(feriados, calendar.getTime())) {
                    List<Colaborador> colaboradorList = alertaRepository.colaboradoresSemRegistroPonto(alerta.getFilial(), calendar.getTime());
                    for (Colaborador c : colaboradorList) {
                        retorno.add(new DataColaborador(calendar.getTime(), c));
                    }
                    calendar.add(Calendar.DATE, 1);
                } else {
                    calendar.add(Calendar.DATE, 1);
                }
            } else {
                calendar.add(Calendar.DATE, 1);
            }
        }
        return retorno;
    }

    // Alerta: Programação diária
    private List<ProgramacaoExecucao> programacaoDiaria(Alerta alerta) {
        int diasExecucao = 0;
        if (alerta.getDiasExecucao() != null) {
            diasExecucao = alerta.getDiasExecucao();
        }
        Calendar dataMenor = Calendar.getInstance();
        dataMenor.add(Calendar.DATE, diasExecucao * -1);
        Calendar dataMaior = Calendar.getInstance();
        dataMaior.add(Calendar.DATE, 0);

        ProgramacaoExecucaoFilter filtro = new ProgramacaoExecucaoFilter();
        filtro.setFilial(alerta.getFilial());
        filtro.setDataDe(dataMenor.getTime());
        filtro.setDataAte(dataMaior.getTime());
        return programacaoExecucaoRepository.programacoes(filtro);
    }

    // Alerta: Programação final de semana
    private List<ProgramacaoExecucao> programacaoFinalDeSemana(Alerta alerta, Date data1, Date data2) {
        ProgramacaoExecucaoFilter filtro = new ProgramacaoExecucaoFilter();
        filtro.setFilial(alerta.getFilial());
        filtro.setDataDe(data1);
        filtro.setDataAte(data2);
        return programacaoExecucaoRepository.programacoes(filtro);
    }

    private void gerarLog(Alerta alerta, Date dataHora) {
        alertaRepository.guardarAlertaLog(new AlertaLog(dataHora, dataHora, alerta));
    }

    private String getTemplateEmail(ETipoAlerta tipoAlerta) {
        // Horas não apontadas
        if (tipoAlerta.equals(ETipoAlerta.HRS_NAO_APT)) {
            return "/emails/horas_nao_apontadas.template";
        }
        // Programação diária
        if (tipoAlerta.equals(ETipoAlerta.PRG_DIA)) {
            return "/emails/programacao_diaria.template";
        }
        // Programação final de semana
        if (tipoAlerta.equals(ETipoAlerta.PRG_FDS)) {
            return "/emails/programacao_final_de_semana.template";
        }
        return "";
    }

    private String getSubjectEmail(ETipoAlerta tipoAlerta) {
        // Horas não apontadas
        if (tipoAlerta.equals(ETipoAlerta.HRS_NAO_APT)) {
            return "[Alerta Mesure] Colaboradores sem apontamento";
        }
        // Programação diária
        if (tipoAlerta.equals(ETipoAlerta.PRG_DIA)) {
            return "[Alerta Mesure] Programação diária";
        }
        // Programação final de semana
        if (tipoAlerta.equals(ETipoAlerta.PRG_FDS)) {
            return "[Alerta Mesure] Programação final de semana";
        }
        return "";
    }

    private MailMessage setObjetosEmail(ETipoAlerta tipoAlerta, MailMessage message) {
        // Horas não apontadas
        switch (tipoAlerta) {
            case HRS_NAO_APT:
                message.put("pontos", getPontos());
                message.put("colaboradores", getColaboradores());
                break;
            // Programação diária
            case PRG_DIA:
                message.put("programacoes", getProgramacoes());
                break;
            // Programação final de semana
            case PRG_FDS:
                message.put("programacoes", getProgramacoes());
                break;
            default:
                break;
        }
        return message;
    }

    public List<ColaboradorPonto> getPontos() {
        return pontos;
    }

    public void setPontos(List<ColaboradorPonto> pontos) {
        this.pontos = pontos;
    }

    public List<ProgramacaoExecucao> getProgramacoes() {
        return programacoes;
    }

    public void setProgramacoes(List<ProgramacaoExecucao> programacoes) {
        this.programacoes = programacoes;
    }

    public List<DataColaborador> getColaboradores() {
        return colaboradores;
    }

    public void setColaboradores(List<DataColaborador> colaboradores) {
        this.colaboradores = colaboradores;
    }

}

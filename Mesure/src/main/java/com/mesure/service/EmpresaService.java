package com.mesure.service;

import com.mesure.model.Empresa;
import com.mesure.repository.EmpresaRepository;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import javax.inject.Inject;

public class EmpresaService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EmpresaRepository empresaRepository;

    @Transactional
    public Empresa salvar(Empresa empresa) {

        if (empresa.getFiliais().isEmpty()) {
            throw new NegocioException("É necessário que a empresa contenha ao menos 1 (uma) filial.");
        }

        return empresaRepository.guardar(empresa);
    }
}

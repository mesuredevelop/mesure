package com.mesure.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.mesure.model.Grupo;
import com.mesure.repository.GrupoRepository;
import com.mesure.util.jpa.Transactional;

public class GrupoService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private GrupoRepository grupoRepository;

	@Transactional
	public Grupo salvar(Grupo grupo) {
		grupo.setNome(grupo.getNome().toUpperCase());
		return grupoRepository.guardar(grupo);
	}
}

package com.mesure.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.mesure.model.Cliente;
import com.mesure.repository.ClienteRepository;
import com.mesure.util.jpa.Transactional;

public class ClienteService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ClienteRepository clienteRepository;
	
	@Transactional
	public Cliente salvar(Cliente cliente) {
		return clienteRepository.guardar(cliente);
	}
}


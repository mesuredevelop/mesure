package com.mesure.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.mesure.model.VeiculoMarca;
import com.mesure.repository.VeiculoMarcaRepository;
import com.mesure.util.jpa.Transactional;

public class VeiculoMarcaService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private VeiculoMarcaRepository veiculoMarcaRepository;
	
	@Transactional
	public VeiculoMarca salvar(VeiculoMarca veiculoMarca) {
		
		return veiculoMarcaRepository.guardar(veiculoMarca);
	}
}


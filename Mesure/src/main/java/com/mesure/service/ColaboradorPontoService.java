package com.mesure.service;

import com.mesure.enumerator.EDiaSemana;
import com.mesure.enumerator.ETipoRegistroPonto;
import com.mesure.model.Colaborador;
import com.mesure.model.ColaboradorPonto;
import com.mesure.model.ColaboradorPontoExtra;
import com.mesure.model.Feriado;
import com.mesure.model.Filial;
import com.mesure.model.ProjetoExecucao;
import com.mesure.repository.ColaboradorPontoRepository;
import com.mesure.repository.FeriadoRepository;
import com.mesure.util.Util;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;

public class ColaboradorPontoService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    ColaboradorPontoRepository colaboradorPontoRepository;

    @Inject
    FeriadoRepository feriadoRepository;

    public ColaboradorPonto salvar(ColaboradorPonto colaboradorPonto) {
        return colaboradorPontoRepository.guardar(colaboradorPonto);
    }

    @Transactional
    public ColaboradorPontoExtra salvarAtividadeExtra(ColaboradorPontoExtra colaboradorPontoExtra) {
        return colaboradorPontoRepository.guardarAtividadeExtra(colaboradorPontoExtra);
    }

    public List<ProjetoExecucao> execucoesRealizadasProjetos(ColaboradorPonto colaboradorPonto) {
        return execucoesRealizadasProjetos(colaboradorPonto.getColaborador(), colaboradorPonto.getData());
    }

    public List<ProjetoExecucao> execucoesRealizadasProjetos(Colaborador colaborador, Date data) {
        return colaboradorPontoRepository.execucoesRealizadasProjetos(colaborador, data);
    }

    public ColaboradorPonto calcularHorasExecucoesProjetos(ColaboradorPonto colaboradorPonto) {
        BigDecimal total = calcularHorasExecucoesProjetos(colaboradorPonto.getColaborador(), colaboradorPonto.getData());
        colaboradorPonto.setTotalHorasProjeto(total);

        return colaboradorPonto;
    }

    public BigDecimal calcularHorasExecucoesProjetos(Colaborador colaborador, Date data) {
        return colaboradorPontoRepository.horasExecucoesProjetos(colaborador, data);
    }

    public List<ColaboradorPontoExtra> atividadesExtras(ColaboradorPonto colaboradorPonto) {
        return atividadesExtras(colaboradorPonto.getColaborador(), colaboradorPonto.getData());
    }

    public List<ColaboradorPontoExtra> atividadesExtras(Colaborador colaborador, Date data) {
        return colaboradorPontoRepository.atividadesExtras(colaborador, data);
    }

    public ColaboradorPonto calcularHorasAtividadesExtras(ColaboradorPonto colaboradorPonto) {
        BigDecimal total = calcularHorasAtividadesExtras(colaboradorPonto.getColaborador(), colaboradorPonto.getData());
        colaboradorPonto.setTotalHorasAtividadeExtra(total);

        return colaboradorPonto;
    }

    public BigDecimal calcularHorasAtividadesExtras(Colaborador colaborador, Date data) {
        return colaboradorPontoRepository.horasAtividadesExtras(colaborador, data);
    }

    public ETipoRegistroPonto sugerirTipoRegistroPonto(ColaboradorPonto ponto) {
        return sugerirTipoRegistroPonto(ponto.getData(), ponto.getColaborador().getFilial());
    }

    public ETipoRegistroPonto sugerirTipoRegistroPonto(Date data, Filial filial) {
        if (isFeriado(data, filial)) {
            return ETipoRegistroPonto.FERIADO;
        }
        EDiaSemana diaSemana = Util.diaDaSemanaEnum(data);
        if (diaSemana == EDiaSemana.SABADO || diaSemana == EDiaSemana.DOMINGO) {
            return ETipoRegistroPonto.FDSEMANA;
        }
        return ETipoRegistroPonto.NORMAL;
    }

    private boolean isFeriado(Date data, Filial filial) {
        List<Feriado> feriados = feriadoRepository.porData(data, filial);
        return (feriados != null && feriados.size() > 0);
    }

}

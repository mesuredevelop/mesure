package com.mesure.service;

import com.mesure.enumerator.ESituacaoEtapa;
import com.mesure.enumerator.EStatusProjeto;
import com.mesure.model.Atividade;
import com.mesure.model.Colaborador;
import com.mesure.model.ColaboradorPonto;
import com.mesure.model.ColaboradorTreinamento;
import com.mesure.model.EnderecoCliente;
import com.mesure.model.Projeto;
import com.mesure.model.ProjetoEtapa;
import com.mesure.model.ProjetoExecucao;
import com.mesure.model.ProjetoProposta;
import com.mesure.model.Treinamento;
import com.mesure.repository.AtividadeRepository;
import com.mesure.repository.ColaboradorPontoRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.filter.ColaboradorPontoFilter;
import com.mesure.repository.filter.ProjetoFilter;
import com.mesure.security.Seguranca;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.util.Util;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;

public class ProjetoService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    ProjetoRepository projetoRepository;

    @Inject
    AtividadeRepository atividadeRepository;

    @Inject
    ColaboradorRepository colaboradorRepository;

    @Inject
    ColaboradorPontoRepository colaboradorPontoRepository;

    @Inject
    Seguranca seguranca;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    @Transactional
    public Projeto salvar(Projeto projeto) {
        if (projeto.isNovo()) {
            projeto.setDataCriacao(new Date());
            projeto.setStatus(EStatusProjeto.COMECAR);
        }
        return projetoRepository.guardar(projeto);
    }

    @Transactional
    public Projeto avancarEstagio(Projeto projeto) {

        boolean avancar;

        // CANCELADO para A COMEÇAR
        switch (projeto.getStatus()) {
            case CANCELADO:
                projeto.setStatus(EStatusProjeto.COMECAR);
                projeto = projetoRepository.guardar(projeto);
                break;
            // A COMEÇAR para EM ANDAMENTO
            case COMECAR:
                // Validar se existe pelo menos uma proposta cadastrada com data da aprovação informada
                avancar = false;
                if (projeto.getPropostas() != null || projeto.getPropostas().size() > 0) {
                    for (ProjetoProposta p : projeto.getPropostas()) {
                        if (p.getDataAprovacao() != null) {
                            avancar = true;
                            break;
                        }
                    }
                }
                if (!avancar) {
                    throw new NegocioException("Não existe uma proposta aprovada para o projeto");
                }
                // Validar se existe pelo menos uma atividade cadastrada
                if (projeto.getAtividades() == null || projeto.getAtividades().isEmpty()) {
                    throw new NegocioException("Não existem atividades para o projeto");
                }
                projeto.setStatus(EStatusProjeto.ANDAMENTO);
                projeto.setDataInicio(new Date());
                projeto = projetoRepository.guardar(projeto);
                break;
            // EM ANDAMENTO para FINALIZADO
            case ANDAMENTO:
                // Validar se todas as etapas estão com a situação "Finalizado" ou "Cancelado";
                if (projeto.getEtapas() != null || projeto.getEtapas().size() > 0) {
                    for (ProjetoEtapa e : projeto.getEtapas()) {
                        if (e.getSituacao() != ESituacaoEtapa.FINALIZADO && e.getSituacao() != ESituacaoEtapa.CANCELADO) {
                            throw new NegocioException("O projeto não pode ser finalizado com etapas pendentes de execução");
                        }
                    }
                }
                // Validar se existe pelo menos uma proposta cadastrada com data de encerramento informada
                avancar = false;
                if (projeto.getPropostas() != null || projeto.getPropostas().size() > 0) {
                    for (ProjetoProposta p : projeto.getPropostas()) {
                        if (p.getDataEncerramento() != null) {
                            avancar = true;
                            break;
                        }
                    }
                }
                if (!avancar) {
                    throw new NegocioException("Não informada a data de encerramento na proposta do projeto");
                }
                projeto.setStatus(EStatusProjeto.FINALIZADO);
                projeto.setDataEncerramento(new Date());
                projeto = projetoRepository.guardar(projeto);
                break;
            // ATIVIDADE EXTERNA para EM ANDAMENTO
            case ATV_EXTERNA:
                projeto.setStatus(EStatusProjeto.ANDAMENTO);
                projeto = projetoRepository.guardar(projeto);
                break;
            default:
                break;
        }

        return projeto;
    }

    @Transactional
    public Projeto retrocederEstagio(Projeto projeto) {

        // FINALIZADO para EM ANDAMENTO
        switch (projeto.getStatus()) {
            case FINALIZADO:
                projeto.setStatus(EStatusProjeto.ANDAMENTO);
                projeto.setDataEncerramento(null);
                projeto = projetoRepository.guardar(projeto);
                break;
            // EM ANDAMENTO para A COMEÇAR
            case ANDAMENTO:
                projeto.setStatus(EStatusProjeto.COMECAR);
                projeto.setDataInicio(null);
                projeto = projetoRepository.guardar(projeto);
                break;
            // A COMEÇAR para CANCELADO
            case COMECAR:
                projeto.setStatus(EStatusProjeto.CANCELADO);
                projeto.setDataInicio(null);
                projeto.setDataEncerramento(null);
                projeto = projetoRepository.guardar(projeto);
                break;
            // ATIVIDADE EXTERNA para EM ANDAMENTO
            case ATV_EXTERNA:
                projeto.setStatus(EStatusProjeto.ANDAMENTO);
                projeto = projetoRepository.guardar(projeto);
                break;
            default:
                break;
        }

        return projeto;
    }

    @Transactional
    public Projeto enviarAtividadeExterna(Projeto projeto) {
        projeto.setStatus(EStatusProjeto.ATV_EXTERNA);
        projeto = projetoRepository.guardar(projeto);
        return projeto;
    }

    // Método que calcula os campos 'Horas Executadas' e 'Percentual Executado' das etapas do projeto
    public ProjetoEtapa calcularHorasExecucaoEtapa(ProjetoEtapa projetoEtapa) {
        if (projetoEtapa != null) {
            projetoEtapa.setHorasExecutadas(projetoRepository.horasExecucoesEtapaProjeto(projetoEtapa));
            projetoEtapa.setPercentualExecutado(BigDecimal.ZERO);

            // Verifica se a etapa possui previsão de horas
            if (projetoEtapa.getQuantidadeHoras() != null && projetoEtapa.getQuantidadeHoras().floatValue() > 0) {
                // Verifica se a etapa possui horas executadas
                if (projetoEtapa.getHorasExecutadas() != null && projetoEtapa.getHorasExecutadas().floatValue() > 0) {
                    // Fórmula: (100 / qtde horas da etapa) * qtde horas executadas
                    float percentualExecutado = 100 / projetoEtapa.getQuantidadeHoras().floatValue();
                    percentualExecutado = percentualExecutado * projetoEtapa.getHorasExecutadas().floatValue();
                    projetoEtapa.setPercentualExecutado(new BigDecimal(percentualExecutado));
                }
            }
        }
        return projetoEtapa;
    }

    public String montarDescricaoEnderecoObra(EnderecoCliente endereco) {
        String retorno = "Não informado";

        if (endereco != null) {
            retorno = endereco.getLogradouro();
            retorno += (StringUtils.isBlank(endereco.getNumero()) ? "" : ", " + endereco.getNumero());
            retorno += (StringUtils.isBlank(endereco.getCidade()) ? "" : ", " + endereco.getCidade());
            retorno += (endereco.getUf() == null ? "" : " - " + endereco.getUf());
        }
        return retorno;
    }

    // Verifica se o colaborador está treinado/qualificado para executar uma atividade que necessite de treinamento/qualificação
    public boolean validarTreinamentosColaboradorParaExecutarAtividade(ProjetoExecucao projetoExecucao) {
        if (projetoExecucao.getEtapa() != null
                && projetoExecucao.getEtapa().getEtapa() != null
                && projetoExecucao.getEtapa().getEtapa().getAtividade() != null) {
            return validarTreinamentosColaboradorParaExecutarAtividade(projetoExecucao.getColaborador(), projetoExecucao.getEtapa().getEtapa().getAtividade(), projetoExecucao.getData());
        }
        return true;
    }

    // Verifica se o colaborador está treinado/qualificado para executar uma atividade que necessite de treinamento/qualificação
    public boolean validarTreinamentosColaboradorParaExecutarAtividade(Colaborador colaborador, Atividade atividade, Date data) {
        if (colaborador != null && atividade != null && data != null) {
            Atividade a = atividadeRepository.treinamentosDaAtividade(atividade);

            // Verifica se a atividade possui treinamentos/qualificações como requisito
            if (a != null && a.getTreinamentos() != null && a.getTreinamentos().size() > 0) {
                Colaborador c = colaboradorRepository.treinamentosDoColaborador(colaborador);

                // Percorre os treinamentos da atividade
                for (Treinamento ta : a.getTreinamentos()) {
                    boolean treinamentoOk = false;

                    // Percorre os treinamentos do colaborador
                    if (c != null && c.getTreinamentos() != null && c.getTreinamentos().size() > 0) {
                        for (ColaboradorTreinamento tc : c.getTreinamentos()) {
                            // Verifica se é o treinamento em questão
                            if (tc.getTreinamento().equals(ta)) {
                                // Verifica se o treinamento é válido na data de referência
                                if ((tc.getInicioVigencia().getTime() <= data.getTime()) && (tc.getTerminoVigencia() == null || tc.getTerminoVigencia().getTime() >= data.getTime())) {
                                    treinamentoOk = true;
                                    break;
                                }
                            }
                        }
                    }
                    // Se não tem o treinamento ou se tem mas está vencido gera excessão
                    if (!treinamentoOk) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    // Método que faz as verificações/consistências necessárias antes de inserir/alterar uma execução do projeto
    public void consistirExecucaoProjeto(ProjetoExecucao projetoExecucao) throws ParseException {

        // Projeto no qual a execução pertence
        Projeto projeto = projetoExecucao.getProjeto();

        if (projeto == null) {
            throw new NegocioException("É necessário informar o projeto no qual a execução pertence");
        }

        // Não permite que o usuário faça execução de um projeto sem que tenha registrado o ponto no dia
        ColaboradorPontoFilter colaboradorPontoFilter = new ColaboradorPontoFilter();
        colaboradorPontoFilter.setColaborador(projetoExecucao.getColaborador());
        colaboradorPontoFilter.setDataPontoDe(projetoExecucao.getData());
        colaboradorPontoFilter.setDataPontoAte(projetoExecucao.getData());
        List<ColaboradorPonto> marcacoes = colaboradorPontoRepository.marcacoes(colaboradorPontoFilter);
        if (marcacoes == null || marcacoes.isEmpty()) {
            throw new NegocioException("Colaborador não possui registro de ponto para este dia. Favor primeiramente fazer o registro do ponto");
        }

        // Consiste dias para alteração retroativo
        Date dataAtual = new Date();
        if (!seguranca.visualizaProjetos() && usuarioLogado.getFilialAtiva().getDiasExecucaoRetroativo() != null && usuarioLogado.getFilialAtiva().getDiasExecucaoRetroativo() < Util.DiferencaEmDias(projetoExecucao.getData(), dataAtual)) {
            throw new NegocioException("Usuário sem permissão para criar ou alterar execução com data retroativa");
        }

        // Consiste a hora de início e término
        if (projetoExecucao.getHoraTermino() != null && projetoExecucao.getHoraInicio().getTime() >= projetoExecucao.getHoraTermino().getTime()) {
            throw new NegocioException("Hora término deve ser maior que Hora início");
        }

        // Verifica se o colaborador está treinado/qualificado para executar a atividade
        if (!validarTreinamentosColaboradorParaExecutarAtividade(projetoExecucao)) {
            throw new NegocioException("Colaborador " + projetoExecucao.getColaborador().getNome() + " não possui treinamento/qualificação para executar esta etapa.");
        }

        // Carrega as execuções do projeto
        List<ProjetoExecucao> execucoes = new ArrayList<>();

        try {
            for (ProjetoExecucao e : projeto.getExecucoes()) {
                execucoes.add(e);
            }
            // Pode ocorrer LazyInitializationException
        } catch (Exception e) {
            if (projeto.isEditando()) {
                execucoes = projetoRepository.ExecucoesDoProjeto(projeto);
            }
        }

        if (execucoes != null) {

            // Verifica se existe conflito de horário com outra execução do projeto
            for (ProjetoExecucao exec : execucoes) {

                if (!exec.equals(projetoExecucao) && exec.getColaborador().equals(projetoExecucao.getColaborador())) {
                    if (exec.getData().getTime() == projetoExecucao.getData().getTime()) {

                        DateFormat df = new SimpleDateFormat("HH:mm");

                        if (exec.getHoraTermino() == null && projetoExecucao.getHoraTermino() == null) {
                            throw new NegocioException("Existe um conflito de horário com outra execução do projeto (" + df.format(exec.getHoraInicio()) + " - 23:59)");
                        } else {
                            Date horaInicioAtu = projetoExecucao.getHoraInicio();
                            Date horaTerminoAtu = projetoExecucao.getHoraTermino();
                            Date horaInicioOld = exec.getHoraInicio();
                            Date horaTerminoOld = exec.getHoraTermino();

                            if (horaTerminoAtu == null) {
                                horaTerminoAtu = df.parse("23:59");
                            }
                            if (horaTerminoOld == null) {
                                horaTerminoOld = df.parse("23:59");
                            }

                            if (horaInicioOld.getTime() < horaTerminoAtu.getTime() && horaTerminoOld.getTime() > horaInicioAtu.getTime()) {
                                throw new NegocioException("Existe um conflito de horário com outra execução do projeto (" + df.format(horaInicioOld) + " - " + df.format(horaTerminoOld) + ")");
                            }
                        }
                    }
                }
            }
        }

        if (projetoExecucao.getHoraTermino() == null) {

            if (execucoes != null) {
                // Verifica se o colaborador possui alguma execução em aberto dentro do projeto atual
                for (ProjetoExecucao exec : execucoes) {
                    if (!exec.equals(projetoExecucao)) {
                        if (exec.getColaborador().equals(projetoExecucao.getColaborador()) && exec.getHoraTermino() == null) {
                            throw new NegocioException("Você já possui uma execução em aberto neste projeto. Não é permitido que possua mais de uma execução em aberto.");
                        }
                    }
                }
            }

            // Verifica se o colaborador possui alguma execução em aberto dentro dos demais projetos que estão em andamento
            ProjetoFilter filtro = new ProjetoFilter();
            EStatusProjeto[] statuses = {EStatusProjeto.COMECAR, EStatusProjeto.ANDAMENTO, EStatusProjeto.ATV_EXTERNA};
            filtro.setStatuses(statuses);
            filtro.setFilial(usuarioLogado.getFilialAtiva());
            List<Projeto> prjs = projetoRepository.filtrados(filtro);

            if (prjs != null && prjs.size() > 0) {
                for (Projeto prj : prjs) {
                    if (!prj.equals(projeto)) {
                        for (ProjetoExecucao exec : prj.getExecucoes()) {
                            if (exec.getColaborador().equals(projetoExecucao.getColaborador()) && exec.getHoraTermino() == null) {
                                throw new NegocioException("Você já possui uma execução em aberto no projeto '" + prj.getTitulo() + "'. Não é permitido que possua mais de uma execução em aberto.");
                            }
                        }
                    }
                }
            }
        }
    }

}

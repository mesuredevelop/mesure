package com.mesure.service;

import com.mesure.enumerator.EAcaoLoteFaturamento;
import com.mesure.enumerator.EDiaSemana;
import com.mesure.enumerator.EStatusLoteFaturamento;
import com.mesure.enumerator.ETipoHoraExtra100;
import com.mesure.model.FaturamentoDetalhe;
import com.mesure.model.FaturamentoLote;
import com.mesure.model.Feriado;
import com.mesure.model.Filial;
import com.mesure.model.Projeto;
import com.mesure.model.ProjetoColaborador;
import com.mesure.model.ProjetoExecucao;
import com.mesure.repository.FaturamentoLoteRepository;
import com.mesure.repository.FeriadoRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.util.Util;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;

public class FaturamentoLoteService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    @Inject
    FaturamentoLoteRepository faturamentoLoteRepository;

    @Inject
    ProjetoRepository projetoRepository;

    @Inject
    FeriadoRepository feriadoRepository;

    private List<FaturamentoLote> lotes;

    @Transactional
    public void executarAcaoLote(EAcaoLoteFaturamento acao) throws ParseException {

        if (lotes == null) {
            throw new NegocioException("Nenhum lote de faturamento foi informado");
        }

        int selecionados = 0;
        List<FaturamentoLote> lotesGravar = new ArrayList<>();

        // Percorre todos os lotes e processa os que o usuário selecionou
        for (FaturamentoLote l : lotes) {
            // Se selecionou o registro para alteração
            if (l.isFlag()) {
                selecionados++;

                // Busca o lote na base de dados para garantir que está atualizado
                FaturamentoLote loteSelecionado = faturamentoLoteRepository.porId(l.getId());

                if (loteSelecionado != null) {
                    switch (acao) {
                        case PROCESSAR:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteFaturamento.PENDENTE)) {
                                throw new NegocioException(String.format("Lote não pendente (id = %d)", loteSelecionado.getId()));
                            }
                            // Faz o processamento do lote
                            processarLote(loteSelecionado);

                            loteSelecionado.setStatus(EStatusLoteFaturamento.PROCESSADO);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        case FECHAR:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteFaturamento.PROCESSADO)) {
                                throw new NegocioException(String.format("Lote não processado (id = %d)", loteSelecionado.getId()));
                            }
                            loteSelecionado.setStatus(EStatusLoteFaturamento.FECHADO);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        case LIBERAR:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteFaturamento.FECHADO)) {
                                throw new NegocioException(String.format("Lote não fechado (id = %d)", loteSelecionado.getId()));
                            }
                            if (!loteSelecionado.getUsuario().equals(usuarioLogado.getUsuario())) {
                                throw new NegocioException(String.format("Somente o usuário %s poderá liberar este lote (id = %d)", loteSelecionado.getUsuario().getNome(), loteSelecionado.getId()));
                            }
                            loteSelecionado.setStatus(EStatusLoteFaturamento.LIBERADO);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        case DESFAZER_LIBERACAO:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteFaturamento.LIBERADO)) {
                                throw new NegocioException(String.format("Lote não liberado (id = %d)", loteSelecionado.getId()));
                            }
                            loteSelecionado.setStatus(EStatusLoteFaturamento.FECHADO);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        case REABRIR:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteFaturamento.FECHADO)) {
                                throw new NegocioException(String.format("Lote não fechado (id = %d)", loteSelecionado.getId()));
                            }
                            if (!loteSelecionado.getUsuario().equals(usuarioLogado.getUsuario())) {
                                throw new NegocioException(String.format("Somente o usuário %s poderá reabrir este lote (id = %d)", loteSelecionado.getUsuario().getNome(), loteSelecionado.getId()));
                            }
                            loteSelecionado.setStatus(EStatusLoteFaturamento.PROCESSADO);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        case DESFAZER:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteFaturamento.PROCESSADO)) {
                                throw new NegocioException(String.format("Lote não processado (id = %d)", loteSelecionado.getId()));
                            }
                            // Faz a exclusão do detalhe do lote
                            excluirDetalheLote(loteSelecionado);

                            loteSelecionado.setStatus(EStatusLoteFaturamento.PENDENTE);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        if (selecionados == 0) {
            throw new NegocioException("Nenhum registro foi selecionado");
        }

        // Faz a gravação dos lotes
        if (lotesGravar.size() > 0) {
            gravarLotes(lotesGravar);
        }
    }

    private void gravarLotes(List<FaturamentoLote> lotes) {
        for (FaturamentoLote l : lotes) {
            faturamentoLoteRepository.guardarLote(l);
        }
    }

    private void processarLote(FaturamentoLote lote) throws ParseException {

        Projeto projeto = lote.getProjeto();

        // Início hora noturna
        Date inicioHoraNoturna = projeto.getFilial().getInicioHoraNoturna();

        // Tipo de hora extra 100%
        ETipoHoraExtra100 tipoHoraExtra100 = projeto.getFilial().getTipoHoraExtra100();

        // Busca as execuções realizadas no projeto do lote, ordenado por data, colaborador e hora início
        List<ProjetoExecucao> execucoes = projetoRepository.execucoesProjetoPeriodo(projeto, lote.getDataInicio(), lote.getDataFim());

        if (execucoes.isEmpty()) {
            throw new NegocioException("Não foram encontradas execuções no projeto para processar o lote");
        }

        List<ProjetoColaborador> participantesProjeto = projeto.getParticipantes();

        // Percorre todas as execuções encontradas
        for (ProjetoExecucao execucao : execucoes) {

            FaturamentoDetalhe detalhe = new FaturamentoDetalhe();

            detalhe.setData(execucao.getData());
            detalhe.setDiaSemana(Util.diaDaSemanaEnum(execucao.getData()));
            detalhe.setLote(lote);
            detalhe.setColaborador(execucao.getColaborador());

            // Inicializa os campos
            detalhe.setValorCobranca(BigDecimal.ZERO);
            detalhe.setValorCobrancaNoturna(BigDecimal.ZERO);
            detalhe.setValor50(BigDecimal.ZERO);
            detalhe.setValor100(BigDecimal.ZERO);
            detalhe.setTotalCobranca(BigDecimal.ZERO);
            detalhe.setTotalCobrancaNoturna(BigDecimal.ZERO);
            detalhe.setTotalCobranca100(BigDecimal.ZERO);

            for (ProjetoColaborador participante : participantesProjeto) {
                if (participante.getColaborador().equals(detalhe.getColaborador())) {
                    // Valor cobrança
                    if (participante.getValorCobranca() != null) {
                        detalhe.setValorCobranca(participante.getValorCobranca());
                    }
                    // Valor cobrança noturna
                    if (participante.getValorCobrancaNoturna() != null) {
                        detalhe.setValorCobrancaNoturna(participante.getValorCobrancaNoturna());
                    }
                    break;
                }
            }

            // Hora início, hora término e total de horas
            if (isHora100Porcento(detalhe, tipoHoraExtra100)) {
                detalhe.setHoraInicioExtra(execucao.getHoraInicio());
                detalhe.setHoraFimExtra(execucao.getHoraTermino());
                detalhe.setTotalHorasExtra(execucao.getTotalHoras());
            } else if (inicioHoraNoturna != null && execucao.getHoraInicio().getTime() >= inicioHoraNoturna.getTime()) {
                detalhe.setHoraInicioNoturno(execucao.getHoraInicio());
                detalhe.setHoraFimNoturno(execucao.getHoraTermino());
                detalhe.setTotalHorasNoturno(execucao.getTotalHoras());
            } else {
                detalhe.setHoraInicio(execucao.getHoraInicio());
                detalhe.setHoraFim(execucao.getHoraTermino());
                detalhe.setTotalHoras(execucao.getTotalHoras());
            }

            if (detalhe.getValorCobranca() != null) {
                // Valor 50% (valor da cobrança *  1,5)
                detalhe.setValor50(detalhe.getValorCobranca().multiply(new BigDecimal(1.5)));
                // Valor 100% (valor da cobrança * 2)
                detalhe.setValor100(detalhe.getValorCobranca().multiply(new BigDecimal(2)));
            }

            // Total cobrança
            if (detalhe.getTotalHoras() != null && detalhe.getValorCobranca() != null) {
                detalhe.setTotalCobranca(detalhe.getTotalHoras().multiply(detalhe.getValorCobranca()));
            }

            // Total cobrança noturna
            if (detalhe.getTotalHorasNoturno() != null && detalhe.getValorCobrancaNoturna() != null) {
                detalhe.setTotalCobrancaNoturna(detalhe.getTotalHorasNoturno().multiply(detalhe.getValorCobrancaNoturna()));
            }

            // Total cobrança 100%
            if (detalhe.getTotalHorasExtra() != null && detalhe.getValor100() != null) {
                detalhe.setTotalCobranca100(detalhe.getTotalHorasExtra().multiply(detalhe.getValor100()));
            }

            // Total geral
            detalhe.setTotalGeral(detalhe.getTotalCobranca().add(detalhe.getTotalCobrancaNoturna().add(detalhe.getTotalCobranca100())));

            // Faz a gravação do registro detalhe
            faturamentoLoteRepository.guardarDetalhe(detalhe);
        }
    }

    private boolean isHora100Porcento(FaturamentoDetalhe detalhe, ETipoHoraExtra100 tipoHoraExtra100) {
        if (isFeriado(detalhe.getData(), detalhe.getColaborador().getFilial())) {
            return true;
        }
        if (detalhe.getDiaSemana().equals(EDiaSemana.SABADO) && tipoHoraExtra100.equals(ETipoHoraExtra100.SAB_DOM_FER)) {
            return true;
        }
        if (detalhe.getDiaSemana().equals(EDiaSemana.DOMINGO) && (tipoHoraExtra100.equals(ETipoHoraExtra100.DOM_FER) || tipoHoraExtra100.equals(ETipoHoraExtra100.SAB_DOM_FER))) {
            return true;
        }
        return false;
    }

    private boolean isFeriado(Date data, Filial filial) {
        List<Feriado> feriados = feriadoRepository.porData(data, filial);
        return (feriados != null && feriados.size() > 0);
    }

    private void excluirDetalheLote(FaturamentoLote lote) {
        lote.getDetalhes().removeAll(lote.getDetalhes());
    }

    public List<FaturamentoLote> getLotes() {
        return lotes;
    }

    public void setLotes(List<FaturamentoLote> lotes) {
        this.lotes = lotes;
    }

}

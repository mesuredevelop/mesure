package com.mesure.service;

import com.mesure.model.Atividade;
import com.mesure.repository.AtividadeRepository;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import javax.inject.Inject;

public class AtividadeService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private AtividadeRepository atividadeRepository;

    @Transactional
    public Atividade salvar(Atividade atividade) {

        if (atividade.getEtapas().isEmpty()) {
            throw new NegocioException("É necessário que a atividade contenha ao menos 1 (uma) etapa.");
        }

        return atividadeRepository.guardar(atividade);
    }
}

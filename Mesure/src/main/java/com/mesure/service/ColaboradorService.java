package com.mesure.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.mesure.enumerator.ESituacaoColaborador;
import com.mesure.model.Colaborador;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.util.jpa.Transactional;

public class ColaboradorService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ColaboradorRepository colaboradorRepository;
	
	@Transactional
	public Colaborador salvar(Colaborador colaborador) {
		
		if (colaborador.getUsuario() != null) {
			Colaborador colaboradorExistente = colaboradorRepository.porUsuario(colaborador.getUsuario());
			
			if (colaboradorExistente != null && !colaboradorExistente.equals(colaborador)) {
				throw new NegocioException("Já existe um colaborador vinculado ao usuário informado.");
			}			
		}
		
		if (colaborador.getSituacao() == ESituacaoColaborador.DEMITIDO && colaborador.getDataDesligamento() == null) {
			throw new NegocioException("É necessário informar Data Desligamento");
		}
		
		if (colaborador.getSituacao() != ESituacaoColaborador.DEMITIDO) {
			colaborador.setDataDesligamento(null);
		}		
		
		return colaboradorRepository.guardar(colaborador);
	}
}


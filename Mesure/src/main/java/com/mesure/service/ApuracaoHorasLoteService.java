package com.mesure.service;

import com.mesure.enumerator.EAcaoLoteApuracaoHoras;
import com.mesure.enumerator.EDiaSemana;
import com.mesure.enumerator.EEstado;
import com.mesure.enumerator.EPeriodoAlimentacao;
import com.mesure.enumerator.ESituacaoBancoHoras;
import com.mesure.enumerator.EStatusLoteApuracaoHoras;
import com.mesure.enumerator.ETipoHoraExtra100;
import com.mesure.enumerator.ETipoRegistroPonto;
import com.mesure.model.ApuracaoHorasDetalhe;
import com.mesure.model.ApuracaoHorasLote;
import com.mesure.model.Colaborador;
import com.mesure.model.ColaboradorPonto;
import com.mesure.model.Feriado;
import com.mesure.model.Filial;
import com.mesure.model.ProgramacaoExecucao;
import com.mesure.model.ProjetoExecucao;
import com.mesure.model.TipoAlimentacao;
import com.mesure.repository.ApuracaoHorasLoteRepository;
import com.mesure.repository.ColaboradorPontoRepository;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.repository.FeriadoRepository;
import com.mesure.repository.ProgramacaoExecucaoRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.TipoAlimentacaoRepository;
import com.mesure.repository.filter.ProgramacaoExecucaoFilter;
import com.mesure.security.UsuarioLogado;
import com.mesure.security.UsuarioSistema;
import com.mesure.util.Util;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.inject.Inject;

public class ApuracaoHorasLoteService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    @UsuarioLogado
    private UsuarioSistema usuarioLogado;

    @Inject
    ApuracaoHorasLoteRepository apuracaoHorasLoteRepository;

    @Inject
    ProgramacaoExecucaoRepository programacaoExecucaoRepository;

    @Inject
    ColaboradorPontoRepository colaboradorPontoRepository;

    @Inject
    ColaboradorRepository colaboradorRepository;

    @Inject
    ProjetoRepository projetoRepository;

    @Inject
    FeriadoRepository feriadoRepository;

    @Inject
    TipoAlimentacaoRepository tipoAlimentacaoRepository;

    private List<ApuracaoHorasLote> lotes;

    @Transactional
    public void executarAcaoLote(EAcaoLoteApuracaoHoras acao) throws ParseException {

        if (lotes == null) {
            throw new NegocioException("Nenhum lote de apuração de horas foi informado");
        }

        int selecionados = 0;
        List<ApuracaoHorasLote> lotesGravar = new ArrayList<>();

        // Percorre todos os lotes e processa os que o usuário selecionou
        for (ApuracaoHorasLote l : lotes) {
            // Se selecionou o registro para alteração
            if (l.isFlag()) {
                selecionados++;

                // Busca o lote na base de dados para garantir que está atualizado
                ApuracaoHorasLote loteSelecionado = apuracaoHorasLoteRepository.porId(l.getId());

                if (loteSelecionado != null) {
                    switch (acao) {
                        case PROCESSAR:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteApuracaoHoras.PENDENTE)) {
                                throw new NegocioException(String.format("Lote não pendente (id = %d)", loteSelecionado.getId()));
                            }
                            // Faz o processamento do lote
                            processarLote(loteSelecionado);

                            loteSelecionado.setStatus(EStatusLoteApuracaoHoras.PROCESSADO);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        case FECHAR:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteApuracaoHoras.PROCESSADO)) {
                                throw new NegocioException(String.format("Lote não processado (id = %d)", loteSelecionado.getId()));
                            }
                            loteSelecionado.setStatus(EStatusLoteApuracaoHoras.FECHADO);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        case LIBERAR:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteApuracaoHoras.FECHADO)) {
                                throw new NegocioException(String.format("Lote não fechado (id = %d)", loteSelecionado.getId()));
                            }
                            if (!loteSelecionado.getUsuario().equals(usuarioLogado.getUsuario())) {
                                throw new NegocioException(String.format("Somente o usuário %s poderá liberar este lote (id = %d)", loteSelecionado.getUsuario().getNome(), loteSelecionado.getId()));
                            }
                            loteSelecionado.setStatus(EStatusLoteApuracaoHoras.LIBERADO);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        case DESFAZER_LIBERACAO:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteApuracaoHoras.LIBERADO)) {
                                throw new NegocioException(String.format("Lote não liberado (id = %d)", loteSelecionado.getId()));
                            }
                            loteSelecionado.setStatus(EStatusLoteApuracaoHoras.FECHADO);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        case REABRIR:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteApuracaoHoras.FECHADO)) {
                                throw new NegocioException(String.format("Lote não fechado (id = %d)", loteSelecionado.getId()));
                            }
                            if (!loteSelecionado.getUsuario().equals(usuarioLogado.getUsuario())) {
                                throw new NegocioException(String.format("Somente o usuário %s poderá reabrir este lote (id = %d)", loteSelecionado.getUsuario().getNome(), loteSelecionado.getId()));
                            }
                            loteSelecionado.setStatus(EStatusLoteApuracaoHoras.PROCESSADO);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        case DESFAZER:
                            if (!loteSelecionado.getStatus().equals(EStatusLoteApuracaoHoras.PROCESSADO)) {
                                throw new NegocioException(String.format("Lote não processado (id = %d)", loteSelecionado.getId()));
                            }
                            // Faz a exclusão do detalhe do lote
                            excluirDetalheLote(loteSelecionado);

                            loteSelecionado.setStatus(EStatusLoteApuracaoHoras.PENDENTE);
                            loteSelecionado.setDataAlteracao(new Date());
                            loteSelecionado.setUsuario(usuarioLogado.getUsuario());
                            lotesGravar.add(loteSelecionado);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        if (selecionados == 0) {
            throw new NegocioException("Nenhum registro foi selecionado");
        }

        // Faz a gravação dos lotes
        if (lotesGravar.size() > 0) {
            gravarLotes(lotesGravar);
        }
    }

    private void gravarLotes(List<ApuracaoHorasLote> lotes) {
        for (ApuracaoHorasLote l : lotes) {
            apuracaoHorasLoteRepository.guardarLote(l);
        }
    }

    private void processarLote(ApuracaoHorasLote lote) throws ParseException {

        Colaborador colaborador = lote.getColaborador();

        // Quantidade de horas previstas (diárias) para o colaborador
        BigDecimal horasPrevistas = colaborador.getHorasTrabalhoDiario();

        // Saldo do banco de horas do colaborador
        BigDecimal saldoBH = colaborador.getSaldoBH();

        // Limite banco de horas
        BigDecimal limiteBH = BigDecimal.ZERO;
        if (colaborador.getFilial().getLimiteBH() != null) {
            limiteBH = BigDecimal.valueOf(colaborador.getFilial().getLimiteBH());
        }

        // Início hora noturna
        Date inicioHoraNoturna = colaborador.getFilial().getInicioHoraNoturna();

        // Tipo de hora extra 100%
        ETipoHoraExtra100 tipoHoraExtra100 = colaborador.getFilial().getTipoHoraExtra100();

        if (horasPrevistas == null) {
            horasPrevistas = BigDecimal.ZERO;
        }

        if (saldoBH == null) {
            saldoBH = BigDecimal.ZERO;
        }

        BigDecimal saldoBHAcumulado = saldoBH;

        boolean faltaNaSemana = false;

        long dif = Util.DiferencaEmDias(lote.getDataInicio(), lote.getDataFim()) + 1;

        Calendar dataAux = new GregorianCalendar();
        dataAux.setTime(lote.getDataInicio());

        // Percorre todo o período do lote
        for (int i = 1; i <= dif; i++) {
            ApuracaoHorasDetalhe detalhe = new ApuracaoHorasDetalhe();

            detalhe.setData(dataAux.getTime());
            detalhe.setDiaSemana(Util.diaDaSemanaEnum(dataAux.getTime()));
            detalhe.setSituacaoBancoHoras(ESituacaoBancoHoras.ZERADO);
            detalhe.setLote(lote);

            Date totalHoras = null;
            BigDecimal totalHorasDecimal = BigDecimal.ZERO;

            // Busca o registro de ponto para o dia em questão
            ColaboradorPonto ponto = colaboradorPontoRepository.pontoColaborador(lote.getColaborador(), dataAux.getTime());

            // Se NÃO possui registro de ponto, então verifica se é FINAL DE SEMANA, FERIADO ou FALTA
            if (ponto == null) {
                EDiaSemana diaSemana = detalhe.getDiaSemana();

                if (isFeriado(detalhe.getData(), colaborador.getFilial())) {
                    detalhe.setTipoRegistro(ETipoRegistroPonto.FERIADO);
                } else if (diaSemana == EDiaSemana.SABADO || diaSemana == EDiaSemana.DOMINGO) {
                    detalhe.setTipoRegistro(ETipoRegistroPonto.FDSEMANA);
                }

                // Se não é FINAL DE SEMANA nem FERIADO, então é FALTA
                if (detalhe.getTipoRegistro() == null) {
                    detalhe.setTipoRegistro(ETipoRegistroPonto.FALTA);
                }
            } else {
                // Copia os dados do ponto para a tabela de apuração detalhe
                detalhe.setTipoRegistro(ponto.getTipoRegistro());
                detalhe.setHoraEntrada(ponto.getHoraEntrada());
                detalhe.setHoraSaida(ponto.getHoraSaida());
                detalhe.setHoraSaidaAlmoco(ponto.getHoraSaidaAlmoco());
                detalhe.setHoraRetornoAlmoco(ponto.getHoraRetornoAlmoco());
                detalhe.setHoraEntradaExtra(ponto.getHoraEntradaExtra());
                detalhe.setHoraSaidaExtra(ponto.getHoraSaidaExtra());

                if (ponto.getTotalHoras() != null) {
                    totalHorasDecimal = ponto.getTotalHoras();
                    totalHoras = Util.converteCentesimalParaHora(totalHorasDecimal.floatValue());

                    // Horas extras 100%
                    if (isHora100Porcento(detalhe, tipoHoraExtra100)) {
                        detalhe.setExtras100(totalHorasDecimal);
                    }
                }
                detalhe.setTotalHoras(totalHoras);
                detalhe.setTotalHorasDecimal(totalHorasDecimal);

                // Horas noturnas e Horas noturnas 100%
                if (inicioHoraNoturna != null) {
                    double horasNoturnas = 0;

                    // Verifica os campos Hora Entrada e Hora Saída
                    if (detalhe.getHoraSaida() != null && detalhe.getHoraSaida().getTime() > inicioHoraNoturna.getTime()) {
                        if (detalhe.getHoraEntrada().getTime() <= inicioHoraNoturna.getTime()) {
                            horasNoturnas = Util.DiferencaEmHoras(inicioHoraNoturna, detalhe.getHoraSaida());
                        } else {
                            horasNoturnas = Util.DiferencaEmHoras(detalhe.getHoraEntrada(), detalhe.getHoraSaida());
                        }
                    }
                    // Verifica os campos Hora Entrada Extra e Hora Saída Extra
                    if (detalhe.getHoraSaidaExtra() != null && detalhe.getHoraSaidaExtra().getTime() > inicioHoraNoturna.getTime()) {
                        if (detalhe.getHoraEntradaExtra().getTime() <= inicioHoraNoturna.getTime()) {
                            horasNoturnas += Util.DiferencaEmHoras(inicioHoraNoturna, detalhe.getHoraSaidaExtra());
                        } else {
                            horasNoturnas += Util.DiferencaEmHoras(detalhe.getHoraEntradaExtra(), detalhe.getHoraSaidaExtra());
                        }
                    }
                    // Verifica se é Hora noturna ou Hora noturna 100%
                    if (horasNoturnas > 0) {
                        if (isHora100Porcento(detalhe, tipoHoraExtra100)) {
                            detalhe.setHoraNoturna100(new BigDecimal(horasNoturnas));
                        } else {
                            detalhe.setHoraNoturna(new BigDecimal(horasNoturnas));
                        }
                    }
                }
            }

            // DSR Negativo
            if (detalhe.getDiaSemana().equals(EDiaSemana.DOMINGO)) {
                if (faltaNaSemana) {
                    detalhe.setDsrNegativo(horasPrevistas);
                }
                faltaNaSemana = false;
            } else {
                if (detalhe.getTipoRegistro().equals(ETipoRegistroPonto.FALTA)) {
                    faltaNaSemana = true;
                }
            }

            BigDecimal saldo = BigDecimal.ZERO;

            // Se não for ATESTADO nem FOLGA então calcula o saldo
            if (!detalhe.getTipoRegistro().equals(ETipoRegistroPonto.ATESTADO)
                    && !detalhe.getTipoRegistro().equals(ETipoRegistroPonto.FOLGA)) {

                switch (detalhe.getTipoRegistro()) {
                    case NORMAL:
                        saldo = detalhe.getTotalHorasDecimal().subtract(horasPrevistas);
                        break;
                    case FALTA:
                        saldo = horasPrevistas.negate();
                        break;
                    case FDSEMANA:
                    case FERIADO:
                        if (ponto != null) {
                            saldo = detalhe.getTotalHorasDecimal();
                        }
                        break;
                    default:
                        break;
                }
            }

            String saldoStr = Util.formatarDouble(Util.arredondarDouble(saldo.floatValue(), 2));

            if (!saldoStr.equalsIgnoreCase("0,00")) {
                // Saldo BH (acumulado até este dia)
                saldoBHAcumulado = saldoBHAcumulado.add(saldo);
                // Verifica se é Positivo ou Negativo
                if (saldo.floatValue() > 0) {
                    detalhe.setBhPositivo(Util.converteCentesimalParaHora(Math.abs(saldo.floatValue())));
                } else {
                    detalhe.setBhNegativo(Util.converteCentesimalParaHora(Math.abs(saldo.floatValue())));
                }
            }

            saldoStr = Util.formatarDouble(Util.arredondarDouble(saldoBHAcumulado.floatValue(), 2));

            if (!saldoStr.equalsIgnoreCase("0,00")) {
                if (saldoBHAcumulado.floatValue() > 0) {
                    detalhe.setSituacaoBancoHoras(ESituacaoBancoHoras.POSITIVO);
                } else {
                    detalhe.setSituacaoBancoHoras(ESituacaoBancoHoras.NEGATIVO);
                }
            } else {
                saldoBHAcumulado = BigDecimal.ZERO;
            }

            detalhe.setBhSaldoDecimal(saldoBHAcumulado);

            // Horas extras 50%
            if (limiteBH.floatValue() > 0 && saldoBHAcumulado.floatValue() > limiteBH.floatValue()) {
                detalhe.setExtras50(saldoBHAcumulado.subtract(limiteBH));
            }

            // Horas de viagem
            detalhe.setHorasViagem(getHorasViagem(detalhe));

            // Valor de alimentação
            detalhe.setValorAlimentacao(getValorAlimentacao(detalhe));

            // Faz a gravação do registro detalhe
            apuracaoHorasLoteRepository.guardarDetalhe(detalhe);

            dataAux.add(Calendar.DAY_OF_MONTH, 1);
        }
    }

    private void excluirDetalheLote(ApuracaoHorasLote lote) {
        lote.getDetalhes().removeAll(lote.getDetalhes());
    }

    private boolean isHora100Porcento(ApuracaoHorasDetalhe detalhe, ETipoHoraExtra100 tipoHoraExtra100) {
        if (detalhe.getTipoRegistro().equals(ETipoRegistroPonto.FERIADO)) {
            return true;
        }
        if (detalhe.getDiaSemana().equals(EDiaSemana.SABADO) && tipoHoraExtra100.equals(ETipoHoraExtra100.SAB_DOM_FER)) {
            return true;
        }
        if (detalhe.getDiaSemana().equals(EDiaSemana.DOMINGO) && (tipoHoraExtra100.equals(ETipoHoraExtra100.DOM_FER) || tipoHoraExtra100.equals(ETipoHoraExtra100.SAB_DOM_FER))) {
            return true;
        }
        return false;
    }

    private boolean isFeriado(Date data, Filial filial) {
        List<Feriado> feriados = feriadoRepository.porData(data, filial);
        return (feriados != null && feriados.size() > 0);
    }

    private BigDecimal getHorasViagem(ApuracaoHorasDetalhe detalhe) {
        BigDecimal retorno = BigDecimal.ZERO;

        ProgramacaoExecucaoFilter filtro = new ProgramacaoExecucaoFilter();
        filtro.setColaborador(detalhe.getLote().getColaborador());
        filtro.setDataDe(detalhe.getData());
        filtro.setDataAte(detalhe.getData());

        // Busca as programações para o colaborador na data
        List<ProgramacaoExecucao> programacoes = programacaoExecucaoRepository.programacoes(filtro);

        if (programacoes != null) {
            for (ProgramacaoExecucao p : programacoes) {
                if (p.getHorasViagem() != null) {
                    retorno = retorno.add(p.getHorasViagem());
                }
            }
        }
        if (retorno.floatValue() == 0) {
            return null;
        }
        return retorno;
    }

    public BigDecimal getValorAlimentacao(ApuracaoHorasDetalhe detalhe) {
        BigDecimal retorno = BigDecimal.ZERO;

        if (detalhe.getHoraEntrada() != null && (detalhe.getHoraSaida() != null || detalhe.getHoraSaidaExtra() != null)) {
            BigDecimal valorCafe = BigDecimal.ZERO;
            BigDecimal valorAlmoco = BigDecimal.ZERO;
            BigDecimal valorJanta = BigDecimal.ZERO;

            long horaEntrada = detalhe.getHoraEntrada() != null ? detalhe.getHoraEntrada().getTime() : 0;
            long horaSaida = detalhe.getHoraSaida() != null ? detalhe.getHoraSaida().getTime() : 0;
            long horaSaidaExtra = detalhe.getHoraSaidaExtra() != null ? detalhe.getHoraSaidaExtra().getTime() : 0;
            long horaSaidaAlmoco = detalhe.getHoraSaidaAlmoco() != null ? detalhe.getHoraSaidaAlmoco().getTime() : 0;
            long horaRetornoAlmoco = detalhe.getHoraRetornoAlmoco() != null ? detalhe.getHoraRetornoAlmoco().getTime() : 0;

            // Busca as execucoes realizadas pelo colaborador na data em questão 
            List<ProjetoExecucao> execucoes = projetoRepository.execucoesColaboradorData(detalhe.getLote().getColaborador(), detalhe.getData());

            for (ProjetoExecucao execucao : execucoes) {
                Filial filial = execucao.getProjeto().getFilial();
                EEstado estado = execucao.getProjeto().getEnderecoObra().getUf();

                // Busca os valores de alimentação para a filial do projeto e estado da obra
                List<TipoAlimentacao> tiposAlimentacao = tipoAlimentacaoRepository.obterTiposAlimentacao(filial, estado);

                // Se encontrou então verifica se deve pagar café da manhã, almoço e/ou janta 
                if (tiposAlimentacao != null && tiposAlimentacao.size() > 0) {
                    for (TipoAlimentacao tipoAlimentacao : tiposAlimentacao) {
                        long horaInicioAlimentacao = tipoAlimentacao.getHoraInicio().getTime();
                        long horaTerminoAlimentacao = tipoAlimentacao.getHoraTermino().getTime();

                        // Café da manhã
                        if (tipoAlimentacao.getPeriodo().equals(EPeriodoAlimentacao.CAFE_MANHA) && valorCafe.floatValue() == 0) {
                            if (horaEntrada >= horaInicioAlimentacao && horaEntrada <= horaTerminoAlimentacao) {
                                valorCafe = valorCafe.add(tipoAlimentacao.getValor());
                            }
                        }
                        // Almoço
                        if (tipoAlimentacao.getPeriodo().equals(EPeriodoAlimentacao.ALMOCO) && valorAlmoco.floatValue() == 0) {
                            if ((horaSaidaAlmoco >= horaInicioAlimentacao && horaSaidaAlmoco <= horaTerminoAlimentacao)
                                    || (horaRetornoAlmoco >= horaInicioAlimentacao && horaRetornoAlmoco <= horaTerminoAlimentacao)) {
                                valorAlmoco = valorAlmoco.add(tipoAlimentacao.getValor());
                            }
                        }
                        // Janta
                        if (tipoAlimentacao.getPeriodo().equals(EPeriodoAlimentacao.JANTA) && valorJanta.floatValue() == 0) {
                            if ((horaSaida >= horaInicioAlimentacao && horaSaida <= horaTerminoAlimentacao)
                                    || (horaSaidaExtra >= horaInicioAlimentacao && horaSaidaExtra <= horaTerminoAlimentacao)) {
                                valorJanta = valorJanta.add(tipoAlimentacao.getValor());
                            }
                        }
                    }
                }
                // Faz a soma dos valores
                retorno = retorno.add(valorCafe).add(valorAlmoco).add(valorJanta);
                break;
            }
        }
        if (retorno.floatValue() == 0) {
            return null;
        }
        return retorno;
    }

    @Transactional
    public void incluirLoteTodosColaboradores(List<Colaborador> colaboradores, List<ApuracaoHorasLote> lotes, ApuracaoHorasLote lote) {
        List<ApuracaoHorasLote> apuracaoLotes = new ArrayList<>();

        for (Colaborador colaborador : colaboradores) {
            ApuracaoHorasLote loteAux = new ApuracaoHorasLote();
            loteAux.setColaborador(colaborador);
            loteAux.setCompetencia(lote.getCompetencia());
            loteAux.setDataAlteracao(lote.getDataAlteracao());
            loteAux.setDataCriacao(lote.getDataCriacao());
            loteAux.setDataFim(lote.getDataFim());
            loteAux.setDataInicio(lote.getDataInicio());
            loteAux.setDescricao(lote.getDescricao());
            loteAux.setFlag(false);
            loteAux.setStatus(lote.getStatus());
            loteAux.setUsuario(lote.getUsuario());
            loteAux.setTodosColaboradores(false);

            apuracaoLotes.add(loteAux);
        }

        for (ApuracaoHorasLote apuracao : apuracaoLotes) {
            lote = apuracaoHorasLoteRepository.guardarLote(apuracao);

            if (!lotes.contains(lote)) {
                lotes.add(lote);
            }
        }
    }

    public List<ApuracaoHorasLote> getLotes() {
        return lotes;
    }

    public void setLotes(List<ApuracaoHorasLote> lotes) {
        this.lotes = lotes;
    }

}

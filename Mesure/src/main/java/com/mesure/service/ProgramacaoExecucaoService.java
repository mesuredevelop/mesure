package com.mesure.service;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoAtividadePrevista;
import com.mesure.model.ProgramacaoExecucao;
import com.mesure.model.ProgramacaoExecucaoColaborador;
import com.mesure.model.ProjetoAtividadePrevista;
import com.mesure.model.ProjetoEtapa;
import com.mesure.model.VeiculoMovimento;
import com.mesure.repository.ProgramacaoExecucaoRepository;
import com.mesure.repository.ProjetoRepository;
import com.mesure.repository.VeiculoRepository;
import com.mesure.util.Util;
import com.mesure.util.jpa.Transactional;

public class ProgramacaoExecucaoService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	ProgramacaoExecucaoRepository programacaoExecucaoRepository;

	@Inject
	ProjetoRepository projetoRepository;
	
	@Inject
	ProjetoService projetoService;

	@Inject
	VeiculoRepository veiculoRepository;

	/* 
	 * Este método faz a gravação da programação de execução e:
	 * 	 -> inclusão: grava a atividade prevista e movimentação do veículo, e duplica para os demais participantes e dias (exceto a movimentação)
	 *   -> alteração: atualiza a atividade prevista e movimentação do veículo quando houver
	 */
	@Transactional
	public ProgramacaoExecucao salvar(ProgramacaoExecucao programacao) throws ParseException {
		
		List<ProgramacaoExecucao> listaProgramacoes = new ArrayList<>();
		
		if (programacao.isNovo()) {
			
			List<ProgramacaoExecucaoColaborador> listaParticipantes = new ArrayList<>();
			
			// Adiciona a própria programação
			listaParticipantes.add(new ProgramacaoExecucaoColaborador(programacao, programacao.getColaborador(), programacao.getHorasViagem()));			

			// Adiciona outros participantes da programação
			for (ProgramacaoExecucaoColaborador c : programacao.getParticipantes()) {
				listaParticipantes.add(c);
			}
			
			// Verifica para quantos dias deve duplicar a programação
			long dif = 1;
			if (programacao.getDataTermino() != null && programacao.getDataTermino().getTime() > programacao.getData().getTime()) {
				dif = Util.DiferencaEmDias(programacao.getData(), programacao.getDataTermino()) + 1;
			}

			Calendar dataAux = new GregorianCalendar();
			dataAux.setTime(programacao.getData());
			
			// Percorre todos os dias em que deve gerar a programação
			for (int i = 1; i <= dif; i++) {
				// Percorre todos os participantes da programação
				for (ProgramacaoExecucaoColaborador part : listaParticipantes) {					
					// Se é a própria programação da tela, apenas adiciona na lista
					if (part.getColaborador().equals(programacao.getColaborador()) && dataAux.getTime().getTime() == programacao.getData().getTime()) {
						listaProgramacoes.add(programacao);
					} else {
						// Cria uma nova programação
						ProgramacaoExecucao programacaoAux = new ProgramacaoExecucao();

						programacaoAux.setData(dataAux.getTime());
						programacaoAux.setFilial(programacao.getFilial());
						programacaoAux.setColaborador(part.getColaborador());
						programacaoAux.setProjeto(programacao.getProjeto());
						programacaoAux.setEtapa(programacao.getEtapa());
						programacaoAux.setVeiculo(programacao.getVeiculo());
						programacaoAux.setHoraInicio(programacao.getHoraInicio());
						programacaoAux.setHoraTermino(programacao.getHoraTermino());
						programacaoAux.setSaidaEmpresa(programacao.getSaidaEmpresa());
						programacaoAux.setRetornoEmpresa(programacao.getRetornoEmpresa());
						if (part.getColaborador().equals(programacao.getColaborador())) {
							programacaoAux.setMotorista(programacao.getMotorista());
							programacaoAux.setVeiculoProprio(programacao.getVeiculoProprio());
						} else {
							programacaoAux.setMotorista(ESimNao.NAO);
							programacaoAux.setVeiculoProprio(ESimNao.NAO);
						}
						programacaoAux.setAtivo(programacao.getAtivo());
						programacaoAux.setHorasViagem(part.getHorasViagem());
						programacaoAux.setTipoAtividadePrevista(programacao.getTipoAtividadePrevista());
						programacaoAux.setObservacao(programacao.getObservacao());

						// Outros participantes da programação
						for (ProgramacaoExecucaoColaborador c : listaParticipantes) {
							// Verifica se não é o próprio colaborador da programação, caso não seja, então adiciona
							if (!c.getColaborador().equals(programacaoAux.getColaborador())) {
								if (programacaoAux.getParticipantes().indexOf(c) < 0) {
									programacaoAux.getParticipantes().add(new ProgramacaoExecucaoColaborador(programacaoAux, c.getColaborador(), c.getHorasViagem()));
								}
							}
						}

						// Outras etapas da programação
						for (ProjetoEtapa e : programacao.getEtapas()) {
							programacaoAux.getEtapas().add(e);
						}

						listaProgramacoes.add(programacaoAux);
					}
				}
				dataAux.add(Calendar.DAY_OF_MONTH, 1);
			}
		} else {
			listaProgramacoes.add(programacao);
		}		
		
		
		// Percorre e grava todas as programações no banco de dados
		for (ProgramacaoExecucao prg : listaProgramacoes) {
			
			// Grava a programação de execução
			if (prg.equals(programacao)) {
				prg = programacaoExecucaoRepository.guardar(prg);
				programacao = prg;
			} else {
				prg = programacaoExecucaoRepository.guardar(prg);
			}
			
			ProjetoAtividadePrevista atividadePrevista = null;
			VeiculoMovimento movimento = null;
			
			// Busca a atividade prevista que foi gerada a partir da programação
			if (prg.isEditando()) {
				atividadePrevista = programacaoExecucaoRepository.atividadePrevistaDaProgramacaoExecucao(prg);	
			}

			if (atividadePrevista == null) {
				atividadePrevista = new ProjetoAtividadePrevista();
			}
			
			// Seta os dados a atividade prevista
			atividadePrevista.setData(prg.getData());
			atividadePrevista.setPausa(ESimNao.NAO);
			atividadePrevista.setSituacao(ESituacaoAtividadePrevista.PENDENTE);
			atividadePrevista.setColaborador(prg.getColaborador());
			atividadePrevista.setProjeto(prg.getProjeto());
			atividadePrevista.setEtapa(prg.getEtapa());
			atividadePrevista.setTipo(prg.getTipoAtividadePrevista());
			atividadePrevista.setProgramacaoExecucao(prg);

			// Grava a atividade prevista, vinculada a esta programação de execução
			projetoRepository.guardarAtividadePrevista(atividadePrevista);
			
			// Se tem veículo e é motorista então insere/atualiza o registro de movimentação do veículo
			if (prg.getVeiculoProprio().equals(ESimNao.NAO) && prg.getMotorista().equals(ESimNao.SIM) && prg.getVeiculo() != null) {
				
				// Busca a movimentação de veículo que foi gerada a partir da programação
				if (prg.isEditando()) {
					movimento = programacaoExecucaoRepository.movimentacaoVeiculoDaProgramacaoExecucao(prg);	
				}
				
				if (movimento == null) {
					movimento = new VeiculoMovimento();
				}
				
				// Verifica se já existe algum movimento para o veículo conflitando com o horário da programação
				List<VeiculoMovimento> movimentos = veiculoRepository.movimentacoes(prg.getVeiculo(), prg.getData(), prg.getData());

				if (movimentos != null) {				
					for (VeiculoMovimento mov : movimentos) {
						
						if (!mov.equals(movimento)) {
							Date hrSaida = mov.getHoraSaida();
							Date hrRetorno = mov.getHoraRetorno();
							
							if (hrRetorno == null) {
								hrRetorno = Util.montarHora("23:59");
							}
							
							// Verifica se há conflito
							if (prg.getSaidaEmpresa().getTime() < hrRetorno.getTime() && prg.getRetornoEmpresa().getTime() > hrSaida.getTime()) {
								throw new NegocioException("Existe um conflito de horário com outra movimentação deste veículo: " + Util.formatarData(mov.getDataMovimento()) + " das " + Util.formatarHora(hrSaida) + " às " + Util.formatarHora(hrRetorno) + ".");
							}
						}
					}
				}

				// Seta os dados da movimentação do veículo
				movimento.setDataMovimento(prg.getData());
				movimento.setDataRetorno(prg.getData());
				movimento.setDestino(projetoService.montarDescricaoEnderecoObra(prg.getProjeto().getEnderecoObra()));
				movimento.setDiaSemana(Util.diaDaSemana(movimento.getDataMovimento()));
				movimento.setHoraSaida(prg.getSaidaEmpresa());
				movimento.setHoraRetorno(prg.getRetornoEmpresa());
				movimento.setPeriodo(Util.periodoDia(movimento.getHoraSaida()));
				movimento.setColaborador(prg.getColaborador());
				movimento.setProjeto(prg.getProjeto());
				movimento.setVeiculo(prg.getVeiculo());
				movimento.setProgramacaoExecucao(prg);
			
				// Grava a movimentação do veículo, vinculada a esta programação de execução
				veiculoRepository.guardarMovimento(movimento);
			}
		}		
		return programacao;
	}

}
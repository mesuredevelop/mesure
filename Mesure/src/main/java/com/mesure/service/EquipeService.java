package com.mesure.service;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Colaborador;
import com.mesure.model.Equipe;
import com.mesure.repository.EquipeRepository;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class EquipeService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EquipeRepository equipeRepository;

    @Transactional
    public Equipe salvar(Equipe equipe) {

        if (equipe.getColaboradores().isEmpty()) {
            throw new NegocioException("É necessário que a equipe contenha ao menos 1 (um) colaborador.");
        }

        return equipeRepository.guardar(equipe);
    }

    public List<Colaborador> colaboradoresDaEquipe(Equipe equipe, ESimNao incluirResponsavel) {

        List<Colaborador> retorno = new ArrayList<>();

        if (incluirResponsavel.equals(ESimNao.SIM)) {
            retorno.add(equipe.getColaborador());
        }

        // Busca a equipe com os colaboradores vinculados a ela
        Equipe e = equipeRepository.equipeComParticipantes(equipe);

        if (e != null & e.getColaboradores() != null) {
            for (Colaborador c : e.getColaboradores()) {
                if (!retorno.contains(c)) {
                    retorno.add(c);
                }
            }
        }
        return retorno;
    }

    public List<Colaborador> colaboradoresDoResponsavel(Colaborador responsavel, ESimNao incluirResponsavel) {

        List<Colaborador> retorno = new ArrayList<>();

        if (incluirResponsavel.equals(ESimNao.SIM)) {
            retorno.add(responsavel);
        }

        // Busca todas as equipes no qual o colaborador é responsável
        List<Equipe> equipes = equipeRepository.equipesReponsavelComParticipantes(responsavel);

        if (equipes != null) {
            for (Equipe e : equipes) {
                for (Colaborador c : e.getColaboradores()) {
                    if (!retorno.contains(c)) {
                        retorno.add(c);
                    }
                }
            }
        }
        return retorno;
    }
}

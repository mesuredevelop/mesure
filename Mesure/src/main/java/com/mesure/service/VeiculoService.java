package com.mesure.service;

import com.mesure.model.Veiculo;
import com.mesure.repository.VeiculoRepository;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;

public class VeiculoService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private VeiculoRepository veiculoRepository;

    @Transactional
    public Veiculo salvar(Veiculo veiculo) {
        if (veiculo.getPlaca().length() != 8) {
            throw new NegocioException("Placa inválida, informe no padrão correto (ex: ABC-9999).");
        }

        Veiculo veiculoExistente = null;

        veiculoExistente = veiculoRepository.porPlaca(veiculo.getPlaca());

        if (veiculoExistente != null && !veiculoExistente.equals(veiculo)) {
            throw new NegocioException("Já existe um veículo com a placa informada.");
        }

        if (StringUtils.isNotBlank(veiculo.getRenavan())) {
            veiculoExistente = veiculoRepository.porRenavan(veiculo.getRenavan());

            if (veiculoExistente != null && !veiculoExistente.equals(veiculo)) {
                throw new NegocioException("Já existe um veículo com o renavan informado.");
            }
        }

        veiculo.setPlaca(veiculo.getPlaca().toUpperCase());

        return veiculoRepository.guardar(veiculo);
    }
}

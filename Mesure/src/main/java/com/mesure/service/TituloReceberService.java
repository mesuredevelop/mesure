package com.mesure.service;

import com.mesure.enumerator.EDebitoCredito;
import com.mesure.enumerator.EStatusTitulo;
import com.mesure.model.MovimentoFinanceiro;
import com.mesure.model.TituloReceber;
import com.mesure.model.TituloReceberBaixa;
import com.mesure.repository.MovimentoFinanceiroRepository;
import com.mesure.repository.TituloReceberRepository;
import com.mesure.util.jpa.Transactional;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;

public class TituloReceberService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    TituloReceberRepository tituReceberRepository;

    @Inject
    MovimentoFinanceiroRepository movimentoFinanceiroRepository;

    @Transactional
    public TituloReceber salvar(TituloReceber tituloReceber) {
        if (tituloReceber.isNovo()) {
            tituloReceber.setSaldo(tituloReceber.getValor());
            tituloReceber.setTotalPago(BigDecimal.ZERO);
        } else {
            tituloReceber.setSaldo(calcularSaldoTituloReceber(tituloReceber));

            if (tituloReceber.getSaldo().doubleValue() < 0D) {
                throw new NegocioException("O saldo do título não pode ser negativo. Valor do título: " + tituloReceber.getValor() + " Total de baixas: " + tituloReceber.getSaldo());
            }

            tituloReceber.setTotalPago(tituReceberRepository.totalValorLiquidoBaixasTituloReceber(tituloReceber));
        }

        if (tituloReceber.getSaldo().doubleValue() == 0D) {
            tituloReceber.setStatus(EStatusTitulo.LIQUIDADO);
        } else {
            tituloReceber.setStatus(EStatusTitulo.ABERTO);
        }

        consistirTituloReceber(tituloReceber);
        gerarMovimentoFinanceiro(tituloReceber);

        return tituReceberRepository.guardar(tituloReceber);
    }

    public void gerarMovimentoFinanceiro(TituloReceber titulo) {
        try {
            for (TituloReceberBaixa baixa : titulo.getBaixas()) {
                // apenas gera movimento financeiro para baixas que ainda não geraram
                if (baixa.getMovimentoFinanceiro() == null) {
                    MovimentoFinanceiro movimento = new MovimentoFinanceiro();
                    movimento.setContaBancaria(baixa.getContaBancaria());
                    movimento.setData(baixa.getDataMovimento());
                    movimento.setTipoMovimento(EDebitoCredito.CREDITO);
                    movimento.setValor(baixa.getValorLiquido());
                    if (!StringUtils.isEmpty(titulo.getDocumento())) {
                        movimento.setDocumento(titulo.getDocumento());
                    } else {
                        movimento.setDocumento(String.valueOf(baixa.getTitulo().getId()));
                    }
                    movimento = movimentoFinanceiroRepository.guardar(movimento);

                    baixa.setMovimentoFinanceiro(movimento);
                }
            }
        } catch (Exception e) {
            throw new NegocioException("Não foi possível gerar o movimento financeiro. Motivo: " + e.getMessage());
        }
    }

    public void consistirTituloReceber(TituloReceber tituloReceber) throws NegocioException {
        if (tituloReceber.getDataVencimento().before(tituloReceber.getDataEmissao())) {
            throw new NegocioException("A data de vencimento não pode ser menor que a data de emissão.");
        }

        if (tituloReceber.getValor().doubleValue() <= 0D) {
            throw new NegocioException("O valor do título deve ter maior que zero.");
        }
    }

    public void consistirBaixaTituloReceber(TituloReceberBaixa baixa) throws ParseException {
        if (baixa.getValorBaixa() == null) {
            throw new NegocioException("É necessário informar o valor da baixa");
        }
    }

    public BigDecimal calcularSaldoTituloReceber(TituloReceber tituloReceber) {
        return tituloReceber.getValor().subtract(tituReceberRepository.totalValorBaixasTituloReceber(tituloReceber));
    }

    public BigDecimal getValorLiquidoTituloReceberBaixa(TituloReceberBaixa baixa) {
        BigDecimal valorLiquido = baixa.getValorBaixa().subtract(baixa.getValorDesconto()).add(baixa.getValorAcrescimo()).add(baixa.getValorJuros()).add(baixa.getValorMulta());

        if (valorLiquido.doubleValue() < 0D) {
            return BigDecimal.ZERO;
        } else {
            return valorLiquido;
        }
    }

    @Transactional
    public TituloReceberBaixa estornarBaixaTitulo(TituloReceberBaixa baixa) {
        TituloReceberBaixa baixaEstorno = new TituloReceberBaixa();

        baixaEstorno.setContaBancaria(baixa.getContaBancaria());
        baixaEstorno.setDataBaixa(baixa.getDataBaixa());
        baixaEstorno.setDataMovimento(baixa.getDataMovimento());
        baixaEstorno.setFlag(false);
        baixaEstorno.setTitulo(baixa.getTitulo());
        baixaEstorno.setValorAcrescimo(new BigDecimal(baixa.getValorAcrescimo().doubleValue() * -1));
        baixaEstorno.setValorBaixa(new BigDecimal(baixa.getValorBaixa().doubleValue() * -1));
        baixaEstorno.setValorDesconto(new BigDecimal(baixa.getValorDesconto().doubleValue() * -1));
        baixaEstorno.setValorJuros(new BigDecimal(baixa.getValorJuros().doubleValue() * -1));
        baixaEstorno.setValorLiquido(new BigDecimal(baixa.getValorLiquido().doubleValue() * -1));
        baixaEstorno.setValorMulta(new BigDecimal(baixa.getValorMulta().doubleValue() * -1));

        return tituReceberRepository.guardarTituloReceberBaixa(baixaEstorno);
    }

}

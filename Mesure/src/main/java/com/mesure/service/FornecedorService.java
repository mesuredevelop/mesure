package com.mesure.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.mesure.model.Fornecedor;
import com.mesure.repository.FornecedorRepository;
import com.mesure.util.jpa.Transactional;

public class FornecedorService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private FornecedorRepository fornecedorRepository;
	
	@Transactional
	public Fornecedor salvar(Fornecedor fornecedor) {
		return fornecedorRepository.guardar(fornecedor);
	}		

}


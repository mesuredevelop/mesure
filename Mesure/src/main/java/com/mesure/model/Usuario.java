package com.mesure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.ESimNao;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private String email;
	private String senha;
	private ESimNao ativo;
	private List<Grupo> grupos = new ArrayList<>();
	private Colaborador colaborador;
	private Long tenantId;

	@Id
	@TableGenerator (
		name="usuario_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="usuario",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "usuario_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	@Size(max = 60)
	@Column(nullable = false, length = 60)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@NotBlank
	@Size(max = 255)
	@Column(nullable = false, unique = true, length = 255)
	@Email
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	// @NotBlank
	// @Size(max = 30)
	@Column(nullable = false, length = 100)
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 3)
	public ESimNao getAtivo() {
		return ativo;
	}

	public void setAtivo(ESimNao ativo) {
		this.ativo = ativo;
	}

	@SuppressWarnings("deprecation")
	@ManyToMany
	@JoinTable(name = "usuario_grupo", 
		joinColumns = { @JoinColumn(name = "usuario_id") }, 
		inverseJoinColumns = { @JoinColumn(name = "grupo_id") })
	@org.hibernate.annotations.ForeignKey(name = "FK_usuario_grupo_usuario", inverseName = "FK_usuario_grupo_grupo")
	public List<Grupo> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}

	@OneToOne(mappedBy = "usuario")
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@Transient
	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}	

	@Transient
	public boolean isNovo() {
		return getId() == null;
	}

	@Transient
	public boolean isEditando() {
		return getId() != null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}

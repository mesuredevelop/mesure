package com.mesure.model;

import com.mesure.enumerator.ETipoContaBancaria;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "conta_bancaria")
public class ContaBancaria implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Filial filial;
    private String descricao;
    private ETipoContaBancaria tipo;
    private String bacen;
    private String banco;
    private String titular;
    private String agencia;
    private String conta;
    private String cedente;

    @Id
    @TableGenerator(
            name = "conta_bancaria_generator",
            table = "generator_id",
            pkColumnName = "GEN_KEY",
            valueColumnName = "GEN_VALUE",
            pkColumnValue = "conta_bancaria",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "conta_bancaria_generator")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    @ManyToOne
    @JoinColumn(name = "filial_id", foreignKey = @ForeignKey(name = "FK_conta_bancaria_filial"), nullable = false)
    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }

    @NotBlank
    @Size(max = 60)
    @Column(nullable = false, length = 60)
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 10)
    public ETipoContaBancaria getTipo() {
        return tipo;
    }

    public void setTipo(ETipoContaBancaria tipo) {
        this.tipo = tipo;
    }

    @NotBlank
    @Size(max = 5)
    @Column(nullable = false, length = 5)
    public String getBacen() {
        return bacen;
    }

    public void setBacen(String bacen) {
        this.bacen = bacen;
    }

    @NotBlank
    @Size(max = 60)
    @Column(nullable = false, length = 60)
    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    @NotBlank
    @Size(max = 60)
    @Column(nullable = false, length = 60)
    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    @NotNull
    @Column(nullable = false, length = 10)
    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    @NotNull
    @Column(nullable = false, length = 10)
    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    @Column(nullable = false, length = 20)
    public String getCedente() {
        return cedente;
    }

    public void setCedente(String cedente) {
        this.cedente = cedente;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContaBancaria other = (ContaBancaria) obj;
        return Objects.equals(this.id, other.id);
    }



}

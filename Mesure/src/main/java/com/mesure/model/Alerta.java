package com.mesure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.mesure.enumerator.EFormaAlerta;
import com.mesure.enumerator.EPeriodicidadeAlerta;
import com.mesure.enumerator.ETipoAlerta;

@Entity
@Table(name = "alerta")
public class Alerta implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private EFormaAlerta forma;
	private ETipoAlerta tipo;
	private EPeriodicidadeAlerta periodicidade;
	private Date inicio;
	private Date termino;
	private Filial filial;
	private Integer diasExecucao;
	private List<AlertaEmail> emails = new ArrayList<>();

	@Id
	@TableGenerator (
		name="alerta_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="alerta",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "alerta_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 10)	
	public EFormaAlerta getForma() {
		return forma;
	}

	public void setForma(EFormaAlerta forma) {
		this.forma = forma;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 15)	
	public ETipoAlerta getTipo() {
		return tipo;
	}

	public void setTipo(ETipoAlerta tipo) {
		this.tipo = tipo;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 10)	
	public EPeriodicidadeAlerta getPeriodicidade() {
		return periodicidade;
	}

	public void setPeriodicidade(EPeriodicidadeAlerta periodicidade) {
		this.periodicidade = periodicidade;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio", nullable = false)	
	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_termino")	
	public Date getTermino() {
		return termino;
	}

	public void setTermino(Date termino) {
		this.termino = termino;
	}
	
	@Column(name = "dias_execucao")
	public Integer getDiasExecucao() {
		return diasExecucao;
	}

	public void setDiasExecucao(Integer diasExecucao) {
		this.diasExecucao = diasExecucao;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "filial_id", foreignKey = @ForeignKey(name = "FK_alerta_filial"), nullable = false)	
	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}
	
	@OneToMany(mappedBy = "alerta", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<AlertaEmail> getEmails() {
		return emails;
	}

	public void setEmails(List<AlertaEmail> emails) {
		this.emails = emails;
	}

	@Transient
	public boolean isNovo() {
		return getId() == null;
	}

	@Transient
	public boolean isEditando() {
		return getId() != null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alerta other = (Alerta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}

}

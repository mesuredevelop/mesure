package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.mesure.enumerator.EDiaSemana;
import com.mesure.enumerator.ETipoRegistroPonto;
import com.mesure.util.Util;

@Entity
@Table(name = "colaborador_ponto")
public class ColaboradorPonto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Date data;
	private EDiaSemana diaSemana;
	private Date horaEntrada;
	private Date horaSaidaAlmoco;
	private Date horaRetornoAlmoco;
	private Date horaSaida;
	private Date horaAlmoco;
	private Date horaEntradaExtra;
	private Date horaSaidaExtra;
	private BigDecimal saldoHoras;
	private BigDecimal totalHoras;
	private BigDecimal totalHorasAlmoco;
	private BigDecimal totalHorasProjeto;
	private BigDecimal totalHorasAtividadeExtra;
	private BigDecimal totalHorasExtra;
	private BigDecimal totalHorasFalta;
	private ETipoRegistroPonto tipoRegistro;
	private Colaborador colaborador;
	
	@Id
	@TableGenerator (
		name="colaborador_ponto_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="colaborador_ponto",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "colaborador_ponto_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data", nullable = false)	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "dia_semana", length = 15)	
	public EDiaSemana getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(EDiaSemana diaSemana) {
		this.diaSemana = diaSemana;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_entrada")	
	public Date getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(Date horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_saida_almoco")
	public Date getHoraSaidaAlmoco() {
		return horaSaidaAlmoco;
	}

	public void setHoraSaidaAlmoco(Date horaSaidaAlmoco) {
		this.horaSaidaAlmoco = horaSaidaAlmoco;
	}	
	
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_retorno_almoco")
	public Date getHoraRetornoAlmoco() {
		return horaRetornoAlmoco;
	}

	public void setHoraRetornoAlmoco(Date horaRetornoAlmoco) {
		this.horaRetornoAlmoco = horaRetornoAlmoco;
	}		
	
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_saida")
	public Date getHoraSaida() {
		return horaSaida;
	}

	public void setHoraSaida(Date horaSaida) {
		this.horaSaida = horaSaida;
	}
	
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_almoco")
	public Date getHoraAlmoco() {
		return horaAlmoco;
	}

	public void setHoraAlmoco(Date horaAlmoco) {
		this.horaAlmoco = horaAlmoco;
	}
	
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_entrada_extra")
	public Date getHoraEntradaExtra() {
		return horaEntradaExtra;
	}

	public void setHoraEntradaExtra(Date horaEntradaExtra) {
		this.horaEntradaExtra = horaEntradaExtra;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_saida_extra")
	public Date getHoraSaidaExtra() {
		return horaSaidaExtra;
	}

	public void setHoraSaidaExtra(Date horaSaidaExtra) {
		this.horaSaidaExtra = horaSaidaExtra;
	}

	@Column(name = "saldo_horas", precision = 18, scale = 14)
	public BigDecimal getSaldoHoras() {
		return saldoHoras;
	}

	public void setSaldoHoras(BigDecimal saldoHoras) {
		this.saldoHoras = saldoHoras;
	}	
	
	@Column(name = "total_horas", precision = 18, scale = 14)
	public BigDecimal getTotalHoras() {
		return totalHoras;
	}

	public void setTotalHoras(BigDecimal totalHoras) {
		this.totalHoras = totalHoras;
	}	
	
	@Column(name = "total_almoco", precision = 18, scale = 14)
	public BigDecimal getTotalHorasAlmoco() {
		return totalHorasAlmoco;
	}

	public void setTotalHorasAlmoco(BigDecimal totalHorasAlmoco) {
		this.totalHorasAlmoco = totalHorasAlmoco;
	}

	@Column(name = "total_horas_projeto", precision = 18, scale = 14)
	public BigDecimal getTotalHorasProjeto() {
		return totalHorasProjeto;
	}

	public void setTotalHorasProjeto(BigDecimal totalHorasProjetos) {
		this.totalHorasProjeto = totalHorasProjetos;
	}

	@Column(name = "total_horas_atividade_extra", precision = 18, scale = 14)
	public BigDecimal getTotalHorasAtividadeExtra() {
		return totalHorasAtividadeExtra;
	}

	public void setTotalHorasAtividadeExtra(BigDecimal totalHorasAtividadeExtra) {
		this.totalHorasAtividadeExtra = totalHorasAtividadeExtra;
	}

	@Column(name = "total_horas_extra", precision = 18, scale = 14)
	public BigDecimal getTotalHorasExtra() {
		return totalHorasExtra;
	}

	public void setTotalHorasExtra(BigDecimal totalHorasExtra) {
		this.totalHorasExtra = totalHorasExtra;
	}

	@Column(name = "total_horas_falta", precision = 18, scale = 14)
	public BigDecimal getTotalHorasFalta() {
		return totalHorasFalta;
	}

	public void setTotalHorasFalta(BigDecimal totalHorasFalta) {
		this.totalHorasFalta = totalHorasFalta;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_registro", length = 10, nullable = false)	
	public ETipoRegistroPonto getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(ETipoRegistroPonto tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_colaborador_ponto_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@Transient
	public void calcularTotais() throws ParseException {
		
		// Dia da semana
		setDiaSemana(Util.diaDaSemanaEnum(getData()));
		
		// Inicializa os atributos a serem calculados		
		setTotalHoras(new BigDecimal(0));
		setSaldoHoras(new BigDecimal(0));
		setTotalHorasExtra(new BigDecimal(0));
		setTotalHorasFalta(new BigDecimal(0));
		setTotalHorasAlmoco(new  BigDecimal(0));
		setHoraAlmoco(null);
		
		// Calcula o tempo de almoço
		if (getHoraSaidaAlmoco() != null && getHoraRetornoAlmoco() != null) {
			// Total no formato centesimal (exemplo: 1,50)
			setTotalHorasAlmoco(new BigDecimal(Util.converteHoraParaCentesimal(getHoraRetornoAlmoco().getTime() - getHoraSaidaAlmoco().getTime())));

			// Total no formato de horas (exemplo: 1:30)			
			if (getTotalHorasAlmoco() != null) {				
				setHoraAlmoco(Util.DiferencaEmHoras2(getHoraSaidaAlmoco(), getHoraRetornoAlmoco()));	
			}
		}
		
		long dif = 0;
		
		// Horas extras informado no cadastro do ponto
		if (getHoraEntradaExtra() != null && getHoraSaidaExtra() != null) {
			dif = dif + (getHoraSaidaExtra().getTime() - getHoraEntradaExtra().getTime());
		}		
		
		// Total horas do dia (horas do dia - almoço)
		if (getHoraSaida() != null) {
			dif = dif + (getHoraSaida().getTime() - getHoraEntrada().getTime());

			if (getHoraAlmoco() != null) {
				dif = (long) (dif - Util.retornaHoraMSExata(getHoraAlmoco()));
			}
			
			if (dif < 0) {
				dif = 0;
			}
		}
		
		if (dif > 0) {
			setTotalHoras(new BigDecimal(Util.converteHoraParaCentesimal(dif)));	
		}		

		// Total hora extra
		// Total hora falta 
		if (getTotalHoras() != null && getTotalHoras().floatValue() > 0) {
			
			BigDecimal horasTrabalhadas = new BigDecimal(0);
			
			// Soma as execuções em projetos + atividades extras
			horasTrabalhadas = horasTrabalhadas.add(getTotalHorasProjeto());
			horasTrabalhadas = horasTrabalhadas.add(getTotalHorasAtividadeExtra());
			
			BigDecimal dif2 = new BigDecimal(getTotalHoras().floatValue());
			dif2 = dif2.subtract(horasTrabalhadas);
			
			if (dif2.floatValue() != 0) {
				if (dif2.floatValue() > 0) {
					// Total hora falta
					setTotalHorasFalta(dif2);
				} else {					
					dif2 = new BigDecimal(Math.abs(dif2.floatValue())); // converte para positivo
					// Total hora extra
					setTotalHorasExtra(dif2);
				}				
			}
			
			// Saldo horas
			if (getColaborador().getHorasTrabalhoDiario() != null && getColaborador().getHorasTrabalhoDiario().floatValue() > 0) {
				BigDecimal totalHoras = new BigDecimal(0);
				
				// Calcula saldo horas
				totalHoras = totalHoras.add(getTotalHoras());
				totalHoras = totalHoras.subtract(getColaborador().getHorasTrabalhoDiario());
				
				setSaldoHoras(totalHoras);	
			}
		}
	}
	
	@Transient
	public boolean isNovo() {
		return getId() == null;
	}
	
	@Transient
	public boolean isEditando() {
		return getId() != null;
	}	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColaboradorPonto other = (ColaboradorPonto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}	
	
}

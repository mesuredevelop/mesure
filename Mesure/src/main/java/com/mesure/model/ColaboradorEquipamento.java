package com.mesure.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "colaborador_equipamento")
public class ColaboradorEquipamento implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;	
	private String tamanho;
	private Date dataEntrega;
	private Date dataDevolucao;
	private Date dataValidade;
	private EquipamentoProtecao equipamento;
	private Colaborador colaborador;

	@Id
	@TableGenerator (
		name="colaborador_equipamento_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="colaborador_equipamento",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "colaborador_equipamento_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Size(max = 60)
	@Column(length = 60)
	public String getTamanho() {
		return tamanho;
	}

	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data_entrega", nullable = false)	
	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_devolucao")	
	public Date getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(Date dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_validade")
	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "equipamento_id", foreignKey = @ForeignKey(name = "FK_colaborador_equipamento_equipamento"), nullable = false)
	public EquipamentoProtecao getEquipamento() {
		return equipamento;
	}

	public void setEquipamento(EquipamentoProtecao equipamento) {
		this.equipamento = equipamento;
	}	
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_colaborador_equipamento_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColaboradorEquipamento other = (ColaboradorEquipamento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
	

}

package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "etapa")
public class Etapa implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descricao;
	private BigDecimal quantidadeHoras;
	private Atividade atividade;
	private List<DispositivoSeguranca> dispositivos = new ArrayList<>();
	private List<EquipamentoProtecao> equipamentos = new ArrayList<>();	

	@Id
	@TableGenerator (
		name="etapa_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="etapa",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "etapa_generator")	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	@Size(max = 255)
	@Column(nullable = false, length = 255)		
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@NotNull
	@Column(name = "qtde_horas", precision = 10, scale = 2, nullable = false)
	public BigDecimal getQuantidadeHoras() {
		return quantidadeHoras;
	}

	public void setQuantidadeHoras(BigDecimal quantidadeHoras) {
		this.quantidadeHoras = quantidadeHoras;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "atividade_id", foreignKey = @ForeignKey(name = "FK_etapa_atividade"), nullable = false)
	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
	
	@SuppressWarnings("deprecation")
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "etapa_equipamento"
		,joinColumns = {@JoinColumn(name = "etapa_id")}
		,inverseJoinColumns = {@JoinColumn(name = "equipamento_id")})
    @org.hibernate.annotations.ForeignKey(name = "FK_etapa_equipamento_etapa"
    	,inverseName = "FK_etapa_equipamento_equipamento")	
	public List<EquipamentoProtecao> getEquipamentos() {
		return equipamentos;
	}	
	
	public void setEquipamentos(List<EquipamentoProtecao> equipamentos) {
		this.equipamentos = equipamentos;
	}	
	
	@SuppressWarnings("deprecation")
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "etapa_dispositivo"
		,joinColumns = {@JoinColumn(name = "etapa_id")}
		,inverseJoinColumns = {@JoinColumn(name = "dispositivo_id")})
    @org.hibernate.annotations.ForeignKey(name = "FK_etapa_dispositivo_etapa"
    	,inverseName = "FK_etapa_dispositivo_dispositivo")	
	public List<DispositivoSeguranca> getDispositivos() {
		return dispositivos;
	}	
	
	public void setDispositivos(List<DispositivoSeguranca> dispositivos) {
		this.dispositivos = dispositivos;
	}	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Etapa other = (Etapa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

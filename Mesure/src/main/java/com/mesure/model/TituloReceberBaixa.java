package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "titulo_receber_baixa")
public class TituloReceberBaixa implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private TituloReceber titulo;
    private BigDecimal valorBaixa;
    private BigDecimal valorLiquido;
    private BigDecimal valorAcrescimo;
    private BigDecimal valorJuros;
    private BigDecimal valorMulta;
    private BigDecimal valorDesconto;
    private Date dataBaixa;
    private Date dataMovimento;
    private ContaBancaria contaBancaria;
    private MovimentoFinanceiro movimentoFinanceiro;
    private boolean flag = false;

    @Id
    @TableGenerator(
            name = "titulo_receber_baixa_generator",
            table = "generator_id",
            pkColumnName = "GEN_KEY",
            valueColumnName = "GEN_VALUE",
            pkColumnValue = "titulo_receber_baixa",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "titulo_receber_baixa_generator")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    @ManyToOne
    @JoinColumn(name = "titulo_id", foreignKey = @ForeignKey(name = "FK_titulo_receber_baixa_titulo"), nullable = false)
    public TituloReceber getTitulo() {
        return titulo;
    }

    public void setTitulo(TituloReceber titulo) {
        this.titulo = titulo;
    }

    @Column(name = "valor_baixa", precision = 10, scale = 2, nullable = false)
    public BigDecimal getValorBaixa() {
        return valorBaixa;
    }

    public void setValorBaixa(BigDecimal valorBaixa) {
        this.valorBaixa = valorBaixa;
    }

    @Column(name = "valor_liquido", precision = 10, scale = 2, nullable = false)
    public BigDecimal getValorLiquido() {
        return valorLiquido;
    }

    public void setValorLiquido(BigDecimal valorLiquido) {
        this.valorLiquido = valorLiquido;
    }

    @Column(name = "valor_acrescimo", precision = 10, scale = 2, nullable = true)
    public BigDecimal getValorAcrescimo() {
        return valorAcrescimo;
    }

    public void setValorAcrescimo(BigDecimal valorAcrescimo) {
        this.valorAcrescimo = valorAcrescimo;
    }

    @Column(name = "valor_juros", precision = 10, scale = 2, nullable = true)
    public BigDecimal getValorJuros() {
        return valorJuros;
    }

    public void setValorJuros(BigDecimal valorJuros) {
        this.valorJuros = valorJuros;
    }

    @Column(name = "valor_multa", precision = 10, scale = 2, nullable = true)
    public BigDecimal getValorMulta() {
        return valorMulta;
    }

    public void setValorMulta(BigDecimal valorMulta) {
        this.valorMulta = valorMulta;
    }

    @Column(name = "valor_desconto", precision = 10, scale = 2, nullable = true)
    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "data_baixa", nullable = false)
    public Date getDataBaixa() {
        return dataBaixa;
    }

    public void setDataBaixa(Date dataBaixa) {
        this.dataBaixa = dataBaixa;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "data_movimento", nullable = true)
    public Date getDataMovimento() {
        return dataMovimento;
    }

    public void setDataMovimento(Date dataMovimento) {
        this.dataMovimento = dataMovimento;
    }

    @NotNull
    @ManyToOne
    @JoinColumn(name = "conta_bancaria_id", foreignKey = @ForeignKey(name = "FK_titulo_receber_baixa_conta_bancaria"), nullable = false)
    public ContaBancaria getContaBancaria() {
        return contaBancaria;
    }

    public void setContaBancaria(ContaBancaria contaBancaria) {
        this.contaBancaria = contaBancaria;
    }

    @NotNull
    @OneToOne
    @JoinColumn(name = "movimento_financeiro_id", foreignKey = @ForeignKey(name = "FK_titulo_receber_baixa_movimento_financeiro"), nullable = true)
    public MovimentoFinanceiro getMovimentoFinanceiro() {
        return movimentoFinanceiro;
    }

    public void setMovimentoFinanceiro(MovimentoFinanceiro movimentoFinanceiro) {
        this.movimentoFinanceiro = movimentoFinanceiro;
    }

    @Transient
    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TituloReceberBaixa other = (TituloReceberBaixa) obj;
        return Objects.equals(this.id, other.id);
    }

}

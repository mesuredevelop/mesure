package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "programacao_execucao_colaborador")
public class ProgramacaoExecucaoColaborador implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private BigDecimal horasViagem;
	private Colaborador colaborador;
	private ProgramacaoExecucao programacaoExecucao;

	public ProgramacaoExecucaoColaborador() {
		
	}
	
	public ProgramacaoExecucaoColaborador(ProgramacaoExecucao programacaoExecucao, Colaborador colaborador, BigDecimal horasViagem) {
		this.programacaoExecucao = programacaoExecucao;
		this.colaborador = colaborador;
		this.horasViagem = horasViagem;
	}

	@Id
	@TableGenerator (
		name="programacao_execucao_colaborador_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="programacao_execucao_colaborador",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "programacao_execucao_colaborador_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="horas_viagem", precision = 5, scale = 2)
	public BigDecimal getHorasViagem() {
		return horasViagem;
	}

	public void setHorasViagem(BigDecimal horasViagem) {
		this.horasViagem = horasViagem;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_programacao_execucao_colaborador_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "programacao_execucao_id", foreignKey = @ForeignKey(name = "FK_programacao_execucao_colaborador_programacao_execucao"), nullable = false)
	public ProgramacaoExecucao getProgramacaoExecucao() {
		return programacaoExecucao;
	}

	public void setProgramacaoExecucao(ProgramacaoExecucao programacaoExecucao) {
		this.programacaoExecucao = programacaoExecucao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProgramacaoExecucaoColaborador other = (ProgramacaoExecucaoColaborador) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
	

}

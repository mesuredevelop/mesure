package com.mesure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.ESituacaoDetran;

@Entity
@Table(name = "veiculo")
public class Veiculo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String codigoInterno;
	private String placa;
	private VeiculoModelo modelo;
	private Integer ano;
	private Integer maximoPassageiros;
	private String renavan;
	private ESituacaoDetran situacaoDetran;
	private String observacao;
	private List<VeiculoMovimento> movimentacoes = new ArrayList<>();
	private List<VeiculoManutencao> manutencoes = new ArrayList<>();
	private List<VeiculoMulta> multas = new ArrayList<>();
	
	@Id
	@TableGenerator (
		name="veiculo_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="veiculo",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "veiculo_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Size(max = 10)
	@Column(name="codigo_interno", length = 10)
	public String getCodigoInterno() {
		return codigoInterno;
	}

	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}

	@NotBlank
	@Size(max = 8)
	@Column(nullable = false, length = 8, unique = true)
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "modelo_id", foreignKey = @ForeignKey(name = "FK_veiculo_veiculo_modelo"), nullable = false)
	public VeiculoModelo getModelo() {
		return modelo;
	}

	public void setModelo(VeiculoModelo modelo) {
		this.modelo = modelo;
	}

	@NotNull @Min(value=1900, message="tem um valor muito baixo")
	@Column(name = "ano", nullable = false)	
	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}
	
	@NotNull
	@Column(name = "maximo_passageiros", nullable = false)	
	public Integer getMaximoPassageiros() {
		return maximoPassageiros;
	}

	public void setMaximoPassageiros(Integer maximoPassageiros) {
		this.maximoPassageiros = maximoPassageiros;
	}	

	@Size(max = 25)
	@Column(length = 25, unique = true)	
	public String getRenavan() {
		return renavan;
	}

	public void setRenavan(String renavan) {
		this.renavan = renavan;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "situacao_detran", nullable = false, length = 25)	
	public ESituacaoDetran getSituacaoDetran() {
		return situacaoDetran;
	}
	
	public void setSituacaoDetran(ESituacaoDetran situacaoDetran) {
		this.situacaoDetran = situacaoDetran;
	}

	@Column(columnDefinition = "text")
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@OneToMany(mappedBy = "veiculo", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<VeiculoMovimento> getMovimentacoes() {
		return movimentacoes;
	}

	public void setMovimentacoes(List<VeiculoMovimento> movimentacoes) {
		this.movimentacoes = movimentacoes;
	}
	
	@OneToMany(mappedBy = "veiculo", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<VeiculoManutencao> getManutencoes() {
		return manutencoes;
	}

	public void setManutencoes(List<VeiculoManutencao> manutencoes) {
		this.manutencoes = manutencoes;
	}
		
	@OneToMany(mappedBy = "veiculo", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<VeiculoMulta> getMultas() {
		return multas;
	}

	public void setMultas(List<VeiculoMulta> multas) {
		this.multas = multas;
	}
	
	@Transient
	public boolean isEditando() {
		return getId() != null;
	}	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Veiculo other = (Veiculo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}


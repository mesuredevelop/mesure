package com.mesure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.EStatusProjeto;

@Entity
@Table(name = "projeto")
public class Projeto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;	
	private String titulo;
	private String descricao;
	private Date dataCriacao;
	private Date dataInicio;
	private Date dataEncerramento;
	private String commerce;
	private Integer prioridade;
	private EStatusProjeto status;
	private EnderecoCliente enderecoObra;
	private TipoProjeto tipo;
	private Cliente cliente;
	private Colaborador responsavel;
	private Filial filial;
	private List<ProjetoProposta> propostas = new ArrayList<>();
	private List<ProjetoHistorico> historicos = new ArrayList<>();
	private List<ProjetoEtapa> etapas = new ArrayList<>();
	private List<ProjetoColaborador> participantes = new ArrayList<>();
	private List<ProjetoExecucao> execucoes = new ArrayList<>();
	private List<ProjetoAtividadePrevista> atividadesPrevistas = new ArrayList<>();
	private List<ProjetoProrrogacao> prorrogacoes = new ArrayList<>();
	private List<Atividade> atividades = new ArrayList<>();
	
	@Id
	@TableGenerator (
		name="projeto_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="projeto",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "projeto_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@NotBlank
	@Size(max = 40)
	@Column(length = 40, nullable = false)
	public String getTitulo() {
		return titulo;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@Column(columnDefinition = "text")
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_criacao", nullable = false)	
	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio")
	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_encerramento")
	public Date getDataEncerramento() {
		return dataEncerramento;
	}

	public void setDataEncerramento(Date dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}

	@Size(max = 50)
	@Column(length = 50)	
	public String getCommerce() {
		return commerce;
	}

	public void setCommerce(String commerce) {
		this.commerce = commerce;
	}
	
	@Column	
	public Integer getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(Integer prioridade) {
		this.prioridade = prioridade;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(length = 15, nullable = false)	
	public EStatusProjeto getStatus() {
		return status;
	}

	public void setStatus(EStatusProjeto status) {
		this.status = status;
	}
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "endereco_id", foreignKey = @ForeignKey(name = "FK_projeto_endereco_cliente"), nullable = false)
	public EnderecoCliente getEnderecoObra() {
		return enderecoObra;
	}

	public void setEnderecoObra(EnderecoCliente enderecoObra) {
		this.enderecoObra = enderecoObra;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "tipo_projeto_id", foreignKey = @ForeignKey(name = "FK_projeto_tipo_projeto"), nullable = false)
	public TipoProjeto getTipo() {
		return tipo;
	}

	public void setTipo(TipoProjeto tipo) {
		this.tipo = tipo;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "cliente_id", foreignKey = @ForeignKey(name = "FK_projeto_cliente"), nullable = false)
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_projeto_colaborador"), nullable = false)
	public Colaborador getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "filial_id", foreignKey = @ForeignKey(name = "FK_projeto_filial"), nullable = false)	
	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	@OneToMany(mappedBy = "projeto", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ProjetoProposta> getPropostas() {
		return propostas;
	}

	public void setPropostas(List<ProjetoProposta> propostas) {
		this.propostas = propostas;
	}
	
	@OneToMany(mappedBy = "projeto", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ProjetoHistorico> getHistoricos() {
		return historicos;
	}

	public void setHistoricos(List<ProjetoHistorico> historicos) {
		this.historicos = historicos;
	}
		
	@OneToMany(mappedBy = "projeto", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ProjetoEtapa> getEtapas() {
		return etapas;
	}

	public void setEtapas(List<ProjetoEtapa> etapas) {
		this.etapas = etapas;
	}
	
	@OneToMany(mappedBy = "projeto", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ProjetoColaborador> getParticipantes() {
		return participantes;
	}

	public void setParticipantes(List<ProjetoColaborador> participantes) {
		this.participantes = participantes;
	}
	
	@OneToMany(mappedBy = "projeto", cascade = CascadeType.ALL, orphanRemoval = true)	
	public List<ProjetoExecucao> getExecucoes() {
		return execucoes;
	}

	public void setExecucoes(List<ProjetoExecucao> execucoes) {
		this.execucoes = execucoes;
	}
	
	@OneToMany(mappedBy = "projeto", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ProjetoAtividadePrevista> getAtividadesPrevistas() {
		return atividadesPrevistas;
	}

	public void setAtividadesPrevistas(List<ProjetoAtividadePrevista> atividadesPrevistas) {
		this.atividadesPrevistas = atividadesPrevistas;
	}

	@OneToMany(mappedBy = "projeto", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ProjetoProrrogacao> getProrrogacoes() {
		return prorrogacoes;
	}

	public void setProrrogacoes(List<ProjetoProrrogacao> prorrogacoes) {
		this.prorrogacoes = prorrogacoes;
	}
	
	@SuppressWarnings("deprecation")
	@ManyToMany
	@JoinTable(name = "projeto_atividade"
		,joinColumns = {@JoinColumn(name = "projeto_id")}
		,inverseJoinColumns = {@JoinColumn(name = "atividade_id")})
    @org.hibernate.annotations.ForeignKey(name = "FK_projeto_atividade_projeto"
    	,inverseName = "FK_projeto_atividade_atividade")	
	public List<Atividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}

	@Transient
	public String getUFEnderecoObra() {
		if (enderecoObra != null && enderecoObra.getUf() != null) {
			return enderecoObra.getUf().getLabel();
		}
		return "";
	}
	
	@Transient
	public String getTituloComCodigoExterno() {
		String retorno = this.getTitulo();
		retorno += (StringUtils.isBlank(this.getCommerce()) ? "" : " - " + this.getCommerce());	
		return retorno;
	}

	@Transient
	public boolean isNovo() {
		return getId() == null;
	}
	
	@Transient
	public boolean isEditando() {
		return getId() != null;
	}
	
	@Transient
	public boolean isNaoIniciado() {
		return getStatus() == EStatusProjeto.COMECAR;
	}

	@Transient
	public boolean isParticipanteProjeto(Colaborador colaborador) {
		if (colaborador == null) {
			return false;
		}
		
		if (responsavel != null && responsavel.equals(colaborador)) {
			return true;
		}
		
		for (ProjetoColaborador c : getParticipantes()) {
			if ((c.getColaborador().equals(colaborador))) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Projeto other = (Projeto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

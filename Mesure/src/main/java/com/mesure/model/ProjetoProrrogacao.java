package com.mesure.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "projeto_prorrogacao")
public class ProjetoProrrogacao implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Date data;
	private Date prazoAtual;
	private Date novoPrazo;
	private Colaborador colaborador;
	private Projeto projeto;

	@Id
	@TableGenerator (
		name="projeto_prorrogacao_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="projeto_prorrogacao",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "projeto_prorrogacao_generator")	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data", nullable = false)		
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "prazo_atual", nullable = false)		
	public Date getPrazoAtual() {
		return prazoAtual;
	}

	public void setPrazoAtual(Date prazoAtual) {
		this.prazoAtual = prazoAtual;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "novo_prazo", nullable = false)
	public Date getNovoPrazo() {
		return novoPrazo;
	}

	public void setNovoPrazo(Date novoPrazo) {
		this.novoPrazo = novoPrazo;
	}
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_projeto_prorrogacao_colaborador"), nullable = false)	
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "projeto_id", foreignKey = @ForeignKey(name = "FK_projeto_prorrogacao_projeto"), nullable = false)
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjetoProrrogacao other = (ProjetoProrrogacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}


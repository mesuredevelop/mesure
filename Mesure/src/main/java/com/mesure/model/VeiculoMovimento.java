package com.mesure.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.EPeriodoDia;

@Entity
@Table(name = "veiculo_movimento")
public class VeiculoMovimento implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String destino;
	private Date dataMovimento;
	private Date dataRetorno;
	private String diaSemana;
	private EPeriodoDia periodo;
	private Date horaSaida;
	private Date horaRetorno;
	private Integer kmInicial;
	private Integer kmFinal;
	private Veiculo veiculo;
	private Projeto projeto;
	private Colaborador colaborador;
	private ProgramacaoExecucao programacaoExecucao;

	@Id
	@TableGenerator (
		name="veiculo_movimento_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="veiculo_movimento",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "veiculo_movimento_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank @Size(max = 150)
	@Column(nullable = false, length = 150)
	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data_movimento")	
	public Date getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(Date data) {
		this.dataMovimento = data;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_retorno")	
	public Date getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(Date dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	@Size(max = 15)
	@Column(name="dia_semana", length = 15)
	public String getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 10)	
	public EPeriodoDia getPeriodo() {
		return periodo;
	}

	public void setPeriodo(EPeriodoDia periodo) {
		this.periodo = periodo;
	}

	@NotNull
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_saida")	
	public Date getHoraSaida() {
		return horaSaida;
	}

	public void setHoraSaida(Date horaSaida) {
		this.horaSaida = horaSaida;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_retorno")	
	public Date getHoraRetorno() {
		return horaRetorno;
	}

	public void setHoraRetorno(Date horaRetorno) {
		this.horaRetorno = horaRetorno;
	}
	
	@Column(name="km_inicial", precision = 10, scale = 2)
	public Integer getKmInicial() {
		return kmInicial;
	}

	public void setKmInicial(Integer kmInicial) {
		this.kmInicial = kmInicial;
	}

	@Column(name="km_final", precision = 10, scale = 2)
	public Integer getKmFinal() {
		return kmFinal;
	}

	public void setKmFinal(Integer kmFinal) {
		this.kmFinal = kmFinal;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "veiculo_id", foreignKey = @ForeignKey(name = "FK_veiculo_movimento_veiculo"), nullable = false)
	public Veiculo getVeiculo() {
		return veiculo;
	}
	
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	@ManyToOne
	@JoinColumn(name = "projeto_id", foreignKey = @ForeignKey(name = "FK_veiculo_movimento_projeto"))
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_veiculo_movimento_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@OneToOne
	@JoinColumn(name = "programacao_execucao_id", foreignKey = @ForeignKey(name = "FK_veiculo_movimento_programacao_execucao"))
	public ProgramacaoExecucao getProgramacaoExecucao() {
		return programacaoExecucao;
	}

	public void setProgramacaoExecucao(ProgramacaoExecucao programacaoExecucao) {
		this.programacaoExecucao = programacaoExecucao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VeiculoMovimento other = (VeiculoMovimento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}

}

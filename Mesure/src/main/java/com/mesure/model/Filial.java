package com.mesure.model;

import com.mesure.enumerator.EEstado;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ETipoHoraExtra100;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "filial")
public class Filial implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String nome;
    private String cnpj;
    private String logradouro;
    private String numero;
    private String complemento;
    private String bairro;
    private String cidade;
    private EEstado uf;
    private String cep;
    private String fone1;
    private String fone2;
    private String email1;
    private String email2;
    private Empresa empresa;
    private BigDecimal horasTrabalhoDiario;
    private Integer diasExecucaoRetroativo;
    private Integer diasApontamentoAtrasado;
    private Integer limiteBH;
    private Date inicioHoraNoturna;
    private ETipoHoraExtra100 tipoHoraExtra100;
    private ESimNao filtrarProjetos;
    private boolean flag = false;

    @Id
    @TableGenerator(
            name = "filial_generator",
            table = "generator_id",
            pkColumnName = "GEN_KEY",
            valueColumnName = "GEN_VALUE",
            pkColumnValue = "filial",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "filial_generator")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    @Size(max = 100)
    @Column(nullable = false, length = 100)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Size(max = 18)
    @Column(length = 18)
    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    @NotBlank
    @Size(max = 100)
    @Column(nullable = false, length = 100)
    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    @Size(max = 6)
    @Column(length = 6)
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Size(max = 100)
    @Column(length = 100)
    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    @Size(max = 40)
    @Column(length = 40)
    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    @NotBlank
    @Size(max = 40)
    @Column(nullable = false, length = 40)
    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 2)
    public EEstado getUf() {
        return uf;
    }

    public void setUf(EEstado uf) {
        this.uf = uf;
    }

    @Size(max = 10)
    @Column(length = 10)
    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    @Size(max = 20)
    @Column(length = 20)
    public String getFone1() {
        return fone1;
    }

    public void setFone1(String fone1) {
        this.fone1 = fone1;
    }

    @Size(max = 20)
    @Column(length = 20)
    public String getFone2() {
        return fone2;
    }

    public void setFone2(String fone2) {
        this.fone2 = fone2;
    }

    @Size(max = 255)
    @Column(length = 255)
    @Email
    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    @Size(max = 255)
    @Column(length = 255)
    @Email
    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    @NotNull
    @ManyToOne
    @JoinColumn(name = "empresa_id", foreignKey = @ForeignKey(name = "FK_filial_empresa"), nullable = false)
    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    @Column(name = "horas_trabalho_diario", precision = 4, scale = 2)
    @Max(value = 24, message = "- valor máximo permitido: 24")
    public BigDecimal getHorasTrabalhoDiario() {
        return horasTrabalhoDiario;
    }

    public void setHorasTrabalhoDiario(BigDecimal horasTrabalhoDiario) {
        this.horasTrabalhoDiario = horasTrabalhoDiario;
    }

    @Column(name = "dias_execucao_retroativo")
    public Integer getDiasExecucaoRetroativo() {
        return diasExecucaoRetroativo;
    }

    public void setDiasExecucaoRetroativo(Integer diasExecucaoRetroativo) {
        this.diasExecucaoRetroativo = diasExecucaoRetroativo;
    }
    
    @Column(name = "dias_apontamento_atrasado")
    public Integer getDiasApontamentoAtrasado() {
		return diasApontamentoAtrasado;
	}

	public void setDiasApontamentoAtrasado(Integer diasApontamentoAtrasado) {
		this.diasApontamentoAtrasado = diasApontamentoAtrasado;
	}

	@Column(name = "limite_bh")
    public Integer getLimiteBH() {
        return limiteBH;
    }

    public void setLimiteBH(Integer limiteBH) {
        this.limiteBH = limiteBH;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "inicio_hora_noturna")
    public Date getInicioHoraNoturna() {
        return inicioHoraNoturna;
    }

    public void setInicioHoraNoturna(Date inicioHoraNoturna) {
        this.inicioHoraNoturna = inicioHoraNoturna;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_hora_extra_100", nullable = false, length = 15)
    public ETipoHoraExtra100 getTipoHoraExtra100() {
        return tipoHoraExtra100;
    }

    public void setTipoHoraExtra100(ETipoHoraExtra100 tipoHoraExtra100) {
        this.tipoHoraExtra100 = tipoHoraExtra100;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "filtrar_projetos", nullable = false, length = 3)
    public ESimNao getFiltrarProjetos() {
        return filtrarProjetos;
    }

    public void setFiltrarProjetos(ESimNao filtrarProjetos) {
        this.filtrarProjetos = filtrarProjetos;
    }

    @Transient
    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Filial other = (Filial) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (id.equals(other.id)) {
            return true;
        }
        return false;
    }

}

package com.mesure.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.EEstado;
import com.mesure.enumerator.ETipoFeriado;

@Entity
@Table(name = "feriado")
public class Feriado implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descricao;
	private Date data;
	private ETipoFeriado tipo;
	private EEstado uf;
	private String cidade;
	private Filial filial;
	private boolean flag = false;

	@Id
	@TableGenerator (
		name="feriado_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="feriado",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "feriado_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank @Size(max = 60)
	@Column(nullable = false, length = 60)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data", nullable = false)	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 10)	
	public ETipoFeriado getTipo() {
		return tipo;
	}
	
	public void setTipo(ETipoFeriado tipo) {
		this.tipo = tipo;
	}

	@Enumerated(EnumType.STRING)
	@Column(length = 2)
	public EEstado getUf() {
		return uf;
	}

	public void setUf(EEstado uf) {
		this.uf = uf;
	}

	@Size(max = 60)
	@Column(length = 60)
	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "filial_id", foreignKey = @ForeignKey(name = "FK_feriado_filial"), nullable = false)	
	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	@Transient
	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Feriado other = (Feriado) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
	

}

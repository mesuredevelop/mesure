package com.mesure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "veiculo_marca")
public class VeiculoMarca implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descricao;
	private List<VeiculoModelo> modelos = new ArrayList<>();

	@Id
	@TableGenerator (
		name="veiculo_marca_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="veiculo_marca",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "veiculo_marca_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	@Size(max = 25)
	@Column(nullable = false, length = 25)	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@OneToMany(mappedBy = "marca", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<VeiculoModelo> getModelos() {
		return modelos;
	}

	public void setModelos(List<VeiculoModelo> modelos) {
		this.modelos = modelos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VeiculoMarca other = (VeiculoMarca) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

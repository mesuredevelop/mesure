package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.mesure.util.Util;

@Entity
@Table(name = "colaborador_ponto_extra")
public class ColaboradorPontoExtra implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Date data;
	private Date horaInicio;
	private Date horaTermino;
	private BigDecimal totalHoras;
	private String descricao;
	private Colaborador colaborador;
	private TipoAtividadeExtra tipoAtividade;
	
	@Id
	@TableGenerator (
		name="colaborador_ponto_extra_generator",
		table="generator_id",
		pkColumnName = "GEN_KEY",
		valueColumnName = "GEN_VALUE",
		pkColumnValue="colaborador_ponto_extra",
		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "colaborador_ponto_extra_generator")	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data", nullable = false)	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@NotNull
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_inicio", nullable = false)
	public Date getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}

	@NotNull
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_termino", nullable = false)
	public Date getHoraTermino() {
		return horaTermino;
	}

	public void setHoraTermino(Date horaTermino) {
		this.horaTermino = horaTermino;
	}

	@NotNull
	@Column(name = "total_horas", precision = 18, scale = 14)	
	public BigDecimal getTotalHoras() {
		return totalHoras;
	}

	public void setTotalHoras(BigDecimal totalHoras) {
		this.totalHoras = totalHoras;
	}
	
	@Column(columnDefinition = "text")
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_colaborador_ponto_extra_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}	

	@ManyToOne
	@JoinColumn(name = "tipo_atividade_id", foreignKey = @ForeignKey(name = "FK_colaborador_ponto_extra_tipo_atividade"))
	public TipoAtividadeExtra getTipoAtividade() {
		return tipoAtividade;
	}

	public void setTipoAtividade(TipoAtividadeExtra tipoAtividade) {
		this.tipoAtividade = tipoAtividade;
	}
	
	@Transient
	public void calcularTotalHora() {
		setTotalHoras(new BigDecimal(0));
		
		// Total de horas
		if (getHoraInicio() != null && getHoraTermino() != null) {
			setTotalHoras(new BigDecimal(Util.converteHoraParaCentesimal(getHoraTermino().getTime() - getHoraInicio().getTime())));
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColaboradorPontoExtra other = (ColaboradorPontoExtra) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}	
	
}

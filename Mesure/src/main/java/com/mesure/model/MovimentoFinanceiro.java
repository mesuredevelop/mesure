package com.mesure.model;

import com.mesure.enumerator.EDebitoCredito;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "movimento_financeiro")
public class MovimentoFinanceiro implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private ContaBancaria contaBancaria;
    private Date data;
    private BigDecimal valor;
    private String documento;
    private EDebitoCredito tipoMovimento;

    @Id
    @TableGenerator(
            name = "movimento_financeiro_generator",
            table = "generator_id",
            pkColumnName = "GEN_KEY",
            valueColumnName = "GEN_VALUE",
            pkColumnValue = "movimento_financeiro",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "movimento_financeiro_generator")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    @ManyToOne
    @JoinColumn(name = "conta_bancaria_id", foreignKey = @ForeignKey(name = "FK_movimento_financeiro_conta_bancaria"), nullable = false)
    public ContaBancaria getContaBancaria() {
        return contaBancaria;
    }

    public void setContaBancaria(ContaBancaria contaBancaria) {
        this.contaBancaria = contaBancaria;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "data", nullable = false)
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Column(name = "valor", precision = 10, scale = 2, nullable = false)
    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @Size(max = 40)
    @Column(length = 40, nullable = false)
    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 15, nullable = false)
    public EDebitoCredito getTipoMovimento() {
        return tipoMovimento;
    }

    public void setTipoMovimento(EDebitoCredito tipoMovimento) {
        this.tipoMovimento = tipoMovimento;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MovimentoFinanceiro other = (MovimentoFinanceiro) obj;
        return Objects.equals(this.id, other.id);
    }

}

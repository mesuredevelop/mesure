package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.EGravidadeMulta;
import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoMulta;

@Entity
@Table(name = "veiculo_multa")
public class VeiculoMulta implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String notificacao;
	private EGravidadeMulta gravidade;
	private Date dataNotificacao;
	private Date dataPrazo;
	private BigDecimal valor;
	private ESituacaoMulta situacao;
	private ESimNao identificou;
	private String numeroAutuacao;
	private String identificadorCondutor;
	private Veiculo veiculo;
	private Colaborador colaborador;
	private String observacao;	

	@Id
	@TableGenerator (
		name="veiculo_multa_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="veiculo_multa",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "veiculo_multa_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	@Size(max = 250)
	@Column(nullable = false, length = 250)	
	public String getNotificacao() {
		return notificacao;
	}

	public void setNotificacao(String notificacao) {
		this.notificacao = notificacao;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 15)		
	public EGravidadeMulta getGravidade() {
		return gravidade;
	}

	public void setGravidade(EGravidadeMulta gravidade) {
		this.gravidade = gravidade;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data_notificacao")		
	public Date getDataNotificacao() {
		return dataNotificacao;
	}

	public void setDataNotificacao(Date dataNotificacao) {
		this.dataNotificacao = dataNotificacao;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data_prazo")		
	public Date getDataPrazo() {
		return dataPrazo;
	}
		
	@Column(precision = 10, scale = 2)
	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public void setDataPrazo(Date dataPrazo) {
		this.dataPrazo = dataPrazo;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 15)	
	public ESituacaoMulta getSituacao() {
		return situacao;
	}

	public void setSituacao(ESituacaoMulta situacao) {
		this.situacao = situacao;
	}
		
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name="identificou", nullable = false, length = 3)
	public ESimNao getIdentificou() {
		return identificou;
	}

	public void setIdentificou(ESimNao identificou) {
		this.identificou = identificou;
	}
	
	@Size(max = 250)
	@Column(name="numero_autuacao", length = 250)
	public String getNumeroAutuacao() {
		return numeroAutuacao;
	}

	public void setNumeroAutuacao(String numeroAutuacao) {
		this.numeroAutuacao = numeroAutuacao;
	}

	@Size(max = 250)
	@Column(name="identificador_condutor", length = 250)
	public String getIdentificadorCondutor() {
		return identificadorCondutor;
	}

	public void setIdentificadorCondutor(String identificadorCondutor) {
		this.identificadorCondutor = identificadorCondutor;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "veiculo_id", foreignKey = @ForeignKey(name = "FK_veiculo_multa_veiculo"), nullable = false)
	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_veiculo_multa_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@Column(columnDefinition = "text")
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VeiculoMulta other = (VeiculoMulta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

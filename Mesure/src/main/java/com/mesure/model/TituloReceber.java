package com.mesure.model;

import com.mesure.enumerator.EStatusTitulo;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "titulo_receber")
public class TituloReceber implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Cliente cliente;
    private Date dataEmissao;
    private Date dataVencimento;
    private BigDecimal valor;
    private BigDecimal totalPago;
    private BigDecimal saldo;
    private String documento;
    private Projeto projeto;
    private Filial filial;
    private EStatusTitulo status;
    private List<TituloReceberBaixa> baixas = new ArrayList<>();

    @Id
    @TableGenerator(
            name = "titulo_receber_generator",
            table = "generator_id",
            pkColumnName = "GEN_KEY",
            valueColumnName = "GEN_VALUE",
            pkColumnValue = "titulo_receber",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "titulo_receber_generator")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    @ManyToOne
    @JoinColumn(name = "cliente_id", foreignKey = @ForeignKey(name = "FK_titulo_receber_cliente"), nullable = false)
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "data_emissao", nullable = false)
    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "data_vencimento", nullable = false)
    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    @Column(name = "valor", precision = 10, scale = 2, nullable = false)
    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @Column(name = "total_pago", precision = 10, scale = 2, nullable = false)
    public BigDecimal getTotalPago() {
        return totalPago;
    }

    public void setTotalPago(BigDecimal totalPago) {
        this.totalPago = totalPago;
    }

    @Column(name = "saldo", precision = 10, scale = 2, nullable = false)
    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    @Size(max = 40)
    @Column(length = 40, nullable = true)
    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    @ManyToOne
    @JoinColumn(name = "projeto_id", foreignKey = @ForeignKey(name = "FK_titulo_receber_projeto"))
    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    @NotNull
    @ManyToOne
    @JoinColumn(name = "filial_id", foreignKey = @ForeignKey(name = "FK_titulo_receber_filial"), nullable = false)
    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 15, nullable = false)
    public EStatusTitulo getStatus() {
        return status;
    }

    public void setStatus(EStatusTitulo status) {
        this.status = status;
    }

    @OneToMany(mappedBy = "titulo", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<TituloReceberBaixa> getBaixas() {
        return baixas;
    }

    public void setBaixas(List<TituloReceberBaixa> baixas) {
        this.baixas = baixas;
    }

    @Transient
    public boolean isNovo() {
        return getId() == null;
    }

    @Transient
    public boolean isEditando() {
        return getId() != null;
    }

    @Transient
    public boolean isTituloAberto() {
        return getStatus() == EStatusTitulo.ABERTO;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TituloReceber other = (TituloReceber) obj;
        return Objects.equals(this.id, other.id);
    }

}

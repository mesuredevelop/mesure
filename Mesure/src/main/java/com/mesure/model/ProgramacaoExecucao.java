package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.mesure.enumerator.ESimNao;

@Entity
@Table(name = "programacao_execucao")
public class ProgramacaoExecucao implements Serializable {

	private static final long serialVersionUID = 1L;
	
	Long id;
	private Date data;
	private Date dataTermino;
	private Filial filial;
	private Colaborador colaborador;
	private Projeto projeto;
	private ProjetoEtapa etapa;
	private Veiculo veiculo;
	private Date horaInicio;
	private Date horaTermino;	
	private Date saidaEmpresa;
	private Date retornoEmpresa;
	private BigDecimal horasViagem;
	private ESimNao motorista;
	private ESimNao ativo;
	private ESimNao veiculoProprio;
	private TipoAtividadePrevista tipoAtividadePrevista;
	private String observacao;	
	private List<ProgramacaoExecucaoColaborador> participantes = new ArrayList<>();
	private List<ProjetoEtapa> etapas = new ArrayList<>();
	private ProjetoAtividadePrevista atividadePrevista;
	private VeiculoMovimento movimentoVeiculo;
	private boolean flag = false;

	@Id
	@TableGenerator (
		name="programacao_execucao_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="programacao_execucao",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "programacao_execucao_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_termino")
	public Date getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "filial_id", foreignKey = @ForeignKey(name = "FK_programacao_filial"), nullable = false)
	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_programacao_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "projeto_id", foreignKey = @ForeignKey(name = "FK_programacao_projeto"), nullable = false)	
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	@ManyToOne
	@JoinColumn(name = "etapa_projeto_id", foreignKey = @ForeignKey(name = "FK_programacao_etapa"))
	public ProjetoEtapa getEtapa() {
		return etapa;
	}

	public void setEtapa(ProjetoEtapa etapa) {
		this.etapa = etapa;
	}

	@ManyToOne
	@JoinColumn(name = "veiculo_id", foreignKey = @ForeignKey(name = "FK_programacao_veiculo"))
	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	@NotNull
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_inicio", nullable = false)
	public Date getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}

	@NotNull
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_termino", nullable = false)
	public Date getHoraTermino() {
		return horaTermino;
	}

	public void setHoraTermino(Date horaTermino) {
		this.horaTermino = horaTermino;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "saida_empresa")
	public Date getSaidaEmpresa() {
		return saidaEmpresa;
	}

	public void setSaidaEmpresa(Date saidaEmpresa) {
		this.saidaEmpresa = saidaEmpresa;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "retorno_empresa")
	public Date getRetornoEmpresa() {
		return retornoEmpresa;
	}

	public void setRetornoEmpresa(Date retornoEmpresa) {
		this.retornoEmpresa = retornoEmpresa;
	}
	
	@Column(name="horas_viagem", precision = 5, scale = 2)
	public BigDecimal getHorasViagem() {
		return horasViagem;
	}

	public void setHorasViagem(BigDecimal horasViagem) {
		this.horasViagem = horasViagem;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 3)	
	public ESimNao getMotorista() {
		return motorista;
	}

	public void setMotorista(ESimNao motorista) {
		this.motorista = motorista;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 3)	
	public ESimNao getAtivo() {
		return ativo;
	}

	public void setAtivo(ESimNao ativo) {
		this.ativo = ativo;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "veiculo_proprio", nullable = false, length = 3)		
	public ESimNao getVeiculoProprio() {
		return veiculoProprio;
	}

	public void setVeiculoProprio(ESimNao veiculoProprio) {
		this.veiculoProprio = veiculoProprio;
	}

	@ManyToOne
	@JoinColumn(name = "atividade_prevista_id", foreignKey = @ForeignKey(name = "FK_programacao_atividade_prevista"), nullable = false)
	public TipoAtividadePrevista getTipoAtividadePrevista() {
		return tipoAtividadePrevista;
	}

	public void setTipoAtividadePrevista(TipoAtividadePrevista tipoAtividadePrevista) {
		this.tipoAtividadePrevista = tipoAtividadePrevista;
	}

	@Column(columnDefinition = "text")
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@OneToMany(mappedBy = "programacaoExecucao", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ProgramacaoExecucaoColaborador> getParticipantes() {
		return participantes;
	}

	public void setParticipantes(List<ProgramacaoExecucaoColaborador> participantes) {
		this.participantes = participantes;
	}
	
	@SuppressWarnings("deprecation")
	@ManyToMany
	@JoinTable(name = "programacao_execucao_etapa"
		,joinColumns = {@JoinColumn(name = "programacao_execucao_id")}
		,inverseJoinColumns = {@JoinColumn(name = "etapa_id")})
    @org.hibernate.annotations.ForeignKey(name = "FK_programacao_execucao_etapa_programacao_execucao"
    	,inverseName = "FK_programacao_execucao_etapa_etapa")	
	public List<ProjetoEtapa> getEtapas() {
		return etapas;
	}

	public void setEtapas(List<ProjetoEtapa> etapas) {
		this.etapas = etapas;
	}	
	
	@OneToOne(mappedBy = "programacaoExecucao", orphanRemoval = true)
	public ProjetoAtividadePrevista getAtividadePrevista() {
		return atividadePrevista;
	}

	public void setAtividadePrevista(ProjetoAtividadePrevista atividadePrevista) {
		this.atividadePrevista = atividadePrevista;
	}

	@OneToOne(mappedBy = "programacaoExecucao", orphanRemoval = true)
	public VeiculoMovimento getMovimentoVeiculo() {
		return movimentoVeiculo;
	}

	public void setMovimentoVeiculo(VeiculoMovimento movimentoVeiculo) {
		this.movimentoVeiculo = movimentoVeiculo;
	}
	
	@Transient
	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	@Transient
	public boolean isNovo() {
		return getId() == null;
	}
	
	@Transient
	public boolean isEditando() {
		return getId() != null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProgramacaoExecucao other = (ProgramacaoExecucao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

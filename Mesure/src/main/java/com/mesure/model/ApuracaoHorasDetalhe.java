package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.mesure.enumerator.EDiaSemana;
import com.mesure.enumerator.ESituacaoBancoHoras;
import com.mesure.enumerator.ETipoRegistroPonto;

@Entity
@Table(name = "apuracao_horas_detalhe")
public class ApuracaoHorasDetalhe implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Date data;
	private EDiaSemana diaSemana;
	private Date horaEntrada;
	private Date horaSaida;
	private Date horaSaidaAlmoco;
	private Date horaRetornoAlmoco;
	private Date horaEntradaExtra;
	private Date horaSaidaExtra;
	private Date totalHoras;
	private Date bhPositivo;
	private Date bhNegativo;
	private BigDecimal totalHorasDecimal;
	private BigDecimal bhSaldoDecimal;
	private BigDecimal dsrNegativo;
	private BigDecimal extras50;
	private BigDecimal extras100;
	private BigDecimal horaNoturna;
	private BigDecimal horaNoturna100;
	private BigDecimal horasViagem;
	private BigDecimal valorAlimentacao;
	private ETipoRegistroPonto tipoRegistro;
	private ESituacaoBancoHoras situacaoBancoHoras;
	private ApuracaoHorasLote lote;

	@Id
	@TableGenerator (
		name="apuracao_horas_detalhe_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="apuracao_horas_detalhe",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "apuracao_horas_detalhe_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data", nullable = false)	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "dia_semana", length = 15, nullable = false)	
	public EDiaSemana getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(EDiaSemana diaSemana) {
		this.diaSemana = diaSemana;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_entrada")	
	public Date getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(Date horaEntrada) {
		this.horaEntrada = horaEntrada;
	}
	
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_saida")
	public Date getHoraSaida() {
		return horaSaida;
	}

	public void setHoraSaida(Date horaSaida) {
		this.horaSaida = horaSaida;
	}	

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_saida_almoco")
	public Date getHoraSaidaAlmoco() {
		return horaSaidaAlmoco;
	}

	public void setHoraSaidaAlmoco(Date horaSaidaAlmoco) {
		this.horaSaidaAlmoco = horaSaidaAlmoco;
	}	
	
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_retorno_almoco")
	public Date getHoraRetornoAlmoco() {
		return horaRetornoAlmoco;
	}

	public void setHoraRetornoAlmoco(Date horaRetornoAlmoco) {
		this.horaRetornoAlmoco = horaRetornoAlmoco;
	}		

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_entrada_extra")
	public Date getHoraEntradaExtra() {
		return horaEntradaExtra;
	}

	public void setHoraEntradaExtra(Date horaEntradaExtra) {
		this.horaEntradaExtra = horaEntradaExtra;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_saida_extra")
	public Date getHoraSaidaExtra() {
		return horaSaidaExtra;
	}

	public void setHoraSaidaExtra(Date horaSaidaExtra) {
		this.horaSaidaExtra = horaSaidaExtra;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "total_horas")
	public Date getTotalHoras() {
		return totalHoras;
	}

	public void setTotalHoras(Date totalHoras) {
		this.totalHoras = totalHoras;
	}
	
	@Temporal(TemporalType.TIME)
	@Column(name = "bh_positivo")	
	public Date getBhPositivo() {
		return bhPositivo;
	}

	public void setBhPositivo(Date bhPositivo) {
		this.bhPositivo = bhPositivo;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "bh_negativo")	
	public Date getBhNegativo() {
		return bhNegativo;
	}

	public void setBhNegativo(Date bhNegativo) {
		this.bhNegativo = bhNegativo;
	}
	
	@Column(name = "total_horas_decimal", precision = 18, scale = 14)
	public BigDecimal getTotalHorasDecimal() {
		return totalHorasDecimal;
	}

	public void setTotalHorasDecimal(BigDecimal totalHorasDecimal) {
		this.totalHorasDecimal = totalHorasDecimal;
	}
	
	@Column(name = "bh_saldo_decimal", precision = 18, scale = 14)
	public BigDecimal getBhSaldoDecimal() {
		return bhSaldoDecimal;
	}

	public void setBhSaldoDecimal(BigDecimal bhSaldoDecimal) {
		this.bhSaldoDecimal = bhSaldoDecimal;
	}
	
	@Column(name="dsr_negativo", precision = 4, scale = 2)
	public BigDecimal getDsrNegativo() {
		return dsrNegativo;
	}

	public void setDsrNegativo(BigDecimal dsrNegativo) {
		this.dsrNegativo = dsrNegativo;
	}

	@Column(name = "extras_50", precision = 18, scale = 14)	
	public BigDecimal getExtras50() {
		return extras50;
	}

	public void setExtras50(BigDecimal extras50) {
		this.extras50 = extras50;
	}
	
	@Column(name = "extras_100", precision = 18, scale = 14)	
	public BigDecimal getExtras100() {
		return extras100;
	}

	public void setExtras100(BigDecimal extras100) {
		this.extras100 = extras100;
	}

	@Column(name = "hora_noturna", precision = 18, scale = 14)
	public BigDecimal getHoraNoturna() {
		return horaNoturna;
	}

	public void setHoraNoturna(BigDecimal horaNoturna) {
		this.horaNoturna = horaNoturna;
	}
	
	@Column(name = "hora_noturna_100", precision = 18, scale = 14)
	public BigDecimal getHoraNoturna100() {
		return horaNoturna100;
	}

	public void setHoraNoturna100(BigDecimal horaNoturna100) {
		this.horaNoturna100 = horaNoturna100;
	}
	
	@Column(name="horas_viagem", precision = 5, scale = 2)
	public BigDecimal getHorasViagem() {
		return horasViagem;
	}

	public void setHorasViagem(BigDecimal horasViagem) {
		this.horasViagem = horasViagem;
	}
	
	@Column(name="valor_alimentacao", precision = 10, scale = 2)	
	public BigDecimal getValorAlimentacao() {
		return valorAlimentacao;
	}

	public void setValorAlimentacao(BigDecimal valorAlimentacao) {
		this.valorAlimentacao = valorAlimentacao;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_registro", length = 10, nullable = false)		
	public ETipoRegistroPonto getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(ETipoRegistroPonto tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "situacao_bh", length = 10)	
	public ESituacaoBancoHoras getSituacaoBancoHoras() {
		return situacaoBancoHoras;
	}

	public void setSituacaoBancoHoras(ESituacaoBancoHoras situacaoBancoHoras) {
		this.situacaoBancoHoras = situacaoBancoHoras;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "lote_id", foreignKey = @ForeignKey(name = "FK_apuracao_horas_detalhe_lote"), nullable = false)
	public ApuracaoHorasLote getLote() {
		return lote;
	}

	public void setLote(ApuracaoHorasLote lote) {
		this.lote = lote;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApuracaoHorasDetalhe other = (ApuracaoHorasDetalhe) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}

}

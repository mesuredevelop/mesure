package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.mesure.enumerator.EDiaSemana;

@Entity
@Table(name = "faturamento_detalhe")
public class FaturamentoDetalhe implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Date data;
	private EDiaSemana diaSemana;
	private Date horaInicio;
	private Date horaFim;
	private Date horaInicioNoturno;
	private Date horaFimNoturno;
	private Date horaInicioExtra;
	private Date horaFimExtra;
	private BigDecimal totalHoras;
	private BigDecimal totalHorasNoturno;
	private BigDecimal totalHorasExtra;
	private BigDecimal valorCobranca;
	private BigDecimal valorCobrancaNoturna;
	private BigDecimal totalCobranca;
	private BigDecimal totalCobrancaNoturna;
	private BigDecimal valor50;
	private BigDecimal valor100;
	private BigDecimal totalCobranca100;
	private BigDecimal totalGeral;
	private Colaborador colaborador;
	private FaturamentoLote lote;

	@Id
	@TableGenerator (
		name="faturamento_detalhe_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="faturamento_detalhe",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "faturamento_detalhe_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data", nullable = false)
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "dia_semana", length = 15, nullable = false)	
	public EDiaSemana getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(EDiaSemana diaSemana) {
		this.diaSemana = diaSemana;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_inicio")
	public Date getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_fim")
	public Date getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(Date horaFim) {
		this.horaFim = horaFim;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_inicio_noturno")
	public Date getHoraInicioNoturno() {
		return horaInicioNoturno;
	}

	public void setHoraInicioNoturno(Date horaInicioNoturno) {
		this.horaInicioNoturno = horaInicioNoturno;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_fim_noturno")
	public Date getHoraFimNoturno() {
		return horaFimNoturno;
	}

	public void setHoraFimNoturno(Date horaFimNoturno) {
		this.horaFimNoturno = horaFimNoturno;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_inicio_extra")
	public Date getHoraInicioExtra() {
		return horaInicioExtra;
	}

	public void setHoraInicioExtra(Date horaInicioExtra) {
		this.horaInicioExtra = horaInicioExtra;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_fim_extra")
	public Date getHoraFimExtra() {
		return horaFimExtra;
	}

	public void setHoraFimExtra(Date horaFimExtra) {
		this.horaFimExtra = horaFimExtra;
	}

	@Column(name = "total_horas", precision = 18, scale = 14)
	public BigDecimal getTotalHoras() {
		return totalHoras;
	}

	public void setTotalHoras(BigDecimal totalHoras) {
		this.totalHoras = totalHoras;
	}

	@Column(name = "total_horas_noturno", precision = 18, scale = 14)
	public BigDecimal getTotalHorasNoturno() {
		return totalHorasNoturno;
	}

	public void setTotalHorasNoturno(BigDecimal totalHorasNoturno) {
		this.totalHorasNoturno = totalHorasNoturno;
	}

	@Column(name = "total_horas_extra", precision = 18, scale = 14)
	public BigDecimal getTotalHorasExtra() {
		return totalHorasExtra;
	}

	public void setTotalHorasExtra(BigDecimal totalHorasExtra) {
		this.totalHorasExtra = totalHorasExtra;
	}
	
	@Column(name="valor_cobranca", precision = 10, scale = 2)	
	public BigDecimal getValorCobranca() {
		return valorCobranca;
	}

	public void setValorCobranca(BigDecimal valorCobranca) {
		this.valorCobranca = valorCobranca;
	}
	
	@Column(name="valor_cobranca_noturna", precision = 10, scale = 2)
	public BigDecimal getValorCobrancaNoturna() {
		return valorCobrancaNoturna;
	}

	public void setValorCobrancaNoturna(BigDecimal valorCobrancaNoturna) {
		this.valorCobrancaNoturna = valorCobrancaNoturna;
	}
	
	@Column(name="total_cobranca", precision = 10, scale = 2)
	public BigDecimal getTotalCobranca() {
		return totalCobranca;
	}

	public void setTotalCobranca(BigDecimal totalCobranca) {
		this.totalCobranca = totalCobranca;
	}

	@Column(name="total_cobranca_noturna", precision = 10, scale = 2)
	public BigDecimal getTotalCobrancaNoturna() {
		return totalCobrancaNoturna;
	}

	public void setTotalCobrancaNoturna(BigDecimal totalCobrancaNoturna) {
		this.totalCobrancaNoturna = totalCobrancaNoturna;
	}
	
	@Column(name="valor_50", precision = 10, scale = 2)
	public BigDecimal getValor50() {
		return valor50;
	}

	public void setValor50(BigDecimal valor50) {
		this.valor50 = valor50;
	}

	@Column(name="valor_100", precision = 10, scale = 2)
	public BigDecimal getValor100() {
		return valor100;
	}

	public void setValor100(BigDecimal valor100) {
		this.valor100 = valor100;
	}
	
	@Column(name="total_cobranca_100", precision = 10, scale = 2)
	public BigDecimal getTotalCobranca100() {
		return totalCobranca100;
	}

	public void setTotalCobranca100(BigDecimal totalCobranca100) {
		this.totalCobranca100 = totalCobranca100;
	}

	@Column(name="total_geral", precision = 10, scale = 2)
	public BigDecimal getTotalGeral() {
		return totalGeral;
	}

	public void setTotalGeral(BigDecimal totalGeral) {
		this.totalGeral = totalGeral;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_faturamento_detalhe_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "lote_id", foreignKey = @ForeignKey(name = "FK_faturamento_detalhe_lote"), nullable = false)
	public FaturamentoLote getLote() {
		return lote;
	}

	public void setLote(FaturamentoLote lote) {
		this.lote = lote;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FaturamentoDetalhe other = (FaturamentoDetalhe) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

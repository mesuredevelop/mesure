package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoColaborador;
import com.mesure.enumerator.ETipoColaborador;

@Entity
@Table(name = "colaborador")
public class Colaborador implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String nome;
    private Date dataAdmissao;
    private String cargo;
    private String foneComercial;
    private String ramal;
    private String emailComercial;
    private String foneParticular;
    private String emailParticular;
    private BigDecimal horasTrabalhoDiario;
    private BigDecimal saldoBH;
    private BigDecimal valorHora;
    private BigDecimal valorHoraBase;
    private BigDecimal salarioBase;
    private ESituacaoColaborador situacao;
    private Date dataDesligamento;
    private Usuario usuario;
    private Filial filial;
    private ETipoColaborador tipo;
    private ESimNao apontamento;
    private ESimNao registraPonto;
    private ESimNao permiteApontamentoAtrasado;
    private ESimNao periculosidade;
    private ESimNao visualizaProjeto;
    private ESimNao editaCadastroProjeto;
    private List<ColaboradorTreinamento> treinamentos = new ArrayList<>();
    private List<ColaboradorAdvertencia> advertencias = new ArrayList<>();
    private List<ColaboradorAtestado> atestados = new ArrayList<>();
    private List<ColaboradorEquipamento> equipamentos = new ArrayList<>();
    private List<ColaboradorEstabilidade> estabilidades = new ArrayList<>();

    @Id
    @TableGenerator(
            name = "colaborador_generator",
            table = "generator_id",
            pkColumnName = "GEN_KEY",
            valueColumnName = "GEN_VALUE",
            pkColumnValue = "colaborador",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "colaborador_generator")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    @Size(max = 60)
    @Column(nullable = false, length = 60)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "data_admissao")
    public Date getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(Date dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    @Size(max = 40)
    @Column(length = 40)
    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Size(max = 20)
    @Column(length = 20)
    public String getFoneComercial() {
        return foneComercial;
    }

    public void setFoneComercial(String foneComercial) {
        this.foneComercial = foneComercial;
    }

    @Size(max = 6)
    @Column(length = 6)
    public String getRamal() {
        return ramal;
    }

    public void setRamal(String ramal) {
        this.ramal = ramal;
    }

    @Size(max = 255)
    @Column(length = 255)
    @Email
    public String getEmailComercial() {
        return emailComercial;
    }

    public void setEmailComercial(String emailComercial) {
        this.emailComercial = emailComercial;
    }

    @Size(max = 20)
    @Column(length = 20)
    public String getFoneParticular() {
        return foneParticular;
    }

    public void setFoneParticular(String foneParticular) {
        this.foneParticular = foneParticular;
    }

    @Size(max = 255)
    @Column(length = 255)
    @Email
    public String getEmailParticular() {
        return emailParticular;
    }

    public void setEmailParticular(String emailParticular) {
        this.emailParticular = emailParticular;
    }

    @Column(name = "horas_trabalho_diario", precision = 4, scale = 2)
    @Max(value = 24, message = "- valor máximo permitido: 24")
    public BigDecimal getHorasTrabalhoDiario() {
        return horasTrabalhoDiario;
    }

    public void setHorasTrabalhoDiario(BigDecimal horasTrabalhoDiario) {
        this.horasTrabalhoDiario = horasTrabalhoDiario;
    }

    @Column(name = "saldo_bh", precision = 18, scale = 14)
    public BigDecimal getSaldoBH() {
        return saldoBH;
    }

    public void setSaldoBH(BigDecimal saldoBH) {
        this.saldoBH = saldoBH;
    }

    @Column(name = "valor_hora", precision = 10, scale = 2)
    public BigDecimal getValorHora() {
        return valorHora;
    }

    public void setValorHora(BigDecimal valorHora) {
        this.valorHora = valorHora;
    }

    @Column(name = "valor_hora_base", precision = 10, scale = 2)
    public BigDecimal getValorHoraBase() {
        return valorHoraBase;
    }

    public void setValorHoraBase(BigDecimal valorHoraBase) {
        this.valorHoraBase = valorHoraBase;
    }

    @Column(name = "salario_base", precision = 10, scale = 2)
    public BigDecimal getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(BigDecimal salarioBase) {
        this.salarioBase = salarioBase;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 20)
    public ESituacaoColaborador getSituacao() {
        return situacao;
    }

    public void setSituacao(ESituacaoColaborador situacao) {
        this.situacao = situacao;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 20)
    public ETipoColaborador getTipo() {
        return tipo;
    }

    public void setTipo(ETipoColaborador tipo) {
        this.tipo = tipo;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 3)
    public ESimNao getApontamento() {
        return apontamento;
    }

    public void setApontamento(ESimNao apontamento) {
        this.apontamento = apontamento;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "registra_ponto", nullable = false, length = 3)
    public ESimNao getRegistraPonto() {
        return registraPonto;
    }

    public void setRegistraPonto(ESimNao registraPonto) {
        this.registraPonto = registraPonto;
    }
    
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "permite_apontamento_atrasado", nullable = false, length = 3)
    public ESimNao getPermiteApontamentoAtrasado() {
		return permiteApontamentoAtrasado;
	}

	public void setPermiteApontamentoAtrasado(ESimNao permiteApontamentoAtrasado) {
		this.permiteApontamentoAtrasado = permiteApontamentoAtrasado;
	}

	@NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "periculosidade", nullable = false, length = 3)
    public ESimNao getPericulosidade() {
        return periculosidade;
    }

    public void setPericulosidade(ESimNao periculosidade) {
        this.periculosidade = periculosidade;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "visualiza_projeto", nullable = false, length = 3)
    public ESimNao getVisualizaProjeto() {
        return visualizaProjeto;
    }

    public void setVisualizaProjeto(ESimNao visualizaProjeto) {
        this.visualizaProjeto = visualizaProjeto;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "edita_cadastro_projeto", nullable = false, length = 3)
    public ESimNao getEditaCadastroProjeto() {
        return editaCadastroProjeto;
    }

    public void setEditaCadastroProjeto(ESimNao editaCadastroProjeto) {
        this.editaCadastroProjeto = editaCadastroProjeto;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "data_desligamento")
    public Date getDataDesligamento() {
        return dataDesligamento;
    }

    public void setDataDesligamento(Date dataDesligamento) {
        this.dataDesligamento = dataDesligamento;
    }

    @OneToOne
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "FK_colaborador_usuario"))
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @NotNull
    @ManyToOne
    @JoinColumn(name = "filial_id", foreignKey = @ForeignKey(name = "FK_colaborador_filial"), nullable = false)
    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }

    @OneToMany(mappedBy = "colaborador", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ColaboradorTreinamento> getTreinamentos() {
        return treinamentos;
    }

    public void setTreinamentos(List<ColaboradorTreinamento> treinamentos) {
        this.treinamentos = treinamentos;
    }

    @OneToMany(mappedBy = "colaborador", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ColaboradorAdvertencia> getAdvertencias() {
        return advertencias;
    }

    public void setAdvertencias(List<ColaboradorAdvertencia> advertencias) {
        this.advertencias = advertencias;
    }

    @OneToMany(mappedBy = "colaborador", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ColaboradorAtestado> getAtestados() {
        return atestados;
    }

    public void setAtestados(List<ColaboradorAtestado> atestados) {
        this.atestados = atestados;
    }

    @OneToMany(mappedBy = "colaborador", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ColaboradorEquipamento> getEquipamentos() {
        return equipamentos;
    }

    public void setEquipamentos(List<ColaboradorEquipamento> equipamentos) {
        this.equipamentos = equipamentos;
    }

    @OneToMany(mappedBy = "colaborador", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ColaboradorEstabilidade> getEstabilidades() {
        return estabilidades;
    }

    public void setEstabilidades(List<ColaboradorEstabilidade> estabilidades) {
        this.estabilidades = estabilidades;
    }

    @Transient
    public boolean isNovo() {
        return getId() == null;
    }

    @Transient
    public boolean isEditando() {
        return getId() != null;
    }

    @Transient
    public boolean isDemitido() {
        return getSituacao() == ESituacaoColaborador.DEMITIDO;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Colaborador other = (Colaborador) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (id.equals(other.id)) {
            return true;
        }
        return false;
    }

}

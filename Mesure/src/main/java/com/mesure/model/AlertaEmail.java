package com.mesure.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "alerta_email")
public class AlertaEmail implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Colaborador colaborador;
	private String email;
	private Alerta alerta;
	
	public AlertaEmail() {

	}
	
	public AlertaEmail(Colaborador colaborador, String email, Alerta alerta) {
		this.colaborador = colaborador;
		this.email = email;
		this.alerta = alerta;
	}

	@Id
	@TableGenerator (
		name="alerta_email_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="alerta_email",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "alerta_email_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_alerta_email_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	@Email
	@NotBlank
	@Size(max = 255)
	@Column(length = 255, nullable = false)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "alerta_id", foreignKey = @ForeignKey(name = "FK_alerta_email_alerta"), nullable = false)
	public Alerta getAlerta() {
		return alerta;
	}

	public void setAlerta(Alerta alerta) {
		this.alerta = alerta;
	}
	
	@Transient
	public boolean isNovo() {
		return getId() == null;
	}

	@Transient
	public boolean isEditando() {
		return getId() != null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlertaEmail other = (AlertaEmail) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.mesure.enumerator.EEstado;
import com.mesure.enumerator.EPeriodoAlimentacao;

@Entity
@Table(name = "tipo_alimentacao")
public class TipoAlimentacao implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Filial filial;
	private EEstado estado;
	private EPeriodoAlimentacao periodo;
	private BigDecimal valor;
	private Date horaInicio;
	private Date horaTermino;

	@Id
	@TableGenerator(name = "tipo_alimentacao_generator", table = "generator_id", pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE", pkColumnValue = "tipo_alimentacao", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "tipo_alimentacao_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "filial_id", foreignKey = @ForeignKey(name = "FK_tipo_alimentacao_filial"), nullable = false)	
	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 2)
	public EEstado getEstado() {
		return estado;
	}

	public void setEstado(EEstado estado) {
		this.estado = estado;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 15)	
	public EPeriodoAlimentacao getPeriodo() {
		return periodo;
	}

	public void setPeriodo(EPeriodoAlimentacao periodo) {
		this.periodo = periodo;
	}

	@NotNull
	@Column(name="valor", precision = 10, scale = 2, nullable = false)	
	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@NotNull
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_inicio", nullable = false)
	public Date getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}

	@NotNull
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_termino", nullable = false)
	public Date getHoraTermino() {
		return horaTermino;
	}

	public void setHoraTermino(Date horaTermino) {
		this.horaTermino = horaTermino;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoAlimentacao other = (TipoAlimentacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}

}

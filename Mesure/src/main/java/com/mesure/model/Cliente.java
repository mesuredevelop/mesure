package com.mesure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.ETipoPessoa;

@Entity
@Table(name = "cliente")
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private String apelido;
	private String cpf;
	private String cnpj;
	private String rg;
	private String inscricaoEstadual;
	private String fone1;
	private String fone2;
	private String email1;
	private String email2;
	private String nomeContato1;
	private String nomeContato2;
	private String site;
	private ETipoPessoa tipo;
	private List<EnderecoCliente> enderecos = new ArrayList<>();
	private List<Projeto> projetos = new ArrayList<>();
	
	@Id
	@TableGenerator (
		name="cliente_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="cliente",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "cliente_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank @Size(max = 100)
	@Column(nullable = false, length = 100)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Size(max = 100) 
	@Column(length = 100)
	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 10)
	public ETipoPessoa getTipo() {
		return tipo;
	}

	public void setTipo(ETipoPessoa tipo) {
		this.tipo = tipo;
	}
	
	@Size(max = 14)
	@Column(length = 14)
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Size(max = 18) 
	@Column(length = 18)
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	@Size(max = 15)
	@Column(length = 15)	
	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	@Size(max = 14)
	@Column(name = "ins_estadual", length = 14)	
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	@Size(max = 20)
	@Column(length = 20)
	public String getFone1() {
		return fone1;
	}

	public void setFone1(String fone1) {
		this.fone1 = fone1;
	}

	@Size(max = 20)
	@Column(length = 20)	
	public String getFone2() {
		return fone2;
	}

	public void setFone2(String fone2) {
		this.fone2 = fone2;
	}

	@Size(max = 255)
	@Column(length = 255)
	@Email
	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	@Size(max = 255)
	@Column(length = 255)
	@Email
	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	
	@Size(max = 255)
	@Column(name = "nome_contato1", length = 255)
	public String getNomeContato1() {
		return nomeContato1;
	}

	public void setNomeContato1(String nomeContato1) {
		this.nomeContato1 = nomeContato1;
	}

	@Size(max = 255)
	@Column(name = "nome_contato2", length = 255)
	public String getNomeContato2() {
		return nomeContato2;
	}

	public void setNomeContato2(String nomeContato2) {
		this.nomeContato2 = nomeContato2;
	}

	@Size(max = 255)
	@Column(length = 255)
	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}
	
	@OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public List<EnderecoCliente> getEnderecos() {
		return enderecos;
	}
	
	public void setEnderecos(List<EnderecoCliente> enderecos) {
		this.enderecos = enderecos;
	}
	
	@OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)	
	public List<Projeto> getProjetos() {
		return projetos;
	}
	
	public void setProjetos(List<Projeto> projetos) {
		this.projetos = projetos;
	}
	
	@Transient
	public String getNomeComInscricao() {
		String retorno = this.getNome();
		if (this.getTipo().equals(ETipoPessoa.FISICA)) {
			retorno += (StringUtils.isBlank(this.getCpf()) ? "" : " - " + this.getCpf());	
		} else {
			retorno += (StringUtils.isBlank(this.getCnpj()) ? "" : " - " + this.getCnpj());
		}
		return retorno;
	}

	@Transient
	public boolean isNovo() {
		return getId() == null;
	}
	
	@Transient
	public boolean isEditando() {
		return getId() != null;
	}	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
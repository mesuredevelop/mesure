package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.ESituacaoEtapa;

@Entity
@Table(name = "projeto_etapa")
public class ProjetoEtapa implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descricao;
	private BigDecimal quantidadeHoras;	
	private BigDecimal horasExecutadas;
	private BigDecimal percentualExecutado;
	private ESituacaoEtapa situacao;
	private Etapa etapa;
	private Projeto projeto;

	public ProjetoEtapa() {
		
	}		
	
	public ProjetoEtapa(Projeto projeto, Etapa etapa, String descricao, BigDecimal quantidadeHoras, BigDecimal horasExecutadas, BigDecimal percentualExecutado, ESituacaoEtapa situacao) {
		this.projeto = projeto;
		this.etapa = etapa;
		this.descricao = descricao;
		this.quantidadeHoras = quantidadeHoras;
		this.horasExecutadas = horasExecutadas;
		this.percentualExecutado = percentualExecutado;
		this.situacao = situacao;
	}
	
	@Id
	@TableGenerator (
		name="projeto_etapa_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="projeto_etapa",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "projeto_etapa_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	@Size(max = 255)
	@Column(nullable = false, length = 255)	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@NotNull
	@Column(name = "qtde_horas", precision = 10, scale = 2, nullable = false)	
	public BigDecimal getQuantidadeHoras() {
		return quantidadeHoras;
	}

	public void setQuantidadeHoras(BigDecimal quantidadeHoras) {
		this.quantidadeHoras = quantidadeHoras;
	}
	
	@Column(name = "horas_executadas", precision = 18, scale = 14)
	public BigDecimal getHorasExecutadas() {
		return horasExecutadas;
	}

	public void setHorasExecutadas(BigDecimal horasExecutadas) {
		this.horasExecutadas = horasExecutadas;
	}

	@Column(name = "percentual_executado", precision = 10, scale = 4)
	public BigDecimal getPercentualExecutado() {
		return percentualExecutado;
	}

	public void setPercentualExecutado(BigDecimal percentualExecutado) {
		this.percentualExecutado = percentualExecutado;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(length = 15, nullable = false)
	public ESituacaoEtapa getSituacao() {
		return situacao;
	}

	public void setSituacao(ESituacaoEtapa situacao) {
		this.situacao = situacao;
	}
	
	@ManyToOne
	@JoinColumn(name = "etapa_id", foreignKey = @ForeignKey(name = "FK_projeto_etapa_etapa"))	
	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "projeto_id", foreignKey = @ForeignKey(name = "FK_projeto_etapa_projeto"), nullable = false)
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	@Transient
	public boolean isNovo() {
		return getId() == null;
	}
	
	@Transient
	public boolean isEditando() {
		return getId() != null;
	}
	
	@Transient
	public String getDescricaoCompleta() {
		if (getEtapa() != null && getEtapa().getAtividade() != null) {
			return getEtapa().getAtividade().getDescricao() + " - " + getDescricao();
		}
		return getDescricao();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjetoEtapa other = (ProjetoEtapa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

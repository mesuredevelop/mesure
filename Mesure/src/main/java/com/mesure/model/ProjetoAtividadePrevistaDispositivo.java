package com.mesure.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "projeto_atividade_prevista_epc")
public class ProjetoAtividadePrevistaDispositivo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private ProjetoAtividadePrevista atividadePrevista;
	private DispositivoSeguranca dispositivoSeguranca;
	
	public ProjetoAtividadePrevistaDispositivo() {
		
	}
	
	public ProjetoAtividadePrevistaDispositivo(ProjetoAtividadePrevista atividadePrevista, DispositivoSeguranca dispositivoSeguranca) {
		this.atividadePrevista = atividadePrevista;
		this.dispositivoSeguranca = dispositivoSeguranca;
	}

	@Id
	@TableGenerator (
		name="projeto_atividade_prevista_epc_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="projeto_atividade_prevista_epc",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "projeto_atividade_prevista_epc_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "atividade_prevista_id", foreignKey = @ForeignKey(name = "FK_atividade_prevista_epc_projeto_atividade_prevista"))
	public ProjetoAtividadePrevista getAtividadePrevista() {
		return atividadePrevista;
	}

	public void setAtividadePrevista(ProjetoAtividadePrevista atividadePrevista) {
		this.atividadePrevista = atividadePrevista;
	}

	@ManyToOne
	@JoinColumn(name = "dispositivo_seguranca_id", foreignKey = @ForeignKey(name = "FK_atividade_prevista_epc_dispositivo_seguranca"))
	public DispositivoSeguranca getDispositivoSeguranca() {
		return dispositivoSeguranca;
	}

	public void setDispositivoSeguranca(DispositivoSeguranca dispositivoSeguranca) {
		this.dispositivoSeguranca = dispositivoSeguranca;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjetoAtividadePrevistaDispositivo other = (ProjetoAtividadePrevistaDispositivo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

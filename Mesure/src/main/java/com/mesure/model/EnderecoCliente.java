package com.mesure.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.EEstado;
import com.mesure.enumerator.ETipoEndereco;

@Entity
@Table(name = "endereco_cliente")
public class EnderecoCliente implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cidade;
	private EEstado uf;
	private String cep;
	private ETipoEndereco tipoEndereco;
	private Cliente cliente;
	
	@Id
	@TableGenerator (
		name="endereco_cliente_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="endereco_cliente",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "endereco_cliente_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@NotBlank
	@Size(max = 100)
	@Column(nullable = false, length = 100)
	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	@Size(max = 6)
	@Column(length = 6)
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Size(max = 100)
	@Column(length = 100)
	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	@Size(max = 40)
	@Column(length = 40)
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	@NotBlank
	@Size(max = 40)
	@Column(nullable = false, length = 40)
	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 2)
	public EEstado getUf() {
		return uf;
	}

	public void setUf(EEstado uf) {
		this.uf = uf;
	}

	@Size(max = 10)
	@Column(length = 10)
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}	

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 10)
	public ETipoEndereco getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(ETipoEndereco tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "cliente_id", foreignKey = @ForeignKey(name = "FK_endereco_cliente_cliente"), nullable = false)
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Transient
	public String getEnderecoCompleto() {
		String retorno = this.getLogradouro();
		retorno += (StringUtils.isBlank(this.getNumero()) ? "" : ", " + this.getNumero());
		retorno += (StringUtils.isBlank(this.getCidade()) ? "" : ", " + this.getCidade());
		retorno += (this.getUf() == null ? "" : " - " + this.getUf());							
		return retorno;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnderecoCliente other = (EnderecoCliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
}


package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "atividade")
public class Atividade implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String descricao;
	private BigDecimal quantidadeHoras;
	private List<Etapa> etapas = new ArrayList<>();
	private List<EquipamentoProtecao> equipamentos = new ArrayList<>();
	private List<DispositivoSeguranca> dispositivos = new ArrayList<>();
	private List<Treinamento> treinamentos = new ArrayList<>();

	@Id
	@TableGenerator (
		name="atividade_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="atividade",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "atividade_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	@Size(max = 60)
	@Column(nullable = false, length = 60)		
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Column(name = "qtde_horas", precision = 10, scale = 2)
	public BigDecimal getQuantidadeHoras() {
		return quantidadeHoras;
	}

	public void setQuantidadeHoras(BigDecimal quantidadeHoras) {
		this.quantidadeHoras = quantidadeHoras;
	}

	@OneToMany(mappedBy = "atividade", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<Etapa> getEtapas() {
		return etapas;
	}

	public void setEtapas(List<Etapa> etapas) {
		this.etapas = etapas;
	}

	@SuppressWarnings("deprecation")
	@ManyToMany
	@JoinTable(name = "atividade_equipamento"
		,joinColumns = {@JoinColumn(name = "atividade_id")}
		,inverseJoinColumns = {@JoinColumn(name = "equipamento_id")})
    @org.hibernate.annotations.ForeignKey(name = "FK_atividade_equipamento_atividade"
    	,inverseName = "FK_atividade_equipamento_equipamento")	
	public List<EquipamentoProtecao> getEquipamentos() {
		return equipamentos;
	}	
	
	public void setEquipamentos(List<EquipamentoProtecao> equipamentos) {
		this.equipamentos = equipamentos;
	}	
	
	@SuppressWarnings("deprecation")
	@ManyToMany
	@JoinTable(name = "atividade_dispositivo"
		,joinColumns = {@JoinColumn(name = "atividade_id")}
		,inverseJoinColumns = {@JoinColumn(name = "dispositivo_id")})
    @org.hibernate.annotations.ForeignKey(name = "FK_atividade_dispositivo_atividade"
    	,inverseName = "FK_atividade_dispositivo_dispositivo")	
	public List<DispositivoSeguranca> getDispositivos() {
		return dispositivos;
	}	
	
	public void setDispositivos(List<DispositivoSeguranca> dispositivos) {
		this.dispositivos = dispositivos;
	}	
	
	@SuppressWarnings("deprecation")
	@ManyToMany
	@JoinTable(name = "atividade_treinamento"
		,joinColumns = {@JoinColumn(name = "atividade_id")}
		,inverseJoinColumns = {@JoinColumn(name = "treinamento_id")})
    @org.hibernate.annotations.ForeignKey(name = "FK_atividade_treinamento_atividade"
    	,inverseName = "FK_atividade_treinamento_treinamento")	
	public List<Treinamento> getTreinamentos() {
		return treinamentos;
	}

	public void setTreinamentos(List<Treinamento> treinamentos) {
		this.treinamentos = treinamentos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atividade other = (Atividade) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}	

}

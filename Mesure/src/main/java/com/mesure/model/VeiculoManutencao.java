package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.ESituacaoManutencao;

@Entity
@Table(name = "veiculo_manutencao")
public class VeiculoManutencao implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Date data;
	private String servicoExecutado;
	private Integer quilometragem;
	private Date proximaManutencao;
	private BigDecimal valor;
	private ESituacaoManutencao situacao;
	private Veiculo veiculo;
	private Colaborador responsavel;
	private Fornecedor fornecedor;

	@Id
	@TableGenerator (
		name="veiculo_manutencao_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="veiculo_manutencao",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "veiculo_manutencao_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data_manutencao")	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@NotBlank
	@Column(name = "servico_executado", columnDefinition = "text", nullable = false)
	public String getServicoExecutado() {
		return servicoExecutado;
	}

	public void setServicoExecutado(String servicoExecutado) {
		this.servicoExecutado = servicoExecutado;
	}

	@Column(name = "quilometragem")	
	public Integer getQuilometragem() {
		return quilometragem;
	}

	public void setQuilometragem(Integer quilometragem) {
		this.quilometragem = quilometragem;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "proxima_manutencao")	
	public Date getProximaManutencao() {
		return proximaManutencao;
	}

	public void setProximaManutencao(Date proximaManutencao) {
		this.proximaManutencao = proximaManutencao;
	}

	@Column(precision = 10, scale = 2)
	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 15)	
	public ESituacaoManutencao getSituacao() {
		return situacao;
	}

	public void setSituacao(ESituacaoManutencao situacao) {
		this.situacao = situacao;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "veiculo_id", foreignKey = @ForeignKey(name = "FK_veiculo_manutencao_veiculo"), nullable = false)
	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_veiculo_manutencao_colaborador"), nullable = false)
	public Colaborador getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "fornecedor_id", foreignKey = @ForeignKey(name = "FK_veiculo_manutencao_fornecedor"), nullable = false)
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VeiculoManutencao other = (VeiculoManutencao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
		
}


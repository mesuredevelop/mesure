package com.mesure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.ECategoriaVeiculo;

@Entity
@Table(name = "veiculo_modelo")
public class VeiculoModelo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String descricao;
	private ECategoriaVeiculo categoria;
	private VeiculoMarca marca;
	private List<Veiculo> veiculos = new ArrayList<>();
	
	@Id
	@TableGenerator (
		name="veiculo_modelo_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="veiculo_modelo",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "veiculo_modelo_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	@NotBlank
	@Size(max = 25)
	@Column(nullable = false, length = 25)	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 20)	
	public ECategoriaVeiculo getCategoria() {
		return categoria;
	}

	public void setCategoria(ECategoriaVeiculo categoria) {
		this.categoria = categoria;
	}	
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "marca_id", foreignKey = @ForeignKey(name = "FK_veiculo_modelo_veiculo_marca"), nullable = false)
	public VeiculoMarca getMarca() {
		return marca;
	}

	public void setMarca(VeiculoMarca marca) {
		this.marca = marca;
	}

	@OneToMany(mappedBy = "modelo", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VeiculoModelo other = (VeiculoModelo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

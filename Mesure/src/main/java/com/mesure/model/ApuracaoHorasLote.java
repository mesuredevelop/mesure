package com.mesure.model;

import com.mesure.enumerator.EStatusLoteApuracaoHoras;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "apuracao_horas_lote")
public class ApuracaoHorasLote implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String descricao;
    private Colaborador colaborador;
    private Date competencia;
    private Date dataInicio;
    private Date dataFim;
    private EStatusLoteApuracaoHoras status;
    private Date dataCriacao;
    private Date dataAlteracao;
    private Usuario usuario;
    private List<ApuracaoHorasDetalhe> detalhes;
    private boolean todosColaboradores = false;
    private boolean flag = false;

    @Id
    @TableGenerator(
            name = "apuracao_horas_lote_generator",
            table = "generator_id",
            pkColumnName = "GEN_KEY",
            valueColumnName = "GEN_VALUE",
            pkColumnValue = "apuracao_horas_lote",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "apuracao_horas_lote_generator")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    @Size(max = 255)
    @Column(nullable = false, length = 255)
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @NotNull
    @ManyToOne
    @JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_apuracao_horas_lote_colaborador"), nullable = false)
    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "competencia", nullable = false)
    public Date getCompetencia() {
        return competencia;
    }

    public void setCompetencia(Date competencia) {
        this.competencia = competencia;
    }

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "data_inicio", nullable = false)
    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "data_fim", nullable = false)
    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 15)
    public EStatusLoteApuracaoHoras getStatus() {
        return status;
    }

    public void setStatus(EStatusLoteApuracaoHoras status) {
        this.status = status;
    }

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_criacao", nullable = false)
    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_alteracao")
    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    @NotNull
    @ManyToOne
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "FK_apuracao_horas_lote_usuario"), nullable = false)
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @OneToMany(mappedBy = "lote", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ApuracaoHorasDetalhe> getDetalhes() {
        return detalhes;
    }

    public void setDetalhes(List<ApuracaoHorasDetalhe> detalhes) {
        this.detalhes = detalhes;
    }

    @Transient
    public boolean isTodosColaboradores() {
        return todosColaboradores;
    }

    public void setTodosColaboradores(boolean todosColaboradores) {
        this.todosColaboradores = todosColaboradores;
    }

    @Transient
    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ApuracaoHorasLote other = (ApuracaoHorasLote) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (id.equals(other.id)) {
            return true;
        }
        return false;
    }

}

package com.mesure.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.ETipoAnotacaoColaborador;

@Entity
@Table(name = "colaborador_advertencia")
public class ColaboradorAdvertencia implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descricao;
	private Date data;
	private ETipoAnotacaoColaborador tipo;
	private Colaborador colaborador;

	@Id
	@TableGenerator (
		name="colaborador_advertencia_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="colaborador_advertencia",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "colaborador_advertencia_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@NotBlank
	@Size(max = 250)
	@Column(nullable = false, length = 250)		
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data", nullable = false)	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(length = 15, nullable = false)	
	public ETipoAnotacaoColaborador getTipo() {
		return tipo;
	}

	public void setTipo(ETipoAnotacaoColaborador tipo) {
		this.tipo = tipo;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_colaborador_advertencia_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColaboradorAdvertencia other = (ColaboradorAdvertencia) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

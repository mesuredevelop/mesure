package com.mesure.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "configuracao")
public class Configuracao implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;	
	// Configuração para envio de e-mail
	private String mailServerHost;
	private Integer mailServerPort;
	private String mailEnableSsl = "true";
	private String mailAuth = "true";
	private String mailUsername;
	private String mailPassword;
	// Filial na qual as configurações pertencem
	private Filial filial;

	@Id
	@TableGenerator (
		name="configuracao_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="configuracao",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "configuracao_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	@Column(name = "mail_server_host", nullable = false, length = 100)
	public String getMailServerHost() {
		return mailServerHost;
	}

	public void setMailServerHost(String mailServerHost) {
		this.mailServerHost = mailServerHost;
	}

	@NotNull
	@Column(name = "mail_server_port", nullable = false)
	public Integer getMailServerPort() {
		return mailServerPort;
	}

	public void setMailServerPort(Integer mailServerPort) {
		this.mailServerPort = mailServerPort;
	}

	@NotBlank
	@Column(name = "mail_enable_ssl", nullable = false, length = 5)
	public String getMailEnableSsl() {
		return mailEnableSsl;
	}

	public void setMailEnableSsl(String mailEnableSsl) {
		this.mailEnableSsl = mailEnableSsl;
	}

	@NotBlank
	@Column(name = "mail_auth", nullable = false, length = 5)
	public String getMailAuth() {
		return mailAuth;
	}

	public void setMailAuth(String mailAuth) {
		this.mailAuth = mailAuth;
	}

	@Email
	@NotBlank
	@Column(name = "mail_username", nullable = false, length = 100)
	public String getMailUsername() {
		return mailUsername;
	}

	public void setMailUsername(String mailUsername) {
		this.mailUsername = mailUsername;
	}

	@NotBlank
	@Column(name = "mail_password", nullable = false, length = 100)
	public String getMailPassword() {
		return mailPassword;
	}

	public void setMailPassword(String mailPassword) {
		this.mailPassword = mailPassword;
	}
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "filial_id", foreignKey = @ForeignKey(name = "FK_configuracao_filial"), nullable = false)	
	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	@Transient
	public boolean isNovo() {
		return getId() == null;
	}

	@Transient
	public boolean isEditando() {
		return getId() != null;
	}

}

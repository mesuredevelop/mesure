package com.mesure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoAtividadePrevista;
import com.mesure.util.Util;

@Entity
@Table(name = "projeto_atividade_prevista")
public class ProjetoAtividadePrevista implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Date data;
	private ESimNao pausa;
	private ESituacaoAtividadePrevista situacao;
	private TipoAtividadePrevista tipo;
	private Colaborador colaborador;
	private ProjetoEtapa etapa;
	private Projeto projeto;
	private ProgramacaoExecucao programacaoExecucao;
	private List<ProjetoAtividadePrevistaEquipamento> equipamentos = new ArrayList<>();
	private List<ProjetoAtividadePrevistaDispositivo> dispositivos = new ArrayList<>(); 

	@Id
	@TableGenerator (
		name="projeto_atividade_prevista_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="projeto_atividade_prevista",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "projeto_atividade_prevista_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	public ESimNao getPausa() {
		return pausa;
	}

	public void setPausa(ESimNao pausa) {
		this.pausa = pausa;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(length = 15, nullable = false)
	public ESituacaoAtividadePrevista getSituacao() {
		return situacao;
	}

	public void setSituacao(ESituacaoAtividadePrevista situacao) {
		this.situacao = situacao;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "tipo_atividade_prevista_id", foreignKey = @ForeignKey(name = "FK_projeto_atividade_prevista_tipo"), nullable = false)
	public TipoAtividadePrevista getTipo() {
		return tipo;
	}

	public void setTipo(TipoAtividadePrevista tipo) {
		this.tipo = tipo;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_projeto_atividade_prevista_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	@ManyToOne
	@JoinColumn(name = "etapa_projeto_id", foreignKey = @ForeignKey(name = "FK_projeto_atividade_prevista_etapa"))
	public ProjetoEtapa getEtapa() {
		return etapa;
	}

	public void setEtapa(ProjetoEtapa etapa) {
		this.etapa = etapa;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "projeto_id", foreignKey = @ForeignKey(name = "FK_projeto_atividade_prevista_projeto"), nullable = false)
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	@OneToOne
	@JoinColumn(name = "programacao_execucao_id", foreignKey = @ForeignKey(name = "FK_projeto_atividade_prevista_programacao_execucao"))
	public ProgramacaoExecucao getProgramacaoExecucao() {
		return programacaoExecucao;
	}

	public void setProgramacaoExecucao(ProgramacaoExecucao programacaoExecucao) {
		this.programacaoExecucao = programacaoExecucao;
	}
	
	@OneToMany(mappedBy = "atividadePrevista", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ProjetoAtividadePrevistaEquipamento> getEquipamentos() {
		return equipamentos;
	}

	public void setEquipamentos(List<ProjetoAtividadePrevistaEquipamento> equipamentos) {
		this.equipamentos = equipamentos;
	}
	
	@OneToMany(mappedBy = "atividadePrevista", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ProjetoAtividadePrevistaDispositivo> getDispositivos() {
		return dispositivos;
	}

	public void setDispositivos(List<ProjetoAtividadePrevistaDispositivo> dispositivos) {
		this.dispositivos = dispositivos;
	}
	
	@Transient
	public String getDescricaoComData() {
		if (getData() != null && getTipo() != null) {
			return Util.formatarData(getData()) + " - " + getTipo().getDescricao();
		} else if (getTipo() != null) {
			return getTipo().getDescricao();
		}
 		return "";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjetoAtividadePrevista other = (ProjetoAtividadePrevista) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

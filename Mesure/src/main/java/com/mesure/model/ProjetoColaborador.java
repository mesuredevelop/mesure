package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;

import com.mesure.enumerator.ESimNao;

@Entity
@Table(name = "projeto_colaborador")
public class ProjetoColaborador implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private ESimNao encerrado;
	private BigDecimal valorCobranca;
	private BigDecimal valorCobrancaNoturna;
	private Colaborador colaborador;	
	private Projeto projeto;
	
	public ProjetoColaborador(ESimNao encerrado, BigDecimal valorCobranca, Colaborador colaborador, Projeto projeto) {
		this.encerrado = encerrado;
		this.colaborador = colaborador;
		this.projeto = projeto;
	}

	public ProjetoColaborador() {
		
	}	
	
	@Id
	@TableGenerator (
		name="projeto_colaborador_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="projeto_colaborador",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "projeto_colaborador_generator")
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(length = 3, nullable = false)
	public ESimNao getEncerrado() {
		return encerrado;
	}

	public void setEncerrado(ESimNao encerrado) {
		this.encerrado = encerrado;
	}
	
	@Column(name="valor_cobranca", precision = 10, scale = 2)	
	public BigDecimal getValorCobranca() {
		return valorCobranca;
	}

	public void setValorCobranca(BigDecimal valorCobranca) {
		this.valorCobranca = valorCobranca;
	}
	
	@Column(name="valor_cobranca_noturna", precision = 10, scale = 2)
	public BigDecimal getValorCobrancaNoturna() {
		return valorCobrancaNoturna;
	}

	public void setValorCobrancaNoturna(BigDecimal valorCobrancaNoturna) {
		this.valorCobrancaNoturna = valorCobrancaNoturna;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_projeto_colaborador_colaborador"), nullable = false)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}	
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "projeto_id", foreignKey = @ForeignKey(name = "FK_projeto_colaborador_projeto"), nullable = false)
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjetoColaborador other = (ProjetoColaborador) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}

}

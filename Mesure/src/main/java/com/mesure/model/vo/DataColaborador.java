package com.mesure.model.vo;

import java.io.Serializable;
import java.util.Date;

import com.mesure.model.Colaborador;

public class DataColaborador implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date data;
	private Colaborador colaborador;
	
	public DataColaborador() {
		
	}
	
	public DataColaborador(Date data, Colaborador colaborador) {
		this.data = data;
		this.colaborador = colaborador;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
}

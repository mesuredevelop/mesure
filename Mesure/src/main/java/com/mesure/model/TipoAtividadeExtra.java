package com.mesure.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.ESimNao;

@Entity
@Table(name = "tipo_atividade_extra")
public class TipoAtividadeExtra implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String descricao;
	private ESimNao horaTrabalhada;

	@Id
	@TableGenerator (
		name="tipo_atividade_extra_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="tipo_atividade_extra",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "tipo_atividade_extra_generator")	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	@Size(max = 50)
	@Column(nullable = false, length = 50)		
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name="hora_trabalhada", nullable = false, length = 3)
	public ESimNao getHoraTrabalhada() {
		return horaTrabalhada;
	}

	public void setHoraTrabalhada(ESimNao horaTrabalhada) {
		this.horaTrabalhada = horaTrabalhada;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoAtividadeExtra other = (TipoAtividadeExtra) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}	

}


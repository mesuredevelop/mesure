package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.mesure.enumerator.ELocalTreinamento;

@Entity
@Table(name = "treinamento")
public class Treinamento implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descricao;
	private ELocalTreinamento local;
	private BigDecimal cargaHoraria;
	private Fornecedor fornecedor;

	@Id
	@TableGenerator (
		name="treinamento_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="treinamento",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "treinamento_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	@Size(max = 60)
	@Column(nullable = false, length = 60)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 20)	
	public ELocalTreinamento getLocal() {
		return local;
	}

	public void setLocal(ELocalTreinamento local) {
		this.local = local;
	}

	@Column(name = "carga_horaria", precision = 10, scale = 2)
	public BigDecimal getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(BigDecimal cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	
	@ManyToOne
	@JoinColumn(name = "fornecedor_id", foreignKey = @ForeignKey(name = "FK_treinamento_fornecedor"))
	public Fornecedor getFornecedor() {
	  return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
	  this.fornecedor = fornecedor;
	}	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Treinamento other = (Treinamento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}

package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.mesure.enumerator.ESimNao;
import com.mesure.enumerator.ESituacaoEtapa;
import com.mesure.util.Util;

@Entity
@Table(name = "projeto_execucao")
public class ProjetoExecucao implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Date data;
	private Date horaInicio;
	private Date horaTermino;
	private Date saidaEmpresa;
	private Date retornoEmpresa;
	private BigDecimal valorHora;
	private BigDecimal totalHoras;
	private BigDecimal valorTotal;
	private String observacao;
	private ESimNao retrabalho;
	private Date dataAlteracao;
	private ProjetoEtapa etapa;
	private ProjetoAtividadePrevista atividadePrevista;
	private Colaborador colaborador;
	private Usuario usuario;
	private Projeto projeto;

	@Id
	@TableGenerator (
		name="projeto_execucao_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="projeto_execucao",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "projeto_execucao_generator")	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@NotNull
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_inicio")
	public Date getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_termino")	
	public Date getHoraTermino() {
		return horaTermino;
	}

	public void setHoraTermino(Date horaTermino) {
		this.horaTermino = horaTermino;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "saida_empresa")	
	public Date getSaidaEmpresa() {
		return saidaEmpresa;
	}

	public void setSaidaEmpresa(Date saidaEmpresa) {
		this.saidaEmpresa = saidaEmpresa;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "retorno_empresa")	
	public Date getRetornoEmpresa() {
		return retornoEmpresa;
	}

	public void setRetornoEmpresa(Date retornoEmpresa) {
		this.retornoEmpresa = retornoEmpresa;
	}

	@Column(name="valor_hora", precision = 10, scale = 2)
	public BigDecimal getValorHora() {
		return valorHora;
	}

	public void setValorHora(BigDecimal valorHora) {
		this.valorHora = valorHora;
	}

	@Column(name = "total_horas", precision = 18, scale = 14)
	public BigDecimal getTotalHoras() {
		return totalHoras;
	}

	public void setTotalHoras(BigDecimal totalHoras) {
		this.totalHoras = totalHoras;
	}

	@Column(name="valor_total", precision = 10, scale = 2)
	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	@Column(columnDefinition = "text")	
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 3)		
	public ESimNao getRetrabalho() {
		return retrabalho;
	}

	public void setRetrabalho(ESimNao retrabalho) {
		this.retrabalho = retrabalho;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")	
	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	@ManyToOne
	@JoinColumn(name = "etapa_projeto_id", foreignKey = @ForeignKey(name = "FK_projeto_execucao_projeto_etapa"))
	public ProjetoEtapa getEtapa() {
		return etapa;
	}

	public void setEtapa(ProjetoEtapa etapa) {
		this.etapa = etapa;
	}
	
	@ManyToOne
	@JoinColumn(name = "atividade_prevista_id", foreignKey = @ForeignKey(name = "FK_projeto_execucao_projeto_atividade_prevista"))
	public ProjetoAtividadePrevista getAtividadePrevista() {
		return atividadePrevista;
	}

	public void setAtividadePrevista(ProjetoAtividadePrevista atividadePrevista) {
		this.atividadePrevista = atividadePrevista;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "colaborador_id", foreignKey = @ForeignKey(name = "FK_projeto_execucao_colaborador"), nullable = false)	
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@ManyToOne
	@JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "FK_projeto_execucao_usuario"))	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "projeto_id", foreignKey = @ForeignKey(name = "FK_projeto_execucao_projeto"), nullable = false)
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	@Transient
	public boolean isNovo() {
		return getId() == null;
	}
	
	@Transient
	public boolean isEditando() {
		return getId() != null;
	}
	
	@Transient
	public boolean isEtapaRetrabalho() {
		return (getEtapa() != null && getEtapa().getSituacao().equals(ESituacaoEtapa.RETRABALHO));
	}
	
	@Transient
	public void calcularTotaisValorHora() {
		setValorHora(new BigDecimal(0));
		setTotalHoras(new BigDecimal(0));
		setValorTotal(new BigDecimal(0));

		// Calcula o Valor Hora, Total Horas e Valor Total
		if (getHoraInicio() != null && getHoraTermino() != null) {
			setTotalHoras(new BigDecimal(Util.converteHoraParaCentesimal(getHoraTermino().getTime() - getHoraInicio().getTime())));
			
			if (getColaborador() != null && getColaborador().getValorHora() != null) {
				setValorHora(getColaborador().getValorHora());
				setValorTotal(getValorHora().multiply(getTotalHoras()));
			}			
		}
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjetoExecucao other = (ProjetoExecucao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}


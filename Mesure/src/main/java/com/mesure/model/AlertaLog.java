package com.mesure.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "alerta_log")
public class AlertaLog implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Date data;
	private Date hora;
	private Alerta alerta;
	
	public AlertaLog() {
	
	}
	
	public AlertaLog(Date data, Date hora, Alerta alerta) {
		this.data = data;
		this.hora = hora;
		this.alerta = alerta;
	}	

	@Id
	@TableGenerator (
		name="alerta_log_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="alerta_log",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "alerta_log_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data", nullable = false)	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	@NotNull
	@Temporal(TemporalType.TIME)
	@Column(name = "hora", nullable = false)
	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "alerta_id", foreignKey = @ForeignKey(name = "FK_alerta_log_alerta"), nullable = false)
	public Alerta getAlerta() {
		return alerta;
	}

	public void setAlerta(Alerta alerta) {
		this.alerta = alerta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlertaLog other = (AlertaLog) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}

}

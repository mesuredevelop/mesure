package com.mesure.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "projeto_proposta")
public class ProjetoProposta implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private BigDecimal valorProjeto;
	private BigDecimal valorHoraNegociado;
	private BigDecimal valorRefeicao;
	private Date dataAprovacao;
	private Date dataInicio;
	private Date dataPrazoFim;
	private Date dataEncerramento;
	private BigDecimal totalHoras;
	private Integer totalDias;
	private Projeto projeto; 
	
	@Id
	@TableGenerator (
		name="projeto_proposta_generator",
		table="generator_id",
  		pkColumnName = "GEN_KEY",
  		valueColumnName = "GEN_VALUE",
  		pkColumnValue="projeto_proposta",
  		allocationSize=1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "projeto_proposta_generator")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Column(name="valor_projeto", precision = 10, scale = 2, nullable = false)
	@Min(value = 1, message= "possui um valor muito baixo")
	public BigDecimal getValorProjeto() {
		return valorProjeto;
	}

	public void setValorProjeto(BigDecimal valorProjeto) {
		this.valorProjeto = valorProjeto;
	}

	@Column(name="valor_hora_negociado", precision = 10, scale = 2)
	public BigDecimal getValorHoraNegociado() {
		return valorHoraNegociado;
	}

	public void setValorHoraNegociado(BigDecimal valorHoraNegociado) {
		this.valorHoraNegociado = valorHoraNegociado;
	}
	
	@Column(name="valor_refeicao", precision = 10, scale = 2)
	public BigDecimal getValorRefeicao() {
		return valorRefeicao;
	}

	public void setValorRefeicao(BigDecimal valorRefeicao) {
		this.valorRefeicao = valorRefeicao;
	}	

	@Temporal(TemporalType.DATE)
	@Column(name = "data_aprovacao")
	public Date getDataAprovacao() {
		return dataAprovacao;
	}

	public void setDataAprovacao(Date dataAprovacao) {
		this.dataAprovacao = dataAprovacao;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio", nullable = false)
	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data_prazo_fim", nullable = false)
	public Date getDataPrazoFim() {
		return dataPrazoFim;
	}

	public void setDataPrazoFim(Date dataPrazoFim) {
		this.dataPrazoFim = dataPrazoFim;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_encerramento")
	public Date getDataEncerramento() {
		return dataEncerramento;
	}

	public void setDataEncerramento(Date dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}

	@Column(name = "total_horas", precision = 10, scale = 2)
	public BigDecimal getTotalHoras() {
		return totalHoras;
	}

	public void setTotalHoras(BigDecimal totalHoras) {
		this.totalHoras = totalHoras;
	}
	
	@Column(name = "total_dias")
	public Integer getTotalDias() {
		return totalDias;
	}

	public void setTotalDias(Integer totalDias) {
		this.totalDias = totalDias;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "projeto_id", foreignKey = @ForeignKey(name = "FK_projeto_proposta_projeto"), nullable = false)
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjetoProposta other = (ProjetoProposta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (id.equals(other.id))
			return true;
		return false;
	}
	
}


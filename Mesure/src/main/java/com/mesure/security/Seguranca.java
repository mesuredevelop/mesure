package com.mesure.security;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Equipe;
import com.mesure.repository.EquipeRepository;

@Named
@RequestScoped
public class Seguranca {

    @Inject
    private EquipeRepository equipeRepository;

    public String getNomeUsuario() {
        String nome = null;

        UsuarioSistema usuarioLogado = getUsuarioLogado();

        if (usuarioLogado != null) {
            nome = usuarioLogado.getUsuario().getNome();
        }

        return nome;
    }

    @Produces
    @UsuarioLogado
    public UsuarioSistema getUsuarioLogado() {
        UsuarioSistema usuario = null;

        UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();

        if (auth != null && auth.getPrincipal() != null) {
            usuario = (UsuarioSistema) auth.getPrincipal();
        }

        return usuario;
    }

    public boolean isResponsavelEquipe() {
        List<Equipe> equipes = equipeRepository.equipesReponsavelSemParticipantes(getUsuarioLogado().getUsuario().getColaborador());
        return (equipes != null && equipes.size() > 0);
    }

    public boolean visualizaProjetos() {
        return getUsuarioLogado().getUsuario().getColaborador().getVisualizaProjeto().equals(ESimNao.SIM);
    }

    public boolean editaCadastroProjetos() {
        return getUsuarioLogado().getUsuario().getColaborador().getEditaCadastroProjeto().equals(ESimNao.SIM);
    }

}

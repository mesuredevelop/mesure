package com.mesure.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.hibernate.exception.SQLGrammarException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.mesure.enumerator.ESimNao;
import com.mesure.model.Grupo;
import com.mesure.model.Permissao;
import com.mesure.model.Usuario;
import com.mesure.repository.UsuarioRepository;
import com.mesure.tenant.jpa.TenantEntityManager;
import com.mesure.tenant.jpa.TenantEntityManagerProducer;
import com.mesure.util.cdi.CDIServiceLocator;

public class AppUserDetailsService implements UserDetailsService {

	private final TenantEntityManagerProducer tenantEntityManagerProducer = CDIServiceLocator.getBean(TenantEntityManagerProducer.class);
	
	@Override
	public UserDetails loadUserByUsername(String email) {

		System.out.println("e-mail: " + email);

		Usuario usuario = null;
		UsuarioSistema user = null;
		EntityManager tenantEM = null;
		TenantEntityManager tenantEntityManager = null;

		try {
			tenantEntityManager = tenantEntityManagerProducer.createTenantEM(email);
			tenantEM = tenantEntityManager.getEm();
			UsuarioRepository usuarioRepository = new UsuarioRepository(tenantEM);
			usuario = usuarioRepository.porEmail(email, ESimNao.SIM);
			if (usuario.getColaborador() == null) {
				throw new NoResultException("Usuário não está associado a um colaborador");
			}
			usuario.setTenantId(tenantEntityManager.getTenantId());
			user = new UsuarioSistema(usuario, getGrupos(usuario));
		} catch (NoResultException | NonUniqueResultException | SQLGrammarException e) {
			throw new UsernameNotFoundException("Usuário não encontrado.");
		} finally {
			if (tenantEM != null && tenantEM.isOpen()) {
				tenantEM.close();
			}
		}

		return user;
	}

	private Collection<? extends GrantedAuthority> getGrupos(Usuario usuario) {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();

		for (Grupo grupo : usuario.getGrupos()) {
			// TODO - remover após ajustar a questão do usuário ADM
			 authorities.add(new SimpleGrantedAuthority("ROLE_" + grupo.getNome().toUpperCase()));
			
			for (Permissao permissao : grupo.getPermissoes()) {
				authorities.add(new SimpleGrantedAuthority("ROLE_" + permissao.getNome().toUpperCase()));
			}
		}

		return authorities;
	}
}
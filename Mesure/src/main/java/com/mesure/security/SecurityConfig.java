package com.mesure.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public AppUserDetailsService userDetailsService() {
        return new AppUserDetailsService();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(new Md5PasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        JsfLoginUrlAuthenticationEntryPoint jsfLoginEntry = new JsfLoginUrlAuthenticationEntryPoint();
        jsfLoginEntry.setLoginFormUrl("/Login.xhtml");
        jsfLoginEntry.setRedirectStrategy(new JsfRedirectStrategy());

        JsfAccessDeniedHandler jsfDeniedEntry = new JsfAccessDeniedHandler();
        jsfDeniedEntry.setLoginPath("/AcessoNegado.xhtml");
        jsfDeniedEntry.setContextRelative(true);

        http
                .csrf().disable()
                .headers().frameOptions().sameOrigin()
                .and()
                .authorizeRequests()
                .antMatchers("/Login.xhtml", "/Erro.xhtml", "/javax.faces.resource/**", "/fonts/**").permitAll()
                .antMatchers("/Home.xhtml", "/AcessoNegado.xhtml", "/dialogos/**").authenticated()
                // Permissões por tela/URL
                .antMatchers("/empresas/PesquisaEmpresas.xhtml").hasRole("GERENCIAR_EMPRESAS")
                .antMatchers("/empresas/CadastroEmpresa.xhtml").hasRole("GERENCIAR_EMPRESAS")
                .antMatchers("/colaboradores/PesquisaColaboradores.xhtml").hasRole("GERENCIAR_COLABORADORES")
                .antMatchers("/colaboradores/CadastroColaborador.xhtml").hasRole("GERENCIAR_COLABORADORES")
                .antMatchers("/colaboradores/Ponto.xhtml").hasRole("GERENCIAR_PARTICIPANTES")
                .antMatchers("/colaboradores/PontoDetalhe.xhtml").hasRole("GERENCIAR_PARTICIPANTES")
                .antMatchers("/colaboradores/LoteApuracao.xhtml").hasRole("GERENCIAR_APURACAO_HORAS")
                .antMatchers("/colaboradores/LoteApuracaoDetalhe.xhtml").hasRole("GERENCIAR_APURACAO_HORAS")
                .antMatchers("/colaboradores/PesquisaTreinamentos.xhtml").hasRole("GERENCIAR_TREINAMENTOS")
                .antMatchers("/cadastros/PesquisaEquipes.xhtml").hasRole("GERENCIAR_EQUIPES")
                .antMatchers("/cadastros/CadastroEquipe.xhtml").hasRole("GERENCIAR_EQUIPES")
                .antMatchers("/clientes/PesquisaClientes.xhtml").hasRole("GERENCIAR_CLIENTES")
                .antMatchers("/clientes/CadastroCliente.xhtml").hasRole("GERENCIAR_CLIENTES")
                .antMatchers("/fornecedores/PesquisaFornecedores.xhtml").hasRole("GERENCIAR_FORNECEDORES")
                .antMatchers("/fornecedores/CadastroFornecedor.xhtml").hasRole("GERENCIAR_FORNECEDORES")
                .antMatchers("/cadastros/TipoAlimentacao.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/cadastros/PesquisaAtividades.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/cadastros/CadastroAtividade.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/cadastros/TiposProjeto.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/cadastros/TiposAtvExtra.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/cadastros/TiposAtvPrevista.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/cadastros/EquipamentosProtecao.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/cadastros/DispositivosSeguranca.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/veiculos/PesquisaMarcas.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/veiculos/CadastroMarca.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/cadastros/Feriados.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/cadastros/ContasBancarias.xhtml").hasRole("GERENCIAR_CADASTROS_DIVERSOS")
                .antMatchers("/cadastros/Configuracao.xhtml").hasRole("GERENCIAR_CONFIGURACOES")
                .antMatchers("/veiculos/PesquisaVeiculos.xhtml").hasRole("GERENCIAR_VEICULOS")
                .antMatchers("/veiculos/CadastroVeiculo.xhtml").hasRole("GERENCIAR_VEICULOS")
                .antMatchers("/usuarios/CadastroUsuario.xhtml").hasRole("GERENCIAR_USUARIOS")
                .antMatchers("/usuarios/PesquisaUsuarios.xhtml").hasRole("GERENCIAR_USUARIOS")
                .antMatchers("/usuarios/PesquisaGrupos.xhtml").hasRole("GERENCIAR_USUARIOS")
                .antMatchers("/projetos/PesquisaProjetos.xhtml").hasRole("GERENCIAR_PROJETOS")
                .antMatchers("/projetos/CadastroProjeto.xhtml").hasRole("GERENCIAR_PROJETOS")
                .antMatchers("/projetos/PesquisaProgramacao.xhtml").hasRole("GERENCIAR_PROGRAMACOES")
                .antMatchers("/projetos/CadastroProgramacao.xhtml").hasRole("GERENCIAR_PROGRAMACOES")
                .antMatchers("/projetos/AlterarProgramacao.xhtml").hasRole("GERENCIAR_PROGRAMACOES")
                .antMatchers("/projetos/PesquisaExecucao.xhtml").hasRole("GERENCIAR_EXECUCOES")
                .antMatchers("/projetos/kbAtvProjeto.xhtml").hasRole("GERENCIAR_PROJETOS")
                .antMatchers("/financeiro/LoteFaturamento.xhtml").hasRole("GERENCIAR_FATURAMENTO")
                .antMatchers("/financeiro/LoteFaturamentoDetalhe.xhtml").hasRole("GERENCIAR_FATURAMENTO")
                .antMatchers("/financeiro/PesquisaTituloReceber.xhtml").hasRole("GERENCIAR_TITULO_RECEBER")
                .antMatchers("/financeiro/CadastroTituloReceber.xhtml").hasRole("GERENCIAR_TITULO_RECEBER")
                .antMatchers("/relatorios/RelatorioApontamentoHora.xhtml").hasRole("GERENCIAR_RELATORIOS_PROJETO")
                .antMatchers("/relatorios/RelatorioDadosProjeto.xhtml").hasRole("GERENCIAR_RELATORIOS_PROJETO")
                .antMatchers("/relatorios/RelatorioSaidaVeiculos.xhtml").hasRole("GERENCIAR_RELATORIOS_PROJETO")
                .antMatchers("/relatorios/RelatorioControlePonto.xhtml").hasRole("GERENCIAR_RELATORIOS_COLABORADOR")
                .antMatchers("/relatorios/RelatorioTreinamentosColaborador.xhtml").hasRole("GERENCIAR_RELATORIOS_COLABORADOR")
                .antMatchers("/relatorios/RelatorioEficiencia.xhtml").hasRole("GERENCIAR_RELATORIO_EFICIENCIA")
                .antMatchers("/relatorios/RelatorioApontamento.xhtml").hasRole("GERENCIAR_RELATORIO_APONTAMENTO")
                .antMatchers("/relatorios/RelatorioApontamentoDetalhe.xhtml").hasRole("GERENCIAR_RELATORIO_APONTAMENTO")
                .antMatchers("/relatorios/RelatorioHorasFaltantes.xhtml").hasRole("GERENCIAR_RELATORIO_HORAS_FALTANTES")
                .antMatchers("/graficos/GraficoProjetos.xhtml").hasRole("GERENCIAR_GRAFICOS_PROJETO")
                .anyRequest().denyAll()
                .and()
                .formLogin()
                .loginPage("/Login.xhtml")
                .failureUrl("/Login.xhtml?invalid=true")
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .and()
                .exceptionHandling()
                .accessDeniedPage("/AcessoNegado.xhtml")
                .authenticationEntryPoint(jsfLoginEntry)
                .accessDeniedHandler(jsfDeniedEntry);
    }

}

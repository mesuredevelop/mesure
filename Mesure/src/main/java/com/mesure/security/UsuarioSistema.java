package com.mesure.security;

import java.util.Collection;

import javax.inject.Inject;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.mesure.model.Colaborador;
import com.mesure.model.Filial;
import com.mesure.model.Usuario;
import com.mesure.repository.ColaboradorRepository;
import com.mesure.service.NegocioException;

public class UsuarioSistema extends User {

	private static final long serialVersionUID = 1L;
	
	@Inject
	ColaboradorRepository colaboradorRepository;
	
	private Usuario usuario;
	
	public UsuarioSistema(Usuario usuario, Collection<? extends GrantedAuthority> authorities) {
		super(usuario.getEmail(), usuario.getSenha(), authorities);
		this.usuario = usuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public Colaborador getColaborador() {
		return usuario.getColaborador();
	}
	
	public Filial getFilialAtiva() {
		return getColaborador().getFilial();
	}
	
	public void atualizarColaborador() {
		Colaborador colaborador = colaboradorRepository.porId(this.usuario.getColaborador().getId());
		if (colaborador == null) {
			throw new NegocioException("Erro ao atualizar informações do colaborador ativo.");
		}
		atualizarColaborador(colaborador);
	}
	
	public void atualizarColaborador(Colaborador colaborador) {
		this.usuario.setColaborador(colaborador);
	}
	
	public void atualizarFilial(Filial filial) {
		this.usuario.getColaborador().setFilial(filial);
	}
	
}
